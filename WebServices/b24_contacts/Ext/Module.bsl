﻿


Функция get()
	КорневаяГруппа = ФабрикаXDTO.Создать(ФабрикаXDTO.Тип("http://www.b24_contacts.org", "СписокКонтрагентов"));
	
	Запрос = New Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	Контрагенты.Ссылка,
	               |	Контрагенты.Наименование,
	               |	Контрагенты.ИНН,
	               |	Контрагенты.КПП,
	               |	Контрагенты.Партнер,
	               |	Контрагенты.ЮрФизЛицо
	               |ИЗ
	               |	Справочник.Контрагенты КАК Контрагенты";
	//ВЫБРАТЬ
	//              |	СправочникНоменклатура.Ссылка КАК Ссылка,
	//              |	СправочникНоменклатура.Код КАК Код,
	//              |	ВЫБОР
	//              |		КОГДА СправочникНоменклатура.ЭтоГруппа = ИСТИНА
	//              |			ТОГДА СправочникНоменклатура.Наименование
	//              |		ИНАЧЕ СправочникНоменклатура.Наименование
	//              |	КОНЕЦ КАК Наименование,
	//              |	СправочникНоменклатура.ГруппаФинансовогоУчета КАК НоменклатурнаяГруппа,
	//              |	ВЫБОР
	//              |		КОГДА СправочникНоменклатура.ЕдиницаИзмерения.Наименование = ""шт""
	//              |			ТОГДА СправочникНоменклатура.ЕдиницаИзмерения.Наименование
	//              |		ИНАЧЕ ""шт""
	//              |	КОНЕЦ КАК Единица,
	//              |	СправочникНоменклатура.Описание КАК Комментарий,
	//              |	ЕСТЬNULL(СправочникНоменклатура.Производитель.Наименование, """") КАК СтранаПроисхождения,
	//              |	ЕСТЬNULL(РегистрЦеныЧелябинск.Цена, 0) КАК Цена,
	//              |	ЕСТЬNULL(РегистрТоварыНаСкладах.ВНаличииОстаток, 0) КАК Количество,
	//              |	СправочникНоменклатура.Наименование КАК ПолноеНаименование,
	//              |	ЕСТЬNULL(РегистрЦеныЕкатеринбург.Цена, 0) КАК ЦенаЕкатеринбург,
	//              |	ЕСТЬNULL(ТоварыНаСкладахBalance.ВНаличииОстаток, 0) - ВЫБОР
	//              |		КОГДА ЕСТЬNULL(РезервированиеТоваровЕКБ.КОтгрузкеОстаток, 0) < 0
	//              |			ТОГДА 0
	//              |		ИНАЧЕ ЕСТЬNULL(РезервированиеТоваровЕКБ.КОтгрузкеОстаток, 0)
	//              |	КОНЕЦ КАК ОстатокЕкатеринбург
	//              |ИЗ
	//              |	Справочник.Номенклатура КАК СправочникНоменклатура
	//              |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрНакопления.СвободныеОстатки.Остатки(, Склад = &Склад) КАК РегистрТоварыНаСкладах
	//              |		ПО (РегистрТоварыНаСкладах.Номенклатура = СправочникНоменклатура.Ссылка)
	//              |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры.СрезПоследних(, ВидЦены = &ВидЦенЧелябинск) КАК РегистрЦеныЧелябинск
	//              |		ПО СправочникНоменклатура.Ссылка = РегистрЦеныЧелябинск.Номенклатура
	//              |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры.СрезПоследних(, ВидЦены = &ВидЦенЕкатеринбург) КАК РегистрЦеныЕкатеринбург
	//              |		ПО СправочникНоменклатура.Ссылка = РегистрЦеныЕкатеринбург.Номенклатура
	//              |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрНакопления.ТоварыНаСкладах.Остатки(, Склад = &СкладЕкатеринбург) КАК ТоварыНаСкладахBalance
	//              |		ПО СправочникНоменклатура.Ссылка = ТоварыНаСкладахBalance.Номенклатура
	//              |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрНакопления.ТоварыКОтгрузке.Остатки(, Склад = &Склад) КАК РезервированиеТоваровЧел
	//              |		ПО СправочникНоменклатура.Ссылка = РезервированиеТоваровЧел.Номенклатура
	//              |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрНакопления.ТоварыКОтгрузке.Остатки(, Склад = &СкладЕкатеринбург) КАК РезервированиеТоваровЕКБ
	//              |		ПО СправочникНоменклатура.Ссылка = РезервированиеТоваровЕКБ.Номенклатура
	//              |ГДЕ
	//              |	НЕ СправочникНоменклатура.ЭтоГруппа
	//              |	И СправочникНоменклатура.ГруппаФинансовогоУчета <> ЗНАЧЕНИЕ(Catalog.ГруппыФинансовогоУчетаНоменклатуры.EmptyRef)
	
	ЗапросВыборка = Запрос.Выполнить().Выбрать();
	Пока (ЗапросВыборка.Следующий()) Цикл
		КонтрагентXDTO = ФабрикаXDTO.создать(ФабрикаXDTO.Тип("http://www.b24_contacts.org", "Контрагент"));
		КонтрагентXDTO.Код					        = ЗапросВыборка.Код;
		КонтрагентXDTO.Наименование					= ЗапросВыборка.Наименование;
		КонтрагентXDTO.ИНН					        = ЗапросВыборка.ИНН;
		КонтрагентXDTO.КПП					        = ЗапросВыборка.КПП;
		КонтрагентXDTO.Партнер					    = Строка(ЗапросВыборка.Партнер);
		КонтрагентXDTO.ЮрФизЛицо					= ЗапросВыборка.ЮрФизЛицо;
		//НоменклатурнаяГруппаXDTO 				= XDTOFactory.Create(XDTOFactory.Type("http://www.nomenklatura.org", "НоменклатурнаяГруппа"));
		//НоменклатурнаяГруппаXDTO.Код 			= Прав(ЗапросВыборка.НоменклатурнаяГруппа.Код, 10);
		//НоменклатурнаяГруппаXDTO.Наименование 	= ЗапросВыборка.НоменклатурнаяГруппа.Description;
		
		//НоменклатураXDTO.НоменклатурнаяГруппа = НоменклатурнаяГруппаXDTO;
		
		КорневаяГруппа.Контрагент.Добавить(КонтрагентXDTO);
	EndDo;
	     
	Return КорневаяГруппа;
КонецФункции



