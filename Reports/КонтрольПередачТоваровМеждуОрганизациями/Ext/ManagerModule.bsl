﻿
///////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС

// Описание дополнительных настроек отчета
// В данной процедуре можно задать описания варинатов отчетов, опредлить дополнительные варианты,
// определить дополнительные разделы варианта отчета
//
// Параметры:
//	Настройки - Структура параметров настройки
//
Процедура НастройкиОтчета(Настройки) Экспорт

	//Описание вариантов отчетов
	ВариантыОтчетов.УстановитьОписаниеВариантаВДопНастройках(Настройки, "ТоварыНаКомиссии", "");
	ВариантыОтчетов.УстановитьОписаниеВариантаВДопНастройках(Настройки, "ОформлениеОтчетовПоКомиссии", "");
	ВариантыОтчетов.УстановитьОписаниеВариантаВДопНастройках(Настройки, "ОформлениеПередачТоваров", "");
	ВариантыОтчетов.УстановитьОписаниеВариантаВДопНастройках(Настройки, "ОформлениеВозвратовТоваров", "");

	//Дополнительные подсистемы
	//ВариантыОтчетов.ДобавитьПодсистемуВариантаВДопНастройках(Настройки, "Ключварианта", "Подсистема\ПодчиненнаяПодсистема");
	//ВариантыОтчетов.УдалитьПодсистемуВариантаВДопНастройках(Настройки, "Ключварианта", "Подсистема\ПодчиненнаяПодсистема");

КонецПроцедуры

