﻿&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ОткрытьФорму(
		"Отчет.АнализЦенПоставщиков.Форма",
		Новый Структура("Отбор,СформироватьПриОткрытии", Новый Структура("Документ", ПараметрКоманды), Истина),
		,
		"Документ=" + ПараметрКоманды,
		ПараметрыВыполненияКоманды.Окно
	);

КонецПроцедуры