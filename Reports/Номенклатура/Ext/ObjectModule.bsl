﻿
Процедура ПриКомпоновкеРезультата(ДокументРезультат, ДанныеРасшифровки, СтандартнаяОбработка)
	
	СписокНоменклатуры = Новый СписокЗначений; 
	
	СтандартнаяОбработка = Ложь;
	
	Схема = ПолучитьМакет("ОсновнаяСхемаКомпоновкиДанных");
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ДоступДилеровКФабрикам.Фабрика
	|ИЗ
	|	РегистрСведений.ДоступДилеровКФабрикам КАК ДоступДилеровКФабрикам
	|ГДЕ
	|	ДоступДилеровКФабрикам.Пользователь = &Пользователь";
	Запрос.УстановитьПараметр("Пользователь", ПараметрыСеанса.ТекущийПользователь);
	ТЗ = Запрос.Выполнить().Выгрузить();
	СписокФабрик = Новый СписокЗначений;
	СписокФабрик = ТЗ.ВыгрузитьКолонку("Фабрика");
	
	Если ЗначениеЗаполнено(Производитель) тогда
		Доступ = СписокФабрик.Найти(Производитель);
		Если Доступ <> Неопределено тогда
			СписокФабрик = Новый СписокЗначений;
			СписокФабрик.Добавить(Производитель);
		КонецЕсли;	
		
		Если СписокФабрик.Количество() > 0 тогда
			
			ПараметрСКД  = КомпоновщикНастроек.Настройки.ПараметрыДанных.Элементы.Найти("ФабрикаДилера");
			ПараметрСКД.Использование = Истина;
			ПараметрСКД.Значение = СписокФабрик;
			
			//Кем
			ПараметрВидЦен = КомпоновщикНастроек.Настройки.ПараметрыДанных.Элементы.Найти("РозничнаяЦена");
			
			ПараметрВидЦен.Использование 	= Истина;
			ПараметрВидЦен.Значение 		= Константы.mgВидЦенДляДилеров.Получить();
			
			// Кудрявцев миг +
			ПараметрВидЦен2 = КомпоновщикНастроек.Настройки.ПараметрыДанных.Элементы.Найти("РозничнаяЦена2");
			
			ПараметрВидЦен2.Использование 	= Истина;
			ПараметрВидЦен2.Значение 		= Константы.mgВидЦенДляДилеров2.Получить();
			// Кудрявцев миг -
			
			Настройки = КомпоновщикНастроек.ПолучитьНастройки();
									
			КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
			ДанныеРасшифровки = Новый ДанныеРасшифровкиКомпоновкиДанных; 
			
			Макет = КомпоновщикМакета.Выполнить(Схема, Настройки, ДанныеРасшифровки);
			
			ПроцессорКомпоновки = Новый ПроцессорКомпоновкиДанных;
			ПроцессорКомпоновки.Инициализировать(Макет, ,ДанныеРасшифровки);
			
			ДокументРезультат.Очистить();
			
			ПроцессорВывода = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВТабличныйДокумент;
			
			ПроцессорВывода.УстановитьДокумент(ДокументРезультат);
			ПроцессорВывода.Вывести(ПроцессорКомпоновки);
			
			
			КоличествоКолонокТаблицы = ДокументРезультат.ШиринаТаблицы; // Количество колонок в отчете
			КоличествоСтрокТаблицы = ДокументРезультат.ВысотаТаблицы;    // Количество строк в отчете
			
			Удаление = Ложь;
			
			//ПроцессорКомпоновки = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВТабличныйДокумент;
			//ПроцессорКомпоновки.ОтображатьПроцентВывода = Истина ;
			//ЭлементКомпоновки = Новый ЭлементРезультатаКомпоновкиДанных;
			//
			//ПроцессорКомпоновки.НачатьВывод();
			//Считаем проценты по своему алгоритму, для примера:
			//Для Сч = 1 По 100 Цикл
			//	ЭлементКомпоновки.ПроцентВывода = Сч;
			//	ПроцессорКомпоновки.ВывестиЭлемент(ЭлементКомпоновки);
			//КонецЦикла;
			//ПроцессорКомпоновки.ЗакончитьВывод();
			
			
			//Прогресс = 0;
			//КэшПрогресс = 0;
			//КолПрогресс = КоличествоСтрокТаблицы*КоличествоКолонокТаблицы;
			
			Для Кл = 1 По КоличествоСтрокТаблицы Цикл // Обходим все ячейки Табличного Документа        
				Для Ст = 1 По КоличествоКолонокТаблицы Цикл
					//Прогресс = Прогресс + 100/КолПрогресс;
					//Если Окр(Прогресс) - 10 >= КэшПрогресс тогда
					//	КэшПрогресс = КэшПрогресс + 10;
					//ОбщегоНазначенияКлиентСервер.СообщитьПользователю("Формирование отчета " + Строка(КэшПрогресс) + "%");
					//КонецЕсли;	
					
					ТекОбласть = ДокументРезультат.Область(Кл, Ст); 
					
					// Если имеется расшифровка ячейки, то проверям наличие полей расшифровки
					Если ТекОбласть.Расшифровка <> Неопределено Тогда
						ПоляРасшифровки = ДанныеРасшифровки.Элементы[ТекОбласть.Расшифровка].ПолучитьПоля();
						Если ПоляРасшифровки.Количество() > 0 Тогда
							// Если тип значения поля расшифровки соответствует справочнику прикрепленных файлов, то
							// выводим связанную картинку
							
							//Если ТипЗнч(ПоляРасшифровки.Получить(0).Значение) = Тип("СправочникСсылка.Номенклатура") тогда
							//	ПоискНом = СписокНоменклатуры.НайтиПоЗначению(ПоляРасшифровки.Получить(0).Значение);
							//	Если ПоискНом = Неопределено тогда
							//		СписокНоменклатуры.Добавить(ПоляРасшифровки.Получить(0).Значение);
							//		Удаление = Ложь;
							//	Иначе
							//		Удаление = Истина;         
							//	КонецЕсли;    
							//КонецЕсли;
							
							//Если (Удаление = Истина) И (ТипЗнч(ПоляРасшифровки.Получить(0).Значение) <> Тип("ПланВидовХарактеристикСсылка.ДополнительныеРеквизитыИСведения")) И (ТипЗнч(ПоляРасшифровки.Получить(0).Значение) <> Тип("СправочникСсылка.ЗначенияСвойствОбъектов")) тогда
							//Если (ДокументРезультат.Область(4, Ст).Текст <> "Дополнительные реквизиты") И  (Удаление = Истина) тогда	
							//	ТекОбласть.Текст = ""; 
							//КонецЕсли; 
							
							
							Если (ТипЗнч(ПоляРасшифровки.Получить(0).Значение) = Тип("СправочникСсылка.НоменклатураПрисоединенныеФайлы")) тогда
								Попытка
									Рисунок = ПолучитьИзображение(ПоляРасшифровки.Получить(0).Значение);
									Если Рисунок <> Неопределено Тогда
										Рис = ДокументРезультат.Рисунки.Добавить(ТипРисункаТабличногоДокумента.Картинка);
										Рис.РазмерКартинки = РазмерКартинки.Пропорционально;
										Рис.Картинка = Рисунок;
										Рис.Расположить(ТекОбласть); 
										ТекОбласть.ВысотаСтроки = 210;
										ТекОбласть.ШиринаКолонки = 30;
									КонецЕсли;
								Исключение
								КонецПопытки;
							КонецЕсли;
							
						КонецЕсли;
					КонецЕсли;              
				КонецЦикла;          
			КонецЦикла;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

Функция ПолучитьИзображение(ЗначениеЯчейки)
	
	КартинкаПоУмолчанию = Неопределено;
	
	Попытка
		Запрос = Новый Запрос;
		Запрос.Текст = 
		"ВЫБРАТЬ
		|    ПрисоединенныеФайлы.ПрисоединенныйФайл КАК ИмяФайла,
		|    ПрисоединенныеФайлы.ХранимыйФайл КАК СодержимоеФайла
		|ИЗ
		|    РегистрСведений.ПрисоединенныеФайлы КАК ПрисоединенныеФайлы
		|ГДЕ
		|    ПрисоединенныеФайлы.ПрисоединенныйФайл = &ПрисоединенныйФайл";
		
		
		Запрос.УстановитьПараметр("ПрисоединенныйФайл", ЗначениеЯчейки);        
		Результат = Запрос.Выполнить();
		Если НЕ Результат.Пустой() Тогда
			Выборка = Результат.Выбрать();
			Выборка.Следующий(); 
			те = Выборка.СодержимоеФайла.Получить();
			Если те <> неопределено тогда
				Возврат Новый Картинка(те, Истина);
			КонецЕсли;
			
		КонецЕсли;
	Исключение
	КонецПопытки;
	
	Возврат КартинкаПоУмолчанию;
	
КонецФункции