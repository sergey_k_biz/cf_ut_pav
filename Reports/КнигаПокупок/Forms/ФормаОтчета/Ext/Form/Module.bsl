﻿////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если НЕ ЗначениеЗаполнено(ПериодОтчета) Тогда
		ПериодОтчета = ТекущаяДата();
	КонецЕсли; 
	
	УстановитьДоступностьДопНастроек();
	УстановитьДоступностьНастроекДопЛистов();
	УстановитьДоступностьВыбораОбособленныхПодразделений(ЭтаФорма);
	
	КварталСтрока = УчетНДСКлиент.ДатаКакКварталПредставление(ПериодОтчета);
	
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ИспользоватьОбособленныеПодразделения = ПолучитьФункциональнуюОпцию("ИспользоватьОбособленныеПодразделенияВыделенныеНаБаланс");
	
	Отчет.СкрытьКолонкиПоСтавке20 = Истина;
	
	НадписьТекущегоЛиста = "не сформированы";
	
	УстановитьДоступностьКнопокВыбораОтчета();
	
КонецПроцедуры
 
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ШАПКИ ФОРМЫ

&НаКлиенте
Процедура КварталСтрокаНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	РезультатВыбора = УчетНДСКлиент.НачалоВыбораИзСпискаПредставленияПериодаРегистрации(Элемент, СтандартнаяОбработка, ПериодОтчета, ЭтаФорма);
	Если РезультатВыбора <> Неопределено Тогда
		КварталСтрока = РезультатВыбора;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура КварталСтрокаРегулирование(Элемент, Направление, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ПериодОтчета = ДобавитьМесяц(ПериодОтчета, 3*Направление);
	КварталСтрока = УчетНДСКлиент.ДатаКакКварталПредставление(ПериодОтчета);
	
КонецПроцедуры

&НаКлиенте
Процедура КварталСтрокаОчистка(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ПериодОтчета = ТекущаяДата();
	КварталСтрока = УчетНДСКлиент.ДатаКакКварталПредставление(ПериодОтчета);
	
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьДопНастройкиПриИзменении(Элемент)
	
	УстановитьДоступностьДопНастроек();
	
КонецПроцедуры

&НаКлиенте
Процедура ФормироватьДополнительныеЛистыПриИзменении(Элемент)
	
	УстановитьДоступностьНастроекДопЛистов();
	Если Отчет.ВыводитьТолькоДопЛисты И НЕ Отчет.ФормироватьДополнительныеЛисты Тогда
		Отчет.ВыводитьТолькоДопЛисты = Ложь;
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура СписокВыбораОтчетаПриИзменении(Элемент)
	
	ПоказатьВыбранныйОтчет();
	
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	УстановитьДоступностьВыбораОбособленныхПодразделений(ЭтаФорма);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура СледующийОтчет(Команда)
	
	КоличествоОтчетов = Отчет.СписокСформированныхОтчетов.Количество();
	ИндексСформированногоОтчета = ПолучитьИндексСформированногоОтчета(СписокВыбораОтчета);
	
	Если ИндексСформированногоОтчета = Неопределено Тогда
		СписокВыбораОтчета = Отчет.СписокСформированныхОтчетов.Получить(0);
		ПоказатьВыбранныйОтчет();
	ИначеЕсли ИндексСформированногоОтчета < КоличествоОтчетов Тогда
		СписокВыбораОтчета = Отчет.СписокСформированныхОтчетов.Получить(ИндексСформированногоОтчета + 1);
		ПоказатьВыбранныйОтчет();
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура ПредыдущийОтчет(Команда)
	
	ИндексСформированногоОтчета = ПолучитьИндексСформированногоОтчета(СписокВыбораОтчета);
	
	Если ИндексСформированногоОтчета = Неопределено Тогда
		СписокВыбораОтчета = Отчет.СписокСформированныхОтчетов.Получить(0);
		ПоказатьВыбранныйОтчет();
	ИначеЕсли ИндексСформированногоОтчета > 0 Тогда
		СписокВыбораОтчета = Отчет.СписокСформированныхОтчетов.Получить(ИндексСформированногоОтчета - 1);
		ПоказатьВыбранныйОтчет();
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура СформироватьОтчетВыполнить(Команда)
	
	Если НЕ ПроверитьЗаполнение() Тогда
		Возврат;
	КонецЕсли;
	
	СформироватьОтчетНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьНовоеОкно(Команда)
	
	ОткрытьФорму("Отчет.КнигаПокупок.Форма.ФормаОтчета",,, Истина);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// Формирование отчета

&НаСервере
Процедура СформироватьОтчетНаСервере()
	
	ОтчетОбъект = РеквизитФормыВЗначение("Отчет");
	ОтчетОбъект.НачалоПериода = НачалоКвартала(ПериодОтчета);
	ОтчетОбъект.КонецПериода = КонецКвартала(ПериодОтчета);
	ОтчетОбъект.СформироватьОтчетПоСтандартнойФорме = НЕ ИспользоватьДопНастройки;
	
	ОтчетОбъект.ДополнительныеЛистыЗаТекущийПериод = Булево(СпособФормированияДопЛистов);
	
	ОтчетОбъект.СформироватьКнигуПокупок();
	
	КоличествоЛистов = ОтчетОбъект.СписокСформированныхОтчетов.Количество();
	
	Элементы.СписокВыбораОтчета.СписокВыбора.Очистить();
	Если ОтчетОбъект.СписокСформированныхОтчетов.Количество() <> 0 Тогда
		Для каждого ЭлКоллекции Из ОтчетОбъект.СписокСформированныхОтчетов Цикл
			Элементы.СписокВыбораОтчета.СписокВыбора.Добавить(ЭлКоллекции.Представление);
		КонецЦикла; 
		СписокВыбораОтчета = ОтчетОбъект.СписокСформированныхОтчетов.Получить(0).Представление;
	КонецЕсли; 
	
	ПоказатьВыбранныйОтчет();
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Прочее

&НаСервере
Функция ПолучитьИндексСформированногоОтчета(ИмяОтчета)
	
	Если ИмяОтчета = Неопределено Тогда
		Возврат Неопределено;
	КонецЕсли; 
	
	Для каждого ЭлКоллекции Из Отчет.СписокСформированныхОтчетов Цикл
		Если ЭлКоллекции.Представление = ИмяОтчета Тогда
			Возврат Отчет.СписокСформированныхОтчетов.Индекс(ЭлКоллекции);
		КонецЕсли; 
	КонецЦикла; 
	
	Возврат Неопределено;
	
КонецФункции

&НаСервере
Процедура ПоказатьВыбранныйОтчет()

	ТабличныйДокумент.Очистить();
	
	НадписьТекущегоЛиста = "не сформированы";
	
	ИндексСформированногоОтчета = ПолучитьИндексСформированногоОтчета(СписокВыбораОтчета);
	Если ИндексСформированногоОтчета <> Неопределено Тогда
		СформированныйОтчет = Отчет.СписокСформированныхОтчетов.Получить(ИндексСформированногоОтчета).Значение;
		ТабличныйДокумент.Вывести(СформированныйОтчет);
		
		НадписьТекущегоЛиста = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = '%1 из %2'"), ИндексСформированногоОтчета + 1, КоличествоЛистов);
	КонецЕсли; 
	
	УстановитьДоступностьКнопокВыбораОтчета();
	
КонецПроцедуры //

&НаКлиенте
Процедура УстановитьДоступностьДопНастроек()

	Элементы.УправлениеДопНастройками.Доступность = ИспользоватьДопНастройки;
	
КонецПроцедуры //

&НаКлиенте
Процедура УстановитьДоступностьНастроекДопЛистов()

	Элементы.СпособФормированияДопЛистов.Доступность = Отчет.ФормироватьДополнительныеЛисты;
	Элементы.ВыводитьТолькоДопЛисты.Доступность = Отчет.ФормироватьДополнительныеЛисты;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьДоступностьКнопокВыбораОтчета()

	Элементы.СледующийОтчет.Доступность  = Ложь;
	Элементы.ПредыдущийОтчет.Доступность = Ложь;
	
	КоличествоОтчетов = Элементы.СписокВыбораОтчета.СписокВыбора.Количество();
	Если КоличествоОтчетов < 2 Тогда
		Возврат;
	КонецЕсли;
	
	ИндексСформированногоОтчета = ПолучитьИндексСформированногоОтчета(СписокВыбораОтчета);
	Если ИндексСформированногоОтчета = Неопределено 
		ИЛИ ИндексСформированногоОтчета < КоличествоОтчетов - 1 Тогда
		Элементы.СледующийОтчет.Доступность = Истина;
	КонецЕсли; 	
	
	Если ИндексСформированногоОтчета = Неопределено 
		ИЛИ ИндексСформированногоОтчета > 0 Тогда
		Элементы.ПредыдущийОтчет.Доступность = Истина;
	КонецЕсли; 	
	
КонецПроцедуры //

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьДоступностьВыбораОбособленныхПодразделений(Форма)

	Если НЕ Форма.ИспользоватьОбособленныеПодразделения Тогда
		Возврат;
	КонецЕсли;
	
	Если НЕ ЗначениеЗаполнено(Форма.Отчет.Организация) Тогда
		Форма.Элементы.ВключаяОбособленныеПодразделения.ТолькоПросмотр = Истина;
		Возврат;
	КонецЕсли;
	
	ЭтоОбособленноеПодразделение = ОбщегоНазначения.ПолучитьЗначениеРеквизита(Форма.Отчет.Организация, "ОбособленноеПодразделение");
	Если ЭтоОбособленноеПодразделение И Форма.Отчет.ВключаяОбособленныеПодразделения Тогда
		Форма.Отчет.ВключаяОбособленныеПодразделения = Ложь;
	КонецЕсли;
	Форма.Элементы.ВключаяОбособленныеПодразделения.ТолькоПросмотр = ЭтоОбособленноеПодразделение;

КонецПроцедуры
