﻿////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	РазрешитьРедактированиеИдентификатор                             = Истина;
	РазрешитьРедактированиеВалютаЦены                                = Истина;
	РазрешитьРедактированиеИспользоватьПриПродаже                    = Истина;
	РазрешитьРедактированиеЦенаВключаетНДС                           = Истина;
	РазрешитьРедактированиеСпособЗаданияЦены                         = Истина;
	РазрешитьРедактированиеИспользоватьПриПередачеМеждуОрганизациями = Истина;
	
	РазрешитьРедактированиеПороговСрабатывания                       = Истина;
	РазрешитьРедактированиеПравилОкругленияЦены                      = Истина;
	РазрешитьРедактированиеФормул                                    = Истина;
	РазрешитьРедактированиеСхемыКомпоновкиДанных                     = Истина;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура РазрешитьРедактирование(Команда)

	Результат = Новый Массив;
	
	Если РазрешитьРедактированиеИдентификатор Тогда
		Результат.Добавить("Идентификатор");
	КонецЕсли;
	
	Если РазрешитьРедактированиеВалютаЦены Тогда
		Результат.Добавить("ВалютаЦены");
	КонецЕсли;
	
	Если РазрешитьРедактированиеИспользоватьПриПродаже Тогда
		Результат.Добавить("ИспользоватьПриПродаже");
	КонецЕсли;
	
	Если РазрешитьРедактированиеИспользоватьПриПередачеМеждуОрганизациями Тогда
		Результат.Добавить("ИспользоватьПриПередачеМеждуОрганизациями");
	КонецЕсли;
	
	Если РазрешитьРедактированиеЦенаВключаетНДС Тогда
		Результат.Добавить("ЦенаВключаетНДС");
	КонецЕсли;
	
	Если РазрешитьРедактированиеСпособЗаданияЦены Тогда
		Результат.Добавить("СпособЗаданияЦены");
	КонецЕсли;
	
	Если РазрешитьРедактированиеПороговСрабатывания Тогда
		Результат.Добавить("ПорогСрабатывания");
		Результат.Добавить("ПорогиСрабатывания");
	КонецЕсли;
	
	Если РазрешитьРедактированиеПравилОкругленияЦены Тогда
		Результат.Добавить("ПравилаОкругленияЦены");
		Результат.Добавить("ОкруглятьВБольшуюСторону");
	КонецЕсли;
	
	Если РазрешитьРедактированиеФормул Тогда
		Результат.Добавить("АлгоритмРасчетаЦены");
		Результат.Добавить("ЦеновыеГруппы");
	КонецЕсли;
	
	Если РазрешитьРедактированиеСхемыКомпоновкиДанных Тогда
		Результат.Добавить("СхемаКомпоновкиДанных");
	КонецЕсли;
	
	Закрыть(Результат);

КонецПроцедуры

&НаКлиенте
Процедура УстановитьВсеОтметки(Команда)
	
	РазрешитьРедактированиеИдентификатор                             = Истина;
	РазрешитьРедактированиеВалютаЦены                                = Истина;
	РазрешитьРедактированиеИспользоватьПриПродаже                    = Истина;
	РазрешитьРедактированиеЦенаВключаетНДС                           = Истина;
	РазрешитьРедактированиеСпособЗаданияЦены                         = Истина;
	РазрешитьРедактированиеИспользоватьПриПередачеМеждуОрганизациями = Истина;
	РазрешитьРедактированиеСхемыКомпоновкиДанных                     = Истина;
	
	РазрешитьРедактированиеПороговСрабатывания                       = Истина;
	РазрешитьРедактированиеПравилОкругленияЦены                      = Истина;
	РазрешитьРедактированиеФормул                                    = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура СнятьВсеОтметки(Команда)
	
	РазрешитьРедактированиеИдентификатор                             = Ложь;
	РазрешитьРедактированиеВалютаЦены                                = Ложь;
	РазрешитьРедактированиеИспользоватьПриПродаже                    = Ложь;
	РазрешитьРедактированиеЦенаВключаетНДС                           = Ложь;
	РазрешитьРедактированиеСпособЗаданияЦены                         = Ложь;
	РазрешитьРедактированиеИспользоватьПриПередачеМеждуОрганизациями = Ложь;
	РазрешитьРедактированиеСхемыКомпоновкиДанных                     = Ложь;
	
	РазрешитьРедактированиеПороговСрабатывания                       = Ложь;
	РазрешитьРедактированиеПравилОкругленияЦены                      = Ложь;
	РазрешитьРедактированиеФормул                                    = Ложь;
	
КонецПроцедуры

