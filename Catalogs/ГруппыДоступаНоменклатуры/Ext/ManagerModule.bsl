﻿////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС

//Функция определяет, используются или нет группы доступа номенклатуры
//
//	Возвращаемое значение:
//		Булево - если ИСТИНА, значит в группы доступа используются
//
Функция ИспользуютсяГруппыДоступа() Экспорт
	
	Если Не ПолучитьФункциональнуюОпцию("ОграничиватьДоступНаУровнеЗаписей") Тогда
		Возврат Ложь;
	КонецЕсли;
	
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьГруппыДоступаНоменклатуры") Тогда
		Возврат Ложь;
	Иначе
		Возврат Истина;
	КонецЕсли;
	
КонецФункции

//Функция возвращает массив групп доступа номенклатуры, по которым
//текущий пользователь может добавлять и изменять номенклатуру
//
//	Возвращаемое значение:
//		Массив - массив ссылок на группы доступа
//
Функция ДоступныеТекущемуПользователюГруппы() Экспорт
	
	Если Не ПолучитьФункциональнуюОпцию("ОграничиватьДоступНаУровнеЗаписей") Тогда
		Возврат Новый Массив;
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Истина);
	Запрос = Новый Запрос;
	
	Если Пользователи.ЭтоПолноправныйПользователь() Тогда
		Запрос.Текст =
		"ВЫБРАТЬ
		|	ГруппыДоступаНоменклатуры.Ссылка КАК ГруппаДоступаНоменклатуры
		|ИЗ
		|	Справочник.ГруппыДоступаНоменклатуры КАК ГруппыДоступаНоменклатуры";
	Иначе	
		Запрос.Текст =
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ЕСТЬNULL(ЗначенияГруппДоступа.ТолькоВидДоступа, ЛОЖЬ) КАК ТолькоВидДоступа,
		|	ЗначенияГруппДоступа.ЗначениеДоступа,
		|	ТаблицыГруппДоступа.ГруппаДоступа
		|ПОМЕСТИТЬ ВидыДоступа
		|ИЗ
		|	РегистрСведений.ТаблицыГруппДоступа КАК ТаблицыГруппДоступа
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ГруппыЗначенийДоступа КАК ГруппыЗначенийДоступа
		|		ПО ТаблицыГруппДоступа.Пользователь = ГруппыЗначенийДоступа.ГруппаДоступа
		|			И (ГруппыЗначенийДоступа.ЗначениеДоступа = &Пользователь)
		|			И (ГруппыЗначенийДоступа.ВидДоступа = ЗНАЧЕНИЕ(ПланВидовХарактеристик.ВидыДоступа.ПустаяСсылка))
		|			И (ТаблицыГруппДоступа.Таблица = ""Справочник.Номенклатура"")
		|			И (ТаблицыГруппДоступа.Добавление)
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЗначенияГруппДоступа КАК ЗначенияГруппДоступа
		|		ПО ТаблицыГруппДоступа.ГруппаДоступа = ЗначенияГруппДоступа.ГруппаДоступа
		|			И (ЗначенияГруппДоступа.ВидДоступа = ЗНАЧЕНИЕ(ПланВидовХарактеристик.ВидыДоступа.ГруппыНоменклатуры))
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	МАКСИМУМ(ВидыДоступа.ТолькоВидДоступа) КАК ЭтоСписокРазрешенных,
		|	ВидыДоступа.ГруппаДоступа,
		|	МИНИМУМ(ВЫБОР
		|			КОГДА ВидыДоступа.ЗначениеДоступа ЕСТЬ NULL 
		|				ТОГДА ИСТИНА
		|			ИНАЧЕ ЛОЖЬ
		|		КОНЕЦ) КАК РазрешеныВсе
		|ПОМЕСТИТЬ НастройкиГруппыДоступа
		|ИЗ
		|	ВидыДоступа КАК ВидыДоступа
		|
		|СГРУППИРОВАТЬ ПО
		|	ВидыДоступа.ГруппаДоступа
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВидыДоступа.ГруппаДоступа,
		|	ВидыДоступа.ЗначениеДоступа,
		|	НастройкиГруппыДоступа.РазрешеныВсе,
		|	НастройкиГруппыДоступа.ЭтоСписокРазрешенных
		|ПОМЕСТИТЬ ВидыДоступаСНастройками
		|ИЗ
		|	ВидыДоступа КАК ВидыДоступа
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ НастройкиГруппыДоступа КАК НастройкиГруппыДоступа
		|		ПО ВидыДоступа.ГруппаДоступа = НастройкиГруппыДоступа.ГруппаДоступа
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ГруппыДоступаНоменклатуры.Ссылка КАК ГруппаДоступаНоменклатуры,
		|	НастройкиГруппыДоступа.ГруппаДоступа КАК ГруппаДоступа
		|ПОМЕСТИТЬ ГруппыДоступаБезСпискаРазрешенных
		|ИЗ
		|	Справочник.ГруппыДоступаНоменклатуры КАК ГруппыДоступаНоменклатуры
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ НастройкиГруппыДоступа КАК НастройкиГруппыДоступа
		|		ПО ((НЕ НастройкиГруппыДоступа.ЭтоСписокРазрешенных))
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ГруппыДоступаНоменклатурыПоГруппамДоступа.ГруппаДоступаНоменклатуры
		|ИЗ
		|	(ВЫБРАТЬ
		|		ВидыДоступаСНастройками.ЗначениеДоступа КАК ГруппаДоступаНоменклатуры
		|	ИЗ
		|		ВидыДоступаСНастройками КАК ВидыДоступаСНастройками
		|	ГДЕ
		|		ВидыДоступаСНастройками.ЭтоСписокРазрешенных
		|		И ВидыДоступаСНастройками.ЗначениеДоступа ССЫЛКА Справочник.ГруппыДоступаНоменклатуры
		|		И ВидыДоступаСНастройками.ЗначениеДоступа <> ЗНАЧЕНИЕ(Справочник.ГруппыДоступаНоменклатуры.ПустаяСсылка)
		|	
		|	ОБЪЕДИНИТЬ ВСЕ
		|	
		|	ВЫБРАТЬ
		|		ГруппыДоступаБезСпискаРазрешенных.ГруппаДоступаНоменклатуры
		|	ИЗ
		|		ГруппыДоступаБезСпискаРазрешенных КАК ГруппыДоступаБезСпискаРазрешенных
		|			ЛЕВОЕ СОЕДИНЕНИЕ ВидыДоступаСНастройками КАК ВидыДоступаСНастройками
		|			ПО ГруппыДоступаБезСпискаРазрешенных.ГруппаДоступаНоменклатуры = ВидыДоступаСНастройками.ЗначениеДоступа
		|				И ГруппыДоступаБезСпискаРазрешенных.ГруппаДоступа = ВидыДоступаСНастройками.ГруппаДоступа
		|	ГДЕ
		|		ВидыДоступаСНастройками.ЗначениеДоступа ЕСТЬ NULL ) КАК ГруппыДоступаНоменклатурыПоГруппамДоступа";
		Запрос.УстановитьПараметр("Пользователь", Пользователи.ТекущийПользователь());
	КонецЕсли;
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("ГруппаДоступаНоменклатуры");
	
КонецФункции