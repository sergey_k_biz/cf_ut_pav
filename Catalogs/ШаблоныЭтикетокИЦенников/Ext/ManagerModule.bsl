﻿////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС

// Функция выполняет получение имени поля из доступных полей компоновки данных.
//
// Параметры
//  ИмяПоля  - Строка - Имя поля
//
// Возвращаемое значение:
//   Строка   - Имя поля в шаблоне
//
Функция ИмяПоляВШаблоне(Знач ИмяПоля) Экспорт
	
	ИмяПоля = СтрЗаменить(ИмяПоля, ".DeletionMark", ".ПометкаУдаления");
	ИмяПоля = СтрЗаменить(ИмяПоля, ".Owner", ".Владелец");
	ИмяПоля = СтрЗаменить(ИмяПоля, ".Code", ".Код");
	ИмяПоля = СтрЗаменить(ИмяПоля, ".Parent", ".Родитель");
	ИмяПоля = СтрЗаменить(ИмяПоля, ".Predefined", ".Предопределенный");
	ИмяПоля = СтрЗаменить(ИмяПоля, ".IsFolder", ".ЭтоГруппа");
	ИмяПоля = СтрЗаменить(ИмяПоля, ".Description", ".Наименование");
	Возврат ИмяПоля;
	
КонецФункции

// Возвращает шаблон указанного назначения, если он один в базе, иначе - пустую ссылку
//	
//	Параметры:
//		Назначение - ПеречислениеСсылка.НазначенияШаблоновЭтикетокИЦенников - назначение шаблона
//	Возвращаемое значение:
//		СправочникСсылка.ШаблоныЭтикетокИЦенников
//
Функция ШаблонПоУмолчанию(Назначение) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ ПЕРВЫЕ 2
	|	ШаблоныЭтикетокИЦенников.Ссылка
	|ИЗ
	|	Справочник.ШаблоныЭтикетокИЦенников КАК ШаблоныЭтикетокИЦенников
	|ГДЕ
	|	ШаблоныЭтикетокИЦенников.Назначение = &Назначение
	|	И НЕ ШаблоныЭтикетокИЦенников.ПометкаУдаления";
	
	Запрос.УстановитьПараметр("Назначение",Назначение);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Количество() = 1 Тогда
		Выборка.Следующий();
		Возврат Выборка.Ссылка;
	Иначе
		Возврат Справочники.ШаблоныЭтикетокИЦенников.ПустаяСсылка();
	КонецЕсли;
	
КонецФункции

////////////////////////////////////////////////////////////////////////////////
//Обновление информационной базы

//Заполняет назначение шаблонов этикетов и ценников значением "Товары"
//
Процедура ЗаполнитьНазначениеВШаблонахЭтикетокИЦенников() Экспорт
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ШаблоныЭтикетокИЦенников.Ссылка
	|ИЗ
	|	Справочник.ШаблоныЭтикетокИЦенников КАК ШаблоныЭтикетокИЦенников
	|ГДЕ
	|	ШаблоныЭтикетокИЦенников.Назначение = ЗНАЧЕНИЕ(Перечисление.НазначенияШаблоновЭтикетокИЦенников.ПустаяСсылка)";
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		СправочникОбъект = Выборка.Ссылка.ПолучитьОбъект();
		
		СправочникОбъект.Назначение = Перечисления.НазначенияШаблоновЭтикетокИЦенников.Товары;
		
		СправочникОбъект.Записать();
		
	КонецЦикла;
	
КонецПроцедуры

