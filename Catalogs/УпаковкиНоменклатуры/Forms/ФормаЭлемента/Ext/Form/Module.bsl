﻿////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	ЗапретРедактированияРеквизитовОбъектов.ЗаблокироватьРеквизиты(ЭтаФорма);
	
	Элементы.СостоитИзДругихУпаковокПереключатель.ТолькоПросмотр = Элементы.СостоитИзДругихУпаковок.ТолькоПросмотр;
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	УстановитьДоступность();
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	Элементы.Высота.Доступность  			= Не Объект.Безразмерная;
	Элементы.Глубина.Доступность 			= Не Объект.Безразмерная;
	Элементы.Ширина.Доступность  			= Не Объект.Безразмерная;
	Элементы.Объем.Доступность  			= Не Объект.Безразмерная;
	ПриЧтенииСозданииНаСервере();
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	ТекущийОбъект.ДополнительныеСвойства.Вставить("РазрешенаСменаРодителя");
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ШАПКИ ФОРМЫ

&НаКлиенте
Процедура ВысотаПриИзменении(Элемент)
	ПересчитатьОбъем();
КонецПроцедуры

&НаКлиенте
Процедура ГлубинаПриИзменении(Элемент)
	ПересчитатьОбъем();
КонецПроцедуры

&НаКлиенте
Процедура ШиринаПриИзменении(Элемент)
	ПересчитатьОбъем();
КонецПроцедуры

&НаКлиенте
Процедура БезразмернаяПриИзменении(Элемент)
	УстановитьДоступность();
КонецПроцедуры

&НаКлиенте
Процедура ТипоразмерПриИзменении(Элемент)
	ТипоразмерПриИзмененииСервер();
	УстановитьДоступность();
КонецПроцедуры

&НаКлиенте
Процедура КоэффициентПриИзменении(Элемент)
	ОбновитьНаименование();
КонецПроцедуры

&НаКлиенте
Процедура ЕдиницаИзмеренияПриИзменении(Элемент)
	ОбновитьНаименование();
КонецПроцедуры

&НаКлиенте
Процедура КонечнаяПриИзменении(Элемент)
	Если СостоитИзДругихУпаковок = 0 Тогда
		Объект.СостоитИзДругихУпаковок = Ложь;
	Иначе
		Объект.СостоитИзДругихУпаковок = Истина;
	КонецЕсли;
	
	УстановитьТекущуюСтраницуКоэффициентов(ЭтаФорма)
КонецПроцедуры

&НаКлиенте
Процедура КоличествоУпаковокПриИзменении(Элемент)
	Объект.Коэффициент = Объект.КоличествоУпаковок * КоэффициентРодителя;
	
	ОбновитьНаименование();
КонецПроцедуры

&НаКлиенте
Процедура РодительПриИзменении(Элемент)
	РодительПриИзмененииСервер();
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

// Обработчик команды, создаваемой механизмом запрета редактирования ключевых реквизитов.
//
&НаКлиенте
Процедура Подключаемый_РазрешитьРедактированиеРеквизитовОбъекта(Команда)
	
	Если Не Объект.Ссылка.Пустая() Тогда
		Результат = ОткрытьФормуМодально("Справочник.УпаковкиНоменклатуры.Форма.РазблокированиеРеквизитов");
		Если ТипЗнч(Результат) = Тип("Массив") И Результат.Количество() > 0 Тогда
			
			ЗапретРедактированияРеквизитовОбъектовКлиент.УстановитьДоступностьЭлементовФормы(ЭтаФорма, Результат);
			Элементы.СостоитИзДругихУпаковокПереключатель.ТолькоПросмотр = Элементы.СостоитИзДругихУпаковок.ТолькоПросмотр;
			
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// При изменении реквизитов

&НаСервере
Процедура РодительПриИзмененииСервер()
	
	ОбновитьКоэффициентРодителя();	
	
	Объект.Коэффициент = Объект.КоличествоУпаковок * КоэффициентРодителя;
	
	ОбновитьНаименование();
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Прочее

&НаКлиенте
Процедура УстановитьДоступность()
	Элементы.Высота.Доступность  			= НЕ Объект.Безразмерная;
	Элементы.Глубина.Доступность 			= НЕ Объект.Безразмерная;
	Элементы.Ширина.Доступность  			= НЕ Объект.Безразмерная;
	Элементы.Объем.Доступность  			= НЕ Объект.Безразмерная;
КонецПроцедуры

&НаСервере
Процедура ПересчитатьОбъем()
	
	КоэффициентПересчетаВЕдиницыОбъема = Константы.КоэффициентПересчетаВЕдиницыОбъема.Получить();
	
	Если НЕ ЗначениеЗаполнено(КоэффициентПересчетаВЕдиницыОбъема) Тогда
		
		ТекстСообщения = НСтр("ru='В настройках программы не установлен коэффициент перерасчета из единиц измерения линейных размеров в единица измерения объема'");
		
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
		
	Иначе
		
		Объект.Объем = Объект.Высота * Объект.Глубина * Объект.Ширина * КоэффициентПересчетаВЕдиницыОбъема;
	
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ТипоразмерПриИзмененииСервер()
	ЗаполнитьЗначенияСвойств(Объект,Объект.Типоразмер, "Глубина, Ширина, Высота, Объем, Безразмерная"); 
КонецПроцедуры

&НаСервере
Процедура ОбновитьНаименование()
	Объект.Наименование = Справочники.УпаковкиНоменклатуры.СформироватьНаименование(Объект.ЕдиницаИзмерения, Объект.Коэффициент, ЕдиницаИзмеренияВладельца);	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	Если Объект.СостоитИзДругихУпаковок Тогда
		СостоитИзДругихУпаковок = 1;
	Иначе
		СостоитИзДругихУпаковок = 0;
	КонецЕсли;
	
	ОбновитьКоэффициентРодителя();	
	
	Элементы.ДекорацияПредупреждение.Видимость =  (ТипЗнч(Объект.Владелец) = Тип("СправочникСсылка.НаборыУпаковок"));
	
	ЕдиницаИзмеренияВладельца = ОбщегоНазначения.ПолучитьЗначениеРеквизита(Объект.Владелец, "ЕдиницаИзмерения");
	
	ЕдиницаИзмеренияЛинейныхРазмеров = Константы.ЕдиницаИзмеренияЛинейныхРазмеров.Получить();
	ЕдиницаИзмененияОбъема 	         = Константы.ЕдиницаИзмеренияОбъема.Получить();
	ЕдиницаИзмеренияВеса 	         = Константы.ЕдиницаИзмеренияВеса.Получить();
	
	Если ТипЗнч(Объект.Владелец) = Тип("СправочникСсылка.Номенклатура") Тогда
		Элементы.Владелец.Заголовок = НСтр("ru='Номенклатура'");
	ИначеЕсли ТипЗнч(Объект.Владелец) = Тип("СправочникСсылка.НаборыУпаковок") Тогда
		Элементы.Владелец.Заголовок = НСтр("ru='Набор упаковок'");
	КонецЕсли;
	
	УстановитьТекущуюСтраницуКоэффициентов(ЭтаФорма)
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьТекущуюСтраницуКоэффициентов(Форма)
	Если Форма.Объект.СостоитИзДругихУпаковок Тогда
		Форма.Элементы.ГруппаКоэффицентыСтраницы.ТекущаяСтраница = Форма.Элементы.ГруппаКоличествоУпаковок;
	Иначе
		Форма.Элементы.ГруппаКоэффицентыСтраницы.ТекущаяСтраница = Форма.Элементы.ГруппаКоэффициентЕдиницаИзмерения;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ОбновитьКоэффициентРодителя()
	
	Если Объект.СостоитИзДругихУпаковок Тогда
		КоэффициентРодителя = ОбщегоНазначения.ПолучитьЗначениеРеквизита(Объект.Родитель, "Коэффициент");
	Иначе
		КоэффициентРодителя = 1;
	КонецЕсли;
	
КонецПроцедуры

