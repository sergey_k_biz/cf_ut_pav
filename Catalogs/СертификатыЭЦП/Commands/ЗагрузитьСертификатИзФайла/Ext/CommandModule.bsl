﻿
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	Если НЕ ПодключитьРасширениеРаботыСКриптографией() Тогда
		ТекстСообщения = НСтр("ru = 'Расширение для работы с криптографией не подключено, операция прервана.'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
		Возврат;
	КонецЕсли;
	
	АдресВХранилище = Неопределено;
	Если НЕ ПоместитьФайл(АдресВХранилище, , , Истина, ПараметрыВыполненияКоманды.Источник.УникальныйИдентификатор) Тогда
		Возврат;
	КонецЕсли;
	
	// Проверка средств криптографии на компьютере.
	Попытка
		МенеджерКриптографии = ЭлектроннаяЦифроваяПодписьКлиент.ПолучитьМенеджерКриптографии();
	Исключение
		ЭлектронныеДокументыКлиентСлужебный.ОбработатьИсключениеПоЭДНаКлиенте(
			НСтр("ru = 'загрузка сертификата из файла'"), ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
		Возврат;
	КонецПопытки;
	
	СтруктураСертификата = ПодготовитьФайлСертификатаНаСервере(АдресВХранилище);
	Если СтруктураСертификата = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ЕстьОрганизации = Ложь;
	Организация = Неопределено;
	
	ЭлектронныеДокументыСлужебный.ПроверитьНаличиеОрганизаций(ЕстьОрганизации, Организация);
	
	Если НЕ ЗначениеЗаполнено(Организация) И ЕстьОрганизации Тогда
		Организация = ОткрытьФормуМодально("Справочник.СертификатыЭЦП.Форма.ВыборОрганизации");
	КонецЕсли;
	
	Если ТипЗнч(Организация) = Тип("СправочникСсылка.Организации") И ЗначениеЗаполнено(Организация) Тогда
		СтруктураСертификата.Вставить("Организация", Организация);
	Иначе
		Возврат;
	КонецЕсли;

	ТекстСообщения = "";
	СсылкаНаОбъект = ЭлектронныеДокументыСлужебный.ЗагрузитьСертификат(СтруктураСертификата, ТекстСообщения);
	Если НЕ ЗначениеЗаполнено(СсылкаНаОбъект) Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
		Возврат;
	КонецЕсли;
	
	ПараметрыФормы = Новый Структура("Ключ", СсылкаНаОбъект);
	ОткрытьФорму("Справочник.СертификатыЭЦП.Форма.ФормаЭлемента", ПараметрыФормы);
	ОповеститьОбИзменении(СсылкаНаОбъект);
	Оповестить("ОбновитьСписокСертификатов");
	
КонецПроцедуры

&НаСервере
Функция ПодготовитьФайлСертификатаНаСервере(Знач АдресВХранилище)
	
	УстановитьПривилегированныйРежим(Истина);
	ДанныеФайлаСертификата = ПолучитьИзВременногоХранилища(АдресВХранилище);
	
	Попытка
		НовыйСертификат = Новый СертификатКриптографии(ДанныеФайлаСертификата);
	Исключение
		ТекстСообщения = НСтр("ru = 'Файл сертификата должен быть в формате DER X.509, операция прервана.'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
		Возврат Неопределено;
	КонецПопытки;
	
	СтруктураСертификата = ЭлектроннаяЦифроваяПодписьКлиентСервер.ЗаполнитьСтруктуруСертификата(НовыйСертификат);
	СтруктураСертификата.Вставить("ДвоичныеДанныеСертификата", ДанныеФайлаСертификата);
	
	Возврат СтруктураСертификата;
	
КонецФункции
