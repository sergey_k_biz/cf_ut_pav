﻿////////////////////////////////////////////////////////////////////////////////
// Подсистема "Базовая функциональность".
//  
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС

// Возвращает список имен объектов метаданных, данные которых могут содержать ссылки на различные объекты метаданных,
// но при этом эти ссылки не должны учитываться в бизнес-логике приложения.
//
// Пример:
// Для документ "Реализация товаров и услуг" настроена подсистема версионирования объектов,
// и подсистема свойств. При этом на экземпляр документа может быть множество ссылок 
// в информационной базе (из других документов, регистров). Часть ссылок имеют значение для бизнес-логики
// (например движения по регистрам). Другая часть ссылок - "техногенные" ссылки на объект из объектов ИБ,
// которые относятся к подсистеме версионирования объектов и подсистеме свойств. Такие "техногенные"
// ссылки должны быть отфильтрованы, например, в обработке удаления помеченных и при поиске ссылок на объект
// в подсистеме запрета редактирования ключевых реквизитов.
// Список таких "техногенных" объектов нужно перечислить в этой функции.
//
// Возвращаемое значение:
//  Массив       - массив строк, например, "РегистрСведений.ВерсииОбъектов".
//
Функция ПолучитьИсключенияПоискаСсылок() Экспорт
	
	Массив = Новый Массив;
	
	// МониторингЦелевыхПоказателей
	Массив.Добавить(Метаданные.РегистрыСведений.ИсточникиДанныхВариантовАнализаЦелевыхПоказателей.ПолноеИмя());
	// Конец МониторингЦелевыхПоказателей
	
	Массив.Добавить(Метаданные.РегистрыСведений.ШтрихкодыНоменклатуры.ПолноеИмя());
	
	Возврат Массив;
	
КонецФункции 

// Обработчик события, возникающего при обновлении
// данных справочника ИдентификаторыОбъектовМетаданных
//
// Параметры:
//  ВидСобытия   - Строка - "Добавление", "Изменение", "Удаление"
//  Свойства     - Структура:
//                   Старые     - Структура - основные поля и значения старого элемента справочника
//                   Новые      - Структура - основные поля и значения нового  элемента справочника
//                   СтандартнаяЗаменаСсылок
//                              - Булево - если свойство задано Истина,
//                                тогда  в информационной базе будет произведена замена
//                                "Свойства.Старые.Ссылка" на "Свойства.Новые.Ссылка". Можно
//                                установить значение Ложь, тогда замена произведена не будет.
//                                 Замена происходит, когда вместо обычного элемента был
//                                добавлен предопределенный элемент или при замене одного
//                                объекта метаданных на другой для безошибочной реструктуризации.
//
Процедура ПриИзмененииИдентификатораОбъектаМетаданных(ВидСобытия, Свойства) Экспорт
	
	
	
КонецПроцедуры

// Возвращает соответствие имен параметров сеанса и обработчиков для их инициализации.
//
Функция ОбработчикиИнициализацииПараметровСеанса() Экспорт
	
	// Для задания обработчиков параметров сеанса следует использовать шаблон:
	// Обработчики.Вставить("<ИмяПараметраСеанса>|<НачалоИмениПараметраСеанса*>", "Обработчик");
	//
	// Примечание. Символ '*'используется в конце имени параметра сеанса и обозначает,
	//             что один обработчик будет вызван для инициализации всех параметров сеанса
	//             с именем, начинающимся на слово НачалоИмениПараметраСеанса
	//
	
	Обработчики = Новый Соответствие;
	
	//ПодключаемоеОборудование
	Обработчики.Вставить("РабочееМестоКлиента", "МенеджерОборудованияСервер.УстановитьПараметрыСеансаПодключаемогоОборудования");
	//Конец ПодключаемоеОборудование
	
	// ОбменДаннымиССайтом
	Обработчики.Вставить("ОбменДаннымиССайтомВключен",    "ОбменССайтом.УстановитьПараметрыСеанса");
	Обработчики.Вставить("ИспользуемыеУзлыОбменаССайтом", "ОбменССайтом.УстановитьПараметрыСеанса");
	// Конец ОбменДаннымиССайтом
	
	Возврат Обработчики;
	
КонецФункции

// Устанавливает текстовое описание предмета
//
// Параметры
//  СсылкаНаПредмет  – ЛюбаяСсылка – объект ссылочного типа.
//  Представление	 - Строка - сюда необходимо поместить текстовое описание.
Процедура УстановитьПредставлениеПредмета(СсылкаНаПредмет, Представление) Экспорт
	
КонецПроцедуры

// Заполняет идентификакторы тех объектов метаданных, которые невозможно
// автоматически найти по типу, но которые требуется сохранять в базе данных (например, подсистемы).
//
// Подробнее: см. ОбщегоНазначения.ДобавитьИдентификатор
// 
Процедура ЗаполнитьПредустановленныеИдентификаторыОбъектовМетаданных(Идентификаторы) Экспорт
	
	// Администрирование
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "775a2d24-f397-4f92-901c-3767c79ae8f5", Метаданные.Подсистемы.Администрирование);
	
	// Запасы и закупки
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "4a8ee6fa-4b71-498f-b0c1-7bf6ec1071e6", Метаданные.Подсистемы.ЗапасыИЗакупки);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "fcef07f6-f716-4d5e-b677-7218b8e62926", Метаданные.Подсистемы.ЗапасыИЗакупки.Подсистемы.ВнутреннееТовародвижение);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "967328a7-15c3-42d2-8e42-6e940e288b49", Метаданные.Подсистемы.ЗапасыИЗакупки.Подсистемы.ЗакупкиИВозврат);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "57a949da-7d03-4ac9-a43f-744e58cf1140", Метаданные.Подсистемы.ЗапасыИЗакупки.Подсистемы.Запасы);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "22d4b5f7-d6cc-4714-9343-a48418c1080e", Метаданные.Подсистемы.ЗапасыИЗакупки.Подсистемы.РаботаСПоставщиками);
	
	// Маркетинг
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "b1730cfe-044d-4ef8-b18e-bf65824f2df1", Метаданные.Подсистемы.Маркетинг);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "ae84b5d4-2dca-427a-8c40-4863ddf04651", Метаданные.Подсистемы.Маркетинг.Подсистемы.КонкурентнаяРазведка);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "96d13952-237b-4113-87b1-227cfa19f6ad", Метаданные.Подсистемы.Маркетинг.Подсистемы.МаркетинговыеМероприятия);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "707a640a-6bf7-4758-b64e-009de6f42a7a", Метаданные.Подсистемы.Маркетинг.Подсистемы.ПравилаПродаж);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "2a897882-9a87-461e-ad45-e2e588665127", Метаданные.Подсистемы.Маркетинг.Подсистемы.Сегментация);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "6c7f0e37-2eeb-4afd-b5c7-75d95c04b154", Метаданные.Подсистемы.Маркетинг.Подсистемы.Ценообразование);
	
	// Мониторинг целевых показателей
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "bd305af9-16ee-4dc6-84d6-212d1105fa94", Метаданные.Подсистемы.МониторингЦелевыхПоказателей);
	
	// Нормативно-справочная информация
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "1e2d0123-c48c-4b33-b1be-0c2e3f3557ad", Метаданные.Подсистемы.НормативноСправочнаяИнформация);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "1a68bb6e-c10a-4196-91de-7388375d99dd", Метаданные.Подсистемы.НормативноСправочнаяИнформация.Подсистемы.БазовыеКлассификаторы);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "2c4b7612-5d74-416f-9b7d-9567855d34b7", Метаданные.Подсистемы.НормативноСправочнаяИнформация.Подсистемы.Номенклатура);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "d848eed5-fe46-44a0-bf2a-08e56bb538d0", Метаданные.Подсистемы.НормативноСправочнаяИнформация.Подсистемы.Партнеры);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "6dc612ed-b2c4-449f-9f3b-b27d359ae1a4", Метаданные.Подсистемы.НормативноСправочнаяИнформация.Подсистемы.Предприятие);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "b9704992-a792-434c-8243-22380d53d502", Метаданные.Подсистемы.НормативноСправочнаяИнформация.Подсистемы.ФизическиеЛица);
	
	// Органайзер
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "ebab41ca-c78b-4032-8c1a-cf19b2b4b9cf", Метаданные.Подсистемы.Органайзер);
	
	// Продажи
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "06d42db8-6efb-41ae-ac0f-64b40daa0ac3", Метаданные.Подсистемы.Продажи);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "b55cbde7-894a-4cee-a691-edfea6159d73", Метаданные.Подсистемы.Продажи.Подсистемы.ВедениеЗаказовКлиентов);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "572e7585-30b6-4a1c-bbc0-7b86eaf59cc9", Метаданные.Подсистемы.Продажи.Подсистемы.КонтрольРасчетовСКлиентами);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "e70c602c-8900-423d-80e8-99fbe0e18bee", Метаданные.Подсистемы.Продажи.Подсистемы.ПретензииКлиентов);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "0c35bc77-3d7a-4bf0-b823-65865768e9ab", Метаданные.Подсистемы.Продажи.Подсистемы.ПроведениеСделок);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "3fac5efa-3c28-4c0d-bf6d-7a3327371216", Метаданные.Подсистемы.Продажи.Подсистемы.ПродажиИВозвраты);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "c1d31dec-f9fa-4ffb-ba6d-be721b3f3c10", Метаданные.Подсистемы.Продажи.Подсистемы.РозничныеПродажи);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "dd657f99-4659-46ed-9825-8be38c57fad1", Метаданные.Подсистемы.Продажи.Подсистемы.УправлениеТорговымиПредставителями);
	
	// Регламентированный учет
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "cf46928c-8408-45f8-a7bc-80af8ee7f327", Метаданные.Подсистемы.РегламентированныйУчет);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "48443bee-fad1-4ff2-b815-6323a6263ad0", Метаданные.Подсистемы.РегламентированныйУчет.Подсистемы.ПродажиМеждуОрганизациями);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "cfb4956a-4133-4602-9ce8-4a1dd1bd27bd", Метаданные.Подсистемы.РегламентированныйУчет.Подсистемы.РаздельныйУчетНДС);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "db29b0e1-3113-47cb-8761-9141c9a4683c", Метаданные.Подсистемы.РегламентированныйУчет.Подсистемы.УчетНДС);
	
	// Склад
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "a0d0fc35-43a2-4caa-ab78-8e4178b584b6", Метаданные.Подсистемы.Склад);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "792c5a43-5e94-45d6-a22e-24a6c320f277", Метаданные.Подсистемы.Склад.Подсистемы.ИзлишкиНедостачиПорчи);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "0e2423a0-614b-4602-be82-36147b817eb7", Метаданные.Подсистемы.Склад.Подсистемы.СкладскиеОперации);
	
	// Финансы
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "59f3a12e-abad-4f37-ae06-4860c6ade511", Метаданные.Подсистемы.Финансы);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "eb54b4e4-493a-4c7a-9309-51547f1a0a6b", Метаданные.Подсистемы.Финансы.Подсистемы.ДенежныеСредства);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "46040bc7-8f75-4104-b84c-486e1a0e966a", Метаданные.Подсистемы.Финансы.Подсистемы.НастройкаАналитики);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "9965c796-5ed4-408c-8459-eb28ebeb096b", Метаданные.Подсистемы.Финансы.Подсистемы.ПланированиеИКонтрольДенежныхСредств);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "65fa824a-a7d4-400a-a57f-3dcee6a224f5", Метаданные.Подсистемы.Финансы.Подсистемы.ПрочиеРасчеты);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "874195d2-ed12-4ae7-b3ac-e89747f2593c", Метаданные.Подсистемы.Финансы.Подсистемы.ФинансовыйРезультат);
	ОбщегоНазначения.ДобавитьИдентификатор(Идентификаторы, "ef41ac5b-15b9-4907-8963-62dc725e7161", Метаданные.Подсистемы.Финансы.Подсистемы.Эквайринг);
	
КонецПроцедуры
