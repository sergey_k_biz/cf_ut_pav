﻿ Функция ВызватьФорму(Обработка) Экспорт
	 //	 Форма = Обработка.ПолучитьФорму();
	 //	 Форма.Открыть();
	 //	 ПолучитьФорму(
	 //	 П = Новый Структура("Ключ", Поставщик);
	 //Форма = ПолучитьФорму("Справочник.Контрагенты.ФормаОбъекта", П);
	 //Форма.Открыть();
 КонецФункции
 
 Функция ДействуетОграничениеНаДобавлениеСтрокДокумента() Экспорт
	 Возврат msОбщийСервер.ДействуетОграничениеНаДобавлениеСтрокДокумента();	
 КонецФункции
 
 
 
 Функция ЗаполнитьПараметрыДокумента_Новый(СтруктураПараметровДляФормы) Экспорт
	 
	 //ФормаПараметровПартионногоУчета = ПолучитьФорму("ОбщаяФорма.ФормаПараметровПартионногоУчета",);
	 //ФормаПараметровПартионногоУчета.ОткрытьФормуМодально(); 
	 ТекСтрока= СтруктураПараметровДляФормы.ТекСтрока;//("ТекСтрока",Элементы.Товары.ТекущиеДанные);
	 //msПартионныйУчет=СтруктураПараметровДляФормы.msПартионныйУчет;//.Вставить("ТекСтрока",Элементы.msПартионныйУчет);
	 
	 // 
	 П = Новый Структура("Номенклатура",ТекСтрока.Номенклатура);
	 П.Вставить("Количество",ТекСтрока.Количество);
	 П.Вставить("ТЧ_msПартионныйУчет",СтруктураПараметровДляФормы.Объект.msПартионныйУчет);
	 П.Вставить("ПоступлениеТекущее",СтруктураПараметровДляФормы.Объект.Ссылка);
	 П.Вставить("ТЧ_Товары",СтруктураПараметровДляФормы.Объект.Товары);
	 П.Вставить("ЕД",ТекСтрока.Номенклатура.ЕдиницаИзмерения);
	 П.Вставить("ВыделенныеСтроки_Массив",СтруктураПараметровДляФормы.ВыделенныеСтроки);
	 
	 
	 
	 //ТЗ=новый ТаблицаЗначений;
	 //ТЗ.Колонки.Добавить("Ном");
	 //ТЗ.Колонки.Добавить("Заказ");
	 //ТЗ.Колонки.Добавить("Кол");
	 //
	 //Для каждого стр Из msПартионныйУчет Цикл
	 //
	 //Новстрока=ТЗ.Добавить();
	 //Новстрока.Ном=стр.номенклатура;
	 //Новстрока.Заказ=стр.ЗаказКлиента;
	 //Новстрока.Кол=стр.Количество;
	 //
	 //
	 //КонецЦикла;
	 //
	 //П.Вставить("ТЗ",Тз);
	 
	 //Форма = ПолучитьФорму("Справочник.Контрагенты.ФормаОбъекта", П);
	 //Форма.Открыть(); 
	 
	 ВыделенныеСтроки  = СтруктураПараметровДляФормы.ВыделенныеСтроки;
	 
	 Если ВыделенныеСтроки.количество()=1 Тогда
		 // отрабатываем простановку наценки с учетом заказов	
		 СтруктураПараметров=ОткрытьФормуМодально("ОбщаяФорма.ВыборПодписей1",П);
		 
	 иначе
		 
		 СтруктураПараметров=ОткрытьФормуМодально("ОбщаяФорма.ФормаПараметровПартионногоУчета1");
		 Попытка
			 
			 Статус_Формы=СтруктураПараметров.Статус;	
			 
		 Исключение
			 
		 КонецПопытки;
		 Попытка
			 РучнаяНаценка=СтруктураПараметров.Ручная;
		 Исключение
			 
		 КонецПопытки;
		 Если Статус_Формы="Закрыта" Тогда
			 // ЗначениеСсылка=СтруктураПараметров.ЗначениеСсылка;
			 // КодВозвратаДиалога
			 //Возврат Неопределено;
			 //Попытка
			 //	СтруктураПараметров.Вставить("КодВозвратаДиалога",КодВозвратаДиалога.Отмена);
			 //	
			 //Исключение
			 //	
			 //КонецПопытки;
			 
		 Иначеесли Статус_Формы="ОК" Тогда	
			 Попытка
				 //	
				 ЗначениеСсылка=СтруктураПараметров.ЗначениеСсылка;	
				 //	
			 Исключение
				 //	
			 КонецПопытки;
			 //Попытка
			 //	СтруктураПараметров.Вставить("КодВозвратаДиалога",КодВозвратаДиалога.ОК);
			 //	
			 //Исключение
			 //	
			 //КонецПопытки;
			 
			 Попытка
				 
				 //Заказ=СтруктураПараметров.Заказ;	
				 //СтруктураПараметров.Вставить("Заказ",Заказ);
				 
			 Исключение
				 
			 КонецПопытки;
			 Попытка
				 СтруктураПараметров.Вставить("РучнаяНаценка",РучнаяНаценка);
				 
			 Исключение
				 
			 КонецПопытки;
			 
			 Если ЗначениеЗаполнено(ЗначениеСсылка) Тогда
				 
				 //ТЗСВариантами = msОбщийСервер.ВернутьСсылкуНаценки(Варианты);
				 СтруктураПараметров.Вставить("Варианты",ЗначениеСсылка);
				 
				 
				 
			 КонецЕсли; 
			 //возврат СтруктураПараметров;
			 
		 КонецЕсли; 
		 
	 КонецЕсли; 
	 Возврат СтруктураПараметров;
	 
	 //Если СтруктураПараметров=КодВозвратаДиалога.Отмена Тогда
	 //
	 //	Возврат Неопределено;
	 
	 //иначе
	 //	
	 //КонецЕсли; 
	 //Попытка
	 //	
	 //	Статус_Формы=СтруктураПараметров[0].Статус;	
	 //	
	 //Исключение
	 //	
	 //КонецПопытки;
	 ////Попытка
	 ////   РучнаяНаценка=СтруктураПараметров.Ручная;
	 ////Исключение
	 ////
	 ////КонецПопытки;
	 //Если Статус_Формы="Закрыта" Тогда
	 //	
	 //	Возврат Неопределено;
	 //   
	 //Иначеесли Статус_Формы="ОК" Тогда	
	 //Попытка
	 //  	 	
	 //  	 Варианты=СтруктураПараметров.Варианты;	
	 //  	 	
	 //Исключение
	 //  	 	
	 //КонецПопытки;
	 
	 //Попытка
	 //  	 
	 //  	 Заказ=СтруктураПараметров.Заказ;	
	 //  	 СтруктураПараметров.Вставить("Заказ",Заказ);
	 //  	 
	 //Исключение
	 //  	 
	 // //КонецПопытки;
	 //	 Попытка
	 //   	 СтруктураПараметров.Вставить("РучнаяНаценка",РучнаяНаценка);
	 
	 //Исключение
	 //
	 //КонецПопытки;
	 
	 //Если ЗначениеЗаполнено(Варианты) Тогда
	 //  	 
	 //  	 ТЗСВариантами = msОбщийСервер.ВернутьСсылкуНаценки(Варианты);
	 //  	 СтруктураПараметров.Вставить("Варианты",ТЗСВариантами);
	 //  	   	 
	 // //КонецЕсли; 
	 //	возврат СтруктураПараметров; // тут массив стурктур
	 //	
	 //КонецЕсли; 
	 
	 
	 
 КонецФункции  
 
 
 Функция ЗаполнитьПараметрыДокумента() Экспорт
	 
	 //ФормаПараметровПартионногоУчета = ПолучитьФорму("ОбщаяФорма.ФормаПараметровПартионногоУчета",);
	 //ФормаПараметровПартионногоУчета.ОткрытьФормуМодально(); 
	 
	 СтруктураПараметров=ОткрытьФормуМодально("ОбщаяФорма.ФормаПараметровПартионногоУчета1");
	 
	 Попытка
		 
		 Статус_Формы=СтруктураПараметров.Статус;	
		 
	 Исключение
		 
	 КонецПопытки;
	 Попытка
		 РучнаяНаценка=СтруктураПараметров.Ручная;
	 Исключение
		 
	 КонецПопытки;
	 Если Статус_Формы="Закрыта" Тогда
		 
		 Возврат Неопределено;
	 Иначеесли Статус_Формы="ОК" Тогда	
		 Попытка
			 //	
			 ЗначениеСсылка=СтруктураПараметров.ЗначениеСсылка;	
			 //	
		 Исключение
			 //	
		 КонецПопытки;
		 //
		 Попытка
			 
			 //Заказ=СтруктураПараметров.Заказ;	
			 //СтруктураПараметров.Вставить("Заказ",Заказ);
			 
		 Исключение
			 
		 КонецПопытки;
		 Попытка
			 СтруктураПараметров.Вставить("РучнаяНаценка",РучнаяНаценка);
			 
		 Исключение
			 
		 КонецПопытки;
		 
		 Если ЗначениеЗаполнено(ЗначениеСсылка) Тогда
			 
			 //ТЗСВариантами = msОбщийСервер.ВернутьСсылкуНаценки(Варианты);
			 СтруктураПараметров.Вставить("Варианты",ЗначениеСсылка);
			 
			 
			 
		 КонецЕсли; 
		 возврат СтруктураПараметров;
		 
	 КонецЕсли; 
	 
	 
	 
	 
 КонецФункции  
 
 
 