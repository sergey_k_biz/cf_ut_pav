﻿////////////////////////////////////////////////////////////////////////////////
// МОДУЛЬ СОДЕРЖИТ ПРОЦЕДУРЫ БИБЛИОТЕКИ УЧЕТА НДС ДЛЯ РАЗДЕЛЬНОГО УЧЕТА НДС
//

// Функция получения выручки от реализации по разным ставкам НДС с детализацией по группам для раздельного учета НДС.
//
// Параметры:
//	Настройка - структура, содержащая настройки учета НДС
//	Период - стандартный период за который нужно получать информацию о выручке
//	СписокРегистраторов - массив документов по которым нужно получать информацию о выручке
//
// Возвращаемое значение:
//	ТаблицаЗначений - колонки: Группа, ВыручкаБезНДС, ВыручкаНДС0,ВыручкаНДС
//
Функция ПолучитьВыручкуПоВидамДеятельности(Настройка, Период, СписокРегистраторов = Неопределено) Экспорт
	
	//Пользователю, ответственному за учет НДС может быть недоступна детальная информация (по номенклатуре или контрагентам) по выручке
	//но нужна сводная информация по реализации по разным ставкам НДС
	//По этому используем привилегированный режим для получения информации о выручке
	УстановитьПривилегированныйРежим(Истина);
	
	СтруктураПараметров = Новый Структура;
	СтруктураПараметров.Вставить("Период", Период);
	СтруктураПараметров.Вставить("ДатаОстатка", Период.ДатаОкончания + 1);
	СтруктураПараметров.Вставить("Организация", Настройка.Организация);
	
	Если СписокРегистраторов <> Неопределено Тогда
		СтруктураПараметров.Вставить("СписокРегистраторов", СписокРегистраторов);
	КонецЕсли;	
	
	СхемаВыручкаОтПродаж = ПолучитьОбщийМакет("СхемаПолученияДанныхОВыручке");
	
	ПользовательскиеНастройкиКомпоновки = Настройка.НастройкиОтборовРаздельногоУчетаНДС.Получить();

	Запрос = УчетНДС.ПолучитьЗапросСУчетомНастроек(СхемаВыручкаОтПродаж, ПользовательскиеНастройкиКомпоновки, СтруктураПараметров);
	
	ТаблицаВыручки = Запрос.Выполнить().Выгрузить();
	
	ТаблицаВыручки.Свернуть("Номенклатура", "ВыручкаНДС,ВыручкаБезНДС,ВыручкаНДС0,КоличествоНДС0,Остаток");
	
	УстановитьПривилегированныйРежим(Ложь);
	
	Возврат ТаблицаВыручки;
	
КонецФункции

// Функция получения таблицы заблокированного НДС к вычету, ожидающего подтверждение реализации по ставке 0%.
//
// Параметры:
//	Настройка - структура, содержащая настройки учета НДС
//	Период - стандартный период за который нужно получать информацию о выручке
//	ТаблицаВыручки - таблица значений, содержащая информацию о выручке по группам
//
// Возвращаемое значение:
//	ТаблицаЗначений
//
Функция ПолучитьТаблицуЗаблокированногоНДС(Настройка, ТаблицаВыручки, Период) Экспорт
	
	ТипСуммы = Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(15,2));
	ТаблицаРаспределения = Новый ТаблицаЗначений();
	ТаблицаРаспределения.Колонки.Добавить("Организация");
	ТаблицаРаспределения.Колонки.Добавить("Номенклатура");
	ТаблицаРаспределения.Колонки.Добавить("СчетФактура");
	ТаблицаРаспределения.Колонки.Добавить("Поставщик");
	ТаблицаРаспределения.Колонки.Добавить("ВидЦенности");
	ТаблицаРаспределения.Колонки.Добавить("СтавкаНДС");
	ТаблицаРаспределения.Колонки.Добавить("Валюта");
	ТаблицаРаспределения.Колонки.Добавить("СуммаБезНДС", ТипСуммы);
	ТаблицаРаспределения.Колонки.Добавить("НДС", ТипСуммы);
	ТаблицаРаспределения.Колонки.Добавить("Количество", ТипСуммы);
	
	ВалютаРеглУчета = Константы.ВалютаРегламентированногоУчета.Получить();
	
	СтруктураОтбора = Новый Структура;
	СтруктураОтбора.Вставить("Организация", Настройка.Организация);
	
	// Получим данные о поступивших в этом месяце счетах фактурах
	Запрос = Новый Запрос("ВЫБРАТЬ
	                      |	НДСЗаблокированныйКВычету.Организация,
	                      |	НДСЗаблокированныйКВычету.Поставщик,
	                      |	НДСЗаблокированныйКВычету.СчетФактура КАК СчетФактура,
	                      |	НДСЗаблокированныйКВычету.Номенклатура КАК Номенклатура,
	                      |	НДСЗаблокированныйКВычету.ВидЦенности,
	                      |	НДСЗаблокированныйКВычету.СтавкаНДС,
	                      |	НДСЗаблокированныйКВычету.СуммаБезНДСОстаток,
	                      |	НДСЗаблокированныйКВычету.НДСОстаток,
	                      |	НДСЗаблокированныйКВычету.КоличествоОстаток
	                      |ИЗ
	                      |	РегистрНакопления.НДСЗаблокированныйКВычету.Остатки(&ДатаОстатков, Организация = &Организация) КАК НДСЗаблокированныйКВычету
	                      |
	                      |УПОРЯДОЧИТЬ ПО
	                      |	НДСЗаблокированныйКВычету.СчетФактура.Дата,
	                      |	СчетФактура
	                      |ИТОГИ ПО
	                      |	Номенклатура");
	Запрос.УстановитьПараметр("ДатаОстатков", Период.ДатаОкончания+1);
	Запрос.УстановитьПараметр("Организация", Настройка.Организация);
	
	Результат = Запрос.Выполнить();
	Если НЕ Результат.Пустой() Тогда
		ВыборкаПоГруппам = Результат.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
		Пока ВыборкаПоГруппам.Следующий() Цикл
			СтрокаТаблицыВыручки = ТаблицаВыручки.Найти(ВыборкаПоГруппам.Номенклатура, "Номенклатура");
			Если СтрокаТаблицыВыручки = Неопределено Тогда
				Продолжить;//По данной номенклатуре не было выручки
			КонецЕсли;
			
			КоличествоОтРеализации = СтрокаТаблицыВыручки.КоличествоНДС0;
			
			Выборка = ВыборкаПоГруппам.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
			Пока Выборка.Следующий() Цикл
				Если КоличествоОтРеализации > Выборка.КоличествоОстаток Тогда
					КоличествоСписания = Выборка.КоличествоОстаток;
				Иначе
					КоличествоСписания = КоличествоОтРеализации;
				КонецЕсли;	
				НоваяСтрока = ТаблицаРаспределения.Добавить();
				ЗаполнитьЗначенияСвойств(НоваяСтрока, Выборка);
				
				НоваяСтрока.СуммаБезНДС = КоличествоСписания / Выборка.КоличествоОстаток * Выборка.СуммаБезНДСОстаток;
				НоваяСтрока.НДС         = КоличествоСписания / Выборка.КоличествоОстаток * Выборка.НДСОстаток;
				НоваяСтрока.Количество  = КоличествоСписания;
				
				НоваяСтрока.Валюта = ВалютаРеглУчета;
				
				КоличествоОтРеализации = КоличествоОтРеализации - КоличествоСписания;
				Если КоличествоОтРеализации = 0 Тогда
					Прервать;
				КонецЕсли;	
			КонецЦикла;
		КонецЦИкла;	
	КонецЕсли;	
	
	Возврат ТаблицаРаспределения;
	
КонецФункции

// Процедура формирования движений по регистру "НераспределенныйНДС".
//
Процедура ОтразитьНераспределенныйНДС(ДополнительныеСвойства, Движения, Отказ) Экспорт
	
	ДанныеПоНДС = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаНераспределенныйНДС;
	
	Если Отказ ИЛИ ДанныеПоНДС.Количество() = 0  Тогда
		Возврат;
	КонецЕсли;
	
	Движения.НераспределенныйНДС.Записывать = Истина;
	Движения.НераспределенныйНДС.Загрузить(ДанныеПоНДС);
	
КонецПроцедуры

// Процедура формирования движений по регистру "НДСЗаблокированныйКВычету".
//
Процедура ОтразитьНДСЗаблокированныйКВычету(ДополнительныеСвойства, Движения, Отказ) Экспорт
	
	ДанныеПоНДС = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаНДСЗаблокированныйКВычету;
	
	Если Отказ ИЛИ ДанныеПоНДС.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Движения.НДСЗаблокированныйКВычету.Записывать = Истина;
	Движения.НДСЗаблокированныйКВычету.Загрузить(ДанныеПоНДС);
	
КонецПроцедуры

// Процедура формирования движений по регистру "НДСПринятыйКВычету".
//
Процедура ОтразитьНДСПринятыйКВычету(ДополнительныеСвойства, Движения, Отказ) Экспорт
	
	ДанныеПоНДС = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаНДСПринятыйКВычету;
	
	Если Отказ ИЛИ ДанныеПоНДС.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Движения.НДСПринятыйКВычету.Записывать = Истина;
	Движения.НДСПринятыйКВычету.Загрузить(ДанныеПоНДС);
	
КонецПроцедуры

// Процедура формирования движений по регистру "НДСВключенныйВСтоимость".
//
Процедура ОтразитьНДСВключенныйВСтоимость(ДополнительныеСвойства, Движения, Отказ) Экспорт
	
	ДанныеПоНДС = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаНДСВключенныйВСтоимость;
	
	Если Отказ ИЛИ ДанныеПоНДС.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Движения.НДСВключенныйВСтоимость.Записывать = Истина;
	Движения.НДСВключенныйВСтоимость.Загрузить(ДанныеПоНДС);
	
КонецПроцедуры
