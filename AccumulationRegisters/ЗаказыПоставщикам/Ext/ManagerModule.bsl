﻿////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// Обновление информационной базы

// Процедура выполняет заполнение пустого поля склад у товаров.
//
Процедура ЗаполнитьИзмерениеСклад() Экспорт

	УстановитьПривилегированныйРежим(Истина);

	Запрос = Новый Запрос(
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ТаблицаРегистра.Регистратор КАК Регистратор
	|ПОМЕСТИТЬ Втрегистраторы
	|ИЗ
	|	РегистрНакопления.ЗаказыПоставщикам КАК ТаблицаРегистра
	|ГДЕ
	|	ТаблицаРегистра.Склад = ЗНАЧЕНИЕ(Справочник.Склады.ПустаяСсылка)
	|	И ТаблицаРегистра.Номенклатура.ТипНоменклатуры = ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.Товар)
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Регистратор
	|");
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Выборка = Запрос.Выполнить().Выбрать();
	Выборка.Следующий();
	Если Выборка.Количество = 0 Тогда
		Возврат;
	КонецЕсли;

	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1000
	|	Втрегистраторы.Регистратор КАК Регистратор
	|ПОМЕСТИТЬ ВтПервыеРегистраторы
	|ИЗ
	|	Втрегистраторы КАК Втрегистраторы
	|ГДЕ
	|	Втрегистраторы.Регистратор > &ПоследнийРегистратор
	|
	|УПОРЯДОЧИТЬ ПО
	|	Регистратор
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаРегистра.*,
	// Склад нужно заполнить только у товаров.
	|	ВЫБОР КОГДА ТаблицаРегистра.Номенклатура.ТипНоменклатуры = ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.Товар) ТОГДА
	|		ТаблицаРегистра.ЗаказПоставщику.Склад 
	|	ИНАЧЕ
	|		НЕОПРЕДЕЛЕНО
	|	КОНЕЦ КАК Склад
	|ИЗ
	|	РегистрНакопления.ЗаказыПоставщикам КАК ТаблицаРегистра
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВтПервыеРегистраторы КАК ВтПервыеРегистраторы
	|		ПО ТаблицаРегистра.Регистратор = ВтПервыеРегистраторы.Регистратор
	|
	|УПОРЯДОЧИТЬ ПО
	|	ТаблицаРегистра.Регистратор, ТаблицаРегистра.НомерСтроки
	|
	|ИТОГИ ПО
	|	ТаблицаРегистра.Регистратор
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|УНИЧТОЖИТЬ ВтПервыеРегистраторы;
	|";
	ПоследнийРегистратор   = Неопределено;
	ЕстьДанныеДляОбработки = Истина;
	НаборЗаписейРегистра   = РегистрыНакопления.ЗаказыПоставщикам.СоздатьНаборЗаписей();

	Пока ЕстьДанныеДляОбработки Цикл 
	
		Запрос.УстановитьПараметр("ПоследнийРегистратор", ПоследнийРегистратор);
		Результат              = Запрос.Выполнить();
		ЕстьДанныеДляОбработки = Не Результат.Пустой();

		ВыборкаРегистраторы = Результат.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
		Пока ВыборкаРегистраторы.Следующий() Цикл

			НаборЗаписейРегистра.Отбор.Регистратор.Установить(ВыборкаРегистраторы.Регистратор);

			ВыборкаДетали = ВыборкаРегистраторы.Выбрать();
			Пока ВыборкаДетали.Следующий() Цикл

				ЗаполнитьЗначенияСвойств(НаборЗаписейРегистра.Добавить(), ВыборкаДетали);

			КонецЦикла;
			НаборЗаписейРегистра.Записать(Истина);
			НаборЗаписейРегистра.Очистить();

			ПоследнийРегистратор = ВыборкаРегистраторы.Регистратор;
		КонецЦикла;

	КонецЦикла;

КонецПроцедуры
