﻿
///////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

///////////////////////////////////////////////////////////////////////////////
// Обновление информационной базы

Процедура ОтразитьРублевыеСуммыПоДокументамОплаты(МассивАналитикУчетаПоПартнерам) Экспорт
	
	ВалютаРегламентированногоУчета = Константы.ВалютаРегламентированногоУчета.Получить();
	КэшКурсовВалют = РаботаСКурсамиВалютУТ.ИнициализироватьКэшКурсовВалют();
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	РасчетыСКлиентами.Период					КАК Период,
	|	РасчетыСКлиентами.Регистратор				КАК ДокументСсылка,
	|	РасчетыСКлиентами.НомерСтроки 				КАК НомерСтроки,
	|	РасчетыСКлиентами.АналитикаУчетаПоПартнерам КАК АналитикаУчетаПоПартнерам,
	|	РасчетыСКлиентами.Валюта					КАК Валюта,
	|	РасчетыСКлиентами.Сумма						КАК Сумма
	|ПОМЕСТИТЬ ВТ_РасчетыСКлиентами
	|ИЗ
	|	РегистрНакопления.РасчетыСКлиентами КАК РасчетыСКлиентами
	|ГДЕ
	|	РасчетыСКлиентами.Валюта 		<> &ВалютаРегламентированногоУчета
	|	И РасчетыСКлиентами.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)
	|	И РасчетыСКлиентами.Сумма 		> 0
	|	И РасчетыСКлиентами.СуммаРегл	= 0
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТ_РасчетыСКлиентами.Период									  КАК Период,
	|	ВТ_РасчетыСКлиентами.ДокументСсылка 						  КАК ДокументСсылка,
	|	ВТ_РасчетыСКлиентами.НомерСтроки							  КАК НомерСтроки,
	|	ВТ_РасчетыСКлиентами.АналитикаУчетаПоПартнерам				  КАК АналитикаУчетаПоПартнерам,
	|	ДанныеДокумента.Валюта										  КАК ВалютаДокумента,
	|	ПоступлениеБезналичныхДенежныхСредствРасшифровкаПлатежа.Сумма КАК Сумма
	|ИЗ
	|	ВТ_РасчетыСКлиентами КАК ВТ_РасчетыСКлиентами
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ПоступлениеБезналичныхДенежныхСредств КАК ДанныеДокумента
	|		ПО ВТ_РасчетыСКлиентами.ДокументСсылка = ДанныеДокумента.Ссылка
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ПоступлениеБезналичныхДенежныхСредств.РасшифровкаПлатежа КАК ПоступлениеБезналичныхДенежныхСредствРасшифровкаПлатежа
	|		ПО ВТ_РасчетыСКлиентами.ДокументСсылка = ПоступлениеБезналичныхДенежныхСредствРасшифровкаПлатежа.Ссылка
	|			И ВТ_РасчетыСКлиентами.НомерСтроки = ПоступлениеБезналичныхДенежныхСредствРасшифровкаПлатежа.НомерСтроки
	|ГДЕ
	|	ВТ_РасчетыСКлиентами.ДокументСсылка ССЫЛКА Документ.ПоступлениеБезналичныхДенежныхСредств
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ВТ_РасчетыСКлиентами.Период									  КАК Период,
	|	ВТ_РасчетыСКлиентами.ДокументСсылка 						  КАК ДокументСсылка,
	|	ВТ_РасчетыСКлиентами.НомерСтроки							  КАК НомерСтроки,
	|	ВТ_РасчетыСКлиентами.АналитикаУчетаПоПартнерам				  КАК АналитикаУчетаПоПартнерам,
	|	ДанныеДокумента.Валюта										  КАК ВалютаДокумента,
	|	ПриходныйКассовыйОрдерРасшифровкаПлатежа.Сумма				  КАК Сумма
	|ИЗ
	|	ВТ_РасчетыСКлиентами КАК ВТ_РасчетыСКлиентами
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ПриходныйКассовыйОрдер КАК ДанныеДокумента
	|		ПО ВТ_РасчетыСКлиентами.ДокументСсылка = ДанныеДокумента.Ссылка
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ПриходныйКассовыйОрдер.РасшифровкаПлатежа КАК ПриходныйКассовыйОрдерРасшифровкаПлатежа
	|		ПО ВТ_РасчетыСКлиентами.ДокументСсылка = ПриходныйКассовыйОрдерРасшифровкаПлатежа.Ссылка
	|			И ВТ_РасчетыСКлиентами.НомерСтроки = ПриходныйКассовыйОрдерРасшифровкаПлатежа.НомерСтроки
	|ГДЕ
	|	ВТ_РасчетыСКлиентами.ДокументСсылка ССЫЛКА Документ.ПриходныйКассовыйОрдер
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ВТ_РасчетыСКлиентами.Период									  КАК Период,
	|	ВТ_РасчетыСКлиентами.ДокументСсылка 						  КАК ДокументСсылка,
	|	ВТ_РасчетыСКлиентами.НомерСтроки							  КАК НомерСтроки,
	|	ВТ_РасчетыСКлиентами.АналитикаУчетаПоПартнерам				  КАК АналитикаУчетаПоПартнерам,
	|	ДанныеДокумента.Валюта										  КАК ВалютаДокумента,
	|	СписаниеБезналичныхДенежныхСредствРасшифровкаПлатежа.Сумма	  КАК Сумма
	|ИЗ
	|	ВТ_РасчетыСКлиентами КАК ВТ_РасчетыСКлиентами
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.СписаниеБезналичныхДенежныхСредств КАК ДанныеДокумента
	|		ПО ВТ_РасчетыСКлиентами.ДокументСсылка = ДанныеДокумента.Ссылка
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.СписаниеБезналичныхДенежныхСредств.РасшифровкаПлатежа КАК СписаниеБезналичныхДенежныхСредствРасшифровкаПлатежа
	|		ПО ВТ_РасчетыСКлиентами.ДокументСсылка = СписаниеБезналичныхДенежныхСредствРасшифровкаПлатежа.Ссылка
	|			И ВТ_РасчетыСКлиентами.НомерСтроки = СписаниеБезналичныхДенежныхСредствРасшифровкаПлатежа.НомерСтроки
	|ГДЕ
	|	ВТ_РасчетыСКлиентами.ДокументСсылка ССЫЛКА Документ.СписаниеБезналичныхДенежныхСредств
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ВТ_РасчетыСКлиентами.Период									  КАК Период,
	|	ВТ_РасчетыСКлиентами.ДокументСсылка 						  КАК ДокументСсылка,
	|	ВТ_РасчетыСКлиентами.НомерСтроки							  КАК НомерСтроки,
	|	ВТ_РасчетыСКлиентами.АналитикаУчетаПоПартнерам				  КАК АналитикаУчетаПоПартнерам,
	|	ДанныеДокумента.Валюта										  КАК ВалютаДокумента,
	|	ОперацияПоПлатежнойКартеРасшифровкаПлатежа.Сумма			  КАК Сумма
	|ИЗ
	|	ВТ_РасчетыСКлиентами КАК ВТ_РасчетыСКлиентами
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ОперацияПоПлатежнойКарте КАК ДанныеДокумента
	|		ПО ВТ_РасчетыСКлиентами.ДокументСсылка = ДанныеДокумента.Ссылка,
	|	Документ.ОперацияПоПлатежнойКарте.РасшифровкаПлатежа КАК ОперацияПоПлатежнойКартеРасшифровкаПлатежа
	|ГДЕ
	|	ВТ_РасчетыСКлиентами.ДокументСсылка ССЫЛКА Документ.ОперацияПоПлатежнойКарте
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ВТ_РасчетыСКлиентами.Период									  КАК Период,
	|	ВТ_РасчетыСКлиентами.ДокументСсылка 						  КАК ДокументСсылка,
	|	ВТ_РасчетыСКлиентами.НомерСтроки							  КАК НомерСтроки,
	|	ВТ_РасчетыСКлиентами.АналитикаУчетаПоПартнерам				  КАК АналитикаУчетаПоПартнерам,
	|	ДанныеДокумента.Валюта										  КАК ВалютаДокумента,
	|	ВозвратТоваровОтКлиентаРасшифровкаПлатежа.Сумма				  КАК Сумма
	|ИЗ
	|	ВТ_РасчетыСКлиентами КАК ВТ_РасчетыСКлиентами
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ВозвратТоваровОтКлиента КАК ДанныеДокумента
	|		ПО ВТ_РасчетыСКлиентами.ДокументСсылка = ДанныеДокумента.Ссылка
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ВозвратТоваровОтКлиента.РасшифровкаПлатежа КАК ВозвратТоваровОтКлиентаРасшифровкаПлатежа
	|		ПО ВТ_РасчетыСКлиентами.ДокументСсылка = ВозвратТоваровОтКлиентаРасшифровкаПлатежа.Ссылка
	|			И ВТ_РасчетыСКлиентами.НомерСтроки = ВозвратТоваровОтКлиентаРасшифровкаПлатежа.НомерСтроки
	|ГДЕ
	|	ВТ_РасчетыСКлиентами.ДокументСсылка ССЫЛКА Документ.ВозвратТоваровОтКлиента
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ВТ_РасчетыСКлиентами.Период									  КАК Период,
	|	ВТ_РасчетыСКлиентами.ДокументСсылка 						  КАК ДокументСсылка,
	|	ВТ_РасчетыСКлиентами.НомерСтроки							  КАК НомерСтроки,
	|	ВТ_РасчетыСКлиентами.АналитикаУчетаПоПартнерам				  КАК АналитикаУчетаПоПартнерам,
	|	ЕСТЬNULL(ДанныеДокумента.Валюта, Неопределено)				  КАК ВалютаДокумента,
	|	ЕСТЬNULL(ВзаимозачетЗадолженностиДебиторскаяЗадолженность.Сумма,0) КАК Сумма
	|ИЗ
	|	ВТ_РасчетыСКлиентами КАК ВТ_РасчетыСКлиентами
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ВзаимозачетЗадолженности КАК ДанныеДокумента
	|		ПО ВТ_РасчетыСКлиентами.ДокументСсылка = ДанныеДокумента.Ссылка
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ВзаимозачетЗадолженности.ДебиторскаяЗадолженность КАК ВзаимозачетЗадолженностиДебиторскаяЗадолженность
	|		ПО ВТ_РасчетыСКлиентами.ДокументСсылка = ВзаимозачетЗадолженностиДебиторскаяЗадолженность.Ссылка
	|			И ВТ_РасчетыСКлиентами.Валюта = ВзаимозачетЗадолженностиДебиторскаяЗадолженность.ВалютаВзаиморасчетов
	|			И ВТ_РасчетыСКлиентами.Сумма = ВзаимозачетЗадолженностиДебиторскаяЗадолженность.СуммаВзаиморасчетов
	|			И ВТ_РасчетыСКлиентами.Сумма = ВзаимозачетЗадолженностиДебиторскаяЗадолженность.СуммаВзаиморасчетов
	|			И ВзаимозачетЗадолженностиДебиторскаяЗадолженность.ТипРасчетов = ЗНАЧЕНИЕ(Перечисление.ТипыРасчетовСПартнерами.РасчетыСКлиентом)
	|ГДЕ
	|	ВТ_РасчетыСКлиентами.ДокументСсылка ССЫЛКА Документ.ВзаимозачетЗадолженности
	|
	|ИТОГИ ПО
	|	ДокументСсылка");
	
	Запрос.УстановитьПараметр("ВалютаРегламентированногоУчета", ВалютаРегламентированногоУчета);
	
	ВыборкаПоДокументам = Запрос.Выполнить().Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
	
	ДвиженияРасчетыСКлиентами = РегистрыНакопления.РасчетыСКлиентами.СоздатьНаборЗаписей();

	Пока ВыборкаПоДокументам.Следующий() Цикл
		
		ДвиженияРасчетыСКлиентами.Отбор.Регистратор.Установить(ВыборкаПоДокументам.ДокументСсылка);
		ДвиженияРасчетыСКлиентами.Прочитать();
		
		Выборка = ВыборкаПоДокументам.Выбрать();
		Пока Выборка.Следующий() Цикл
			
			ЗаписьРасчетыСКлиентам = ДвиженияРасчетыСКлиентами[Выборка.НомерСтроки-1];
			
			Если Выборка.ВалютаДокумента = ВалютаРегламентированногоУчета Тогда
				ЗаписьРасчетыСКлиентам.СуммаРегл = Выборка.Сумма;
			Иначе
				СтруктураКурсаВалюты = РаботаСКурсамиВалютУТ.ПолучитьСтруктуруКурсаВалютыИзКэша(Выборка.ВалютаДокумента, НачалоДня(Выборка.Период), КэшКурсовВалют);
				
				ЗаписьРасчетыСКлиентам.СуммаРегл = ?(СтруктураКурсаВалюты.Кратность <> 0,
													Выборка.Сумма * СтруктураКурсаВалюты.Курс / СтруктураКурсаВалюты.Кратность,
													0);				
			КонецЕсли;
			
			Если МассивАналитикУчетаПоПартнерам.Найти(Выборка.АналитикаУчетаПоПартнерам) = Неопределено Тогда
				МассивАналитикУчетаПоПартнерам.Добавить(Выборка.АналитикаУчетаПоПартнерам);
			КонецЕсли;
			
		КонецЦикла;
		
		ДвиженияРасчетыСКлиентами.Записать();
		
	КонецЦикла;	

КонецПроцедуры
