﻿////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ 

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	Если Не СкладыСервер.ИспользоватьАдресноеХранение(Склад,Помещение) Тогда
		МассивНепроверяемыхРеквизитов.Добавить("Товары.Упаковка");
	КонецЕсли;
	
	ОбработкаТабличнойЧастиСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект,МассивНепроверяемыхРеквизитов,Отказ);
	
	ОбработкаТабличнойЧастиСервер.ПроверитьЗаполнениеСерий(ЭтотОбъект,Обработки.ПроверкаКоличестваТоваровВДокументе.ПараметрыУказанияСерий(ЭтотОбъект),Отказ);
		
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
КонецПроцедуры
