﻿
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	СтруктураПараметров = ПолучитьПараметры(Параметры);
	ЗаполнитьЗначенияСвойств(ЭтаФорма, СтруктураПараметров);
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	
	Если Модифицированность И Не СохранитьПараметры Тогда
		
		СписокКнопок = Новый СписокЗначений();
		СписокКнопок.Добавить("Закрыть", НСтр("ru = 'Закрыть'"));
		СписокКнопок.Добавить("НеЗакрывать", НСтр("ru = 'Не закрывать'"));
		
		ОтветНаВопрос = Вопрос(НСтр("ru = 'Реквизиты печати заявления на возврат были изменены. Закрыть форму без сохранения реквизитов?'"), СписокКнопок);
		
		Если ОтветНаВопрос = "НеЗакрывать" Тогда
			Отказ = Истина;
			СохранитьПараметры = Ложь;
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	
	Если СохранитьПараметры Тогда
		
		СтруктураПараметров = ПолучитьПараметры(ЭтаФорма);
		ОповеститьОВыборе(СтруктураПараметров);
		
	КонецЕсли;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ШАПКИ ФОРМЫ

&НаКлиенте
Процедура ПричинаВозвратаНачалоВыбораИзСписка(Элемент, СтандартнаяОбработка)
	
	Элемент.СписокВыбора.Очистить();
	Элемент.СписокВыбора.ЗагрузитьЗначения(ПолучитьПричиныВозврата());
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура ОК(Команда)
	
	Если Не ТолькоПросмотр Тогда
		СохранитьПараметры = Истина;
	КонецЕсли;
	
	Закрыть();
	
КонецПроцедуры

&НаКлиенте
Процедура Отмена(Команда)
	
	Закрыть();
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// Прочее

&НаКлиентеНаСервереБезКонтекста
Функция ПолучитьПараметры(Источник)
	
	СтруктураПараметров = Новый Структура();
	СтруктураПараметров.Вставить("ДатаРожденияПокупателя",              Источник.ДатаРожденияПокупателя);
	СтруктураПараметров.Вставить("Покупатель",                          Источник.Покупатель);
	СтруктураПараметров.Вставить("Контрагент",                          Источник.Контрагент);
	СтруктураПараметров.Вставить("ПричинаВозврата",                     Источник.ПричинаВозврата);
	СтруктураПараметров.Вставить("ВидДокументаПокупателя",              Источник.ВидДокументаПокупателя);
	СтруктураПараметров.Вставить("СерияДокументаПокупателя",            Источник.СерияДокументаПокупателя);
	СтруктураПараметров.Вставить("НомерДокументаПокупателя",            Источник.НомерДокументаПокупателя);
	СтруктураПараметров.Вставить("ДатаВыдачиДокументаПокупателя",       Источник.ДатаВыдачиДокументаПокупателя);
	СтруктураПараметров.Вставить("СрокДействияДокументаПокупателя",     Источник.СрокДействияДокументаПокупателя);
	СтруктураПараметров.Вставить("КемВыданДокументПокупателя",          Источник.КемВыданДокументПокупателя);
	СтруктураПараметров.Вставить("КодПодразделенияДокументаПокупателя", Источник.КодПодразделенияДокументаПокупателя);
	
	Возврат СтруктураПараметров;
	
КонецФункции

&НаКлиенте
Процедура ПокупательНачалоВыбораИзСписка(Элемент, СтандартнаяОбработка)
	
	Элементы.Покупатель.СписокВыбора.Очистить();
	Элементы.Покупатель.СписокВыбора.Добавить(Контрагент, Контрагент);
	
КонецПроцедуры

&НаСервере
Функция ПолучитьПричиныВозврата()
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
	|	* 
	|ИЗ (
	|	ВЫБРАТЬ
	|		ЗаявкаНаВозврат.ПричинаВозврата КАК ПричинаВозврата
	|	ИЗ
	|		Документ.ЗаявкаНаВозвратТоваровОтКлиента КАК ЗаявкаНаВозврат
	|	ГДЕ
	|		ЗаявкаНаВозврат.ПричинаВозврата <> """"
	|		И ЗаявкаНаВозврат.Проведен
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		Возврат.ПричинаВозврата КАК ПричинаВозврата
	|	ИЗ
	|		Документ.ВозвратТоваровОтКлиента КАК Возврат
	|	ГДЕ
	|		Возврат.ПричинаВозврата <> """"
	|		И Возврат.Проведен
	|) КАК ВложенныйЗапрос
	|УПОРЯДОЧИТЬ ПО
	|	ПричинаВозврата ВОЗР
	|");
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("ПричинаВозврата");
	
КонецФункции