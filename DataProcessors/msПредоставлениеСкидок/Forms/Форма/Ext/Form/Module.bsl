﻿Функция ПолучитьостатокПартий()
	Запрос = Новый Запрос;
	Запрос.Текст =  "ВЫБРАТЬ
	|	mgУправленческиеПартииОстатки.Партия КАК Партия,
	|	mgУправленческиеПартииОстатки.Номенклатура,
	|	mgУправленческиеПартииОстатки.КоличествоОстаток,
	|	mgУправленческиеПартииОстатки.ЗаказПокупателя
	|ИЗ
	|	РегистрНакопления.mgУправленческиеПартии.Остатки КАК mgУправленческиеПартииОстатки
	|ГДЕ
	|	mgУправленческиеПартииОстатки.Номенклатура = &Номенклатура
	|
	|УПОРЯДОЧИТЬ ПО
	|	Партия
	|АВТОУПОРЯДОЧИВАНИЕ" ;
	Запрос.УстановитьПараметр("Номенклатура", Объект.Номенклатура);
	Если Объект.Номенклатура <> Справочники.Номенклатура.ПустаяСсылка()Тогда 
		РезультатЗапроса = Запрос.Выполнить().Выгрузить();
	КонецЕсли;
	Возврат РезультатЗапроса;
КонецФункции


&НаСервере
Процедура СформироватьНаСервере(СписокЦен)
	ЗапросПартии = Новый Запрос("ВЫБРАТЬ РАЗРЕШЕННЫЕ
	                            |	mgУправленческиеПартии.Партия КАК Партия,
	                            |	mgУправленческиеПартии.ВалютаJUK0,
	                            |	mgУправленческиеПартии.ЖУК0 / mgУправленческиеПартии.Количество КАК ЦенаЖУк0,
	                            |	mgУправленческиеПартии.Стоимость / mgУправленческиеПартии.Количество КАК ЦенаСебестоимость,
	                            |	mgУправленческиеПартии.ВалютаСтоимости КАК ВалютаСебестоимости,
	                            |	ПоступлениеТоваровУслугТовары.Цена КАК ЦенаВДокументеПоступления,
	                            |	mgУправленческиеПартии.Партия.ВалютаВзаиморасчетов,
	                            |	mgУправленческиеПартии.НомерСтроки,
	                            |	mgУправленческиеПартии.Регистратор.Дата КАК ДатаРеалтзации,
	                            |	mgУправленческиеПартии.Партия.Валюта,
	                            |	mgУправленческиеПартии.ЗаказПокупателя,
	                            |	mgУправленческиеПартии.Номенклатура,
	                            |	ПоступлениеТоваровУслугТовары.СуммаСНДС / ПоступлениеТоваровУслугТовары.Количество КАК ЦенаВДокументеПоступленияСоСкидкой
	                            |ПОМЕСТИТЬ Данные
	                            |ИЗ
	                            |	РегистрНакопления.mgУправленческиеПартии КАК mgУправленческиеПартии
	                            |		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ПоступлениеТоваровУслуг.Товары КАК ПоступлениеТоваровУслугТовары
	                            |		ПО mgУправленческиеПартии.Партия.Ссылка = ПоступлениеТоваровУслугТовары.Ссылка
	                            |			И mgУправленческиеПартии.Номенклатура = ПоступлениеТоваровУслугТовары.Номенклатура
	                            |ГДЕ
	                            |	mgУправленческиеПартии.Номенклатура = &Номенклатура
	                            |	И mgУправленческиеПартии.ВидДвижения = &ВидДвиженияПриход
	                            |;
	                            |
	                            |////////////////////////////////////////////////////////////////////////////////
	                            |ВЫБРАТЬ
	                            |	ЦеныНоменклатурыСрезПоследних.Номенклатура КАК Номенклатура,
	                            |	ЦеныНоменклатурыСрезПоследних.Цена КАК Цена,
	                            |	ЦеныНоменклатурыСрезПоследних.Валюта КАК Валюта
	                            |ПОМЕСТИТЬ Цены
	                            |ИЗ
	                            |	РегистрСведений.ЦеныНоменклатуры.СрезПоследних КАК ЦеныНоменклатурыСрезПоследних
	                            |ГДЕ
	                            |	ЦеныНоменклатурыСрезПоследних.ВидЦены = &ВидЦены
	                            |	И ЦеныНоменклатурыСрезПоследних.Номенклатура = &Номенклатура
	                            |;
	                            |
	                            |////////////////////////////////////////////////////////////////////////////////
	                            |ВЫБРАТЬ
	                            |	Данные.Партия,
	                            |	Данные.ВалютаJUK0,
	                            |	Данные.ЦенаЖУк0,
	                            |	Данные.ЦенаСебестоимость,
	                            |	Данные.ВалютаСебестоимости,
	                            |	Данные.ЦенаВДокументеПоступления,
	                            |	Данные.ПартияВалютаВзаиморасчетов,
	                            |	Данные.ПартияВалюта,
	                            |	Данные.ЗаказПокупателя,
	                            |	Цены.Валюта,
	                            |	ЕСТЬNULL(Цены.Цена, 0) КАК Цена,
	                            |	Данные.ЦенаВДокументеПоступленияСоСкидкой
	                            |ИЗ
	                            |	Данные КАК Данные
	                            |		ЛЕВОЕ СОЕДИНЕНИЕ Цены КАК Цены
	                            |		ПО Данные.Номенклатура = Цены.Номенклатура
	                            |
	                            |СГРУППИРОВАТЬ ПО
	                            |	Данные.Партия,
	                            |	Данные.ВалютаJUK0,
	                            |	Данные.ВалютаСебестоимости,
	                            |	Данные.ПартияВалютаВзаиморасчетов,
	                            |	Данные.ПартияВалюта,
	                            |	Данные.ЗаказПокупателя,
	                            |	Цены.Валюта,
	                            |	Данные.ЦенаЖУк0,
	                            |	Данные.ЦенаСебестоимость,
	                            |	Данные.ЦенаВДокументеПоступления,
	                            |	ЕСТЬNULL(Цены.Цена, 0),
	                            |	Данные.ЦенаВДокументеПоступленияСоСкидкой");
	ЗапросПартии.УстановитьПараметр("Номенклатура",Объект.Номенклатура);
	ЗапросПартии.УстановитьПараметр("ВидЦены",Константы.ОсновнойВидЦенПаркАвеню.Получить());
	ЗапросПартии.УстановитьПараметр("ВидДвиженияПриход",ВидДвиженияНакопления.Приход);
	РезультатЗапроса = ЗапросПартии.Выполнить().Выгрузить();
	ОстаткиПратий = ПолучитьостатокПартий();
	ЗапросЦен = Новый Запрос;
	ЗапросЦен.Текст =  "ВЫБРАТЬ
	|	ЦеныНоменклатурыСрезПоследних.Цена,
	|	ЦеныНоменклатурыСрезПоследних.Валюта
	|ИЗ
	|	РегистрСведений.ЦеныНоменклатуры.СрезПоследних КАК ЦеныНоменклатурыСрезПоследних
	|ГДЕ
	|	ЦеныНоменклатурыСрезПоследних.Номенклатура = &Номенклатура
	|	И ЦеныНоменклатурыСрезПоследних.ВидЦены В(&СписокЦен)" ;
	ЗапросЦен.УстановитьПараметр("Номенклатура",Объект.Номенклатура);
	ЗапросЦен.УстановитьПараметр("СписокЦен",СписокЦен);
	РезультатЦен =	 ЗапросЦен.Выполнить().Выгрузить();
	Для Каждого СтрокаЗапроса из РезультатЗапроса Цикл
		Попытка
			Если не Объект.Валюта = Справочники.Валюты.ПустаяСсылка() тогда
				Если СтрокаЗапроса.ВалютаJUK0 <> Объект.Валюта Тогда
					ЦенаЖука = msОбщий.ПересчитатьВалюту(СтрокаЗапроса.ЦенаЖУк0,СтрокаЗапроса.ВалютаJUK0,Объект.Валюта,?(ПересчетНаДатуДокумента,СтрокаЗапроса.ДатаРеалтзации,ТекущаяДата()));
				Иначе
					ЦенаЖука = СтрокаЗапроса.ЦенаЖУк0;
				КонецЕсли;
				Если СтрокаЗапроса.ВалютаСебестоимости <> Объект.Валюта Тогда
					Себестоимость = msОбщий.ПересчитатьВалюту(СтрокаЗапроса.ЦенаСебестоимость,СтрокаЗапроса.ВалютаСебестоимости,Объект.Валюта,?(ПересчетНаДатуДокумента,СтрокаЗапроса.ДатаРеалтзации,ТекущаяДата()));
				Иначе
					Себестоимость = СтрокаЗапроса.ЦенаСебестоимость;
				КонецЕсли;
				Если СтрокаЗапроса.ЦенаВДокументеПоступленияСоСкидкой <> Null Тогда
					Если СтрокаЗапроса.ПартияВалюта <> Объект.Валюта Тогда
						Закупочная = msОбщий.ПересчитатьВалюту(СтрокаЗапроса.ЦенаВДокументеПоступленияСоСкидкой,СтрокаЗапроса.ПартияВалюта,Объект.Валюта,?(ПересчетНаДатуДокумента,СтрокаЗапроса.ДатаРеалтзации,ТекущаяДата()));
					Иначе
						Закупочная = СтрокаЗапроса.ЦенаВДокументеПоступленияСоСкидкой;
					КонецЕсли;
				КонецЕсли;
				Розничная = 0;
				Для Каждого СтрокаЦены из РезультатЦен цикл
					Если СтрокаЦены.Цена <> 0 Тогда 
						Если СтрокаЦены.Валюта = Объект.Валюта Тогда
							Розничная = СтрокаЦены.Цена;
						Иначе
							Розничная = 0;
						КонецЕсли;
					КонецЕсли;
				КонецЦикла;
				Если Розничная = 0 Тогда 
					Розничная = msОбщий.ПересчитатьВалюту(СтрокаЗапроса.Цена,СтрокаЗапроса.Валюта,Объект.Валюта,ТекущаяДата());
				КонецЕсли;
			КонецЕсли;
			СтруктураОтбора = Новый Структура;
			СтруктураОтбора.Вставить("Партия",СтрокаЗапроса.Партия);
			Если СтрокаЗапроса.ЗаказПокупателя <> Документы.ЗаказКлиента.ПустаяСсылка() Тогда 
				
				СтруктураОтбора.Вставить("ЗаказПокупателя", СтрокаЗапроса.ЗаказПокупателя);
			КонецЕсли;
			ОстатокПратии = ОстаткиПратий.НайтиСтроки(СтруктураОтбора);
			//РозничнаяЦенаПартииВалюта = СтрокаЗапроса.РозничнаяЦенаПартии;
			РозничнаяЦена = СтрокаЗапроса.Цена;
			НоваяСтрока = Объект.ПартииТоваров.Добавить();
			Если СтрокаЗапроса.ЦенаВДокументеПоступления <> Null Тогда
				новаяСтрока.ЗакупочнаяЦена =Закупочная ;
				
			КонецЕсли;
			Если  ОстатокПратии <> Неопределено и ОстатокПратии.Количество() >0 Тогда 
				НоваяСтрока.ОстатокПартии=  ОстатокПратии[0].КоличествоОстаток;
				ОстатокПратии[0].КоличествоОстаток = 0;
			КонецЕсли; 
			НоваяСтрока.Партия = СтрокаЗапроса.Партия;
			НоваяСтрока.РозницнаяЦенаПартии =ЦенаЖука ;
			НоваяСтрока.Себестоимость = Себестоимость;
			
			НоваяСтрока.ТекущаяРозничнаяЦена = Розничная;
			НоваяСтрока.ЦенаВПредложении = Розничная;
			РассчитатьПрофитыАвто(НоваяСтрока);
			
			//НоваяСтрока.ЦенаСУчетомСкидки = (НоваяСтрока.РозницнаяЦенаПартии / 100) * (100-объект.ПроцентСкидки);
			//Процент = (Объект.СуммаСоСкидкой / СтрокаЗапроса.РозничнаяЦенаПартии) * 100;
			//НоваяСтрока.ПроцентСкидки = объект.ПроцентСкидки;
			//НоваяСтрока.ПрофитЗакуп = Окр(((НоваяСтрока.ТекущаяРозничнаяЦена*100)/новаяСтрока.ЗакупочнаяЦена)-100,2);
			Попытка
				НоваяСтрока.ПрофитПратии = Окр(((НоваяСтрока.ТекущаяРозничнаяЦена)-НоваяСтрока.РозницнаяЦенаПартии)/НоваяСтрока.РозницнаяЦенаПартии*100 ,2);
			Исключение
			КонецПопытки;
		Исключение
			Сообщить("Не верные данные по партионному учету по документу партии    " + СтрокаЗапроса.Партия);
		КонецПопытки;
		
	КонецЦикла;
	
КонецПроцедуры


&НаКлиенте
Процедура Сформировать(Команда)
	
	Если Объект.ПартииТоваров.Количество()>0 Тогда 
		
		Режим = РежимДиалогаВопрос.ДаНет;
		Ответ = Вопрос("Очистить табличную часть",Режим,0);
		
		Если Ответ = КодВозвратаДиалога.Да Тогда 
			Объект.ПартииТоваров.Очистить();
			Объект.Реализации.Очистить();
		КонецЕсли;
		
	КонецЕсли;
	
	СписокЦен = Новый СписокЗначений;
	СписокЦен.Добавить(Справочники.ВидыЦен.НайтиПоНаименованию("Розничная Парк Авеню"));
	СписокЦен.Добавить(Справочники.ВидыЦен.НайтиПоНаименованию("Розничная Парк Авеню USD")); 
	СписокЦен.Добавить(Справочники.ВидыЦен.НайтиПоНаименованию("Розничная Парк Авеню EUR"));
	
	СформироватьНаСервере(СписокЦен);
	
	СформироватьРеализации();
	
	//::::::::::::::::::::::::::::::::::::::::::::::::::::Qwest
	РезультатОтчета.Очистить();	
	СформироватьОтчет(ВидыЦен);	
	СформироватьОтчетКоличествоДнейПродажи();
	//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	РезультатОтчета.Показать();
	
КонецПроцедуры
&НаСервере
Процедура СформироватьРеализации()
	Запросреализаций = Новый Запрос;
	Запросреализаций.Текст = "ВЫБРАТЬ
	|	mgУправленческиеПартии.Партия,
	|	mgУправленческиеПартии.ВалютаJUK0,
	|	mgУправленческиеПартии.ЖУК0 КАК ЖУК01,
	|	mgУправленческиеПартии.ВалютаСтоимости,
	|	mgУправленческиеПартии.Стоимость КАК Стоимость1,
	|	mgУправленческиеПартии.ВалютаВыручки,
	|	mgУправленческиеПартии.Выручка / mgУправленческиеПартии.Количество КАК РозничнаяЦенаПартии,
	|	mgУправленческиеПартии.Регистратор.Дата КАК ДатаРеалтзации,
	|	mgУправленческиеПартии.Регистратор,
	|	mgУправленческиеПартии.Количество,
	|	РеализацияТоваровУслугТовары.Ссылка.Валюта,
	|	РеализацияТоваровУслугТовары.СуммаСНДС / mgУправленческиеПартии.Количество КАК ЦенаВДокументе,
	|	mgУправленческиеПартии.ПричинаСкидки,
	|	mgУправленческиеПартии.ПроцентСкидки,
	|	mgУправленческиеПартии.ЖУК0 / mgУправленческиеПартии.Количество КАК ЖУК0,
	|	mgУправленческиеПартии.Стоимость / mgУправленческиеПартии.Количество КАК Стоимость
	|ИЗ
	|	РегистрНакопления.mgУправленческиеПартии КАК mgУправленческиеПартии
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.РеализацияТоваровУслуг.Товары КАК РеализацияТоваровУслугТовары
	|		ПО mgУправленческиеПартии.Регистратор.Ссылка = РеализацияТоваровУслугТовары.Ссылка.Ссылка
	|			И mgУправленческиеПартии.НомерСтроки = РеализацияТоваровУслугТовары.НомерСтроки
	|ГДЕ
	|	mgУправленческиеПартии.Номенклатура = &Номенклатура
	|	И mgУправленческиеПартии.Выручка > 0" ;
	
	Запросреализаций.УстановитьПараметр("Номенклатура", Объект.Номенклатура );
	СписокРеализаций =  Запросреализаций.Выполнить().Выгрузить();
	ВалютаРуб = Справочники.Валюты.НайтиПоКоду(643);
	Для Каждого Строкареализации из СписокРеализаций Цикл
		Попытка
			Если не Объект.Валюта = Справочники.Валюты.ПустаяСсылка() тогда
				//Если Строкареализации.ВалютаJUK0 <> Объект.Валюта Тогда
				ЦенаЖука = msОбщий.ПересчитатьВалюту(Строкареализации.ЖУК0,Строкареализации.ВалютаJUK0,Объект.Валюта,Строкареализации.ДатаРеалтзации);
				ЦенаЖукаРуб = msОбщий.ПересчитатьВалюту(Строкареализации.ЖУК0,Строкареализации.ВалютаJUK0,ВалютаРуб,Строкареализации.ДатаРеалтзации);
				//Иначе
				//ЦенаЖука = Строкареализации.ЖУК0;
				ЦенаЖукаРуб = msОбщий.ПересчитатьВалюту(Строкареализации.ЖУК0,Строкареализации.ВалютаJUK0,ВалютаРуб,Строкареализации.ДатаРеалтзации);
				//КонецЕсли;
				//Если Строкареализации.ВалютаСтоимости <> Объект.Валюта Тогда
				Стоимость = msОбщий.ПересчитатьВалюту(Строкареализации.Стоимость,Строкареализации.ВалютаСтоимости,Объект.Валюта,Строкареализации.ДатаРеалтзации);
				//Иначе
				//	Стоимость = Строкареализации.Стоимость;
				//	КонецЕсли;
				//Если Строкареализации.ВалютаВыручки <> Объект.Валюта Тогда
				Розничная = msОбщий.ПересчитатьВалюту(Строкареализации.РозничнаяЦенаПартии,Строкареализации.ВалютаВыручки,Объект.Валюта,Строкареализации.ДатаРеалтзации);
				РозничнаяРуб = msОбщий.ПересчитатьВалюту(Строкареализации.РозничнаяЦенаПартии,Строкареализации.ВалютаВыручки,ВалютаРуб,Строкареализации.ДатаРеалтзации);
				//Иначе
				//	Розничная = Строкареализации.РозничнаяЦенаПартии;
				РозничнаяРуб = msОбщий.ПересчитатьВалюту(Строкареализации.РозничнаяЦенаПартии,Строкареализации.ВалютаВыручки,ВалютаРуб,Строкареализации.ДатаРеалтзации);
				//КонецЕсли;
				
			КонецЕсли;
			
			НоваяСтрока = Объект.Реализации.Добавить();
			НоваяСтрока.Регистратор  = Строкареализации.Регистратор;
			НоваяСтрока.Партия = Строкареализации.Партия;
			НоваяСтрока.Количество  = Строкареализации.Количество;
			НоваяСтрока.ЦенаЖУК0 = Стоимость ;
			НоваяСтрока.ПартионнаяЦена =  ЦенаЖука;
			НоваяСтрока.Выручка = Розничная * Строкареализации.Количество;
			//НоваяСтрока.Скидка = Строкареализации.ПроцентСкидки;
			НоваяСтрока.ПричинаСкидки = Строкареализации.ПричинаСкидки;
			НоваяСтрока.СуммаПрибыли = (РозничнаяРуб - ЦенаЖукаРуб) * Строкареализации.Количество;
			Попытка
				НоваяСтрока.ПрофитПартионный = Окр(((ЦенаЖука)-Стоимость)/Стоимость*100 ,2);	
				НоваяСтрока.Профит1 = Окр(((Розничная)-ЦенаЖука)/ЦенаЖука*100 ,2); 
				НоваяСтрока.Профит2 = Окр((Розничная-ЦенаЖука)/(Розничная)*100 ,2);
			Исключение
			КонецПопытки;
		Исключение
			Сообщить("Не верные данные по партионному учету по документу реализации    " + Строкареализации.Регистратор);
			
		КонецПопытки;
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура КодНоменклатурыПриИзменении(Элемент)
	КодНоменклатурыПриИзмененииНаСервере();
	
		Процент = НоменклатураПриИзмененииНаСервере(Объект.Номенклатура);
	Если значениеЗаполнено(Процент) тогда
	Объект.СкидкаНоменклатуры = ""+Процент + "%";
	Иначе
	Объект.СкидкаНоменклатуры = "скидка не назначена";
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура КодНоменклатурыПриИзмененииНаСервере()
	Объект.Номенклатура = Справочники.Номенклатура.НайтиПоКоду(Объект.КодНоменклатуры);
КонецПроцедуры

&НаКлиенте
Процедура ПроцентСкидкиПриИзменении(Элемент)
	//Для Каждого СтрокаТоваров из Объект.ПартииТоваров цикл
	//	Попытка
	//	СтрокаТоваров.ЦенаСУчетомСкидки = (СтрокаТоваров.РозницнаяЦенаПартии / 100) * (100-объект.ПроцентСкидки);
	//	Если СтрокаТоваров.ЦенаСУчетомСкидки <> 0 Тогда 
	//	СтрокаТоваров.ПрофитСоСкидкой = Окр(((СтрокаТоваров.ЦенаСУчетомСкидки*100)/СтрокаТоваров.ТекущаяРозничнаяЦена )-100,2);
	//	КонецЕсли;
	
	//	СтрокаТоваров.ПроцентСкидки = Объект.ПроцентСкидки;
	//Исключение
	//	КонецПопытки;
	//	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура СуммаСоСкидкойПриИзменении(Элемент)
	Объект.ПроцентСкидки = 0;
	для Каждого СтрокаТоваров из Объект.ПартииТоваров цикл
		Попытка
			СтрокаТоваров.ЦенаСУчетомСкидки = Объект.СуммаСоСкидкой;
			//СтрокаТоваров.ПроцентСкидки =  Окр(((СтрокаТоваров.РозницнаяЦенаПартии-Объект.СуммаСоСкидкой)/Объект.СуммаСоСкидкой ),2);
			РозничаняЦена = СтрокаТоваров.РозницнаяЦенаПартии; 
			Процент = (Объект.СуммаСоСкидкой / РозничаняЦена) * 100;
			СтрокаТоваров.ПроцентСкидки = 100 - процент;
			Если СтрокаТоваров.ЦенаСУчетомСкидки <> 0 Тогда 
				СтрокаТоваров.ПрофитСоСкидкой = Окр(((СтрокаТоваров.ЦенаСУчетомСкидки*100)/СтрокаТоваров.ТекущаяРозничнаяЦена )-100,2);
			КонецЕсли;
		Исключение
		КонецПопытки;
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ПриОткрытииНаСервере(Отказ);	
КонецПроцедуры

&НаСервере
Процедура ПриОткрытииНаСервере(Отказ)
	
	Объект.Валюта = Справочники.Валюты.НайтиПоКоду(840);
	Объект.НачалоПериода = Дата('20130101000000');
	Объект.КонецПериода  = ТекущаяДата();
	
	РозничнаяПаркАвенюEUR = Справочники.ВидыЦен.НайтиПоНаименованию("Розничная Парк Авеню USD");
	
	Если РозничнаяПаркАвенюEUR <> Справочники.ВидыЦен.ПустаяСсылка() Тогда 
		ВидыЦен.Добавить(РозничнаяПаркАвенюEUR, РозничнаяПаркАвенюEUR.Наименование);
	КонецЕсли;	
	
КонецПроцедуры	


&НаКлиенте
Процедура ПартииТоваровЦенаСУчетомСкидкиПриИзменении(Элемент)
	ТекСтрока = ЭтаФорма.ТекущийЭлемент.ТекущиеДанные.ПолучитьИдентификатор();
	РассчитатьПрофиты(ТекСтрока);
КонецПроцедуры
Процедура РассчитатьПрофиты(ИдентификаторСтроки)
	СтрокаРассчета = Объект.ПартииТоваров.НайтиПоИдентификатору(ИдентификаторСтроки);
	ЦенаСоСкидкой = (СтрокаРассчета.ЦенаВПредложении / 100) * (100-СтрокаРассчета.ПроцентСкидки);
	СтрокаРассчета.ИтоговаяЦена = ЦенаСоСкидкой;
	СтрокаРассчета.Профит1 = Окр(((СтрокаРассчета.ИтоговаяЦена)-СтрокаРассчета.РозницнаяЦенаПартии)/СтрокаРассчета.РозницнаяЦенаПартии*100 ,2);
	СтрокаРассчета.Профит2 = Окр((СтрокаРассчета.ИтоговаяЦена-СтрокаРассчета.РозницнаяЦенаПартии)/(СтрокаРассчета.ИтоговаяЦена)*100,2);
КонецПроцедуры
Процедура РассчитатьПрофитыАвто(Строка)
	СтрокаРассчета = Строка;
	ЦенаСоСкидкой = (СтрокаРассчета.ЦенаВПредложении / 100) * (100-СтрокаРассчета.ПроцентСкидки);
	СтрокаРассчета.ИтоговаяЦена = ЦенаСоСкидкой;
	СтрокаРассчета.Профит1 = Окр(((СтрокаРассчета.ИтоговаяЦена)-СтрокаРассчета.РозницнаяЦенаПартии)/СтрокаРассчета.РозницнаяЦенаПартии*100 ,2);
	СтрокаРассчета.Профит2 = Окр((СтрокаРассчета.ИтоговаяЦена-СтрокаРассчета.РозницнаяЦенаПартии)/(СтрокаРассчета.ИтоговаяЦена)*100,2);
КонецПроцедуры

&НаКлиенте
Процедура ПартииТоваровПроцентСкидкиПриИзменении(Элемент)
	ТекСтрока = ЭтаФорма.ТекущийЭлемент.ТекущиеДанные.ПолучитьИдентификатор();
	РассчитатьПрофиты(ТекСтрока);
КонецПроцедуры

//::::::::::::::::::::::::::::::::::::::::::::::::::::Qwest	
&НаСервере
Процедура СформироватьОтчет(ВидыЦен)	
	
	СхемаКомпоновкиДанных = Обработки.msПредоставлениеСкидок.ПолучитьМакет("Макет");
	
	Настройки = СхемаКомпоновкиДанных.НастройкиПоУмолчанию;
	
	НачалоПериода = Настройки.ПараметрыДанных.НайтиЗначениеПараметра(
	Новый ПараметрКомпоновкиДанных("НачалоПериода"));
	НачалоПериода.Значение = Объект.НачалоПериода;
	
	КонецПериода = Настройки.ПараметрыДанных.НайтиЗначениеПараметра(
	Новый ПараметрКомпоновкиДанных("КонецПериода"));
	КонецПериода.Значение = КонецДня(Объект.КонецПериода);
	
	Номенклатура = Настройки.ПараметрыДанных.НайтиЗначениеПараметра(
	Новый ПараметрКомпоновкиДанных("Номенклатура"));
	Номенклатура.Значение = Объект.Номенклатура;
	
	ВидЦены = Настройки.ПараметрыДанных.НайтиЗначениеПараметра(
	Новый ПараметрКомпоновкиДанных("ВидЦены"));
	ВидЦены.Значение = ВидыЦен;
	
	КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
	
	МакетКомпоновки = КомпоновщикМакета.Выполнить(СхемаКомпоновкиДанных, Настройки);
	
	ПроцессорКомпоновкиДанных = Новый ПроцессорКомпоновкиДанных;
	
	ПроцессорКомпоновкиДанных.Инициализировать(МакетКомпоновки);
	
	ПроцессорВывода = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВТабличныйДокумент;
	
	ПроцессорВывода.УстановитьДокумент(РезультатОтчета);
	
	ПроцессорВывода.Вывести(ПроцессорКомпоновкиДанных);
	
КонецПроцедуры	

&НаСервере
Функция СформироватьОтчетКоличествоДнейПродажи()
	
	СхемаКомпоновкиДанных = Обработки.msПредоставлениеСкидок.ПолучитьМакет("Макет1");
	
	Настройки = СхемаКомпоновкиДанных.НастройкиПоУмолчанию;
	
	НачалоПериода = Настройки.ПараметрыДанных.НайтиЗначениеПараметра(
	Новый ПараметрКомпоновкиДанных("НачалоПериода"));
	НачалоПериода.Значение = Объект.НачалоПериода;
	
	КонецПериода = Настройки.ПараметрыДанных.НайтиЗначениеПараметра(
	Новый ПараметрКомпоновкиДанных("КонецПериода"));
	КонецПериода.Значение = КонецДня(Объект.КонецПериода);
	
	Номенклатура = Настройки.ПараметрыДанных.НайтиЗначениеПараметра(
	Новый ПараметрКомпоновкиДанных("Номенклатура"));
	Номенклатура.Значение = Объект.Номенклатура;
	
	КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
	
	МакетКомпоновки = КомпоновщикМакета.Выполнить(СхемаКомпоновкиДанных, Настройки);
	
	ПроцессорКомпоновкиДанных = Новый ПроцессорКомпоновкиДанных;
	
	ПроцессорКомпоновкиДанных.Инициализировать(МакетКомпоновки);
	
	ПроцессорВывода = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВТабличныйДокумент;
	
	ПроцессорВывода.УстановитьДокумент(РезультатОтчета);
	
	ПроцессорВывода.Вывести(ПроцессорКомпоновкиДанных);
	
КонецФункции

&НаКлиенте
Процедура НоменклатураПриИзменении(Элемент)
	Процент = НоменклатураПриИзмененииНаСервере(Объект.Номенклатура);
	Если значениеЗаполнено(Процент) тогда
	Объект.СкидкаНоменклатуры = ""+Процент + "%";
	Иначе
	Объект.СкидкаНоменклатуры = "скидка не назначена";
	КонецЕсли;

КонецПроцедуры

&НаСервереБезКонтекста		
функция НоменклатураПриИзмененииНаСервере(Номенклатура)
			//{{КОНСТРУКТОР_ЗАПРОСА_С_ОБРАБОТКОЙ_РЕЗУЛЬТАТА
	// Данный фрагмент построен конструктором.
	// При повторном использовании конструктора, внесенные вручную изменения будут утеряны!!!
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	РаспродажныеТоварыНаСайте.Номенклатура,
		|	РаспродажныеТоварыНаСайте.ПроцентСкидки
		|ИЗ
		|	РегистрСведений.РаспродажныеТоварыНаСайте КАК РаспродажныеТоварыНаСайте
		|ГДЕ
		|	РаспродажныеТоварыНаСайте.Номенклатура = &Номенклатура";
	
	Запрос.УстановитьПараметр("Номенклатура", Номенклатура);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	ВыборкаДетальныеЗаписи.Следующий(); 
	

			
	 Возврат   ВыборкаДетальныеЗаписи.ПроцентСкидки;
	//}}КОНСТРУКТОР_ЗАПРОСА_С_ОБРАБОТКОЙ_РЕЗУЛЬТАТА

Конецфункции
//::::::::::::::::::::::::::::::::::::::::::::::::::::