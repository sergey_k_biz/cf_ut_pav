﻿
&НаСервере
Процедура ВыгрузитьНаСервере()
		МассивКаталогов = Новый Массив;
ЗапросГрупп = Новый Запрос("ВЫБРАТЬ
                           |	Номенклатура.Ссылка КАК Группа
                           |ИЗ
                           |	Справочник.Номенклатура КАК Номенклатура
                           |ГДЕ
                           |	Номенклатура.ПометкаУдаления = ЛОЖЬ
                           |	И Номенклатура.ЭтоГруппа = ИСТИНА");
Выборка = ЗапросГрупп.Выполнить().Выбрать();
Пока Выборка.Следующий() Цикл

	ВыгрузитьКартинкиВКаталогНаСервере(Выборка.Группа);
КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура Выгрузить(Команда)
	ВыгрузитьНаСервере();
КонецПроцедуры

#Область ВыгрузкаКартинок



&НаСервере
Процедура СоздатьКаталогНаКлиенте(ПолныйПутьКФайлу) 
	СтрокаКоманды = СокрЛП(ПолныйПутьКФайлу);
Попытка
	СоздатьКаталог(СтрокаКоманды);
исключение
КонецПопытки;
КонецПроцедуры

&НаСервере
Функция СозданиеКаталоговНоменклатуры(Номенклатура,Путь)
	
	МассивКаталогов 		= Новый Массив;
	
	ЗапросГруппНоменклатуры = Новый Запрос("ВЫБРАТЬ
	                                       |	Номенклатура.Родитель
	                                       |ИЗ
	                                       |	Справочник.Номенклатура КАК Номенклатура
	                                       |ГДЕ
	                                       |	Номенклатура.ПометкаУдаления = ЛОЖЬ
	                                       |	И Номенклатура.Ссылка = &Ссылка");
	ЗапросГруппНоменклатуры.УстановитьПараметр("Ссылка",Номенклатура);
	ВыборкаГрупп = ЗапросГруппНоменклатуры.Выполнить().Выгрузить();
//Выполняем рекурсивно и получаем полный путь к файлу;
	Для Каждого Группа из ВыборкаГрупп Цикл
		Если ЗначениеЗаполнено(Группа.Родитель) Тогда	
			
			Путь = "\"+Строка(Группа.Родитель) + ?(Путь = "",Путь,"\" + Путь);
		Иначе
			
			//Путь = ?(Путь = "",Путь,"\" + Путь);
		КонецЕсли;
	
		 СозданиеКаталоговНоменклатуры(Группа.Родитель,Путь);
		
	КонецЦикла;
	
Возврат Путь;

КонецФункции

&НаСервере
Функция СозданиеНоменклатуры(группа) Экспорт
	МассивКаталогов = Новый Массив;
	
	Запрос = Новый Запрос("ВЫБРАТЬ
	                      |	спрНоменклатура.Ссылка КАК Номенклатура,
	                      |	спрНоменклатура.Код,
	                      |	спрНоменклатура.Родитель КАК Группа,
	                      |	ПрисоединенныеФайлы.ХранимыйФайл КАК Хранилище
	                      |ИЗ
	                      |	Справочник.Номенклатура КАК спрНоменклатура
	                      |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПрисоединенныеФайлы КАК ПрисоединенныеФайлы
	                      |		ПО спрНоменклатура.ФайлКартинки = ПрисоединенныеФайлы.ПрисоединенныйФайл,
	                      |	РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаХарактеристика
	                      |ГДЕ
	                      |	НЕ ПрисоединенныеФайлы.ХранимыйФайл ЕСТЬ NULL 
	                      |	И спрНоменклатура.ЭтоГруппа = ЛОЖЬ
	                      |	И спрНоменклатура.Родитель.ПометкаУдаления = ЛОЖЬ
	                      |	И спрНоменклатура.ПометкаУдаления = ЛОЖЬ
	                      |	И спрНоменклатура.Родитель = &Родитель");
	Запрос.УстановитьПараметр("Родитель",Группа);
						  
	
		Результат = запрос.Выполнить().Выгрузить();
	Для Каждого Строка из Результат Цикл
		
		стСтрокаТаблицы = Новый Структура;
		
		Для Каждого ИмяКолонки Из Результат.Колонки Цикл
			
			стСтрокаТаблицы.Вставить(ИмяКолонки.Имя, Строка[ИмяКолонки.Имя]);
			
		КонецЦикла;
		
				Путь = "";
		
		
		ПолныйПутьККаталогу 		= СозданиеКаталоговНоменклатуры(Строка.Номенклатура,Путь);
		ПолныйПутьКФайлу 			= ПолныйПутьККаталогу+"\"+Формат(Строка.Код,"ЧГ=");
		//КаталогВерхнегоУровня 		= СоздатьКаталогВерхнегоУровня(Строка.Номенклатура,Путь);
		//ФайлВВерхнемКаталоге		= КаталогВерхнегоУровня+"\"+Формат(Строка.Код,"ЧГ=");
		
				
		стСтрокаТаблицы.Вставить("ПолныйПутьККаталогу",ПолныйПутьККаталогу);
		стСтрокаТаблицы.Вставить("ПолныйПутьКФайлу",ПолныйПутьКФайлу);
		//стСтрокаТаблицы.Вставить("ФайлВВерхнемКаталоге",ФайлВВерхнемКаталоге);
		//стСтрокаТаблицы.вставить("КаталогВерхнегоУровня",КаталогВерхнегоУровня);
		МассивКаталогов.Добавить(стСтрокаТаблицы);
		
	КонецЦикла;

	Возврат МассивКаталогов;
КонецФункции


&НаСервере
Функция УдалитьКартинки()
МассивКаталогов = Новый Массив;
	Запрос = Новый Запрос("ВЫБРАТЬ
	                      |	спрНоменклатура.Ссылка КАК Номенклатура,
	                      |	спрНоменклатура.Код,
	                      |	ЕСТЬNULL(ТоварыВПути.ЗаказаноОстаток, 0) КАК ОстатокВПути,
	                      |	спрНоменклатура.Родитель КАК Группа,
	                      |	ПрисоединенныеФайлы.ХранимыйФайл КАК Хранилище,
	                      |	ЕСТЬNULL(ТоварыНаСкладахОстатки.ВНаличииОстаток, 0) КАК ОстатокВНаличии,
	                      |	ЕСТЬNULL(ТоварыНаСкладахОстатки.ВРезервеОстаток, 0) КАК ОстатокВРезерве
	                      |ПОМЕСТИТЬ ОбщиеСведения
	                      |ИЗ
	                      |	Справочник.Номенклатура КАК спрНоменклатура
	                      |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПрисоединенныеФайлы КАК ПрисоединенныеФайлы
	                      |		ПО спрНоменклатура.ФайлКартинки = ПрисоединенныеФайлы.ПрисоединенныйФайл
	                      |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрНакопления.ТоварыНаСкладах.Остатки(, ) КАК ТоварыНаСкладахОстатки
	                      |		ПО спрНоменклатура.Ссылка = ТоварыНаСкладахОстатки.Номенклатура
	                      |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрНакопления.ЗаказыПоставщикам.Остатки КАК ТоварыВПути
	                      |		ПО спрНоменклатура.Ссылка = ТоварыВПути.Номенклатура
	                      |ГДЕ
	                      |	НЕ ПрисоединенныеФайлы.ХранимыйФайл ЕСТЬ NULL 
	                      |	И спрНоменклатура.ЭтоГруппа = ЛОЖЬ
	                      |	И НЕ спрНоменклатура.Родитель В
	                      |				(ВЫБРАТЬ
	                      |					ИсключаемыйИзВыгрузкиСегмент.ГруппаНоменклатуры
	                      |				ИЗ
	                      |					РегистрСведений.ИсключаемыйИзВыгрузкиСегмент КАК ИсключаемыйИзВыгрузкиСегмент)
	                      |;
	                      |
	                      |////////////////////////////////////////////////////////////////////////////////
	                      |ВЫБРАТЬ
	                      |	НоменклатураДополнительныеРеквизиты.Ссылка,
	                      |	НоменклатураДополнительныеРеквизиты.Свойство,
	                      |	НоменклатураДополнительныеРеквизиты.Значение
	                      |ПОМЕСТИТЬ СвойстаЗначения
	                      |ИЗ
	                      |	Справочник.Номенклатура.ДополнительныеРеквизиты КАК НоменклатураДополнительныеРеквизиты
	                      |		ЛЕВОЕ СОЕДИНЕНИЕ ПланВидовХарактеристик.ДополнительныеРеквизитыИСведения КАК ДополнительныеРеквизитыИСведения
	                      |		ПО НоменклатураДополнительныеРеквизиты.Свойство = ДополнительныеРеквизитыИСведения.Ссылка
	                      |ГДЕ
	                      |	ДополнительныеРеквизитыИСведения.Наименование ПОДОБНО ""%Тип%Мебели%""
	                      |;
	                      |
	                      |////////////////////////////////////////////////////////////////////////////////
	                      |ВЫБРАТЬ
	                      |	ОбщиеСведения.Номенклатура,
	                      |	ОбщиеСведения.Код КАК Код,
	                      |	ОбщиеСведения.ОстатокВПути,
	                      |	ОбщиеСведения.Группа,
	                      |	ОбщиеСведения.Хранилище,
	                      |	ЕСТЬNULL(СвойстаЗначения.Свойство, """") КАК Свойство,
	                      |	ЕСТЬNULL(СвойстаЗначения.Значение, """") КАК Значение,
	                      |	ОбщиеСведения.ОстатокВНаличии,
	                      |	ОбщиеСведения.ОстатокВРезерве
	                      |ИЗ
	                      |	ОбщиеСведения КАК ОбщиеСведения
	                      |		ЛЕВОЕ СОЕДИНЕНИЕ СвойстаЗначения КАК СвойстаЗначения
	                      |		ПО ОбщиеСведения.Номенклатура = СвойстаЗначения.Ссылка
	                      |
	                      |УПОРЯДОЧИТЬ ПО
	                      |	Код");
						  
	
		Результат = запрос.Выполнить().Выгрузить();
	Для Каждого Строка из Результат Цикл
		
		стСтрокаТаблицы = Новый Структура;
		
		Для Каждого ИмяКолонки Из Результат.Колонки Цикл
			
			стСтрокаТаблицы.Вставить(ИмяКолонки.Имя, Строка[ИмяКолонки.Имя]);
			
		КонецЦикла;
		
				
		Путь = "";
		
		
		ПолныйПутьККаталогу 		= СозданиеКаталоговНоменклатуры(Строка.Номенклатура,Путь);
		ПолныйПутьКФайлу 			= ПолныйПутьККаталогу + "\"+Строка(Формат(Число(Строка.Код),"ЧГ=0"));
		КаталогВерхнегоУровня 		= СоздатьКаталогВерхнегоУровня(Строка.Номенклатура,Путь);
		ФайлВВерхнемКаталоге		= КаталогВерхнегоУровня + "\"+Строка(Формат(Число(Строка.Код),"ЧГ=0"));
		
		стСтрокаТаблицы.Вставить("ПолныйПутьККаталогу",ПолныйПутьККаталогу);
		стСтрокаТаблицы.Вставить("ПолныйПутьКФайлу",ПолныйПутьКФайлу);
		стСтрокаТаблицы.Вставить("ФайлВВерхнемКаталоге",ФайлВВерхнемКаталоге);
		
		МассивКаталогов.Добавить(стСтрокаТаблицы);
		
	КонецЦикла;
	Возврат МассивКаталогов;
КонецФункции

&НаСервере
Процедура УдалитьКартинкиИзКаталогаНаСервере(Группа) Экспорт
	СписокКаталогов 	= СозданиеНоменклатуры(Группа);

	Для Каждого Каталог из СписокКаталогов Цикл
		
		//GoogleDrive  \\Ts01\gd
		попытка
				УдалитьФайлыНаКлиенте(Константы.mgGoogleDrive.Получить() + Каталог.ПолныйПутьККаталогу);
				Если ЗначениеЗаполнено(Каталог.КаталогВерхнегоУровня) Тогда
				УдалитьФайлыНаКлиенте(Константы.mgGoogleDrive.Получить() + Каталог.КаталогВерхнегоУровня);
				КонецЕсли;
				
				Если ЗначениеЗаполнено(Каталог.КаталогТоварыВПути) Тогда
				УдалитьФайлыНаКлиенте(Константы.mgGoogleDrive.Получить() + Каталог.КаталогТоварыВПути);	
				КонецЕсли;
			Исключение
				
				Сообщить(Описаниеошибки());
				
		КонецПопытки;
		
	КонецЦикла;

   КонецПроцедуры


&НаСервере
Функция УдалитьФайлыНаКлиенте(ПолныйПутьКФайлу)
УдалитьФайлы(ПолныйПутьКФайлу,"*.jpg");	
КонецФункции

&НаСервере
Функция СоздатьКаталогВерхнегоУровня(Номенклатура,Путь)
	МассивКаталогов 		= Новый Массив;
	
	ЗапросГруппНоменклатуры = Новый Запрос("ВЫБРАТЬ
	                                       |	Номенклатура.Родитель
	                                       |ИЗ
	                                       |	Справочник.Номенклатура КАК Номенклатура
	                                       |ГДЕ
	                                       |	НЕ Номенклатура.Родитель В
	                                       |				(ВЫБРАТЬ
	                                       |					Сегмент.ГруппаНоменклатуры
	                                       |				ИЗ
	                                       |					РегистрСведений.ИсключаемыйИзВыгрузкиСегмент КАК Сегмент)
	                                       |	И Номенклатура.ПометкаУдаления = ЛОЖЬ
	                                       |	И Номенклатура.Ссылка = &Ссылка");
	ЗапросГруппНоменклатуры.УстановитьПараметр("Ссылка",Номенклатура);
	ВыборкаГрупп = ЗапросГруппНоменклатуры.Выполнить().Выгрузить();
//Выполняем рекурсивно и получаем полный путь к файлу;
	Для Каждого Группа из ВыборкаГрупп Цикл
		Если ЗначениеЗаполнено(Группа.Родитель) Тогда	
			
			Путь = "\"+Строка(Группа.Родитель);
		Иначе
			
			//Путь = ?(Путь = "",Путь,"\" + Путь);
		КонецЕсли;
	
		 СоздатьКаталогВерхнегоУровня(Группа.Родитель,Путь);
		
	КонецЦикла;
	Возврат Путь;

КонецФункции


&НаСервере
Процедура ВыгрузитьКартинкиВКаталогНаСервере(Группа) Экспорт
	СписокКаталогов 	= СозданиеНоменклатуры(Группа);
	
	Для Каждого Каталог из СписокКаталогов Цикл
		
		//GoogleDrive  \\Ts01\gd
		попытка
				СоздатьКаталогНаКлиенте(Объект.ПутьККартинкам + Каталог.ПолныйПутьККаталогу);
			Исключение
		КонецПопытки;
		
		Картинка = Каталог.Хранилище.Получить();
		
		Попытка
			//GoogleDrive  \\Ts01\gd
		Картинка.Записать(Объект.ПутьККартинкам + Каталог.ПолныйПутьКФайлу 		+ ".jpg");
		//Картинка.Записать(Объект.ПутьККартинкам + Каталог.ФайлВВерхнемКаталоге 	+ ".jpg");
		исключение
			Сообщить(ОписаниеОшибки());
		КонецПопытки;
	   		
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура ПутьККартинкамНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
		
	СтандартнаяОбработка = ЛОЖЬ;
	Диалог = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.ВыборКаталога);
	Диалог.Заголовок = "Выберите каталог";
	Диалог.ПолноеИмяФайла = "";
	//Диалог.Фильтр = "Все файлы (*.*)|*.*|"; 
	Если НЕ Диалог.Выбрать() Тогда Возврат; КонецЕсли;
	Объект.ПутьККартинкам = Диалог.Каталог;
	

КонецПроцедуры


#КонецОбласти
