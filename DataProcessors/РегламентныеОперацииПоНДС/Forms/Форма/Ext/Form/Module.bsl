﻿////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ВРег(ИмяСобытия) = ВРег("Запись_НастройкаУчетаНДС") Тогда
		ЗаполнитьДанныеНаСтраницеНастройкаУчетаНДС();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если НЕ ЗначениеЗаполнено(Объект.Период) Тогда
		Объект.Период = ТекущаяДата();
	КонецЕсли; 
	
	КварталСтрока = УчетНДСКлиент.ДатаКакКварталПредставление(Объект.Период);
	
	// Восстановление значений реквизитов формы при повторном открытии
	ОбновитьДанныеФормыПоПериодуИОрганизации(Истина, Истина);
	
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьНовыйИдентификаторОтбора(ЖурналКорректировкиНДС.Отбор.Элементы[0],              "ЖурналКорректировкиНДС");
	УстановитьНовыйИдентификаторОтбора(СписокПодтверждениеНулевойСтавкиНДС.Отбор.Элементы[0], "СписокПодтверждениеНулевойСтавкиНДС");
	УстановитьНовыйИдентификаторОтбора(СписокСчетФактураВыданныйАванс.Отбор.Элементы[0],      "СписокСчетФактураВыданныйАванс");
	УстановитьНовыйИдентификаторОтбора(СписокСчетФактураПолученныйАванс.Отбор.Элементы[0],    "СписокСчетФактураПолученныйАванс");
	
	// Получим значения констант
	ВалютаРеглУчета = Константы.ВалютаРегламентированногоУчета.Получить();
	СтавкаНДС18_118 = Перечисления.СтавкиНДС.НДС18_118;
	СтавкаНДС10_110 = Перечисления.СтавкиНДС.НДС10_110;
	
	Элементы.ВыданныеАвансыСуммаАвансаРегл.Заголовок = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = 'Сумма (%1)'"), ВалютаРеглУчета);
	
	НеиспользуемыеСчетаФактуры.Параметры.УстановитьЗначениеПараметра("НеиспользуемыеСчетаФактуры", Новый СписокЗначений);
	
	// Заполним список организаций, для которых введена настройка учета НДС
	ЗаполнитьСписокОрганизацийВедущихУчетНДС();
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ШАПКИ ФОРМЫ

&НаКлиенте
Процедура КварталСтрокаНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	РезультатВыбора = УчетНДСКлиент.НачалоВыбораИзСпискаПредставленияПериодаРегистрации(Элемент, СтандартнаяОбработка, Объект.Период, ЭтаФорма);
	Если РезультатВыбора <> Неопределено Тогда
		КварталСтрока = РезультатВыбора;
		ПриИзмененииОтчетногоПериода();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура КварталСтрокаРегулирование(Элемент, Направление, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Объект.Период = ДобавитьМесяц(Объект.Период, 3*Направление);
	КварталСтрока = УчетНДСКлиент.ДатаКакКварталПредставление(Объект.Период);
	
	ПриИзмененииОтчетногоПериода();
	
КонецПроцедуры

&НаКлиенте
Процедура КварталСтрокаОчистка(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Объект.Период = ТекущаяДата();
	КварталСтрока = УчетНДСКлиент.ДатаКакКварталПредставление(Объект.Период);
	
	ПриИзмененииОтчетногоПериода();
	
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	ОбновитьДанныеФормыПоПериодуИОрганизации(Ложь, Истина);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ТАБЛИЦЫ ФОРМЫ СПИСОК СЧЕТОВ-ФАКТУР

&НаКлиенте
Процедура СписокСчетовФактурСтавкаНДСПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.СписокСчетовФактур.ТекущиеДанные;
	
	СтруктураПересчетаСуммы = ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ();
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", СтруктураПересчетаСуммы);
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, Неопределено);
	
КонецПроцедуры

&НаКлиенте
Процедура СписокСчетовФактурСуммаАвансаПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.СписокСчетовФактур.ТекущиеДанные;
	
	СтруктураПересчетаСуммы = ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ();
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", СтруктураПересчетаСуммы);
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, Неопределено);
	
	ТекущаяСтрока.ВалютнаяСумма = ТекущаяСтрока.Сумма;
	
	// Проверим валюту, если она отличается от валюты регл. учета, то пересчитаем суммы
	ВалютаДанных = ТекущаяСтрока.ВалютаДокумента;
	Если ЗначениеЗаполнено(ВалютаДанных)
		И ВалютаДанных <> ВалютаРеглУчета Тогда
			
		СтруктураКурса = РаботаСКурсамиВалют.ПолучитьКурсВалюты(ВалютаДанных, ТекущаяСтрока.ДатаДокументаОснования);
		
		ТекущаяСтрока.ВалютнаяСумма = РаботаСКурсамиВалютКлиентСервер.ПересчитатьИзВалютыВВалюту(
														ТекущаяСтрока.Сумма,
														ВалютаРеглУчета, ВалютаДанных,
														1, СтруктураКурса.Курс,
														1, СтруктураКурса.Кратность);
		
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура СписокСчетовФактурВалютнаяСуммаПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.СписокСчетовФактур.ТекущиеДанные;
	
	ТекущаяСтрока.Сумма = ТекущаяСтрока.ВалютнаяСумма;
	
	// Проверим валюту, если она отличается от валюты регл. учета, то пересчитаем суммы
	ВалютаДанных = ТекущаяСтрока.ВалютаДокумента;
	Если ЗначениеЗаполнено(ВалютаДанных)
		И ВалютаДанных <> ВалютаРеглУчета Тогда
			
		СтруктураКурса = РаботаСКурсамиВалют.ПолучитьКурсВалюты(ВалютаДанных, ТекущаяСтрока.ДатаДокументаОснования);
		
		ТекущаяСтрока.Сумма = РаботаСКурсамиВалютКлиентСервер.ПересчитатьИзВалютыВВалюту(
														ТекущаяСтрока.ВалютнаяСумма,
														ВалютаДанных, ВалютаРеглУчета,
														СтруктураКурса.Курс, 1,
														СтруктураКурса.Кратность, 1);
			
		
	КонецЕсли; 

	СтруктураПересчетаСуммы = ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ();
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", СтруктураПересчетаСуммы);
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, Неопределено);
	
КонецПроцедуры

&НаКлиенте
Процедура СписокСчетовФактурВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ДанныеСтроки = Элементы.СписокСчетовФактур.ДанныеСтроки(ВыбраннаяСтрока);
	
	Если Поле = Элементы.СписокСчетовФактурСчетФактура 
		И ЗначениеЗаполнено(ДанныеСтроки.СчетФактура) Тогда
		
		СтандартнаяОбработка = Ложь;
		ОткрытьФорму("Документ.СчетФактураВыданныйАванс.ФормаОбъекта", Новый Структура("Ключ", ДанныеСтроки.СчетФактура));
	КонецЕсли; 
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ТАБЛИЦЫ ФОРМЫ ВЫДАННЫЕ АВАНСЫ

&НаКлиенте
Процедура ВыданныеАвансыВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ВвестиСчетФактуруПолученныйАванс();
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура ОтчетКонтрольНДС(Команда)
	
	ПериодОтчета = Новый СтандартныйПериод;
	ПериодОтчета.ДатаНачала    = НачалоКвартала(Объект.Период);
	ПериодОтчета.ДатаОкончания = КонецКвартала(Объект.Период);
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("Организация", Объект.Организация);
	ПараметрыФормы.Вставить("Период", ПериодОтчета);
	ПараметрыФормы.Вставить("КлючНазначенияИспользования", "Основной");
	ПараметрыФормы.Вставить("КлючВарианта", "Основной");
	ПараметрыФормы.Вставить("СформироватьПриОткрытии", Истина);
	
	ОткрытьФорму("Отчет.КонтрольНДС.ФормаОбъекта", ПараметрыФормы);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьСписокСчетовФактур(Команда)
	
	Если ЗаполненаОрганизация() Тогда
		ЗаполнитьСписокСчетовФактурНаСервере();
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура ЗарегистрироватьСчетаФактуры(Команда)
	
	Если НЕ ПроверитьЗаполнение() Тогда
		Возврат;
	КонецЕсли;
	
	ЗарегистрироватьСчетаФактурыНаСервере();
	
	ОповеститьОбИзменении(Тип("ДокументСсылка.СчетФактураВыданныйАванс"));
	
	ПоказатьОповещениеПользователя("Завершена регистрация с/ф на аванс");
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтавкуНДС18_118(Команда)
	
	УстановитьСтавкуНДС("18%", СтавкаНДС18_118);
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтавкуНДС10_110(Команда)
	
	УстановитьСтавкуНДС("10%", СтавкаНДС10_110);
	
КонецПроцедуры

&НаКлиенте
Процедура ИзменитьНастройкуУчетаНДС(Команда)
	
	Если НЕ ЗаполненаОрганизация() Тогда
		Возврат;
	КонецЕсли; 
	
	НастройкаУчета = УчетНДС.ПолучитьНастройкиУчетаНДС(Объект.Период, Объект.Организация);
	
	Если НастройкаУчета <> Неопределено Тогда
		ДанныеКлючаЗаписи = Новый  Структура("Организация,Период", Объект.Организация, НастройкаУчета.ПериодНастройки);
		МассивПараметровКонструктора = Новый Массив();
		МассивПараметровКонструктора.Добавить(ДанныеКлючаЗаписи);
		
		КлючЗаписи = Новый("РегистрСведенийКлючЗаписи.НастройкаУчетаНДС", МассивПараметровКонструктора);	
		
		ОткрытьФорму("РегистрСведений.НастройкаУчетаНДС.ФормаЗаписи", Новый Структура("Ключ", КлючЗаписи));
	Иначе
		ОткрытьФорму("РегистрСведений.НастройкаУчетаНДС.ФормаЗаписи", Новый Структура("ЗначенияЗаполнения", Новый Структура("Период,Организация", Объект.Период, Объект.Организация)));
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура ВвестиСчетФактуруПолученныйАвансВыполнить(Команда)
	
	ВвестиСчетФактуруПолученныйАванс();
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьПлатежныйДокументАвансВыполнить(Команда)
	
	ОткрытьПлатежныйДокументАванс();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьВыданныеАвансыВыполнить(Команда)
	
	ЗаполнитьВыданныеАвансы();
	
КонецПроцедуры

&НаКлиенте
Процедура СформироватьРегламентныеДокументы(Команда)
	
	Если НЕ ЗаполненаОрганизация() Тогда
		Возврат;
	КонецЕсли; 
	
	Если УчетНДС.СформироватьДвиженияЗаОтчетныйПериод(Объект.Организация, Объект.Период) Тогда
		ПоказатьОповещениеПользователя(НСтр("ru = 'Формирование движений'"),, НСтр("ru = 'Регламентные документы сформированы'"), БиблиотекаКартинок.Информация32);
		ЗаполнитьДанныеНаСтраницеРегламентныеДокументы();
		
		ОповеститьОбИзменении(Тип("ДокументСсылка.ФормированиеЗаписейКнигиПокупокИКнигиПродаж"));		
		ОповеститьОбИзменении(Тип("ДокументСсылка.РаспределениеНДС"));		
	Иначе
		Предупреждение(НСтр("ru = 'Не удалось сформировать регламентные документы.'"),, НСтр("ru = 'Формирование движений'"));
	КонецЕсли; 
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// При изменении реквизитов

&НаСервере
Процедура ПриИзмененииОтчетногоПериода()

	ОбновитьДанныеФормыПоПериодуИОрганизации(Истина, Ложь);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Получение данных формы

&НаСервере
Процедура ЗаполнитьВыданныеАвансы()

	Запрос = Новый Запрос;
	ТекстЗапроса = "ВЫБРАТЬ
	               |	ВыпискиПоРасчетномуСчету.Документ
				   |ПОМЕСТИТЬ ВыпискиПоРасчетномуСчету
				   |ИЗ
	               |	РегистрНакопления.ДенежныеСредстваКСписаниюБезналичные.ОстаткиИОбороты(&НачалоПериода, &КонецПериода, День, , Организация = &Организация) КАК ВыпискиПоРасчетномуСчету
	               |ГДЕ
	               |	ВыпискиПоРасчетномуСчету.СуммаКонечныйОстаток = 0
				   |
				   |////////////////////////////////////////////////////////////////////////////////
				   |;
				   |ВЫБРАТЬ
	               |	Расчеты.РасчетныйДокумент КАК ДокументОснование,
	               |	ВЫБОР
	               |		КОГДА Расчеты.РасчетныйДокумент ССЫЛКА Документ.РасходныйКассовыйОрдер
	               |			ТОГДА ВЫРАЗИТЬ(Расчеты.РасчетныйДокумент КАК Документ.РасходныйКассовыйОрдер).Валюта
	               |		КОГДА Расчеты.РасчетныйДокумент ССЫЛКА Документ.СписаниеБезналичныхДенежныхСредств
	               |			ТОГДА ВЫРАЗИТЬ(Расчеты.РасчетныйДокумент КАК Документ.СписаниеБезналичныхДенежныхСредств).Валюта
	               |		ИНАЧЕ Расчеты.Валюта
	               |	КОНЕЦ КАК ВалютаДокумента,
	               |	СУММА(ВЫБОР
	               |			КОГДА Расчеты.РасчетныйДокумент ССЫЛКА Документ.РасходныйКассовыйОрдер
	               |					И ВЫРАЗИТЬ(Расчеты.РасчетныйДокумент КАК Документ.РасходныйКассовыйОрдер).Валюта = Константы.ВалютаРегламентированногоУчета
	               |				ТОГДА Расчеты.ПредоплатаРеглПриход
	               |			КОГДА Расчеты.РасчетныйДокумент ССЫЛКА Документ.СписаниеБезналичныхДенежныхСредств
	               |					И ВЫРАЗИТЬ(Расчеты.РасчетныйДокумент КАК Документ.СписаниеБезналичныхДенежныхСредств).Валюта = Константы.ВалютаРегламентированногоУчета
	               |				ТОГДА Расчеты.ПредоплатаРеглПриход
	               |			ИНАЧЕ Расчеты.ПредоплатаПриход
	               |		КОНЕЦ) КАК ВалютнаяСумма,
	               |	СУММА(Расчеты.ПредоплатаРеглПриход) КАК СуммаАвансаРегл,
	               |	СУММА(СчетФактураПолученныйАванс.Сумма) КАК СуммаАванса,
	               |	АналитикаУчета.Контрагент КАК Контрагент,
	               |	Расчеты.РасчетныйДокумент.Дата КАК Дата,
	               |	Расчеты.РасчетныйДокумент.Номер КАК Номер,
	               |	СчетФактураПолученныйАванс.НомерВходящегоДокумента КАК НомерВходящегоДокумента,
	               |	СчетФактураПолученныйАванс.ДатаВходящегоДокумента КАК ДатаВходящегоДокумента,
	               |	СчетФактураПолученныйАванс.Ссылка КАК СчетФактура
	               |ИЗ
	               |	РегистрНакопления.РасчетыСПоставщикамиПоДокументам.Обороты(&НачалоПериода, &КонецПериода, Запись, ) КАК Расчеты
	               |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.АналитикаУчетаПоПартнерам КАК АналитикаУчета
	               |		ПО Расчеты.АналитикаУчетаПоПартнерам = АналитикаУчета.КлючАналитики
	               |		ЛЕВОЕ СОЕДИНЕНИЕ Документ.СчетФактураПолученныйАванс КАК СчетФактураПолученныйАванс
	               |		ПО Расчеты.РасчетныйДокумент = СчетФактураПолученныйАванс.ДокументОснование
	               |			И (АналитикаУчета.Контрагент = СчетФактураПолученныйАванс.Контрагент)
	               |			И ((НЕ СчетФактураПолученныйАванс.ПометкаУдаления))
	               |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Константы КАК Константы
	               |		ПО (ИСТИНА)
	               |		ЛЕВОЕ СОЕДИНЕНИЕ ВыпискиПоРасчетномуСчету КАК ВыпискиПоРасчетномуСчету
	               |		ПО ВыпискиПоРасчетномуСчету.Документ = Расчеты.РасчетныйДокумент
	               |ГДЕ
	               |	АналитикаУчета.Организация = &Организация
	               |	И Расчеты.ПредоплатаПриход > 0
	               |	И (СчетФактураПолученныйАванс.Ссылка ЕСТЬ NULL 
	               |			ИЛИ (НЕ СчетФактураПолученныйАванс.Проведен))
				   |	И (НЕ ВыпискиПоРасчетномуСчету.Документ ЕСТЬ NULL
				   |			ИЛИ НЕ Расчеты.РасчетныйДокумент ССЫЛКА Документ.СписаниеБезналичныхДенежныхСредств)
	               |	И (ЛОЖЬ
				   |		//УсловиеТипРасчетногоДокумента//
				   |		)
	               |	И (ЛОЖЬ
				   |		//УсловиеТипРегистратора//
				   |		)
	               |СГРУППИРОВАТЬ ПО
	               |	Расчеты.РасчетныйДокумент,
	               |	АналитикаУчета.Контрагент,
	               |	Расчеты.РасчетныйДокумент.Дата,
	               |	Расчеты.РасчетныйДокумент.Номер,
	               |	СчетФактураПолученныйАванс.НомерВходящегоДокумента,
	               |	СчетФактураПолученныйАванс.ДатаВходящегоДокумента,
	               |	СчетФактураПолученныйАванс.Ссылка,
	               |	ВЫБОР
	               |		КОГДА Расчеты.РасчетныйДокумент ССЫЛКА Документ.РасходныйКассовыйОрдер
	               |			ТОГДА ВЫРАЗИТЬ(Расчеты.РасчетныйДокумент КАК Документ.РасходныйКассовыйОрдер).Валюта
	               |		КОГДА Расчеты.РасчетныйДокумент ССЫЛКА Документ.СписаниеБезналичныхДенежныхСредств
	               |			ТОГДА ВЫРАЗИТЬ(Расчеты.РасчетныйДокумент КАК Документ.СписаниеБезналичныхДенежныхСредств).Валюта
	               |		ИНАЧЕ Расчеты.Валюта
	               |	КОНЕЦ
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	Дата,
	               |	Номер,
	               |	ДокументОснование";
				   
	// Могут быть авансы по документам для которых не вводится счет-фактура
	// Обычно такая ситуация возникает если не выполнены отложенные движения по расчетам с партнерами
	УсловиеТипРасчетногоДокумента = "";
	УсловиеТипРегистратора = "";
	ТипыДокументовОснований = Метаданные.Документы.СчетФактураПолученныйАванс.Реквизиты.ДокументОснование.Тип.Типы();
	Для Каждого ТипДокОснования Из ТипыДокументовОснований Цикл
		МетаДок = Метаданные.НайтиПоТипу(ТипДокОснования);
		УсловиеТипРасчетногоДокумента = УсловиеТипРасчетногоДокумента + "
				|		ИЛИ ТИПЗНАЧЕНИЯ(Расчеты.РасчетныйДокумент) = Тип(Документ." + МетаДок.Имя + ")";
				
		УсловиеТипРегистратора = УсловиеТипРегистратора + "
				|		ИЛИ ТИПЗНАЧЕНИЯ(Расчеты.Регистратор) = Тип(Документ." + МетаДок.Имя + ")";
	КонецЦикла;
	
	ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "//УсловиеТипРасчетногоДокумента//", УсловиеТипРасчетногоДокумента);
	ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "//УсловиеТипРегистратора//",        УсловиеТипРегистратора);
	
	Запрос.Текст = ТекстЗапроса;
	Запрос.УстановитьПараметр("Организация",   Объект.Организация);
	Запрос.УстановитьПараметр("НачалоПериода", НачалоКвартала(Объект.Период));
	Запрос.УстановитьПараметр("КонецПериода",  КонецКвартала(Объект.Период));
	
	Объект.ВыданныеАвансы.Загрузить(Запрос.Выполнить().Выгрузить());
	
КонецПроцедуры

&НаСервере
Процедура ПолучитьДанныеФормы()

	ЗаполнитьВыданныеАвансы();
	
КонецПроцедуры
 
&НаСервере
Процедура ЗаполнитьСписокОрганизацийВедущихУчетНДС()
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
	|	НастройкаУчетаНДС.Организация,
	|	НастройкаУчетаНДС.Организация.Наименование
	|ИЗ
	|	РегистрСведений.НастройкаУчетаНДС КАК НастройкаУчетаНДС
	|ГДЕ
	|	НЕ НастройкаУчетаНДС.Организация.Предопределенный
	|
	|УПОРЯДОЧИТЬ ПО
	|	НастройкаУчетаНДС.Организация.Наименование";
	
	МассивОрганизаций = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Организация");
	
	Элементы.Организация.СписокВыбора.ЗагрузитьЗначения(МассивОрганизаций);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Настройка учета НДС

&НаСервере
Процедура ЗаполнитьДанныеНаСтраницеРегламентныеДокументы()

	ДокументФормированиеЗаписейКнигиПокупокИКнигиПродаж = УчетНДС.ПолучитьСсылкуНаДокументФормированияДвижений(Объект.Организация, Объект.Период);
	ДокументРаспределениеНДС = УчетНДС.ПолучитьСсылкуНаДокументРаспределениеНДС(Объект.Организация, Объект.Период);
	
	Элементы.ДокументФормированиеЗаписейКнигиПокупокИКнигиПродаж.Заголовок = "";
	Элементы.ДокументРаспределениеНДС.Заголовок = "";
	Если ДокументФормированиеЗаписейКнигиПокупокИКнигиПродаж.Пустая() И ДокументРаспределениеНДС.Пустая() Тогда
		Элементы.ДокументФормированиеЗаписейКнигиПокупокИКнигиПродаж.Заголовок = НСтр("ru = '<Документы не введены>'") ;
	КонецЕсли; 
	
	Если НЕ ДокументФормированиеЗаписейКнигиПокупокИКнигиПродаж.Пустая() Тогда
		Элементы.ДокументФормированиеЗаписейКнигиПокупокИКнигиПродаж.Заголовок = НСтр("ru = 'Открыть документ ""Формирование записей книги покупок и книги продаж""'") ;
	КонецЕсли; 
	
	Если НЕ ДокументРаспределениеНДС.Пустая() Тогда
		Элементы.ДокументРаспределениеНДС.Заголовок = НСтр("ru = 'Открыть документ ""Распределение НДС""'");
	КонецЕсли; 
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДанныеНаСтраницеНастройкаУчетаНДС()

	НастройкаУчета = УчетНДС.ПолучитьНастройкиУчетаНДС(Объект.Период, Объект.Организация);
	
	Если НастройкаУчета = Неопределено Тогда
		Элементы.СведенияОНастройках.ТекущаяСтраница = Элементы.НастройкиНеЗаданы;
		
		Если НЕ ЗначениеЗаполнено(Объект.Организация) Тогда
			ЗаголовокЭлемента = НСтр("ru = 'Не выбрана организация");
		Иначе
			ЗаголовокЭлемента = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
										НСтр("ru = 'По организации %1 не ведется учет НДС на %2'"),
										Объект.Организация,
										КварталСтрока);
		КонецЕсли; 
		
		Элементы.ДекорацияНастройкиНеЗаданы.Заголовок = ЗаголовокЭлемента;
		Возврат;
	КонецЕсли;
	              
	// Информация об установленных настройках учета
	Элементы.СведенияОНастройках.ТекущаяСтраница = Элементы.НастройкиЗаданы;
	СистемаНалогообложения                = ?(НастройкаУчета.СистемаНалогообложения = Неопределено, "<не установлена>", Строка(НастройкаУчета.СистемаНалогообложения));
	ФормироватьДвиженияАвтоматически      = НастройкаУчета.ФормироватьДвиженияАвтоматически;
	ПорядокРегистрацииСчетовФактурНаАванс = НастройкаУчета.ПорядокРегистрацииСчетовФактурНаАванс;
	УстановленыОтборы                     = НастройкаУчета.УстановленыОтборы;
	
	// Информация о формировании движений
	Если НастройкаУчета.ФормироватьДвиженияАвтоматически Тогда
		РасписаниеРегламентногоЗадания = РегистрыСведений.НастройкаУчетаНДС.РасписаниеФормированияДвижений(Объект.Организация);
		ПредставлениеРасписания = Строка(РасписаниеРегламентногоЗадания);
	
		ЗаголовокНадписи = НСтр("ru = 'Движения формируются автоматически: %1'");
		ЗаголовокНадписи = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ЗаголовокНадписи, ПредставлениеРасписания);
	Иначе
		ЗаголовокНадписи = НСтр("ru = 'Движения формируются вручную'");
	КонецЕсли; 
	
	Элементы.ИнформацияОФормированииДвижений.Заголовок = ЗаголовокНадписи;

	// Установка видимости полученных авансов
	Если ПорядокРегистрацииСчетовФактурНаАванс = Перечисления.ПорядокРегистрацииСчетовФактурНаАванс.НеРегистрироватьСчетаФактурыНаАвансы Тогда
		Если Элементы.СтраницаАвансыПолученные.Видимость Тогда
			Элементы.СтраницаАвансыПолученные.Видимость = Ложь;
		КонецЕсли;
	ИначеЕсли НЕ Элементы.СтраницаАвансыПолученные.Видимость Тогда
		Элементы.СтраницаАвансыПолученные.Видимость = Истина;
	КонецЕсли; 
		
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Авансы полученные

&НаСервере
Процедура ЗаполнитьСписокСчетовФактурНаСервере()
	
	Объект.СписокСчетовФактур.Очистить();
	
	// Получим список счетов-фактур
	ОбработкаОбъект = РеквизитФормыВЗначение("Объект");
	ОбработкаОбъект.ЗаполнитьСписокСчетовФактур();
	
	ЗначениеВРеквизитФормы(ОбработкаОбъект, "Объект");
	
	НеиспользуемыеСчетаФактуры.Параметры.УстановитьЗначениеПараметра("НеиспользуемыеСчетаФактуры", Объект.НеиспользуемыеСчетаФактуры);
	
КонецПроцедуры

&НаСервере
Процедура ЗарегистрироватьСчетаФактурыНаСервере()

	ОбработкаОбъект = РеквизитФормыВЗначение("Объект");
	ОбработкаОбъект.ЗарегистрироватьСчетаФактуры();
	
	ЗначениеВРеквизитФормы(ОбработкаОбъект, "Объект");
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтавкуНДС(ТекстСтавкаНДС, СтавкаНДС)
	
	Если Элементы.СписокСчетовФактур.ВыделенныеСтроки.Количество() = 0 Тогда
		// Пользователь не выбрал строки
		Возврат;
	КонецЕсли; 
	
	ТекстВопроса = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru='В выбранных строках будет установлена ставка НДС ""%1"". Продолжить?'"),
		ТекстСтавкаНДС);
		
	Если Вопрос(ТекстВопроса,РежимДиалогаВопрос.ДаНет) <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	СтруктураПересчетаСуммы = ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ();
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", СтруктураПересчетаСуммы);
	
	ТекПроцентНДС = ЦенообразованиеКлиентСервер.ПолучитьСтавкуНДСЧислом(СтавкаНДС);
	Для каждого ИндексСтроки Из Элементы.СписокСчетовФактур.ВыделенныеСтроки Цикл
		ДанныеСтроки = Элементы.СписокСчетовФактур.ДанныеСтроки(ИндексСтроки);
		Если ДанныеСтроки.СтавкаНДС <> СтавкаНДС Тогда
			ДанныеСтроки.СтавкаНДС = СтавкаНДС;
			
			ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ДанныеСтроки, СтруктураДействий, Неопределено);
		КонецЕсли; 
	КонецЦикла; 
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Авансы выданные

&НаСервере
Процедура ОбновитьДанныеСтрокиВыданныхАвансов(ДокументОснование, Контрагент)
	
	РеквизитыСчетФактуры = Новый Структура;
	РеквизитыСчетФактуры.Вставить("НомерВходящегоДокумента", "");
	РеквизитыСчетФактуры.Вставить("ДатаВходящегоДокумента", '00010101');
	РеквизитыСчетФактуры.Вставить("СуммаАванса", 0);
	РеквизитыСчетФактуры.Вставить("СчетФактура", Неопределено);
	
	// Получим данные счета-фактуры
	Запрос = Новый Запрос;
	
	ТекстЗапроса = "ВЫБРАТЬ РАЗРЕШЕННЫЕ
	               |	СчетФактураПолученныйАванс.Ссылка КАК СчетФактура,
	               |	СчетФактураПолученныйАванс.НомерВходящегоДокумента,
	               |	СчетФактураПолученныйАванс.ДатаВходящегоДокумента,
	               |	СчетФактураПолученныйАванс.Сумма КАК СуммаАванса
	               |ИЗ
	               |	Документ.СчетФактураПолученныйАванс КАК СчетФактураПолученныйАванс
	               |ГДЕ
	               |	СчетФактураПолученныйАванс.ДокументОснование = &ДокументОснование
	               |	И СчетФактураПолученныйАванс.Контрагент = &Контрагент
	               |	И НЕ СчетФактураПолученныйАванс.ПометкаУдаления";
	 
	Запрос.Текст = ТекстЗапроса;
	Запрос.УстановитьПараметр("ДокументОснование", ДокументОснование);
	Запрос.УстановитьПараметр("Контрагент",        Контрагент);
	Результат = Запрос.Выполнить();
	Выборка = Результат.Выбрать();
	
	Если Выборка.Следующий() Тогда
		ЗаполнитьЗначенияСвойств(РеквизитыСчетФактуры, Выборка);
	КонецЕсли; 
	
	// Аванс может быть в разных валютах, в этом случае будет несколько строк
	СписокАвансов = Объект.ВыданныеАвансы.НайтиСтроки(Новый Структура("ДокументОснование,Контрагент", ДокументОснование,Контрагент));
	Для каждого ЭлКоллекции Из СписокАвансов Цикл
		ЗаполнитьЗначенияСвойств(ЭлКоллекции, РеквизитыСчетФактуры);
	КонецЦикла; 
	
КонецПроцедуры

&НаКлиенте
Процедура ВвестиСчетФактуруПолученныйАванс()

	ТекущиеДанные = Элементы.ВыданныеАвансы.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если ТекущиеДанные.СчетФактура.Пустая() Тогда
		СуммаАванса = ТекущиеДанные.СуммаАвансаРегл;
		Если СуммаАванса = 0 И ТекущиеДанные.ВалютаДокумента = ВалютаРеглУчета Тогда
			СуммаАванса = ТекущиеДанные.ВалютнаяСумма;
		КонецЕсли; 
		
		СтруктураОснования = Новый Структура;
		СтруктураОснования.Вставить("Организация",       Объект.Организация);
		СтруктураОснования.Вставить("ДокументОснование", ТекущиеДанные.ДокументОснование);
		СтруктураОснования.Вставить("Контрагент",        ТекущиеДанные.Контрагент);
		СтруктураОснования.Вставить("Валюта",            ВалютаРеглУчета);
		СтруктураОснования.Вставить("Дата",              ТекущиеДанные.Дата);
		СтруктураОснования.Вставить("Сумма",             СуммаАванса);
		
		ПараметрыФормы = Новый Структура("Основание", СтруктураОснования);
	Иначе
		ПараметрыФормы = Новый Структура("Ключ", ТекущиеДанные.СчетФактура);
	КонецЕсли; 
	
	ОткрытьФормуМодально("Документ.СчетФактураПолученныйАванс.ФормаОбъекта", ПараметрыФормы);
	
	ОбновитьДанныеСтрокиВыданныхАвансов(ТекущиеДанные.ДокументОснование, ТекущиеДанные.Контрагент);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьПлатежныйДокументАванс()

	ТекущиеДанные = Элементы.ВыданныеАвансы.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ИмяТаблицы = ОбщегоНазначения.ИмяТаблицыПоСсылке(ТекущиеДанные.ДокументОснование);
	
	ОткрытьФорму(ИмяТаблицы + ".ФормаОбъекта", Новый Структура("Ключ", ТекущиеДанные.ДокументОснование))
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Формирование движений

&НаКлиенте
Процедура ДокументФормированиеЗаписейКнигиПокупокИКнигиПродажНажатие(Элемент)
	
	Если НЕ ДокументФормированиеЗаписейКнигиПокупокИКнигиПродаж.Пустая() Тогда
		ОткрытьФорму("Документ.ФормированиеЗаписейКнигиПокупокИКнигиПродаж.ФормаОбъекта", Новый Структура("Ключ", ДокументФормированиеЗаписейКнигиПокупокИКнигиПродаж));
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура ДокументРаспределениеНДСНажатие(Элемент)
	
	Если НЕ ДокументРаспределениеНДС.Пустая() Тогда
		ОткрытьФорму("Документ.РаспределениеНДС.ФормаОбъекта", Новый Структура("Ключ", ДокументРаспределениеНДС));
	КонецЕсли; 
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Прочее

&НаСервере
Процедура УстановитьНовыйИдентификаторОтбора(ЭлементОтбора, ИмяСписка)

	НовыйУИ = Новый УникальныйИдентификатор;
	ЭлементОтбора.ИдентификаторПользовательскойНастройки = НовыйУИ;
	
	ИдентификаторыОтбораПоОрганизации.Добавить(ИмяСписка, НовыйУИ);
	
КонецПроцедуры

&НаСервере
Процедура УстановитьПериодВСписках()

	Для каждого ЭлКоллекции Из ИдентификаторыОтбораПоОрганизации Цикл
		ЭлементСписок = Элементы[ЭлКоллекции.Значение];
		ЭлементСписок.Период.ДатаНачала    = НачалоКвартала(Объект.Период);
		ЭлементСписок.Период.ДатаОкончания = КонецКвартала(Объект.Период);
		
		ЭлементСписок.Обновить();
	КонецЦикла; 
	
КонецПроцедуры

&НаСервере
Процедура УстановитьОтборПоОрганизацииВСписках()

	Для каждого ЭлКоллекции Из ИдентификаторыОтбораПоОрганизации Цикл
		ЭлементыОтбора = ЭтаФорма[ЭлКоллекции.Значение].Отбор.Элементы;
		ИмяСписка = ЭлКоллекции.Значение;
	
		Для каждого ЭлОтбор Из ЭлементыОтбора Цикл
			Если ЭлОтбор.ИдентификаторПользовательскойНастройки = ЭлКоллекции.Представление Тогда
				ЭлОтбор.ПравоеЗначение = Объект.Организация;
				Прервать;
			КонецЕсли; 
		КонецЦикла; 
	КонецЦикла; 
	
КонецПроцедуры

&НаКлиенте
Функция ЗаполненаОрганизация()
	
	Если Объект.Организация.Пустая() Тогда
		ТекстОшибки = НСтр("ru='Необходимо выбрать организацию'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			ТекстОшибки,
			Неопределено, // ОбъектИлиСсылка
			"Организация",
			"Объект", // ПутьКДанным
		);
		Возврат Ложь;
	КонецЕсли;
	
	Возврат Истина;
	
КонецФункции

&НаСервере
Процедура ОбновитьДанныеФормыПоПериодуИОрганизации(ПериодВСписках, ОтборПоОрганизацииВСписках)

	ЗаполнитьДанныеНаСтраницеНастройкаУчетаНДС();
	ЗаполнитьДанныеНаСтраницеРегламентныеДокументы();
	ПолучитьДанныеФормы();
	
	Если ПериодВСписках Тогда
		УстановитьПериодВСписках();
	КонецЕсли; 
	
	Если ОтборПоОрганизацииВСписках Тогда
		УстановитьОтборПоОрганизацииВСписках();	
	КонецЕсли; 	
	
	// Очистим списки
	Объект.СписокСчетовФактур.Очистить();
	Объект.ВыданныеАвансы.Очистить();
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ()

	СтруктураЗаполненияЦены = Новый Структура;
	СтруктураЗаполненияЦены.Вставить("ЦенаВключаетНДС", Истина);
	
	Возврат СтруктураЗаполненияЦены;

КонецФункции
