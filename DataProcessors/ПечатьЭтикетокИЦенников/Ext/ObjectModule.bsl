﻿
Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	МассивНепроверяемыхРеквизитов = Новый Массив;

	Если НазначениеШаблона = Перечисления.НазначенияШаблоновЭтикетокИЦенников.Товары Тогда
		МассивНепроверяемыхРеквизитов.Добавить("СкладскиеЯчейки");
		МассивНепроверяемыхРеквизитов.Добавить("ШаблонЭтикетки");
		МассивНепроверяемыхРеквизитов.Добавить("КоличествоЭкземпляров");
	КонецЕсли;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
КонецПроцедуры
