﻿////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
	
		Возврат;
	
	КонецЕсли;
	
	ЗакрыватьПриВыборе = Ложь;
	
	Склад 					= Параметры.Склад;
	СпособПополненияЗапаса 	= Параметры.СпособПополненияЗапаса;
	Номенклатура		 	= Параметры.Номенклатура;
	
	ОбновитьГраницыКонтроляНаСервере();
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура Актуализировать(Команда)
	
	ПоказатьОповещениеПользователя(НСтр("ru = 'Управление запасами'"),, НСтр("ru = 'Выполняется актуализация границ контроля...'"), БиблиотекаКартинок.Информация32);
	
	АктуализироватьНаСервере();
	ОповеститьОВыборе(Новый Структура("ВыполняемаяОперация", "АктуализацияГраницКонтроля"));
	
	ПоказатьОповещениеПользователя(НСтр("ru = 'Управление запасами'"),, НСтр("ru = 'Выполнена актуализация границ контроля!'"), БиблиотекаКартинок.Информация32);
	
КонецПроцедуры

&НаКлиенте
Процедура СнятьФлажки(Команда)
	
	СнятьФлажкиНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьФлажки(Команда)
	
	УстановитьФлажкиНаСервере();
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// Прочее

&НаСервере
Процедура АктуализироватьНаСервере()
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	ГраницыКонтроля.Склад КАК Склад,
	|	ГраницыКонтроля.Номенклатура КАК Номенклатура,
	|	ГраницыКонтроля.Характеристика КАК Характеристика,
	|	ГраницыКонтроля.НоваяГраницаКонтроля КАК НоваяГраницаКонтроля
	|ПОМЕСТИТЬ ГраницыКонтроля
	|ИЗ
	|	&ГраницыКонтроля КАК ГраницыКонтроля
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	ГраницыКонтроля.Склад КАК Склад,
	|	ГраницыКонтроля.Номенклатура КАК Номенклатура,
	|	ГраницыКонтроля.Характеристика КАК Характеристика,
	|	ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомГрафика) КАК ВариантКонтроля,
	|	ГраницыКонтроля.НоваяГраницаКонтроля КАК ГраницаГрафикаДоступности,
	|	ЕСТЬNULL(НастройкаКонтроляОстатков.СрокПоставки, 0) КАК СрокПоставки
	|ИЗ
	|	ГраницыКонтроля КАК ГраницыКонтроля
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаКонтроляОстатков
	|		ПО ГраницыКонтроля.Склад = НастройкаКонтроляОстатков.Склад
	|			И ГраницыКонтроля.Номенклатура = НастройкаКонтроляОстатков.Номенклатура
	|			И ГраницыКонтроля.Характеристика = НастройкаКонтроляОстатков.Характеристика");
	
	Запрос.УстановитьПараметр("ГраницыКонтроля", ГраницыКонтроля.Выгрузить(Новый Структура("Выбран", Истина)));
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Если РезультатЗапроса.Пустой() Тогда
		
		Возврат;
		
	КонецЕсли;
	
	Выборка = РезультатЗапроса.Выбрать();
	
	Пока Выборка.Следующий() Цикл
		
		МенеджерЗаписи = РегистрыСведений.НастройкаКонтроляОстатков.СоздатьМенеджерЗаписи();
		ЗаполнитьЗначенияСвойств(МенеджерЗаписи, Выборка);
		МенеджерЗаписи.Записать();
		
	КонецЦикла;
	
	ОбновитьГраницыКонтроляНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьФлажкиНаСервере()
	
	НайденныеСтроки = ГраницыКонтроля.НайтиСтроки(Новый Структура("Выбран", Ложь));
	
	Для каждого НайденнаяСтрока из НайденныеСтроки Цикл
		
		НайденнаяСтрока.Выбран = Истина;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура СнятьФлажкиНаСервере()
	
	НайденныеСтроки = ГраницыКонтроля.НайтиСтроки(Новый Структура("Выбран", Истина));
	
	Для каждого НайденнаяСтрока из НайденныеСтроки Цикл
		
		НайденнаяСтрока.Выбран = Ложь;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПолучитьБлижайшийРабочийДеньПредприятия(НачалоПериода)
	
	Запрос = Новый Запрос();
	Запрос.Текст = 
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	КалендарныйГрафик.ДатаГрафика
		|ИЗ
		|	РегистрСведений.КалендарныеГрафики КАК КалендарныйГрафик
		|ГДЕ
		|	КалендарныйГрафик.Календарь = &ОсновнойКалендарьПредприятия
		|	И КалендарныйГрафик.ДатаГрафика >= &НачалоПериода
		|	И КалендарныйГрафик.ДеньВключенВГрафик
		|
		|УПОРЯДОЧИТЬ ПО
		|	КалендарныйГрафик.ДатаГрафика";
	Запрос.УстановитьПараметр("ОсновнойКалендарьПредприятия", Константы.ОсновнойКалендарьПредприятия.Получить());
	Запрос.УстановитьПараметр("НачалоПериода", НачалоПериода);
	Результат = Запрос.Выполнить();
	
	Если Результат.Пустой() Тогда
		
		БлижайшийРабочийДеньПредприятия = НачалоПериода;
		
	Иначе 
		
		БлижайшийРабочийДеньПредприятия = Результат.Выгрузить()[0].ДатаГрафика;
		
	КонецЕсли;
	
	Возврат БлижайшийРабочийДеньПредприятия;	
	
КонецФункции //ПолучитьБлижайшийРабочийДеньПредприятия()

&НаСервере
Процедура ОбновитьГраницыКонтроляНаСервере()
	
	ГраницыКонтроля.Очистить();
	
	ТекстЗапроса =
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	ИСТИНА КАК Выбран,
	|	ТранспортныеОграничения.Склад КАК Склад,
	|	ТранспортныеОграничения.Номенклатура КАК Номенклатура,
	|	ТранспортныеОграничения.Характеристика КАК Характеристика,
	|	ВЫБОР
	|		КОГДА МАКСИМУМ(ЕСТЬNULL(НастройкаХарактеристика.ГраницаГрафикаДоступности, ЕСТЬNULL(НастройкаНоменклатура.ГраницаГрафикаДоступности, НастройкаСклад.ГраницаГрафикаДоступности))) >= &ТекущаяДата
	|			ТОГДА МАКСИМУМ(ЕСТЬNULL(НастройкаХарактеристика.ГраницаГрафикаДоступности, ЕСТЬNULL(НастройкаНоменклатура.ГраницаГрафикаДоступности, НастройкаСклад.ГраницаГрафикаДоступности)))
	|		КОГДА МАКСИМУМ(ЕСТЬNULL(НастройкаХарактеристика.СрокПоставки, ЕСТЬNULL(НастройкаНоменклатура.СрокПоставки, НастройкаСклад.СрокПоставки))) > 0
	|			ТОГДА ДОБАВИТЬКДАТЕ(&ТекущаяДата, ДЕНЬ, МАКСИМУМ(ЕСТЬNULL(НастройкаХарактеристика.СрокПоставки, ЕСТЬNULL(НастройкаНоменклатура.СрокПоставки, НастройкаСклад.СрокПоставки))) - 1)
	|		ИНАЧЕ ДАТАВРЕМЯ(1, 1, 1)
	|	КОНЕЦ КАК ГраницаКонтроля,
	|	ДОБАВИТЬКДАТЕ(ЕСТЬNULL(ЕСТЬNULL(МИНИМУМ(КалендарныеГрафикиСледующаяДата.ДатаГрафика), МИНИМУМ(КалендарныеГрафикиСледующаяДатаСледующийГод.ДатаГрафика)), ВЫБОР
	|				КОГДА МАКСИМУМ(ЕСТЬNULL(Константы.ОсновнойКалендарьПредприятия, ТранспортныеОграничения.Календарь)) = ЗНАЧЕНИЕ(Справочник.Календари.ПустаяСсылка)
	|					ТОГДА ДОБАВИТЬКДАТЕ(&НачалоПериодаРабочийДень, ДЕНЬ, МАКСИМУМ(ТранспортныеОграничения.СрокТранспортировки) + 1)
	|			КОНЕЦ), ДЕНЬ, -1) КАК НоваяГраницаКонтроля
	|ИЗ
	|	РегистрСведений.ТранспортныеОграничения КАК ТранспортныеОграничения
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаХарактеристика
	|		ПО ТранспортныеОграничения.Склад = НастройкаХарактеристика.Склад
	|			И ТранспортныеОграничения.Номенклатура = НастройкаХарактеристика.Номенклатура
	|			И ТранспортныеОграничения.Характеристика = НастройкаХарактеристика.Характеристика
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаНоменклатура
	|		ПО ТранспортныеОграничения.Склад = НастройкаНоменклатура.Склад
	|			И ТранспортныеОграничения.Номенклатура = НастройкаНоменклатура.Номенклатура
	|			И (НастройкаНоменклатура.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
	|			И (НастройкаХарактеристика.Склад ЕСТЬ NULL )
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаСклад
	|		ПО ТранспортныеОграничения.Склад = НастройкаСклад.Склад
	|			И (НастройкаСклад.Номенклатура = ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка))
	|			И (НастройкаСклад.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
	|			И (НастройкаХарактеристика.Склад ЕСТЬ NULL )
	|			И (НастройкаНоменклатура.Склад ЕСТЬ NULL )
	|		ЛЕВОЕ СОЕДИНЕНИЕ Константы КАК Константы
	|		ПО (ТранспортныеОграничения.Календарь = ЗНАЧЕНИЕ(Справочник.календари.ПустаяСсылка))
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.КалендарныеГрафики КАК КалендарныеГрафикиНачалоПериода
	|		ПО (ЕСТЬNULL(Константы.ОсновнойКалендарьПредприятия, ТранспортныеОграничения.Календарь) = КалендарныеГрафикиНачалоПериода.Календарь)
	|			И (КалендарныеГрафикиНачалоПериода.ДатаГрафика = ДОБАВИТЬКДАТЕ(&НачалоПериодаРабочийДень, ДЕНЬ, ТранспортныеОграничения.СрокТранспортировки))
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.КалендарныеГрафики КАК КалендарныеГрафикиБлижайшаяДата
	|		ПО (ЕСТЬNULL(Константы.ОсновнойКалендарьПредприятия, ТранспортныеОграничения.Календарь) = КалендарныеГрафикиБлижайшаяДата.Календарь)
	|			И (КалендарныеГрафикиНачалоПериода.Год = КалендарныеГрафикиБлижайшаяДата.Год)
	|			И (КалендарныеГрафикиНачалоПериода.КоличествоДнейВГрафикеСНачалаГода = КалендарныеГрафикиБлижайшаяДата.КоличествоДнейВГрафикеСНачалаГода
	|					И КалендарныеГрафикиНачалоПериода.ДеньВключенВГрафик
	|					И КалендарныеГрафикиБлижайшаяДата.ДеньВключенВГрафик
	|				ИЛИ КалендарныеГрафикиНачалоПериода.КоличествоДнейВГрафикеСНачалаГода + 1 = КалендарныеГрафикиБлижайшаяДата.КоличествоДнейВГрафикеСНачалаГода
	|					И НЕ КалендарныеГрафикиНачалоПериода.ДеньВключенВГрафик
	|					И КалендарныеГрафикиБлижайшаяДата.ДеньВключенВГрафик)
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.КалендарныеГрафики КАК КалендарныеГрафикиБлижайшаяДатаСледующийГод
	|		ПО (ЕСТЬNULL(Константы.ОсновнойКалендарьПредприятия, ТранспортныеОграничения.Календарь) = КалендарныеГрафикиБлижайшаяДатаСледующийГод.Календарь)
	|			И (КалендарныеГрафикиБлижайшаяДатаСледующийГод.КоличествоДнейВГрафикеСНачалаГода = 1)
	|			И (КалендарныеГрафикиНачалоПериода.Год + 1 = КалендарныеГрафикиБлижайшаяДатаСледующийГод.Год)
	|			И (КалендарныеГрафикиБлижайшаяДатаСледующийГод.ДеньВключенВГрафик)
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.КалендарныеГрафики КАК КалендарныеГрафикиСледующаяДата
	|		ПО (ЕСТЬNULL(Константы.ОсновнойКалендарьПредприятия, ТранспортныеОграничения.Календарь) = КалендарныеГрафикиСледующаяДата.Календарь)
	|			И (КалендарныеГрафикиНачалоПериода.Год = КалендарныеГрафикиСледующаяДата.Год)
	|			И (КалендарныеГрафикиНачалоПериода.КоличествоДнейВГрафикеСНачалаГода + 1 = КалендарныеГрафикиСледующаяДата.КоличествоДнейВГрафикеСНачалаГода
	|					И КалендарныеГрафикиНачалоПериода.ДеньВключенВГрафик
	|					И КалендарныеГрафикиСледующаяДата.ДеньВключенВГрафик
	|				ИЛИ КалендарныеГрафикиНачалоПериода.КоличествоДнейВГрафикеСНачалаГода + 2 = КалендарныеГрафикиСледующаяДата.КоличествоДнейВГрафикеСНачалаГода
	|					И НЕ КалендарныеГрафикиНачалоПериода.ДеньВключенВГрафик
	|					И КалендарныеГрафикиСледующаяДата.ДеньВключенВГрафик)
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.КалендарныеГрафики КАК КалендарныеГрафикиСледующаяДатаСледующийГод
	|		ПО (ЕСТЬNULL(Константы.ОсновнойКалендарьПредприятия, ТранспортныеОграничения.Календарь) = КалендарныеГрафикиСледующаяДатаСледующийГод.Календарь)
	|			И (КалендарныеГрафикиСледующаяДатаСледующийГод.КоличествоДнейВГрафикеСНачалаГода = 2)
	|			И (КалендарныеГрафикиНачалоПериода.Год + 1 = КалендарныеГрафикиСледующаяДатаСледующийГод.Год)
	|			И (КалендарныеГрафикиСледующаяДатаСледующийГод.ДеньВключенВГрафик)
	|ГДЕ
	|	ТранспортныеОграничения.РеквизитДопУпорядочивания = 1
	|	И ТранспортныеОграничения.СрокТранспортировки > 0
	|	И ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомГрафика)
	|{ГДЕ
	|	ТранспортныеОграничения.Склад КАК Склад,
	|	ТранспортныеОграничения.СпособПополненияЗапаса КАК СпособПополненияЗапаса,
	|	ТранспортныеОграничения.Номенклатура КАК Номенклатура}
	|
	|СГРУППИРОВАТЬ ПО
	|	ТранспортныеОграничения.Номенклатура,
	|	ТранспортныеОграничения.Характеристика,
	|	ТранспортныеОграничения.Склад
	|
	|ИМЕЮЩИЕ
	|	ВЫБОР
	|		КОГДА МАКСИМУМ(ЕСТЬNULL(НастройкаХарактеристика.ГраницаГрафикаДоступности, ЕСТЬNULL(НастройкаНоменклатура.ГраницаГрафикаДоступности, НастройкаСклад.ГраницаГрафикаДоступности))) >= &ТекущаяДата
	|			ТОГДА МАКСИМУМ(ЕСТЬNULL(НастройкаХарактеристика.ГраницаГрафикаДоступности, ЕСТЬNULL(НастройкаНоменклатура.ГраницаГрафикаДоступности, НастройкаСклад.ГраницаГрафикаДоступности)))
	|		КОГДА МАКСИМУМ(ЕСТЬNULL(НастройкаХарактеристика.СрокПоставки, ЕСТЬNULL(НастройкаНоменклатура.СрокПоставки, НастройкаСклад.СрокПоставки))) > 0
	|			ТОГДА ДОБАВИТЬКДАТЕ(&ТекущаяДата, ДЕНЬ, МАКСИМУМ(ЕСТЬNULL(НастройкаХарактеристика.СрокПоставки, ЕСТЬNULL(НастройкаНоменклатура.СрокПоставки, НастройкаСклад.СрокПоставки))) - 1)
	|		ИНАЧЕ ДАТАВРЕМЯ(1, 1, 1)
	|	КОНЕЦ < ДОБАВИТЬКДАТЕ(ЕСТЬNULL(ЕСТЬNULL(МИНИМУМ(КалендарныеГрафикиСледующаяДата.ДатаГрафика), МИНИМУМ(КалендарныеГрафикиСледующаяДатаСледующийГод.ДатаГрафика)), ВЫБОР
	|				КОГДА МАКСИМУМ(ЕСТЬNULL(Константы.ОсновнойКалендарьПредприятия, ТранспортныеОграничения.Календарь)) = ЗНАЧЕНИЕ(Справочник.Календари.ПустаяСсылка)
	|					ТОГДА ДОБАВИТЬКДАТЕ(&НачалоПериодаРабочийДень, ДЕНЬ, МАКСИМУМ(ТранспортныеОграничения.СрокТранспортировки) + 1)
	|			КОНЕЦ), ДЕНЬ, -1)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Склад,
	|	Номенклатура,
	|	Характеристика";
	
	ПостроительЗапроса = Новый ПостроительЗапроса(ТекстЗапроса);
	
	Если ЗначениеЗаполнено(Склад) Тогда
		
		ОтборСклад = ПостроительЗапроса.Отбор.Добавить("Склад");
		ОтборСклад.Установить(Склад);
		
		УстановитьПривилегированныйРежим(Истина);
		
		СкладЭтоГруппа = ОбщегоНазначения.ПолучитьЗначениеРеквизита(Склад, "ЭтоГруппа");
		
		УстановитьПривилегированныйРежим(Ложь);
		
		Если ЗначениеЗаполнено(СкладЭтоГруппа) И СкладЭтоГруппа Тогда
			
			ОтборСклад.ВидСравнения = ВидСравнения.ВИерархии;
			
		КонецЕсли;
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(СпособПополненияЗапаса) Тогда
		
		ОтборСпособПополненияЗапаса = ПостроительЗапроса.Отбор.Добавить("СпособПополненияЗапаса");
		ОтборСпособПополненияЗапаса.Установить(СпособПополненияЗапаса);
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Номенклатура) Тогда
		
		ОтборНоменклатура = ПостроительЗапроса.Отбор.Добавить("Номенклатура");
		ОтборНоменклатура.Установить(Номенклатура);
		
	КонецЕсли;
	
	Запрос = ПостроительЗапроса.ПолучитьЗапрос();	
	
	НачалоПериода = НачалоДня(ТекущаяДатаСеанса());
	НачалоПериодаРабочийДень = ПолучитьБлижайшийРабочийДеньПредприятия(НачалоПериода);

	
	Запрос.УстановитьПараметр("НачалоПериодаРабочийДень", НачалоПериодаРабочийДень);
	Запрос.УстановитьПараметр("ТекущаяДата", НачалоПериода);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Выборка = РезультатЗапроса.Выбрать();
	
	Пока Выборка.Следующий() Цикл
		
		ЗаполнитьЗначенияСвойств(ГраницыКонтроля.Добавить(), Выборка);
		
	КонецЦикла;
	
КонецПроцедуры
