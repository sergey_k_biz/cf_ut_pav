﻿&НаКлиенте
Функция КонтрольЗавершенияВсехСеансовКромеСвоего()
	
	//Procedure КонтрольРежимаЗавершенияРаботыПользователей() Экспорт
	МассивСеансов=GetInfoBaseSessions();
	ЕстьСеансыКромеТекущего=ложь;
	For Each стр In МассивСеансов Do
		//Message(стр.User.Name);
		If стр.User.Name <> "Update" Then
		
			ЕстьСеансыКромеТекущего=истина;
            WriteLogEvent("#ОбновленияКонфы#",EventLogLevel.Information,,,"Есть активный пользователь: "+стр.User.Name);	
		
		EndIf;
			
	EndDo;
	If не ЕстьСеансыКромеТекущего Then
		WriteLogEvent("#ОбновленияКонфы#",EventLogLevel.Information,,,"Запуск конфигуратора для обновления "+CurrentDate());	
		Попытка
			//Exit(Ложь,Истина," CONFIG /UpdateDBCfg /UCPackageUpdating -NoTruncate  /DisableStartupMessages /RunEnterprise ""ENTERPRISE /CStartWorkUsers""");	
			ЗавершитьРаботуСистемы(Ложь,Истина,"CONFIG /UCPackageUpdating /UpdateDBCfg /DisableStartupMessages /RunEnterprise ""/CStartWorkUsers""");	
		исключение
			WriteLogEvent("#ОбновленияКонфы#",EventLogLevel.Information,,,"Ошибка при запуске обновления "+ОписаниеОшибки());	
		КонецПопытки;
		WriteLogEvent("#ОбновленияКонфы#",EventLogLevel.Information,,,"После запуска конфигуратора для обновления");	
	EndIf;
	
КонецФункции


Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	Попытка
	
	отказ=истина;	
	
	Исключение
	
	КонецПопытки;
	
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ПриОткрытииНаСервере(ПараметрЗапуска);
	//Если ВыполнитьВыход = "Запустить1С" тогда
	//	WriteLogEvent("#ОбновленияКонфы#",EventLogLevel.Information,,,"Запускаем 1С начало");	
	//	Exit(Ложь,Истина," CONFIG /UpdateDBCfg /UCPackageUpdating -NoTruncate  /DisableStartupMessages /RunEnterprise ""ENTERPRISE /CStartWorkUsers""");	
	//	WriteLogEvent("#ОбновленияКонфы#",EventLogLevel.Information,,,"Запускаем 1С после запуска");	
	//ИначеЕсли ВыполнитьВыход = "Закрыть1С" тогда
	//	WriteLogEvent("#ОбновленияКонфы#",EventLogLevel.Information,,,"Закрываем 1С");	
	//	Exit(Ложь,Ложь);
	//КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытииНаСервере(LaunchParameter)
	WriteLogEvent("#ОбновленияКонфы#",EventLogLevel.Information,,,"Пришли сюда LaunchParameter = "+LaunchParameter);
	If LaunchParameter="finishWorkUsers" Then
		
		Блокировка = Новый SessionsLock;
		// код для входа при блокировки
		Блокировка.КодРазрешения = "PackageUpdating";
		// дата начала
		Блокировка.Начало = CurrentDate(); 
		// дата окончания блокировки
		Блокировка.Конец = CurrentDate()+60*10; // продолжительность  10 минут 
		// сообщение выдоваемое пользователю
		Блокировка.Сообщение = "!!! Внимание !!! Выполняется обновление.После завершения 1С запустится АВТОМАТИЧЕСКИ";
		
		// Включаем блокировку, если Ложь то отключаем
		Блокировка.Установлена = Истина;
		// устанавливаем блокировку
		SetSessionsLock(Блокировка);
		//
		AttachIdleHandler("КонтрольЗавершенияВсехСеансовКромеСвоего",1);
	ElsIf LaunchParameter="StartWorkUsers" Then
		
		Блокировка = Новый SessionsLock;
		// код для входа при блокировки
		Блокировка.КодРазрешения = "PackageUpdating";
		// дата начала
		Блокировка.Начало = CurrentDate(); 
		// дата окончания блокировки
		Блокировка.Конец = CurrentDate()+60*10; // продолжительность  10 минут 
		// сообщение выдоваемое пользователю
		Блокировка.Сообщение = "!!! Внимание !!! Выполняется обновление.После завершения 1С запустится АВТОМАТИЧЕСКИ";
		
		// Включаем блокировку, если Ложь то отключаем
		Блокировка.Установлена = Ложь ;
		// устанавливаем блокировку
		SetSessionsLock(Блокировка);
		
		WriteLogEvent("#ОбновленияКонфы#",EventLogLevel.Information,,,"Блокировку сняли, успешное обновление."+LaunchParameter);
		//Возврат "Закрыть1С";
		Exit(Ложь,Ложь);
		
	EndIf;
КонецПроцедуры
