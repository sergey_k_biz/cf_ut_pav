﻿&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	Если ТипЗнч(ПараметрКоманды) = Тип("СправочникСсылка.Номенклатура") Тогда
		
		ПараметрыФормы = Новый Структура("Отбор", Новый Структура("Номенклатура", ПараметрКоманды));
		ОткрытьФорму("РегистрСведений.МетодыУправленияЗапасами.Форма.МетодыУправленияЗапасамиНоменклатуры", ПараметрыФормы, ПараметрыВыполненияКоманды.Источник, ПараметрыВыполненияКоманды.Уникальность, ПараметрыВыполненияКоманды.Окно);
		
	ИначеЕсли ТипЗнч(ПараметрКоманды) = Тип("СправочникСсылка.Склады") Тогда
		
		ПараметрыФормы = Новый Структура("Отбор", Новый Структура("Склад", ПараметрКоманды));
		ОткрытьФорму("РегистрСведений.МетодыУправленияЗапасами.Форма.МетодыУправленияЗапасамиСклада", ПараметрыФормы, ПараметрыВыполненияКоманды.Источник, ПараметрыВыполненияКоманды.Уникальность, ПараметрыВыполненияКоманды.Окно);
			
	КонецЕсли;
	
КонецПроцедуры
