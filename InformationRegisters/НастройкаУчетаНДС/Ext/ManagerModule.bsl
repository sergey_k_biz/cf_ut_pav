﻿////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС

// Получает расписание регламентного задания
//
// Параметры:
//  Организация	- СправочникСсылка.Организации - организация для которой требуется получить расписание
//
// Возвращаемое значение:
//   РасписаниеРегламентногоЗадания - расписание регламентного задания
//
Функция РасписаниеФормированияДвижений(Организация) Экспорт
	
	// возвращаемое значение функции
	РасписаниеРегламентногоЗадания = Неопределено;
	
	РегламентноеЗаданиеGUID = ГУИДРегламентногоЗадания(Организация);
	
	РегламентноеЗаданиеОбъект = УчетНДС.НайтиРегламентноеЗаданиеПоПараметру(РегламентноеЗаданиеGUID);
	
	Если РегламентноеЗаданиеОбъект <> Неопределено Тогда
		
		РасписаниеРегламентногоЗадания = РегламентноеЗаданиеОбъект.Расписание;
		
	КонецЕсли;
	
	Возврат РасписаниеРегламентногоЗадания;
	
КонецФункции

// Записывает расписание выполнения регламентного задания
//
// Параметры:
//  Организация						- СправочникСсылка.Организации - организация для которой требуется записать расписание
//  Использование					- Булево - использование расписания 
//  РасписаниеРегламентногоЗадания	- РасписаниеРегламентногоЗадания - расписание регламентного задания
//  Отказ							- Булево - устанавливается в Истина, если возникла ошибка при записи расписания
//
Процедура ОбновитьДанныеРегламентногоЗадания(Организация, Использование, РасписаниеРегламентногоЗадания, Отказ) Экспорт
	
	// Получаем регламентное задание по идентификатору, если объект не находим, то создаем новый
	РегламентноеЗаданиеОбъект = СоздатьРегламентноеЗаданиеПриНеобходимости(Организация);
	
	// Обновляем свойства РЗ
	УстановитьПараметрыРегламентногоЗадания(РегламентноеЗаданиеОбъект, РасписаниеРегламентногоЗадания, Организация, Использование);
	
	// Записываем измененное задание
	УчетНДС.ЗаписатьРегламентноеЗадание(Отказ, РегламентноеЗаданиеОбъект);
	
	ЗаписатьСведенияОРегламентномЗадании(Организация, Строка(РегламентноеЗаданиеОбъект.УникальныйИдентификатор), Отказ);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// Обработка регламентных заданий

Процедура ЗаписатьСведенияОРегламентномЗадании(Организация, РегламентноеЗаданиеGUID, Отказ)

	УстановитьПривилегированныйРежим(Истина);
	
	МенеджерЗаписи = РегистрыСведений.СведенияОРегламентныхЗаданияхПоУчетуНДС.СоздатьМенеджерЗаписи();
	МенеджерЗаписи.Организация = Организация;
	МенеджерЗаписи.РегламентноеЗаданиеGUID = РегламентноеЗаданиеGUID;
	
	Попытка
	
		МенеджерЗаписи.Записать();
	
	Исключение
	
		НСтрока = НСтр("ru = 'Произошла ошибка при сохранении расписания. Возможно данные расписания были изменены.
		                     |Подробное описание ошибки: %1'");

		СтрокаСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтрока, КраткоеПредставлениеОшибки(ИнформацияОбОшибке()));
		
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(СтрокаСообщения,,,, Отказ);
		
	КонецПопытки; 
	
КонецПроцедуры

Функция СоздатьРегламентноеЗаданиеПриНеобходимости(Организация)
	
	РегламентноеЗаданиеGUID = ГУИДРегламентногоЗадания(Организация);
	
	РегламентноеЗаданиеОбъект = УчетНДС.НайтиРегламентноеЗаданиеПоПараметру(РегламентноеЗаданиеGUID);
	
	// при необходимости создаем регл. задание
	Если РегламентноеЗаданиеОбъект = Неопределено Тогда
		
		РегламентноеЗаданиеОбъект = РегламентныеЗадания.СоздатьРегламентноеЗадание("ФормированиеДвиженийПоУчетуНДС");
		
	КонецЕсли;
	
	Возврат РегламентноеЗаданиеОбъект;
	
КонецФункции

Процедура УстановитьПараметрыРегламентногоЗадания(РегламентноеЗаданиеОбъект, РасписаниеРегламентногоЗадания, Организация, Использование)
	
	ПараметрыРегламентногоЗадания = Новый Массив;
	ПараметрыРегламентногоЗадания.Добавить(Организация);
	
	НСтрока = НСтр("ru = 'Формирование движений по учету НДС по данным организации: %1'");
	НаименованиеРегламентногоЗадания = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтрока, СокрЛП(Организация));
	
	РегламентноеЗаданиеОбъект.Наименование  = Лев(НаименованиеРегламентногоЗадания, 120);
	РегламентноеЗаданиеОбъект.Использование = Использование;
	РегламентноеЗаданиеОбъект.Параметры     = ПараметрыРегламентногоЗадания;
	
	// обновляем расписание, если оно было изменено
	Если РасписаниеРегламентногоЗадания <> Неопределено Тогда
		РегламентноеЗаданиеОбъект.Расписание = РасписаниеРегламентногоЗадания;
	КонецЕсли;
	
КонецПроцедуры

Функция ГУИДРегламентногоЗадания(Организация)

	Запрос = Новый Запрос;
	
	ТекстЗапроса = "ВЫБРАТЬ
	               |	СведенияОРегламентныхЗаданияхПоУчетуНДС.РегламентноеЗаданиеGUID КАК РегламентноеЗаданиеGUID
	               |ИЗ
	               |	РегистрСведений.СведенияОРегламентныхЗаданияхПоУчетуНДС КАК СведенияОРегламентныхЗаданияхПоУчетуНДС
	               |ГДЕ
	               |	СведенияОРегламентныхЗаданияхПоУчетуНДС.Организация = &Организация";
	 
	Запрос.Текст = ТекстЗапроса;
	Запрос.УстановитьПараметр("Организация", Организация);
	Результат = Запрос.Выполнить();
	Если Результат.Пустой() Тогда
		Возврат "";
	КонецЕсли;
	
	Выборка = Результат.Выбрать();
	
	Выборка.Следующий();
	
	Возврат	Выборка.РегламентноеЗаданиеGUID

КонецФункции
 
