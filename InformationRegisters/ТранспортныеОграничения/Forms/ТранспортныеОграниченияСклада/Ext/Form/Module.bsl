﻿////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура ПереместитьЭлементВверх(Команда)
	
	Если Элементы.ТранспортныеОграничения.ТекущаяСтрока <> Неопределено Тогда
		
		ПереместитьЭлементВверхНаСервере(Элементы.ТранспортныеОграничения.ТекущаяСтрока);
		Элементы.ТранспортныеОграничения.Обновить();
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПереместитьЭлементВниз(Команда)
	
	Если Элементы.ТранспортныеОграничения.ТекущаяСтрока <> Неопределено Тогда
		
		ПереместитьЭлементВнизНаСервере(Элементы.ТранспортныеОграничения.ТекущаяСтрока);
		Элементы.ТранспортныеОграничения.Обновить();
		
	КонецЕсли;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// Прочее

&НаСервере
Процедура ПереместитьЭлементВверхНаСервере(КлючЗаписи)
	
	МенеджерЗаписи = РегистрыСведений.ТранспортныеОграничения.СоздатьМенеджерЗаписи();
	
	МенеджерЗаписи.Склад = КлючЗаписи.Склад;
	МенеджерЗаписи.Номенклатура = КлючЗаписи.Номенклатура;
	МенеджерЗаписи.Характеристика = КлючЗаписи.Характеристика;
	МенеджерЗаписи.СпособПополненияЗапаса = КлючЗаписи.СпособПополненияЗапаса;
	
	МенеджерЗаписи.Прочитать();
	
	Если МенеджерЗаписи.РеквизитДопУпорядочивания > 1 Тогда
		
		МенеджерЗаписи.РеквизитДопУпорядочивания = МенеджерЗаписи.РеквизитДопУпорядочивания - 1;
		МенеджерЗаписи.Записать();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПереместитьЭлементВнизНаСервере(КлючЗаписи)
	
	МенеджерЗаписи = РегистрыСведений.ТранспортныеОграничения.СоздатьМенеджерЗаписи();
	
	МенеджерЗаписи.Склад = КлючЗаписи.Склад;
	МенеджерЗаписи.Номенклатура = КлючЗаписи.Номенклатура;
	МенеджерЗаписи.Характеристика = КлючЗаписи.Характеристика;
	МенеджерЗаписи.СпособПополненияЗапаса = КлючЗаписи.СпособПополненияЗапаса;
	
	МенеджерЗаписи.Прочитать();
	
	МенеджерЗаписи.РеквизитДопУпорядочивания = МенеджерЗаписи.РеквизитДопУпорядочивания + 1;
	МенеджерЗаписи.Записать();
	
КонецПроцедуры
