﻿
///////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС

// Функция получает элемент справочника - ключ аналитики учета.
//
// Параметры:
//	ПараметрыАналитики - Коллекция - Коллекция параметров для получения ключа
//
// Возвращаемое значение:
//	СправочникСсылка.КлючиАналитикиУчетаПоПартнерам - Найденный элемент справочника
//
Функция ЗначениеКлючаАналитики(ПараметрыАналитики) Экспорт

	МенеджерЗаписи = РегистрыСведений.АналитикаУчетаПоПартнерам.СоздатьМенеджерЗаписи();
	ЗаполнитьЗначенияСвойств(МенеджерЗаписи, ПараметрыАналитики, "Организация, Партнер, Контрагент");
	МенеджерЗаписи.Прочитать();

	Если МенеджерЗаписи.Выбран() Тогда
		ЭлементКлючАналитики = МенеджерЗаписи.КлючАналитики;
	Иначе
        ЭлементКлючАналитики = СоздатьКлючАналитики(ПараметрыАналитики);
	КонецЕсли;

	Возврат ЭлементКлючАналитики;

КонецФункции

// Функция получает элемент справочника - ключ аналитики учета.
//
// Параметры:
//	ПараметрыАналитики - Выборка или Структура  с полями "Организация, Партнер, Контрагент".
//
// Возвращаемое значение:
//	СправочникСсылка.КлючиАналитикиУчетаПоПартнерам - Найденный элемент справочника
//
Функция СоздатьКлючАналитики(ПараметрыАналитики) Экспорт

	МенеджерЗаписи = РегистрыСведений.АналитикаУчетаПоПартнерам.СоздатьМенеджерЗаписи();
	ЗаполнитьЗначенияСвойств(МенеджерЗаписи, ПараметрыАналитики, "Организация, Партнер, Контрагент");

	// Создание нового ключа аналитики.
	СправочникОбъект = Справочники.КлючиАналитикиУчетаПоПартнерам.СоздатьЭлемент();
	СправочникОбъект.Наименование = ПолучитьПолноеНаименованиеКлючаАналитики(МенеджерЗаписи);
	ЗаполнитьЗначенияСвойств(СправочникОбъект, ПараметрыАналитики, "Организация, Партнер, Контрагент");
	СправочникОбъект.Записать();

	Результат = СправочникОбъект.Ссылка;

	МенеджерЗаписи.КлючАналитики = Результат;
	МенеджерЗаписи.Записать(Ложь);

	Возврат Результат;

КонецФункции

///////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

///////////////////////////////////////////////////////////////////////////////
// Прочее

Функция ПолучитьПолноеНаименованиеКлючаАналитики(МенеджерЗаписи)
	
	Наименование = "";
	
	МетаданныеИзмерения = Метаданные.РегистрыСведений.АналитикаУчетаПоПартнерам.Измерения;
	Для Каждого Измерение Из МетаданныеИзмерения Цикл
		
		// Получим представление значения, которое указано в измерении регистра сведений.
		ТекстЗначения = Строка(МенеджерЗаписи[Измерение.Имя]);
		Если Не ПустаяСтрока(ТекстЗначения) Тогда
			Наименование = Наименование + ТекстЗначения + "; ";
		КонецЕсли;
		
	КонецЦикла;
	
	Если Прав(Наименование, 2) = "; " Тогда
		Наименование = Лев(Наименование, СтрДлина(Наименование) - 2);
	КонецЕсли;
	
	Возврат Наименование;
	
КонецФункции
