﻿
///////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;

	Организация = Параметры.Организация;
	ХозяйственнаяОперация = Параметры.ХозяйственнаяОперация;
	
	Если ПолучитьФункциональнуюОпцию("ИспользоватьОбособленныеПодразделенияВыделенныеНаБаланс") Тогда
		
		Если ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПоступлениеОплатыОтКлиента
			Или ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратОплатыКлиенту Тогда
			ЭтоРасчетыСКлиентами = Истина;
		Иначе
			ЭтоРасчетыСКлиентами = Ложь;
		КонецЕсли;
		
		Запрос = Новый Запрос("ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	Организации.Ссылка КАК Ссылка
		|ИЗ
		|	Справочник.Организации КАК Организации
		|ГДЕ
		|	Организации.ГоловнаяОрганизация = &Организация
		|	И (Организации.ДопускаютсяВзаиморасчетыСКлиентамиЧерезГоловнуюОрганизацию
		|				И &ЭтоРасчетыСКлиентами
		|			ИЛИ Организации.ДопускаютсяВзаиморасчетыСПоставщикамиЧерезГоловнуюОрганизацию
		|				И НЕ &ЭтоРасчетыСКлиентами)
		|	И Организации.ОбособленноеПодразделение");
		Запрос.УстановитьПараметр("ЭтоРасчетыСКлиентами", ЭтоРасчетыСКлиентами);
		Запрос.УстановитьПараметр("Организация", Организация);
		
		ДоступныеОрганизации.ЗагрузитьЗначения(Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Ссылка"));
		
	КонецЕсли;
	
	ДоступныеОрганизации.Добавить(Организация);
	
	Если ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ОплатаДенежныхСредствВДругуюОрганизацию
		Или ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратДенежныхСредствВДругуюОрганизацию
		Или ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратТоваровМеждуОрганизациями
	Тогда
		Партнер = Справочники.Партнеры.НашеПредприятие;
	Иначе
		Партнер = Параметры.Партнер;
	КонецЕсли;
	
	Контрагент = Параметры.Контрагент;
	Если Контрагент = Неопределено Тогда
		Контрагент = Справочники.Контрагенты.ПустаяСсылка();
	КонецЕсли;
	
	Валюта = Параметры.Валюта;
	СуммаДокумента = Параметры.СуммаДокумента;
	ДатаДокумента = Параметры.ДатаДокумента;
	АдресПлатежейВХранилище = Параметры.АдресПлатежейВХранилище;
	
	ЗаполнитьТаблицуПоРасчетамСПартнерами();
	УстановитьВидимость();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	РассчитатьСуммуПлатежей();
	
КонецПроцедуры

///////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ШАПКИ ФОРМЫ

&НаКлиенте
Процедура ПартнерПриИзменении(Элемент)
	
	ЗаполнитьТаблицуПоРасчетамСПартнерами();
	
КонецПроцедуры

&НаКлиенте
Процедура КонтрагентПриИзменении(Элемент)
	
	ЗаполнитьТаблицуПоРасчетамСПартнерами();
	
КонецПроцедуры

&НаКлиенте
Процедура ТаблицаОстатковРасчетовПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	
	РассчитатьСуммуПлатежей();
	
КонецПроцедуры

&НаКлиенте
Процедура ТаблицаОстатковРасчетовВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Если Поле.Имя = "ТаблицаОстатковРасчетовЗаказ" Тогда
		СтрокаТаблицы = Элементы.ТаблицаОстатковРасчетов.ТекущиеДанные;
		Если СтрокаТаблицы <> Неопределено
		 И ЗначениеЗаполнено(СтрокаТаблицы.Заказ) Тогда
			ОткрытьЗначение(СтрокаТаблицы.Заказ);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

///////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура ПеренестиВДокументВыполнить()

	ПоместитьПлатежиВХранилище();
	Закрыть(КодВозвратаДиалога.OK);
	
	Структура = Новый Структура("АдресПлатежейВХранилище, ХозяйственнаяОперация", АдресПлатежейВХранилище, ХозяйственнаяОперация);
	ОповеститьОВыборе(Структура);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыбратьПлатежиВыполнить()

	Для Каждого СтрокаТаблицы Из ТаблицаОстатковРасчетов Цикл
		СтрокаТаблицы.Выбран = Истина;
	КонецЦикла;
	РассчитатьСуммуПлатежей();
	
КонецПроцедуры

&НаКлиенте
Процедура ИсключитьПлатежиВыполнить()

	Для Каждого СтрокаТаблицы Из ТаблицаОстатковРасчетов Цикл
		СтрокаТаблицы.Выбран = Ложь
	КонецЦикла;
	РассчитатьСуммуПлатежей();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыбратьВыделенныеПлатежи(Команда)
	
	МассивСтрок = Элементы.ТаблицаОстатковРасчетов.ВыделенныеСтроки;
	Для Каждого НомерСтроки Из МассивСтрок Цикл
		СтрокаТаблицы = ТаблицаОстатковРасчетов.НайтиПоИдентификатору(НомерСтроки);
		Если СтрокаТаблицы <> Неопределено Тогда
			СтрокаТаблицы.Выбран = Истина;
		КонецЕсли;
	КонецЦикла;
	РассчитатьСуммуПлатежей();
	
КонецПроцедуры

&НаКлиенте
Процедура ИсключитьВыделенныеПлатежи(Команда)
	
	МассивСтрок = Элементы.ТаблицаОстатковРасчетов.ВыделенныеСтроки;
	Для Каждого НомерСтроки Из МассивСтрок Цикл
		СтрокаТаблицы = ТаблицаОстатковРасчетов.НайтиПоИдентификатору(НомерСтроки);
		Если СтрокаТаблицы <> Неопределено Тогда
			СтрокаТаблицы.Выбран = Ложь;
		КонецЕсли;
	КонецЦикла;
	РассчитатьСуммуПлатежей();
	
КонецПроцедуры

///////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

///////////////////////////////////////////////////////////////////////////////
// Управление элементами формы

&НаСервере
Процедура УстановитьВидимость()
	
	Перем МассивВсехРеквизитов;
	Перем МассивРеквизитовОперации;
	
	ЗаполнитьИменаРеквизитовПоХозяйственнойОперации(
		ХозяйственнаяОперация, 
		МассивВсехРеквизитов, 
		МассивРеквизитовОперации
	);
	ДенежныеСредстваСервер.УстановитьВидимостьЭлементовПоМассиву(
		Элементы,
		МассивВсехРеквизитов,
		МассивРеквизитовОперации
	);
	
	Элементы.Контрагент.ТолькоПросмотр = (ХозяйственнаяОперация <> Перечисления.ХозяйственныеОперации.АвансовыйОтчет);
	
	Если ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ОплатаДенежныхСредствВДругуюОрганизацию
		Или ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратДенежныхСредствВДругуюОрганизацию
		Или ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратТоваровМеждуОрганизациями
	Тогда
		Заголовок = "Подбор по расчетам между организациями";
	КонецЕсли;
	
	НесколькоОрганизаций = ДоступныеОрганизации.Количество() > 1;
	Элементы.Организация.Видимость = Не НесколькоОрганизаций;
	Элементы.ТаблицаОстатковРасчетовОрганизация.Видимость = НесколькоОрганизаций;
	
КонецПроцедуры

///////////////////////////////////////////////////////////////////////////////
// Прочее

&НаСервере
Процедура ПоместитьПлатежиВХранилище() 
	
	Платежи = ТаблицаОстатковРасчетов.Выгрузить(, "Выбран, Сумма, Заказ, ВалютаВзаиморасчетов, Партнер, Контрагент, ТипРасчетов");
	МассивУдаляемыхСтрок = Новый Массив;
	Для Каждого СтрокаТаблицы Из Платежи Цикл
		Если Не СтрокаТаблицы.Выбран Тогда
			МассивУдаляемыхСтрок.Добавить(СтрокаТаблицы);
		КонецЕсли;
	КонецЦикла;
	
	Для Каждого СтрокаТаблицы Из МассивУдаляемыхСтрок Цикл
		Платежи.Удалить(СтрокаТаблицы);
	КонецЦикла;
	
	Если Платежи.Итог("Сумма") < СуммаДокумента Тогда
		СтрокаТаблицы = Платежи.Добавить();
		СтрокаТаблицы.Сумма = СуммаДокумента - Платежи.Итог("Сумма");
		Если ЗначениеЗаполнено(Партнер) Тогда
			СтрокаТаблицы.Партнер = Партнер;
		Иначе
			СтрокаТаблицы.Партнер = ДенежныеСредстваСервер.ПолучитьПартнераПоКонтрагенту(Контрагент);
		КонецЕсли; 
		Если ЗначениеЗаполнено(ДоговорКонтрагента) Тогда
			СтрокаТаблицы.Заказ = ДоговорКонтрагента;
		Иначе
			СтрокаТаблицы.Заказ = ПродажиСервер.ПолучитьДоговорПоУмолчанию(
				Неопределено,
				СтрокаТаблицы.Партнер,
				Контрагент,
				Организация,
				ПолучитьХозяйственнуюОперациюДоговора(ХозяйственнаяОперация)
			);
		КонецЕсли; 
	КонецЕсли; 
	
	Платежи.Колонки.Добавить("ОснованиеПлатежа");
	Платежи.ЗагрузитьКолонку(Платежи.ВыгрузитьКолонку("Заказ"), "ОснованиеПлатежа");
	
	АдресПлатежейВХранилище = ПоместитьВоВременноеХранилище(Платежи, УникальныйИдентификатор);
	
КонецПроцедуры

&НаСервере
Функция ПолучитьХозяйственнуюОперациюДоговора(ОперацияДокумента)
	
	ХозяйственнаяОперацияДоговора = Новый Массив;
	Если ОперацияДокумента = Перечисления.ХозяйственныеОперации.ПоступлениеОплатыОтКлиента
	 ИЛИ ОперацияДокумента = Перечисления.ХозяйственныеОперации.ВозвратОплатыКлиенту Тогда
		
		ХозяйственнаяОперацияДоговора.Добавить(Перечисления.ХозяйственныеОперации.РеализацияКлиенту);
		ХозяйственнаяОперацияДоговора.Добавить(Перечисления.ХозяйственныеОперации.ПередачаНаКомиссию);
		
	ИначеЕсли ОперацияДокумента = Перечисления.ХозяйственныеОперации.ОплатаПоставщику
	 ИЛИ ОперацияДокумента = Перечисления.ХозяйственныеОперации.ВозвратДенежныхСредствОтПоставщика Тогда
		
		ХозяйственнаяОперацияДоговора.Добавить(Перечисления.ХозяйственныеОперации.ЗакупкаУПоставщика);
		ХозяйственнаяОперацияДоговора.Добавить(Перечисления.ХозяйственныеОперации.ЗакупкаПоИмпорту);
		ХозяйственнаяОперацияДоговора.Добавить(Перечисления.ХозяйственныеОперации.ПриемНаКомиссию);
		
	КонецЕсли;
	
	Возврат ХозяйственнаяОперацияДоговора;
	
КонецФункции

&НаСервере
Функция ТекстЗапросаПоОстаткамРасчетовСПоставщиками()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ЗНАЧЕНИЕ(Перечисление.ТипыРасчетовСПартнерами.РасчетыСПоставщиком) КАК ТипРасчетов,
	|	ВЫБОР КОГДА ТаблицаПлатежей.Сумма ЕСТЬ NULL ТОГДА
	|		Ложь
	|	ИНАЧЕ
	|		Истина
	|	КОНЕЦ КАК Выбран,
	|	ЕСТЬNULL(ТаблицаПлатежей.Сумма, 0) КАК Сумма,
	|	&ВалютаДокумента КАК ВалютаДокумента,
	|
	|	РасчетыСПоставщиками.ЗаказПоставщику.Дата КАК Дата,
	|	РасчетыСПоставщиками.ЗаказПоставщику.Номер КАК Номер,
	|	РасчетыСПоставщиками.ЗаказПоставщику КАК Заказ,
	|	РасчетыСПоставщиками.Валюта КАК ВалютаВзаиморасчетов,
	|	АналитикаПоПартнерам.Организация КАК Организация,
	|	АналитикаПоПартнерам.Партнер КАК Партнер,
	|	АналитикаПоПартнерам.Контрагент КАК Контрагент,
	|	
	|	ВЫБОР КОГДА РасчетыСПоставщиками.СуммаОстаток > 0 ТОГДА
	|		РасчетыСПоставщиками.СуммаОстаток
	|	ИНАЧЕ
	|		0
	|	КОНЕЦ КАК ДолгПартнера,
	|
	|	ВЫБОР КОГДА РасчетыСПоставщиками.КОплатеОстаток < 0
	|		И Не &ТолькоБезусловнаяЗадолженность
	|	ТОГДА
	|		-РасчетыСПоставщиками.КОплатеОстаток
	|	ИНАЧЕ
	|		0
	|	КОНЕЦ КАК КОплате,
	|	
	|	ВЫБОР КОГДА РасчетыСПоставщиками.СуммаОстаток < 0 ТОГДА
	|		-РасчетыСПоставщиками.СуммаОстаток
	|	ИНАЧЕ
	|		0
	|	КОНЕЦ КАК НашДолг	
	|ИЗ
	|	РегистрНакопления.РасчетыСПоставщиками.Остатки(, 
	|		АналитикаУчетаПоПартнерам В (
	|			ВЫБРАТЬ
	|				АналитикаПоПартнерам.КлючАналитики КАК КлючАналитики
	|			ИЗ
	|				АналитикаУчетаПоПартнерам КАК АналитикаПоПартнерам
	|			)
	|	) КАК РасчетыСПоставщиками
	|		
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ 
	|		АналитикаУчетаПоПартнерам КАК АналитикаПоПартнерам
	|	ПО 
	|		РасчетыСПоставщиками.АналитикаУчетаПоПартнерам = АналитикаПоПартнерам.КлючАналитики
	|		
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		ТаблицаПлатежей КАК ТаблицаПлатежей
	|	ПО
	|		РасчетыСПоставщиками.ЗаказПоставщику = ТаблицаПлатежей.Заказ
	|		И РасчетыСПоставщиками.Валюта = ТаблицаПлатежей.Валюта
	|
	|ГДЕ
	|	(&ДебиторскаяЗадолженность
	|	И РасчетыСПоставщиками.СуммаОстаток > 0)
	|	ИЛИ 
	|	(Не &ДебиторскаяЗадолженность
	|	И РасчетыСПоставщиками.КОплатеОстаток < 0
	|	И Не &ТолькоБезусловнаяЗадолженность
	|	)
	|	ИЛИ 
	|	(Не &ДебиторскаяЗадолженность
	|	И РасчетыСПоставщиками.СуммаОстаток < 0
	|	)
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

&НаСервере
Функция ТекстЗапросаПоОстаткамРасчетовСКлиентами()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ЗНАЧЕНИЕ(Перечисление.ТипыРасчетовСПартнерами.РасчетыСКлиентом) КАК ТипРасчетов,
	|	ВЫБОР КОГДА ТаблицаПлатежей.Сумма ЕСТЬ NULL ТОГДА
	|		Ложь
	|	ИНАЧЕ
	|		Истина
	|	КОНЕЦ КАК Выбран,
	|	ЕСТЬNULL(ТаблицаПлатежей.Сумма, 0) КАК Сумма,
	|	&ВалютаДокумента КАК ВалютаДокумента,
	|
	|	РасчетыСКлиентами.ЗаказКлиента.Дата КАК Дата,
	|	РасчетыСКлиентами.ЗаказКлиента.Номер КАК Номер,
	|	РасчетыСКлиентами.ЗаказКлиента КАК Заказ,
	|	РасчетыСКлиентами.Валюта КАК ВалютаВзаиморасчетов,
	|	АналитикаПоПартнерам.Организация КАК Организация,
	|	АналитикаПоПартнерам.Партнер КАК Партнер,
	|	АналитикаПоПартнерам.Контрагент КАК Контрагент,
	|	
	|	ВЫБОР КОГДА РасчетыСКлиентами.СуммаОстаток > 0 ТОГДА
	|		РасчетыСКлиентами.СуммаОстаток
	|	ИНАЧЕ
	|		0
	|	КОНЕЦ КАК ДолгПартнера,
	|	
	|	ВЫБОР КОГДА РасчетыСКлиентами.КОплатеОстаток > 0
	|		И Не &ТолькоБезусловнаяЗадолженность
	|	ТОГДА
	|		РасчетыСКлиентами.КОплатеОстаток
	|	ИНАЧЕ
	|		0
	|	КОНЕЦ КАК КОплате,
	|	
	|	ВЫБОР КОГДА РасчетыСКлиентами.СуммаОстаток < 0 ТОГДА
	|		-РасчетыСКлиентами.СуммаОстаток
	|	ИНАЧЕ
	|		0
	|	КОНЕЦ КАК НашДолг
	|ИЗ
	|	РегистрНакопления.РасчетыСКлиентами.Остатки(, 
	|		АналитикаУчетаПоПартнерам В (
	|			ВЫБРАТЬ
	|				АналитикаПоПартнерам.КлючАналитики КАК КлючАналитики
	|			ИЗ
	|				АналитикаУчетаПоПартнерам КАК АналитикаПоПартнерам
	|			)
	|	) КАК РасчетыСКлиентами
	|		
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ 
	|		АналитикаУчетаПоПартнерам КАК АналитикаПоПартнерам
	|	ПО 
	|		РасчетыСКлиентами.АналитикаУчетаПоПартнерам = АналитикаПоПартнерам.КлючАналитики
	|		
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		ТаблицаПлатежей КАК ТаблицаПлатежей
	|	ПО
	|		РасчетыСКлиентами.ЗаказКлиента = ТаблицаПлатежей.Заказ
	|		И РасчетыСКлиентами.Валюта = ТаблицаПлатежей.Валюта
	|
	|ГДЕ
	|	(Не &ДебиторскаяЗадолженность
	|	И РасчетыСКлиентами.СуммаОстаток < 0)
	|	ИЛИ 
	|	(&ДебиторскаяЗадолженность
	|	И РасчетыСКлиентами.КОплатеОстаток > 0
	|	И Не &ТолькоБезусловнаяЗадолженность
	|	)
	|	ИЛИ 
	|	(&ДебиторскаяЗадолженность
	|	И РасчетыСКлиентами.СуммаОстаток > 0
	|	)
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

&НаСервере
Процедура ЗаполнитьТаблицуПоРасчетамСПартнерами()
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Организация", ДоступныеОрганизации);
	Запрос.УстановитьПараметр("ВалютаДокумента", Валюта);
	Запрос.УстановитьПараметр("Контрагент", Контрагент);
	
	Если Не ЗначениеЗаполнено(Партнер) Тогда
		Запрос.УстановитьПараметр("Партнер", Неопределено);
	Иначе
		Запрос.УстановитьПараметр("Партнер", Партнер);
	КонецЕсли;
	Запрос.УстановитьПараметр("ПартнерПрочиеОтношения", ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПеречислениеТаможне);
	
	МассивРасчетыСПоставщиками = Новый Массив;
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.ОплатаПоставщику);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.ВозвратДенежныхСредствОтПоставщика);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.ПеречислениеТаможне);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.ВозвратТоваровПоставщику);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.СписаниеДебиторскойЗадолженности);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.СписаниеКредиторскойЗадолженности);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.ОплатаДенежныхСредствВДругуюОрганизацию);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.АвансовыйОтчет);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.ВозвратТоваровМеждуОрганизациями);
	ЕстьРасчетыСПоставщиками = МассивРасчетыСПоставщиками.Найти(ХозяйственнаяОперация) <> Неопределено;
	
	МассивРасчетыСКлиентами = Новый Массив;
	МассивРасчетыСКлиентами.Добавить(Перечисления.ХозяйственныеОперации.ПоступлениеОплатыОтКлиента);
	МассивРасчетыСКлиентами.Добавить(Перечисления.ХозяйственныеОперации.ВозвратОплатыКлиенту);
	МассивРасчетыСКлиентами.Добавить(Перечисления.ХозяйственныеОперации.ВозвратТоваровОтКлиента);
	МассивРасчетыСКлиентами.Добавить(Перечисления.ХозяйственныеОперации.ВозвратОтРозничногоПокупателя);
	МассивРасчетыСКлиентами.Добавить(Перечисления.ХозяйственныеОперации.СписаниеДебиторскойЗадолженности);
	МассивРасчетыСКлиентами.Добавить(Перечисления.ХозяйственныеОперации.СписаниеКредиторскойЗадолженности);
	МассивРасчетыСКлиентами.Добавить(Перечисления.ХозяйственныеОперации.ВозвратДенежныхСредствВДругуюОрганизацию);
	ЕстьРасчетыСКлиентами = МассивРасчетыСКлиентами.Найти(ХозяйственнаяОперация) <> Неопределено;
	
	Если Не ЕстьРасчетыСКлиентами И Не ЕстьРасчетыСПоставщиками Тогда
		Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru = 'Подбор по остаткам расчетов с партнерами не поддерживается для операции %1'"),
			ХозяйственнаяОперация
		);
		ВызватьИсключение Текст;
	КонецЕсли;
	
	МассивДебиторскаяЗадолженность = Новый Массив;
	МассивДебиторскаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.СписаниеДебиторскойЗадолженности);
	МассивДебиторскаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.ПоступлениеОплатыОтКлиента);
	МассивДебиторскаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.ВозвратТоваровОтКлиента);
	МассивДебиторскаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.ВозвратОтРозничногоПокупателя);
	МассивДебиторскаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.ВозвратДенежныхСредствОтПоставщика);
	Запрос.УстановитьПараметр("ДебиторскаяЗадолженность", МассивДебиторскаяЗадолженность.Найти(ХозяйственнаяОперация) <> Неопределено);
	
	МассивТолькоБезусловнаяЗадолженность = Новый Массив;
	МассивТолькоБезусловнаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.СписаниеДебиторскойЗадолженности);
	МассивТолькоБезусловнаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.СписаниеКредиторскойЗадолженности);
	МассивТолькоБезусловнаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.ВозвратОплатыКлиенту);
	МассивТолькоБезусловнаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.ВозвратДенежныхСредствОтПоставщика);
	МассивТолькоБезусловнаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.ВозвратДенежныхСредствВДругуюОрганизацию);
	Запрос.УстановитьПараметр("ТолькоБезусловнаяЗадолженность", МассивТолькоБезусловнаяЗадолженность.Найти(ХозяйственнаяОперация) <> Неопределено);
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	РасшифровкаПлатежа.Заказ КАК Заказ,
	|	РасшифровкаПлатежа.ВалютаВзаиморасчетов КАК Валюта,
	|	РасшифровкаПлатежа.Сумма КАК Сумма
	|
	|ПОМЕСТИТЬ ТаблицаПлатежей
	|ИЗ
	|	&РасшифровкаПлатежа КАК РасшифровкаПлатежа
	|;
	|///////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	АналитикаПоПартнерам.КлючАналитики КАК КлючАналитики,
	|	АналитикаПоПартнерам.Организация КАК Организация,
	|	АналитикаПоПартнерам.Партнер КАК Партнер,
	|	АналитикаПоПартнерам.Контрагент КАК Контрагент
	|
	|ПОМЕСТИТЬ АналитикаУчетаПоПартнерам
	|ИЗ
	|	РегистрСведений.АналитикаУчетаПоПартнерам КАК АналитикаПоПартнерам
	|ГДЕ
	|	&Партнер <> Неопределено
	|	И АналитикаПоПартнерам.Организация В (&Организация)
	|	И АналитикаПоПартнерам.Партнер = &Партнер
	|	И АналитикаПоПартнерам.Контрагент = &Контрагент
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	АналитикаПоПартнерам.КлючАналитики КАК КлючАналитики,
	|	АналитикаПоПартнерам.Организация КАК Организация,
	|	АналитикаПоПартнерам.Партнер КАК Партнер,
	|	АналитикаПоПартнерам.Контрагент КАК Контрагент
	|ИЗ
	|	РегистрСведений.АналитикаУчетаПоПартнерам КАК АналитикаПоПартнерам
	|ГДЕ
	|	&Партнер = Неопределено
	|	И АналитикаПоПартнерам.Организация В (&Организация)
	|	И АналитикаПоПартнерам.Контрагент = &Контрагент
	|	И (&ПартнерПрочиеОтношения И АналитикаПоПартнерам.Партнер.ПрочиеОтношения
	|		ИЛИ Не &ПартнерПрочиеОтношения)
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	КлючАналитики
	|;
	|///////////////////////////////////////////////////////////////////////////////
	|
	|//ТекстЗапросаРасчетыСКлиентами
	|
	|//ТекстОбъединитьВсе
	|
	|//ТекстЗапросаРасчетыСПоставщиками
	|
	|УПОРЯДОЧИТЬ ПО
	|	Организация,
	|	Контрагент,
	|	Дата,
	|	Номер
	|";
	Если ЕстьРасчетыСКлиентами Тогда
		ТекстЗапроса = СтрЗаменить(
			ТекстЗапроса,
			"//ТекстЗапросаРасчетыСКлиентами",
			ТекстЗапросаПоОстаткамРасчетовСКлиентами()
		);
	КонецЕсли;
	Если ЕстьРасчетыСПоставщиками Тогда
		ТекстЗапроса = СтрЗаменить(
			ТекстЗапроса,
			"//ТекстЗапросаРасчетыСПоставщиками",
			ТекстЗапросаПоОстаткамРасчетовСПоставщиками()
		);
	КонецЕсли;
	Если ЕстьРасчетыСКлиентами И ЕстьРасчетыСПоставщиками Тогда
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "//ТекстОбъединитьВсе", "ОБЪЕДИНИТЬ ВСЕ");
	КонецЕсли;

	Запрос.Текст = ТекстЗапроса;
	
	Если ЗначениеЗаполнено(АдресПлатежейВХранилище) Тогда
		РасшифровкаПлатежа = ПолучитьИзВременногоХранилища(АдресПлатежейВХранилище);
	Иначе
		РасшифровкаПлатежа = ТаблицаОстатковРасчетов.Выгрузить(,).СкопироватьКолонки();
	КонецЕсли;
	РасшифровкаПлатежа.Свернуть("Заказ, ВалютаВзаиморасчетов", "Сумма");
	Запрос.УстановитьПараметр("РасшифровкаПлатежа", РасшифровкаПлатежа);
	
	ТаблицаОстатковРасчетов.Загрузить(Запрос.Выполнить().Выгрузить());
	
	Если РасшифровкаПлатежа.Количество() = 0 Тогда
		СуммаКРаспределению = СуммаДокумента;
	Иначе
		СуммаКРаспределению = 0;
	КонецЕсли;
	
	СоответствиеВалютаКурс = Новый Соответствие;
	
	Для Каждого СтрокаТаблицы Из ТаблицаОстатковРасчетов Цикл
		
		Если Не СтрокаТаблицы.Выбран Тогда
			
			Если СтрокаТаблицы.КОплате <> 0 Тогда
				СтрокаТаблицы.Сумма = СтрокаТаблицы.КОплате;
				
			ИначеЕсли СтрокаТаблицы.НашДолг <> 0 Тогда
				СтрокаТаблицы.Сумма = СтрокаТаблицы.НашДолг;
				
			ИначеЕсли СтрокаТаблицы.ДолгПартнера <> 0 Тогда
				СтрокаТаблицы.Сумма = СтрокаТаблицы.ДолгПартнера;
				
			КонецЕсли;
			
			Если Валюта <> СтрокаТаблицы.ВалютаВзаиморасчетов Тогда
				
				Коэффициенты = СоответствиеВалютаКурс.Получить(СтрокаТаблицы.ВалютаВзаиморасчетов);
				Если Коэффициенты = Неопределено Тогда
					Коэффициенты = РаботаСКурсамивалютУТ.ПолучитьКоэффициентыПересчетаВалюты(Валюта, СтрокаТаблицы.ВалютаВзаиморасчетов, ?(ДатаДокумента <> '00010101', ДатаДокумента, ТекущаяДата()));
					СоответствиеВалютаКурс.Вставить(СтрокаТаблицы.ВалютаВзаиморасчетов, Коэффициенты);
				КонецЕсли;
					
				СтрокаТаблицы.Сумма = ?(Коэффициенты.КоэффициентПересчетаВВалютуВзаиморасчетов <> 0, СтрокаТаблицы.Сумма / Коэффициенты.КоэффициентПересчетаВВалютуВзаиморасчетов, 0);
				
			КонецЕсли;
			
		КонецЕсли;
		
		Если СуммаКРаспределению > 0 Тогда
			
			СтрокаТаблицы.Выбран = Истина;
			Если СтрокаТаблицы.Сумма > СуммаКРаспределению Тогда
				СтрокаТаблицы.Сумма = СуммаКРаспределению;
			КонецЕсли;
			СуммаКРаспределению = СуммаКРаспределению - СтрокаТаблицы.Сумма;
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ЗаполнитьИменаРеквизитовПоХозяйственнойОперации(ХозяйственнаяОперация, МассивВсехРеквизитов, МассивРеквизитовОперации)
	
	МассивВсехРеквизитов = Новый Массив;
	МассивВсехРеквизитов.Добавить("Партнер");
	МассивВсехРеквизитов.Добавить("Контрагент");
	МассивВсехРеквизитов.Добавить("ТаблицаОстатковРасчетов.Заказ");
	МассивВсехРеквизитов.Добавить("ТаблицаОстатковРасчетов.КОплате");
	МассивВсехРеквизитов.Добавить("ТаблицаОстатковРасчетов.КОплатеВалюта");
	МассивВсехРеквизитов.Добавить("ТаблицаОстатковРасчетов.НашДолг");
	МассивВсехРеквизитов.Добавить("ТаблицаОстатковРасчетов.ДолгПартнера");
	МассивВсехРеквизитов.Добавить("ТаблицаОстатковРасчетов.Партнер");
	МассивВсехРеквизитов.Добавить("ТаблицаОстатковРасчетов.ТипРасчетов");
	
	Если ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПоступлениеОплатыОтКлиента Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Партнер");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплате");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплатеВалюта");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.ДолгПартнера");
		
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратТоваровОтКлиента
	 ИЛИ ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратОтРозничногоПокупателя
	Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Партнер");
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплате");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплатеВалюта");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.ДолгПартнера");	
		
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратОплатыКлиенту Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Партнер");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.НашДолг");
		
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ОплатаПоставщику
	 ИЛИ ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПеречислениеТаможне Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Партнер");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплате");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплатеВалюта");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.НашДолг");
		
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратТоваровПоставщику Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Партнер");
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплате");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплатеВалюта");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.НашДолг");	
		
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ОплатаДенежныхСредствВДругуюОрганизацию
		Или ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратДенежныхСредствВДругуюОрганизацию
		Или ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратТоваровМеждуОрганизациями
	Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплате");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.НашДолг");	
	
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратДенежныхСредствОтПоставщика Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Партнер");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.ДолгПартнера");
		
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.СписаниеДебиторскойЗадолженности Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Партнер");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.ДолгПартнера");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.ТипРасчетов");
		
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.СписаниеКредиторскойЗадолженности Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Партнер");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.НашДолг");	
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.ТипРасчетов");
		
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.АвансовыйОтчет Тогда	
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Партнер");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплате");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплатеВалюта");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.НашДолг");	
		
	КонецЕсли;
	
КонецФункции

&НаКлиенте
Процедура РассчитатьСуммуПлатежей()
	
	СуммаПлатежей = 0;
	Для Каждого СтрокаТаблицы Из ТаблицаОстатковРасчетов Цикл
		
		Если СтрокаТаблицы.Выбран Тогда
			СуммаПлатежей = СуммаПлатежей + СтрокаТаблицы.Сумма;
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры
