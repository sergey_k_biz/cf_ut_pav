﻿////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ДЕЙСТВИЯ КОМАНД ФОРМЫ

&НаКлиенте
Процедура НастройкиКриптографии(Команда)
	
	ПараметрыФормы = Новый Структура("ПоказыватьНастройкиШифрования", Истина);
	ОткрытьФормуМодально("Общаяформа.НастройкиЭЦП", ПараметрыФормы);
	
КонецПроцедуры

&НаКлиенте
Процедура НастройкаЭЦП(Команда)
	
	СистемнаяИнформация = Новый СистемнаяИнформация();
	
	ВебКлиентВLinux = Ложь;
	Если СистемнаяИнформация.ТипПлатформы = ТипПлатформы.Linux_x86 ИЛИ СистемнаяИнформация.ТипПлатформы = ТипПлатформы.Linux_x86_64 Тогда
		ВебКлиентВLinux = Истина;
	КонецЕсли;
	
	ПараметрыФормы = Новый Структура("ВебКлиентВLinux", ВебКлиентВLinux);
	ОткрытьФорму("Общаяформа.ПерсональныеНастройкиЭЦП", ПараметрыФормы);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "ОбновитьСписокСертификатов" Тогда
		Элементы.Список.Обновить();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Если НЕ Константы.ИспользоватьЭлектронныеЦифровыеПодписи.Получить() Тогда
		ТекстСообщения = ЭлектронныеДокументыСлужебный.ТекстСообщенияОНеобходимостиНастройкиСистемы("НастройкаКриптографии");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,,,,Отказ);
	КонецЕсли;
КонецПроцедуры

