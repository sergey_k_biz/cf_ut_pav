﻿
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Параметры.Свойство("Идентификатор", Идентификатор);
	Заголовок = НСтр("ru='ККМ Offline'") + " """ + Строка(Идентификатор) + """";
	
	времБазаТоваров 	= Неопределено;
	времФайлОтчета 		= Неопределено;
	времФлагВыгрузки 	= Неопределено;
	Параметры.Свойство("БазаТоваров", 	времБазаТоваров);
	Параметры.Свойство("ФайлОтчета", 	времФайлОтчета);
	Параметры.Свойство("ФлагВыгрузки", 	времФлагВыгрузки);
	БазаТоваров 	= ?(времБазаТоваров  = Неопределено, "", времБазаТоваров);
	ФайлОтчета 		= ?(времФайлОтчета 	 = Неопределено, "", времФайлОтчета);
	ФлагВыгрузки 	= ?(времФлагВыгрузки = Неопределено, "", времФлагВыгрузки);
	
	Драйвер = НСтр("ru='Не требуется'");
	Версия  = НСтр("ru='Не определена'");
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьИЗакрыть(Команда)
	
	ОписаниеОшибки = "";

	Если Не ЗначениеЗаполнено(БазаТоваров) Тогда
		ОписаниеОшибки =  НСтр("ru = ' - Не указано имя файла базы товаров.'");
	КонецЕсли;

	Если Не ЗначениеЗаполнено(ФайлОтчета) Тогда
		ОписаниеОшибки = ОписаниеОшибки + ?(ПустаяСтрока(ОписаниеОшибки),
		                                    "",
		                                    "
		                                    |")
		                                + НСтр("ru= ' - Не указано имя файла отчёта.'");
	КонецЕсли;

	Если ПустаяСтрока(ОписаниеОшибки) Тогда
		Параметры.ПараметрыНастройки.Добавить(БазаТоваров,  "БазаТоваров");
		Параметры.ПараметрыНастройки.Добавить(ФайлОтчета,   "ФайлОтчета");
		Параметры.ПараметрыНастройки.Добавить(ФлагВыгрузки,	"ФлагВыгрузки");

		ОчиститьСообщения();
		Закрыть(КодВозвратаДиалога.ОК);
	Иначе
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru= 'При проверке были обнаружены следующие ошибки:'")+"
		               |" + ОписаниеОшибки);
	КонецЕсли;
				   
КонецПроцедуры

&НаКлиенте
Процедура БазаТоваровНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ВыбратьФайл(РежимДиалогаВыбораФайла.Открытие, БазаТоваров);
	СтандартнаяОбработка = Ложь;

КонецПроцедуры

&НаКлиенте
Процедура ФлагВыгрузкиНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ВыбратьФайл(РежимДиалогаВыбораФайла.Открытие, ФлагВыгрузки);
	СтандартнаяОбработка = Ложь;

КонецПроцедуры

&НаКлиенте
Процедура ФайлОтчётаНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ВыбратьФайл(РежимДиалогаВыбораФайла.Открытие, ФайлОтчета);
	СтандартнаяОбработка = Ложь;

КонецПроцедуры

&НаКлиенте
Процедура ВыбратьФайл(РежимОткрытияФайла, ИмяФайла)
	Диалог = Новый ДиалогВыбораФайла(РежимОткрытияФайла);
	Диалог.МножественныйВыбор = Ложь;
	Диалог.ПолноеИмяФайла = ИмяФайла;

	Если Диалог.Выбрать() Тогда
		ИмяФайла = Диалог.ПолноеИмяФайла;
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ТестУстройства(Команда)
	РезультатТеста = Неопределено;

	ВходныеПараметры  = Неопределено;
	ВыходныеПараметры = Неопределено;

	времПараметрыУстройства = Новый Структура();
	времПараметрыУстройства.Вставить("БазаТоваров", БазаТоваров);
	времПараметрыУстройства.Вставить("ФайлОтчета", 	ФайлОтчета);
	времПараметрыУстройства.Вставить("ФлагВыгрузки",ФлагВыгрузки);

	Результат = МенеджерОборудованияКлиент.ВыполнитьДополнительнуюКоманду("ТестУстройства",
	                                                                      ВходныеПараметры,
	                                                                      ВыходныеПараметры,
	                                                                      Идентификатор,
	                                                                      времПараметрыУстройства);

	ДополнительноеОписание = ?(ТипЗнч(ВыходныеПараметры) = Тип("Массив")
	                           И ВыходныеПараметры.Количество() >= 2,
	                           НСтр("ru = 'Дополнительное описание:'") + " " + ВыходныеПараметры[1],
	                           "");
	Если Результат Тогда
		ТекстСообщения = НСтр("ru = 'Тест успешно выполнен.%ПереводСтроки%%ДополнительноеОписание%'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ПереводСтроки%", ?(ПустаяСтрока(ДополнительноеОписание),
		                                                                  "",
		                                                                  Символы.ПС));
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ДополнительноеОписание%", ?(ПустаяСтрока(ДополнительноеОписание),
		                                                                           "",
		                                                                           ДополнительноеОписание));
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
	Иначе
		ТекстСообщения = НСтр("ru = 'Тест не пройден.%ПереводСтроки%%ДополнительноеОписание%'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ПереводСтроки%", ?(ПустаяСтрока(ДополнительноеОписание),
		                                                                  "",
		                                                                  Символы.ПС));
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ДополнительноеОписание%", ?(ПустаяСтрока(ДополнительноеОписание),
		                                                                           "",
		                                                                           ДополнительноеОписание));
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
	КонецЕсли;
КонецПроцедуры
