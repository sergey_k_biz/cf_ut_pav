﻿
&НаКлиенте

Процедура ОК(Команда)
	СтруктураПараметров = Новый Структура("Заказ,Варианты");
	//СтруктураПараметров.Вставить("Заказ",msЗаказ);
	
	Для каждого стр Из ТЧ Цикл	
		Если стр.Выбран Тогда
			ТекВыбЗначение=стр.ЗначениеСсылка;		
		КонецЕсли; 			
	КонецЦикла;
	Выб=ложь;
	Если ЗначениеЗаполнено(ТекВыбЗначение) Тогда
		
		Выб=истина;
		
	КонецЕсли; 
	
	Если ЗначениеЗаполнено(РучнойПроцент) и РучнойПроцент > 0 Тогда
		
		Выб=истина;
		
	КонецЕсли; 
	
	Если не Выб Тогда
		
		
		Предупреждение("Не заполнено значение");
		
	иначе
		СтруктураПараметров.Вставить("ЗначениеСсылка",ТекВыбЗначение);
		СтруктураПараметров.Вставить("Статус","ОК");
		СтруктураПараметров.Вставить("Ручная",ЭтаФорма.РучнойПроцент);
		Закрыть(СтруктураПараметров);  
	КонецЕсли;
	
	
	
КонецПроцедуры


&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	//НовыйРеквизит  = Новый РеквизитФормы("Варианты",Новый ОписаниеТипов("Число"),,"Виды наценок");
	//ДобавляемыеРеквизиты = новый Массив;
	//ДобавляемыеРеквизиты.Добавить(НовыйРеквизит);
	//ИзменитьРеквизиты(ДобавляемыеРеквизиты);
	//НовыйЭлемент = Элементы.Добавить("ПолеВариант",Тип("ПолеФормы"),Элементы.ГруппаВариантов);
	//НовыйЭлемент.ПутьКДанным="Варианты";
	//
	ТЧ_=ЗначениеИзСтрокиВнутр(msОбщийСервер.ВернутьТаблицуПараметровСкидок());
	ЧислоКолонок=ТЧ_.количество();
	Если ЧислоКолонок > 0 Тогда
	//	
	//	НовыйЭлемент.Вид = ВидПоляФормы.ПолеПереключателя;
	//	НовыйЭлемент.КоличествоКолонок=ЧислоКолонок;
	//	
		Для каждого стр Из ТЧ_ Цикл
			Если стр.Ссылка=Справочники.msВидыНаценки.Вручную Тогда
				
				Продолжить;
					
			КонецЕсли; 
			
			новстрока=тч.добавить();
			новстрока.Название=стр.Наименование+" - "+стр.Процент+"%";
			новстрока.Выбран=ложь;
			новстрока.ЗначениеСсылка=стр.Ссылка;

			
			
			
			//НовыйЭлемент.СписокВыбора.добавить(стр.НомерПП,стр.наименование+" "+стр.процент+"%");	
			
		КонецЦикла;
	//	
	//	
	//	
	КонецЕсли; 
КонецПроцедуры

&НаКлиенте
Процедура Закрыть_(Команда)
	СтруктураПараметров = Новый Структура("Заказ,Варианты");
	СтруктураПараметров.Вставить("Статус","Закрыта");	
	Закрыть(); 
КонецПроцедуры


&НаКлиенте
Процедура ТЧВыбранПриИзменении(Элемент)
	
	Для каждого стр Из ТЧ Цикл
	
		
		
	
	КонецЦикла;
	//ТЧВыбранПриИзмененииНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ТЧПриИзменении(Элемент)
	Если Элемент.ТекущиеДанные.Выбран Тогда
		
		РучнойПроцент=0;
		
	
	КонецЕсли; 
	Для каждого стр Из ТЧ Цикл
		
		Если Элемент.ТекущиеДанные.Название<>стр.Название Тогда
	   
	   	  стр.Выбран=ложь;
	   
	   КонецЕсли; 
		
	КонецЦикла;

КонецПроцедуры

&НаКлиенте
Процедура РучнойПроцентПриИзменении(Элемент)
	Попытка
	
	Текчисло=Число(Элемент.ТекстРедактирования);	
	
	Исключение
	
	КонецПопытки;
	Если ЗначениеЗаполнено(Текчисло) и Текчисло>0 Тогда
	
	Для каждого стр Из ТЧ Цикл
		
		//Если Элемент.ТекущиеДанные.Название<>стр.Название Тогда
	   
	   	  стр.Выбран=ложь;
	   
	   //КонецЕсли; 
		
	КонецЦикла;
	
	
	КонецЕсли; 
КонецПроцедуры

&НаКлиенте
Процедура ТЧПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	Отказ=истина;
КонецПроцедуры

&НаКлиенте
Процедура ТЧПередУдалением(Элемент, Отказ)
	Отказ=истина;
КонецПроцедуры


//&НаСервере
//Процедура ТЧВыбранПриИзмененииНаСервере()
//	
//	  перес=1;
//	
//КонецПроцедуры

