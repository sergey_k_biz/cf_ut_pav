﻿
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	Если УправлениеПечатьюКлиент.ПроверитьДокументыПроведены(ПараметрКоманды, ПараметрыВыполненияКоманды.Источник) Тогда
		УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(
			"Документ.ЧекККМВозврат",
			"КМ3",
			ПараметрКоманды,
			ПараметрыВыполненияКоманды.Источник,
			Неопределено
		);
	КонецЕсли;
	
КонецПроцедуры
