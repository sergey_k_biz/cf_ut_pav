﻿////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС

//Возвращает параметры указания серий для товаров, указанных в документе
//Параметры
//			Объект - ДокументОбъект или ДанныеФормыСтруктура - документ, для которого нужно сфомировать параметры проверки
//Возвращаемое значение
//			Тип Структура
//				Состав полей определяется требованиями фукнции ОбработкаТабличнойЧастиСервер.ЗаполнитьСтатусыУказанияСерий
Функция ПараметрыУказанияСерий(Объект)Экспорт
	
	ПоляСвязи = Новый Массив;
	СкладскиеОперации = Новый Массив;
	СкладскиеОперации.Добавить(Перечисления.СкладскиеОперации.Пересчет);
	
	Если СкладыСервер.ИспользоватьАдресноеХранение(Объект.Склад,Объект.Помещение) Тогда
		ПоляСвязи.Добавить("Ячейка");
		ПоляСвязи.Добавить("Упаковка");
	КонецЕсли;
	
	ПараметрыУказанияСерий = Новый Структура;
	ПараметрыУказанияСерий.Вставить("ИспользоватьСерииНоменклатуры", 
		ПолучитьФункциональнуюОпцию("ИспользоватьСерииНоменклатурыСклад",Новый Структура("Склад",Объект.Склад)));
	ПараметрыУказанияСерий.Вставить("ПоляСвязи",ПоляСвязи);
	ПараметрыУказанияСерий.Вставить("ЭтоОрдер", Истина);
	ПараметрыУказанияСерий.Вставить("ИмяТЧСерии", "Товары");
	ПараметрыУказанияСерий.Вставить("Склад", Объект.Склад);
	
	ПараметрыУказанияСерий.Вставить("СкладскиеОперации", СкладскиеОперации);
	
	Возврат ПараметрыУказанияСерий;
КонецФункции

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// Проведение

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства) Экспорт

	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ПересчетТоваров.Дата КАК Период,
	|	ПересчетТоваров.Статус КАК Статус,
	|	ПересчетТоваров.Склад КАК Склад,
	|	ПересчетТоваров.Склад.ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач КАК ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач,
	|	ВЫБОР
	|		КОГДА ПересчетТоваров.Склад.ИспользоватьСкладскиеПомещения
	|			ТОГДА ПересчетТоваров.Помещение.ИспользоватьАдресноеХранение
	|		ИНАЧЕ ПересчетТоваров.Склад.ИспользоватьАдресноеХранение
	|	КОНЕЦ КАК ИспользоватьАдресноеХранение,
	|	ПересчетТоваров.Помещение КАК Помещение,
	|	ПересчетТоваров.БлокироватьЯчейки КАК БлокироватьЯчейки,
	|	ПересчетТоваров.ДокументОснование КАК ДокументОснование
	|ИЗ
	|	Документ.ПересчетТоваров КАК ПересчетТоваров
	|ГДЕ
	|	ПересчетТоваров.Ссылка = &Ссылка";
	Реквизиты = Запрос.Выполнить().Выбрать();
	Реквизиты.Следующий();

	Запрос.УстановитьПараметр("Статус",      Реквизиты.Статус);
	Запрос.УстановитьПараметр("Период",      Реквизиты.Период);
	Запрос.УстановитьПараметр("ИспользоватьАдресноеХранение", Реквизиты.ИспользоватьАдресноеХранение);
	Запрос.УстановитьПараметр("ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач",Реквизиты.ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач);
	Запрос.УстановитьПараметр("Склад", Реквизиты.Склад);
	Запрос.УстановитьПараметр("Помещение", Реквизиты.Помещение);
 	Запрос.УстановитьПараметр("ДокументОснование", Реквизиты.ДокументОснование);
 	Запрос.УстановитьПараметр("БлокироватьЯчейки", Реквизиты.БлокироватьЯчейки);
                               
	Запрос.Текст =
	"
	// 0 ТаблицаТоварыКОформлениюИзлишковНедостач
	|ВЫБРАТЬ
	|	ПересчетТоваровТовары.Номенклатура КАК Номенклатура,
	|	ПересчетТоваровТовары.Характеристика КАК Характеристика,
	|	ВЫБОР
	|		КОГДА &ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач
	|			ТОГДА СУММА(ПересчетТоваровТовары.КоличествоФакт - ПересчетТоваровТовары.Количество)
	|		ИНАЧЕ 0
	|	КОНЕЦ КАК КОформлениюОрдеров,
	|	ВЫБОР
	|		КОГДА &ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач
	|			ТОГДА 0
	|		ИНАЧЕ СУММА(ПересчетТоваровТовары.КоличествоФакт - ПересчетТоваровТовары.Количество)
	|	КОНЕЦ КАК КОформлениюАктов,
	|	ПересчетТоваровТовары.Серия КАК Серия,
	|	&Период КАК Период,
	|	&Склад КАК Склад,
	|	&Помещение КАК Помещение,
	|	&ДокументОснование КАК ДокументОснование,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
	|	МАКСИМУМ(ПересчетТоваровТовары.НомерСтроки) КАК НомерСтроки
	|ИЗ
	|	Документ.ПересчетТоваров.Товары КАК ПересчетТоваровТовары
	|ГДЕ
	|	&Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыПересчетовТоваров.Выполнено)
	|	И ПересчетТоваровТовары.Ссылка = &Ссылка
	|
	|СГРУППИРОВАТЬ ПО
	|	ПересчетТоваровТовары.Номенклатура,
	|	ПересчетТоваровТовары.Характеристика,
	|	ПересчетТоваровТовары.Серия
	|
	|ИМЕЮЩИЕ
	|	СУММА(ПересчетТоваровТовары.КоличествоФакт - ПересчетТоваровТовары.Количество) > 0
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ПересчетТоваровТовары.Номенклатура,
	|	ПересчетТоваровТовары.Характеристика,
	|	ВЫБОР
	|		КОГДА &ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач
	|			ТОГДА -СУММА(ПересчетТоваровТовары.КоличествоФакт - ПересчетТоваровТовары.Количество)
	|		ИНАЧЕ 0
	|	КОНЕЦ,
	|	ВЫБОР
	|		КОГДА &ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач
	|			ТОГДА 0
	|		ИНАЧЕ -СУММА(ПересчетТоваровТовары.КоличествоФакт - ПересчетТоваровТовары.Количество)
	|	КОНЕЦ,
	|	ПересчетТоваровТовары.Серия КАК Серия,
	|	&Период,
	|	&Склад,
	|	&Помещение,
	|	&ДокументОснование,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход),
	|	МАКСИМУМ(ПересчетТоваровТовары.НомерСтроки)
	|ИЗ
	|	Документ.ПересчетТоваров.Товары КАК ПересчетТоваровТовары
	|ГДЕ
	|	&Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыПересчетовТоваров.Выполнено)
	|	И ПересчетТоваровТовары.Ссылка = &Ссылка
	|
	|СГРУППИРОВАТЬ ПО
	|	ПересчетТоваровТовары.Номенклатура,
	|	ПересчетТоваровТовары.Характеристика,
	|	ПересчетТоваровТовары.Серия
	|
	|ИМЕЮЩИЕ
	|	СУММА(ПересчетТоваровТовары.КоличествоФакт - ПересчетТоваровТовары.Количество) < 0
	|;
	// 1 ТаблицаТоварыВСкладскихЯчейках
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаТовары.Номенклатура КАК Номенклатура,
	|	ТаблицаТовары.Упаковка КАК Упаковка,
	|	ТаблицаТовары.Характеристика КАК Характеристика,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияСерий В (4, 6, 8, 10)
	|			ТОГДА ТаблицаТовары.Серия
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ КАК Серия,
	|	ТаблицаТовары.КоличествоУпаковокФакт - ТаблицаТовары.КоличествоУпаковок КАК ВНаличии,
	|	ТаблицаТовары.Ячейка КАК Ячейка,
	|	&Период КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
	|	ТаблицаТовары.НомерСтроки КАК НомерСтроки
	|ИЗ
	|	Документ.ПересчетТоваров.Товары КАК ТаблицаТовары
	|ГДЕ
	|	&ИспользоватьАдресноеХранение
	|	И &Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыПересчетовТоваров.Выполнено)
	|	И ТаблицаТовары.Ссылка = &Ссылка
	|	И ТаблицаТовары.КоличествоУпаковокФакт - ТаблицаТовары.КоличествоУпаковок > 0
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ТаблицаТовары.Номенклатура,
	|	ТаблицаТовары.Упаковка,
	|	ТаблицаТовары.Характеристика,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияСерий В (4, 6, 8, 10)
	|			ТОГДА ТаблицаТовары.Серия
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ КАК Серия,
	|	-(ТаблицаТовары.КоличествоУпаковокФакт - ТаблицаТовары.КоличествоУпаковок),
	|	ТаблицаТовары.Ячейка,
	|	&Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход),
	|	ТаблицаТовары.НомерСтроки
	|ИЗ
	|	Документ.ПересчетТоваров.Товары КАК ТаблицаТовары
	|ГДЕ
	|	&ИспользоватьАдресноеХранение
	|	И &Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыПересчетовТоваров.Выполнено)
	|	И ТаблицаТовары.Ссылка = &Ссылка
	|	И ТаблицаТовары.КоличествоУпаковокФакт - ТаблицаТовары.КоличествоУпаковок < 0
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|
	// 2 ТаблицаБлокировкиСкладскихЯчеек
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПересчетТоваровТовары.Ячейка КАК Ячейка,
	|	ЗНАЧЕНИЕ(Перечисление.ТипыБлокировокСкладскихЯчеек.Полная) КАК ТипБлокировки,
	|	МАКСИМУМ(ПересчетТоваровТовары.НомерСтроки) КАК НомерСтроки
	|ИЗ
	|	Документ.ПересчетТоваров.Товары КАК ПересчетТоваровТовары
	|ГДЕ
	|	&ИспользоватьАдресноеХранение
	|	И &БлокироватьЯчейки
	|	И ПересчетТоваровТовары.Ссылка = &Ссылка
	|
	|СГРУППИРОВАТЬ ПО
	|	ПересчетТоваровТовары.Ячейка
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;	
	// 3 ТаблицаДвиженияСерийТоваров
	|ВЫБРАТЬ
	|	ТаблицаСерии.Номенклатура             КАК Номенклатура,
	|	ТаблицаСерии.Характеристика           КАК Характеристика,
	|	ТаблицаСерии.Серия                    КАК Серия,
	|	ТаблицаСерии.КоличествоФакт           КАК Количество,
	|	&Ссылка                               КАК Документ,
	|	&Период                               КАК Период,
	|	&Ссылка                               КАК Регистратор,
	|	&Склад                                КАК Склад,
	|	&Помещение                            КАК Помещение,
	|	ЗНАЧЕНИЕ(Перечисление.СкладскиеОперации.Пересчет) КАК СкладскаяОперация	
	|ИЗ
	|	Документ.ПересчетТоваров.Товары КАК ТаблицаСерии
	|
	|ГДЕ
	|	ТаблицаСерии.Ссылка = &Ссылка
	|	И &Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыПересчетовТоваров.Выполнено)
	|	И ТаблицаСерии.Серия <> ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки";
	
	Результат = Запрос.ВыполнитьПакет();

	ДополнительныеСвойства.ТаблицыДляДвижений.Вставить("ТаблицаТоварыКОформлениюИзлишковНедостач", Результат[0].Выгрузить());
 	ДополнительныеСвойства.ТаблицыДляДвижений.Вставить("ТаблицаТоварыВСкладскихЯчейках",           Результат[1].Выгрузить());
  	ДополнительныеСвойства.ТаблицыДляДвижений.Вставить("ТаблицаБлокировкиСкладскихЯчеек",          Результат[2].Выгрузить());
  	ДополнительныеСвойства.ТаблицыДляДвижений.Вставить("ТаблицаДвиженияСерийТоваров",              Результат[3].Выгрузить());

КонецПроцедуры // ИнициализироватьДанныеДокумента()

////////////////////////////////////////////////////////////////////////////////
// Печать

Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "ЗаданиеНаПересчет") Тогда
		
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(
			КоллекцияПечатныхФорм,
			"ЗаданиеНаПересчет",
			"Задание на пересчет товаров",
			ПечатьЗадания(МассивОбъектов, ОбъектыПечати, ПараметрыПечати)
		);
	КонецЕсли;
	
КонецПроцедуры

Функция ПечатьЗадания(МассивОбъектов, ОбъектыПечати, ПараметрыПечати)
	КолонкаКодов = ФормированиеПечатныхФорм.ИмяДополнительнойКолонки();
	
	ВыводитьКоды 	= ЗначениеЗаполнено(КолонкаКодов);
	ВыводитьГрадацииКачества    = ПараметрыПечати.ВыводитьГрадацииКачества;
	ВыводитьРезультатыПересчета = ПараметрыПечати.ВыводитьРезультатыПересчета;
	
	ТабличныйДокумент = Новый ТабличныйДокумент;
	ТабличныйДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ПересчетТоваров_ЗаданиеНаПересчетТоваров";
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	ПересчетТоваров.Ссылка КАК Ссылка,
	|	ПересчетТоваров.Представление КАК ПредставлениеДокумента,
	|	ПересчетТоваров.Дата КАК Дата,
	|	ПересчетТоваров.Номер КАК Номер,
	|	ПересчетТоваров.Склад КАК Склад,
	|	ПересчетТоваров.Помещение КАК Помещение,
	|	ПересчетТоваров.ЯчейкаКонсолидацииИзлишковТоваров КАК ЯчейкаИзлишков,
	|	ПересчетТоваров.ЯчейкаКонсолидацииИспорченныхТоваров КАК ЯчейкаИспорченных,
	|	ПРЕДСТАВЛЕНИЕ(ПересчетТоваров.Склад) КАК СкладПредставление,
	|	ПРЕДСТАВЛЕНИЕ(ПересчетТоваров.Помещение) КАК ПомещениеПредставление,
	|	ПересчетТоваров.Исполнитель.ФизическоеЛицо КАК Исполнитель,
	|	ПересчетТоваров.ПечататьКоличествоПоУчету КАК ПечататьКоличествоПоУчету,
	|	ПересчетТоваров.Статус КАК Статус
	|ИЗ
	|	Документ.ПересчетТоваров КАК ПересчетТоваров
	|ГДЕ
	|	ПересчетТоваров.Ссылка В(&МассивОбъектов)
	|
	|УПОРЯДОЧИТЬ ПО
	|	ПересчетТоваров.Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПересчетТоваровТовары.НомерСтроки 									КАК НомерСтроки,
	|	ПересчетТоваровТовары.Количество 									КАК Количество,
	|	ПересчетТоваровТовары.КоличествоУпаковок 							КАК КоличествоУпаковок,";
	Если ВыводитьРезультатыПересчета Тогда
		ТекстЗапроса = ТекстЗапроса + "
		|	ПересчетТоваровТовары.КоличествоФакт								КАК КоличествоФакт,
		|	ПересчетТоваровТовары.КоличествоУпаковокФакт						КАК КоличествоУпаковокФакт,";
	Иначе
		ТекстЗапроса = ТекстЗапроса + "
		|	0								                                   КАК КоличествоФакт,
		|	0						                                           КАК КоличествоУпаковокФакт,";
	КонецЕсли;
	ТекстЗапроса = ТекстЗапроса + "
	|	ПересчетТоваровТовары.Номенклатура.НаименованиеПолное 				КАК НоменклатураПредставление,
	|	ПересчетТоваровТовары.Характеристика.НаименованиеПолное 			КАК ХарактеристикаПредставление,
	|	ВЫБОР КОГДА ЕСТЬNULL(ПересчетТоваровТовары.Упаковка.Коэффициент, 1) = 1
	|		ТОГДА НЕОПРЕДЕЛЕНО
	|		ИНАЧЕ ПересчетТоваровТовары.Упаковка.Наименование
	|	КОНЕЦ 																КАК УпаковкаПредставление,
	|	ПересчетТоваровТовары.Серия.Наименование                			КАК СерияПредставление,
	|	ПРЕДСТАВЛЕНИЕ(ПересчетТоваровТовары.Номенклатура.ЕдиницаИзмерения) 	КАК ЕдиницаИзмеренияПредставлениеНоменклатура,
	|	ПРЕДСТАВЛЕНИЕ(ПересчетТоваровТовары.Упаковка.ЕдиницаИзмерения) 		КАК ЕдиницаИзмеренияПредставлениеУпаковка,
	|	ПРЕДСТАВЛЕНИЕ(ПересчетТоваровТовары.Ячейка) 						КАК ЯчейкаПредставление,
	|	ПересчетТоваровТовары.Номенклатура.Код 								КАК Код,
	|	ПересчетТоваровТовары.Номенклатура.Артикул 							КАК Артикул,
	|	ПересчетТоваровТовары.Ссылка 										КАК Ссылка
	|ИЗ
	|	Документ.ПересчетТоваров.Товары КАК ПересчетТоваровТовары
	|ГДЕ
	|	ПересчетТоваровТовары.Ссылка В(&МассивОбъектов)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Ссылка,
	|	НомерСтроки
	|ИТОГИ ПО
	|	Ссылка";
	Запрос = Новый Запрос;
	Запрос.Текст = ТекстЗапроса; 
	Запрос.УстановитьПараметр("МассивОбъектов", МассивОбъектов);
	
	Результаты = Запрос.ВыполнитьПакет();
	
	ВыборкаПоДокументам = Результаты[0].Выбрать();
	ВыборкаПоТабличнымЧастям = Результаты[1].Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
	
	ПервыйДокумент = Истина;
	
	
	Если ВыводитьРезультатыПересчета Тогда
		СинонимДокумента = НСтр("ru='Результаты пересчета товаров'");
	Иначе
		СинонимДокумента = НСтр("ru='Задание на пересчет товаров'");
	КонецЕсли;
	
	Пока ВыборкаПоДокументам.Следующий() Цикл
		РеквизитыДокумента = Новый Структура("Номер, Дата, Префикс");
		ЗаполнитьЗначенияСвойств(РеквизитыДокумента, ВыборкаПоДокументам);
		Заголовок = ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(РеквизитыДокумента, СинонимДокумента);
		
		Если ВыводитьРезультатыПересчета
			И Не ВыборкаПоДокументам.Статус = Перечисления.СтатусыПересчетовТоваров.Выполнено Тогда
			
			ТекстСообщения = НСтр("ru = 'Документ ""%ПредставлениеДокумента%"" находится в статусе ""%Статус%"". Печать результатов пересчета возможна только в статусе ""Выполнено""'");
			
			РеквизитыДокумента.Вставить("Представление",ВыборкаПоДокументам.ПредставлениеДокумента);
			ПредставлениеДокумента = ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(РеквизитыДокумента);
			
			ТекстСообщения = СтрЗаменить(ТекстСообщения,"%ПредставлениеДокумента%",ПредставлениеДокумента);
			ТекстСообщения = СтрЗаменить(ТекстСообщения,"%Статус%",ВыборкаПоДокументам.Статус);
			
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
			Продолжить;
			
		КонецЕсли;

		
		ВыводитьПоУчету = ВыборкаПоДокументам.ПечататьКоличествоПоУчету Или ВыводитьРезультатыПересчета;
		ПоАдресномуСкладу = СкладыСервер.ИспользоватьАдресноеХранение(ВыборкаПоДокументам.Склад, ВыборкаПоДокументам.Помещение);
		Макет = УправлениеПечатью.ПолучитьМакет("Документ.ПересчетТоваров.ПФ_MXL_ЗаданиеПересчетТоваров");
			
		ОбластьЗаголовок 		= Макет.ПолучитьОбласть("Заголовок");
		ОбластьСкладИсполнитель = Макет.ПолучитьОбласть("СкладИсполнитель");
		ОбластьЯчейкаИзлишков	= Макет.ПолучитьОбласть("ОбластьЯчейкаИзлишков");
		ОбластьЯчейкаИспорченных	= Макет.ПолучитьОбласть("ОбластьЯчейкаИспорченных");
		
		Если ПоАдресномуСкладу Тогда
			
			Если ВыводитьКоды Тогда
				ОбластьШапкаТаблицыНачало 			= Макет.ПолучитьОбласть("ШапкаТаблицыЯчейкаКодУчет|НачалоСтроки");
				ОбластьСтрокаТаблицыНачало 			= Макет.ПолучитьОбласть("СтрокаТаблицыЯчейкаКодУчет|НачалоСтроки");
				ОбластьПодвалТаблицыНачало 			= Макет.ПолучитьОбласть("ПодвалТаблицыЯчейкаКодУчет|НачалоСтроки");
				ОбластьШапкаТаблицыКолонкаКодов 	= Макет.ПолучитьОбласть("ШапкаТаблицыЯчейкаКодУчет|КолонкаКодов");
				ОбластьСтрокаТаблицыКолонкаКодов 	= Макет.ПолучитьОбласть("СтрокаТаблицыЯчейкаКодУчет|КолонкаКодов");
				ОбластьПодвалТаблицыКолонкаКодов 	= Макет.ПолучитьОбласть("ПодвалТаблицыЯчейкаКодУчет|КолонкаКодов");
				ОбластьШапкаТаблицыКолонкаТоваров 	= Макет.ПолучитьОбласть("ШапкаТаблицыЯчейкаКодУчет|КолонкаТоваровКодЯчейка");
				ОбластьСтрокаТаблицыКолонкаТоваров 	= Макет.ПолучитьОбласть("СтрокаТаблицыЯчейкаКодУчет|КолонкаТоваровКодЯчейка");
				ОбластьПодвалТаблицыКолонкаТоваров 	= Макет.ПолучитьОбласть("ПодвалТаблицыЯчейкаКодУчет|КолонкаТоваровКодЯчейка");
			Иначе
				ОбластьШапкаТаблицыНачало 			= Макет.ПолучитьОбласть("ШапкаТаблицыЯчейка|НачалоСтроки");
				ОбластьСтрокаТаблицыНачало 			= Макет.ПолучитьОбласть("СтрокаТаблицыЯчейка|НачалоСтроки");
				ОбластьПодвалТаблицыНачало 			= Макет.ПолучитьОбласть("ПодвалТаблицыЯчейка|НачалоСтроки");
				ОбластьШапкаТаблицыКолонкаТоваров 	= Макет.ПолучитьОбласть("ШапкаТаблицыЯчейка|КолонкаТоваровБезКодовЯчейки");
				ОбластьСтрокаТаблицыКолонкаТоваров 	= Макет.ПолучитьОбласть("СтрокаТаблицыЯчейка|КолонкаТоваровБезКодовЯчейки");
				ОбластьПодвалТаблицыКолонкаТоваров 	= Макет.ПолучитьОбласть("ПодвалТаблицыЯчейка|КолонкаТоваровБезКодовЯчейки");
			КонецЕсли;
		Иначе 
			ОбластьШапкаТаблицыНачало 			= Макет.ПолучитьОбласть("ШапкаТаблицы|Номер");
			ОбластьСтрокаТаблицыНачало 			= Макет.ПолучитьОбласть("СтрокаТаблицы|Номер");
			ОбластьПодвалТаблицыНачало 			= Макет.ПолучитьОбласть("ПодвалТаблицы|Номер");
			
			Если ВыводитьКоды Тогда
				
				ОбластьШапкаТаблицыКолонкаКодов 	= Макет.ПолучитьОбласть("ШапкаТаблицыКодУчет|КолонкаЯчейкаКод");
				ОбластьСтрокаТаблицыКолонкаКодов 	= Макет.ПолучитьОбласть("СтрокаТаблицыКодУчет|КолонкаЯчейкаКод");
				ОбластьПодвалТаблицыКолонкаКодов 	= Макет.ПолучитьОбласть("ПодвалТаблицыКодУчет|КолонкаЯчейкаКод");
				ОбластьШапкаТаблицыКолонкаТоваров 	= Макет.ПолучитьОбласть("ШапкаТаблицыКодУчет|КолонкаТоваровБезКодовЯчейки");
				ОбластьСтрокаТаблицыКолонкаТоваров 	= Макет.ПолучитьОбласть("СтрокаТаблицыКодУчет|КолонкаТоваровБезКодовЯчейки");
				ОбластьПодвалТаблицыКолонкаТоваров 	= Макет.ПолучитьОбласть("ПодвалТаблицыКодУчет|КолонкаТоваровБезКодовЯчейки");
			Иначе 
				ОбластьШапкаТаблицыКолонкаТоваров 	= Макет.ПолучитьОбласть("ШапкаТаблицы|КолонкаТоваров");
				ОбластьСтрокаТаблицыКолонкаТоваров 	= Макет.ПолучитьОбласть("СтрокаТаблицы|КолонкаТоваров");
				ОбластьПодвалТаблицыКолонкаТоваров 	= Макет.ПолучитьОбласть("ПодвалТаблицы|КолонкаТоваров");
			КонецЕсли; 
		КонецЕсли;
		
				ОбластьШапкаТаблицыУпаковки 	= Макет.ПолучитьОбласть("ШапкаТаблицы|КолонкаУпаковок");
				ОбластьСтрокаТаблицыУпаковки 	= Макет.ПолучитьОбласть(?(ПоАдресномуСкладу, "СтрокаТаблицыЯчейка|КолонкаУпаковок", "СтрокаТаблицы|КолонкаУпаковок"));
				ОбластьПодвалТаблицыУпаковки 	= Макет.ПолучитьОбласть("ПодвалТаблицы|КолонкаУпаковок");
				
		Если ВыводитьКоды Тогда
			
			ОбластьШапкаТаблицыКолонкаКодов.Параметры.ИмяКолонкиКодов = КолонкаКодов; 
			
		КонецЕсли;	
		
		Если Не ВыводитьПоУчету Тогда
			Если ВыводитьГрадацииКачества Тогда
				ОбластьШапкаТаблицыФакт 	= Макет.ПолучитьОбласть("ШапкаТаблицыГрадацииКачества|ГрадацииКачества");
				ОбластьСтрокаТаблицыФакт 	= Макет.ПолучитьОбласть("СтрокаТаблицыГрадацииКачества|ГрадацииКачества");
				ОбластьПодвалТаблицыФакт 	= Макет.ПолучитьОбласть("ПодвалТаблицыГрадацииКачества|ГрадацииКачества");
			Иначе 	
				ОбластьШапкаТаблицыФакт 	= Макет.ПолучитьОбласть("ШапкаТаблицы|ФактБезКоличестваПоУчету");
				ОбластьСтрокаТаблицыФакт 	= Макет.ПолучитьОбласть("СтрокаТаблицы|ФактБезКоличестваПоУчету");
				ОбластьПодвалТаблицыФакт 	= Макет.ПолучитьОбласть("ПодвалТаблицы|ФактБезКоличестваПоУчету");
			КонецЕсли;	
		Иначе
			
			ОбластьШапкаТаблицыПоУчету 		= Макет.ПолучитьОбласть("ШапкаТаблицыКодУчет|КолонкаПоУчету");
			ОбластьСтрокаТаблицыПоУчету 	= Макет.ПолучитьОбласть("СтрокаТаблицыКодУчет|КолонкаПоУчету");
			ОбластьПодвалТаблицыПоУчету 	= Макет.ПолучитьОбласть("ПодвалТаблицыКодУчет|КолонкаПоУчету");
			Если ВыводитьГрадацииКачества Тогда
				ОбластьШапкаТаблицыФакт 	= Макет.ПолучитьОбласть("ШапкаТаблицыГрадацииКачества|ГрадацииКачества");
				ОбластьСтрокаТаблицыФакт 	= Макет.ПолучитьОбласть("СтрокаТаблицыГрадацииКачества|ГрадацииКачества");
				ОбластьПодвалТаблицыФакт 	= Макет.ПолучитьОбласть("ПодвалТаблицыГрадацииКачества|ГрадацииКачества");
			Иначе 
				ОбластьШапкаТаблицыФакт 	= Макет.ПолучитьОбласть("ШапкаТаблицыКодУчет|КолонкаФакт");
				ОбластьСтрокаТаблицыФакт 	= Макет.ПолучитьОбласть("СтрокаТаблицыКодУчет|КолонкаФакт");
				ОбластьПодвалТаблицыФакт 	= Макет.ПолучитьОбласть("ПодвалТаблицыКодУчет|КолонкаФакт");
			КонецЕсли;	
		КонецЕсли;		
		
			
		Если НЕ ВыборкаПоТабличнымЧастям.НайтиСледующий(Новый Структура("Ссылка",ВыборкаПоДокументам.Ссылка)) Тогда
			Продолжить;
		КонецЕсли;
			
		ВыборкаПоСтрокамТЧ = ВыборкаПоТабличнымЧастям.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
		
		Если Не ПервыйДокумент Тогда
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;
		
		ПервыйДокумент = Ложь;
		НомерСтрокиНачало = ТабличныйДокумент.ВысотаТаблицы + 1;
		
		ОбластьЗаголовок.Параметры.ТекстЗаголовка = Заголовок;
		
		ШтрихкодированиеПечатныхФорм.ВывестиШтрихкодВТабличныйДокумент(ТабличныйДокумент, Макет, ОбластьЗаголовок, ВыборкаПоДокументам.Ссылка);
		ТабличныйДокумент.Вывести(ОбластьЗаголовок);
		
		ОбластьСкладИсполнитель.Параметры.ИсполнительПредставление	= ФизическиеЛица.ФамилияИнициалыФизЛица(ВыборкаПоДокументам.Исполнитель);
		ОбластьСкладИсполнитель.Параметры.СкладПредставление		= СкладыСервер.ПолучитьПредставлениеСклада(ВыборкаПоДокументам.СкладПредставление,ВыборкаПоДокументам.ПомещениеПредставление);
		
		ТабличныйДокумент.Вывести(ОбластьСкладИсполнитель);
		
		Если ЗначениеЗаполнено(ВыборкаПоДокументам.ЯчейкаИзлишков) Тогда
			ЗаполнитьЗначенияСвойств(ОбластьЯчейкаИзлишков.Параметры, ВыборкаПоДокументам);
			ТабличныйДокумент.Вывести(ОбластьЯчейкаИзлишков);
		КонецЕсли;
		
		Если ЗначениеЗаполнено(ВыборкаПоДокументам.ЯчейкаИспорченных) Тогда
			ЗаполнитьЗначенияСвойств(ОбластьЯчейкаИспорченных.Параметры, ВыборкаПоДокументам);
			ТабличныйДокумент.Вывести(ОбластьЯчейкаИспорченных);
		КонецЕсли;
		
		ТабличныйДокумент.Вывести(ОбластьШапкаТаблицыНачало);
		
		Если ВыводитьКоды Тогда
			ТабличныйДокумент.Присоединить(ОбластьШапкаТаблицыКолонкаКодов);
		КонецЕсли;
		ТабличныйДокумент.Присоединить(ОбластьШапкаТаблицыКолонкаТоваров);
		ТабличныйДокумент.Присоединить(ОбластьШапкаТаблицыУпаковки);
		
		Если ВыводитьПоУчету Тогда
			ТабличныйДокумент.ПолеСправа = 5;
			ТабличныйДокумент.Присоединить(ОбластьШапкаТаблицыПоУчету);
		КонецЕсли;
		
		ТабличныйДокумент.Присоединить(ОбластьШапкаТаблицыФакт);
		
		Пока ВыборкаПоСтрокамТЧ.Следующий() Цикл
			
			ОбластьСтрокаТаблицыНачало.Параметры.Заполнить(ВыборкаПоСтрокамТЧ);
			ТабличныйДокумент.Вывести(ОбластьСтрокаТаблицыНачало);
			
			Если ВыводитьКоды Тогда
				ОбластьСтрокаТаблицыКолонкаКодов.Параметры.Артикул = ВыборкаПоСтрокамТЧ[КолонкаКодов];
				ТабличныйДокумент.Присоединить(ОбластьСтрокаТаблицыКолонкаКодов);
			КонецЕсли;
			
			ОбластьСтрокаТаблицыКолонкаТоваров.Параметры.Товар = ФормированиеПечатныхФорм.ПолучитьПредставлениеНоменклатурыДляПечати(
				ВыборкаПоСтрокамТЧ.НоменклатураПредставление,
				ВыборкаПоСтрокамТЧ.ХарактеристикаПредставление,
				,
				ВыборкаПоСтрокамТЧ.СерияПредставление
			);
			ТабличныйДокумент.Присоединить(ОбластьСтрокаТаблицыКолонкаТоваров);
			
			ОбластьСтрокаТаблицыУпаковки.Параметры.Заполнить(ВыборкаПоСтрокамТЧ);
			
			ТабличныйДокумент.Присоединить(ОбластьСтрокаТаблицыУпаковки);	
			
			Если ВыводитьПоУчету Тогда
				ОбластьСтрокаТаблицыПоУчету.Параметры.Заполнить(ВыборкаПоСтрокамТЧ);
				ТабличныйДокумент.Присоединить(ОбластьСтрокаТаблицыПоУчету);
			КонецЕсли;
			
			ОбластьСтрокаТаблицыФакт.Параметры.Заполнить(ВыборкаПоСтрокамТЧ);
			
			ТабличныйДокумент.Присоединить(ОбластьСтрокаТаблицыФакт);
		КонецЦикла;
		
		ТабличныйДокумент.Вывести(ОбластьПодвалТаблицыНачало);
		Если ВыводитьКоды Тогда
			ТабличныйДокумент.Присоединить(ОбластьПодвалТаблицыКолонкаКодов);
		КонецЕсли;
		
		ТабличныйДокумент.Присоединить(ОбластьПодвалТаблицыКолонкаТоваров);
		ТабличныйДокумент.Присоединить(ОбластьПодвалТаблицыУпаковки);
		Если ВыводитьПоУчету Тогда
			ТабличныйДокумент.Присоединить(ОбластьПодвалТаблицыПоУчету);
		КонецЕсли;
		ТабличныйДокумент.Присоединить(ОбластьПодвалТаблицыФакт);
		УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ТабличныйДокумент, НомерСтрокиНачало, ОбъектыПечати, ВыборкаПоДокументам.Ссылка);
	КонецЦикла;
	
	ТабличныйДокумент.АвтоМасштаб = Истина;
	
	Возврат ТабличныйДокумент;	
КонецФункции

Функция ПолучитьДанныеДляПечатнойФормыОтборРазмещениеТоваров(ПараметрыПечати, МассивДокументов) Экспорт
	Запрос = Новый Запрос;
	Запрос.Текст = 
	
	"ВЫБРАТЬ
	|	ПРЕДСТАВЛЕНИЕ(ПересчетТоваров.Склад) КАК СкладПредставление,
	|	ПРЕДСТАВЛЕНИЕ(ПересчетТоваров.Помещение) КАК ПомещениеПредставление,
	|	ПРЕДСТАВЛЕНИЕ(ПересчетТоваров.Исполнитель.ФизическоеЛицо) КАК ИсполнительПредставление,
	|	ПРЕДСТАВЛЕНИЕ(ПересчетТоваров.Ссылка) КАК СсылкаПредставление,
	|	ПересчетТоваров.Ссылка КАК Ссылка,
	|	ПересчетТоваров.ПечататьКоличествоПоУчету КАК ВыводитьПоУчету,
	|	ПересчетТоваров.Дата КАК Дата,
	|	ПересчетТоваров.Номер КАК Номер,
	|	ЛОЖЬ КАК НарушенаОрдернаяСхема,
	|	ЛОЖЬ КАК СкладыВТЧ,
	|	NULL КАК ВидОперации,
	|	ВЫБОР
	|		КОГДА ПересчетТоваров.Склад.ИспользоватьСкладскиеПомещения
	|			ТОГДА ПересчетТоваров.Помещение.ИспользоватьАдресноеХранениеСправочно
	|		ИНАЧЕ ПересчетТоваров.Склад.ИспользоватьАдресноеХранениеСправочно
	|	КОНЕЦ КАК ИспользуетсяСправочноеХранение
	|ИЗ
	|	Документ.ПересчетТоваров КАК ПересчетТоваров
	|ГДЕ
	|	ПересчетТоваров.Ссылка В(&МассивОбъектов)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаТовары.Ссылка КАК Ссылка,
	|	ТаблицаТовары.НомерСтроки КАК НомерСтроки,
	|	ТаблицаТовары.Номенклатура.Код КАК Код,
	|	ТаблицаТовары.Номенклатура КАК Номенклатура,
	|	ТаблицаТовары.Характеристика КАК Характеристика,
	|	ТаблицаТовары.Упаковка КАК Упаковка,
	|	ТаблицаТовары.Упаковка.Наименование КАК ПредставлениеУпаковки,
	|	ТаблицаТовары.Номенклатура.Артикул КАК Артикул,
	|	ТаблицаТовары.Номенклатура.НаименованиеПолное КАК ПредставлениеНоменклатуры,
	|	ПРЕДСТАВЛЕНИЕ(ТаблицаТовары.Номенклатура.ЕдиницаИзмерения) КАК ПредставлениеБазовойЕдиницыИзмерения,
	|	ТаблицаТовары.Характеристика.НаименованиеПолное КАК ПредставлениеХарактеристики,
	|	ТаблицаТовары.Серия.Наименование КАК ПредставлениеСерии,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.Упаковка = ЗНАЧЕНИЕ(Справочник.УпаковкиНоменклатуры.ПустаяСсылка)
	|			ТОГДА ПРЕДСТАВЛЕНИЕ(ТаблицаТовары.Номенклатура.ЕдиницаИзмерения)
	|		ИНАЧЕ ПРЕДСТАВЛЕНИЕ(ТаблицаТовары.Упаковка)
	|	КОНЕЦ КАК ПредставлениеЕдининицыИзмеренияУпаковки,
	|	ТаблицаТовары.Количество КАК КоличествоУпаковок,
	|	0 КАК Количество,
	|	ЕСТЬNULL(РазмещениеОсновнаяЯчейка.Ячейка.РабочийУчасток, ЗНАЧЕНИЕ(Справочник.РабочиеУчастки.ПустаяСсылка)) КАК РабочийУчасток,
	|	ЕСТЬNULL(РазмещениеОсновнаяЯчейка.Ячейка.ПорядокОбхода, 0) КАК ПорядокОбхода,
	|	ЕСТЬNULL(РазмещениеОсновнаяЯчейка.Ячейка, ЗНАЧЕНИЕ(Справочник.СкладскиеЯчейки.ПустаяСсылка)) КАК ОсновнаяЯчейка,
	|	ЕСТЬNULL(РазмещениеОстальныеЯчейки.Ячейка.ПорядокОбхода, 0) КАК ПорядокОбходаДополнительнаяЯчейка,
	|	ПРЕДСТАВЛЕНИЕ(РазмещениеОсновнаяЯчейка.Ячейка.РабочийУчасток) КАК ПредставлениеРабочегоУчастка,
	|	РазмещениеОсновнаяЯчейка.Ячейка.Код КАК ОсновнаяЯчейкаПредставление,
	|	РазмещениеОстальныеЯчейки.Ячейка.Код КАК ЯчейкаПредставление,
	|	ТаблицаТовары.Серия КАК Серия
	|ИЗ
	|	Документ.ПересчетТоваров.Товары КАК ТаблицаТовары
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.РазмещениеНоменклатурыПоСкладскимЯчейкам КАК РазмещениеОсновнаяЯчейка
	|		ПО ТаблицаТовары.Номенклатура = РазмещениеОсновнаяЯчейка.Номенклатура
	|			И (РазмещениеОсновнаяЯчейка.ОсновнаяЯчейка = ИСТИНА)
	|			И (ВЫБОР
	|				КОГДА ТаблицаТовары.Ссылка.Склад.ИспользоватьСкладскиеПомещения
	|					ТОГДА РазмещениеОсновнаяЯчейка.Помещение = ТаблицаТовары.Ссылка.Помещение
	|				ИНАЧЕ
	|					РазмещениеОсновнаяЯчейка.Склад = ТаблицаТовары.Ссылка.Склад
	|			КОНЕЦ)
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.РазмещениеНоменклатурыПоСкладскимЯчейкам КАК РазмещениеОстальныеЯчейки
	|		ПО ТаблицаТовары.Номенклатура = РазмещениеОстальныеЯчейки.Номенклатура
	|			И (РазмещениеОстальныеЯчейки.ОсновнаяЯчейка = ЛОЖЬ)
	|			И (ВЫБОР
	|				КОГДА ТаблицаТовары.Ссылка.Склад.ИспользоватьСкладскиеПомещения
	|					ТОГДА РазмещениеОстальныеЯчейки.Помещение = ТаблицаТовары.Ссылка.Помещение
	|				ИНАЧЕ
	|					РазмещениеОстальныеЯчейки.Склад = ТаблицаТовары.Ссылка.Склад
	|				КОНЕЦ)
	|ГДЕ
	|	ТаблицаТовары.Ссылка В(&МассивОбъектов)
	|	И ТаблицаТовары.Номенклатура.ТипНоменклатуры = ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.Товар)
	|	И ВЫБОР
	|			КОГДА ТаблицаТовары.Ссылка.Склад.ИспользоватьСкладскиеПомещения
	|				ТОГДА ТаблицаТовары.Ссылка.Помещение.ИспользоватьАдресноеХранениеСправочно
	|			ИНАЧЕ ТаблицаТовары.Ссылка.Склад.ИспользоватьАдресноеХранениеСправочно
	|		КОНЕЦ
	|
	|УПОРЯДОЧИТЬ ПО
	|	ТаблицаТовары.Ссылка,
	|	ПорядокОбхода,
	|	ОсновнаяЯчейкаПредставление,
	|	ПредставлениеНоменклатуры,
	|	ПредставлениеХарактеристики,
	|	ПредставлениеСерии,
	|	ПредставлениеУпаковки,
	|	ПорядокОбходаДополнительнаяЯчейка,
	|	ЯчейкаПредставление
	|ИТОГИ ПО
	|	Ссылка,
	|	РабочийУчасток,
	|	ОсновнаяЯчейка,
	|	Номенклатура,
	|	Характеристика,
	|	Серия,
	|	Упаковка";
	
	Запрос.УстановитьПараметр("МассивОбъектов", МассивДокументов); 
	МассивРезультатов 			= Запрос.ВыполнитьПакет();
	РезультатПоШапке			= МассивРезультатов[0];
	РезультатПоТабличнойЧасти 	= МассивРезультатов[1];
	СтруктураДанныхДляПечати 	= Новый Структура("РезультатПоШапке, РезультатПоТабличнойЧасти",РезультатПоШапке, РезультатПоТабличнойЧасти);
	
	Возврат СтруктураДанныхДляПечати;
			
КонецФункции

Функция ПолучитьЯчейкиИзлишковИБракаПоУмолчанию(Ссылка,Склад,Помещение) Экспорт
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ПересчетТоваров.ЯчейкаКонсолидацииИзлишковТоваров,
	|	ПересчетТоваров.ЯчейкаКонсолидацииИспорченныхТоваров,
	|	ПересчетТоваров.ИспользоватьОтдельнуюЯчейкуИзлишков,
	|	ПересчетТоваров.ИспользоватьОтдельнуюЯчейкуПорчи
	|ИЗ
	|	Документ.ПересчетТоваров КАК ПересчетТоваров
	|ГДЕ
	|	ПересчетТоваров.Проведен
	|	И ПересчетТоваров.Склад = &Склад
	|	И ПересчетТоваров.Помещение = &Помещение
	|	И ПересчетТоваров.Ссылка <> &Ссылка
	|
	|УПОРЯДОЧИТЬ ПО
	|	ПересчетТоваров.Дата УБЫВ";
	
	Запрос.УстановитьПараметр("Склад",Склад);
	Запрос.УстановитьПараметр("Помещение",Помещение);
	Запрос.УстановитьПараметр("Ссылка",Ссылка);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	СтруктураВозврата = Новый Структура("ЯчейкаКонсолидацииИзлишковТоваров,ЯчейкаКонсолидацииИспорченныхТоваров,ИспользоватьОтдельнуюЯчейкуИзлишков,ИспользоватьОтдельнуюЯчейкуПорчи");
	
	Если Выборка.Следующий() Тогда
		ЗаполнитьЗначенияСвойств(СтруктураВозврата,Выборка);
	КонецЕсли;
	
	Возврат СтруктураВозврата;
КонецФункции

Функция ПолучитьЗначениеПризнакаПечататьКоличествоПоУчетуПоУмолчанию(Ссылка,Ответственный) Экспорт
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ ПЕРВЫЕ 1
	|	ПересчетТоваров.ПечататьКоличествоПоУчету
	|ИЗ
	|	Документ.ПересчетТоваров КАК ПересчетТоваров
	|ГДЕ
	|	ПересчетТоваров.Проведен
	|	И ПересчетТоваров.Ответственный = &Ответственный
	|	И ПересчетТоваров.Ссылка <> &Ссылка
	|
	|УПОРЯДОЧИТЬ ПО
	|	ПересчетТоваров.Дата УБЫВ";
	
	Запрос.УстановитьПараметр("Ответственный",Ответственный);
	Запрос.УстановитьПараметр("Ссылка",Ссылка);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		ПечататьКоличествоПоУчету = Выборка.ПечататьКоличествоПоУчету;
	КонецЕсли;
	
	Возврат ПечататьКоличествоПоУчету;
КонецФункции