﻿&НаКлиенте
Перем КэшированныеЗначения; //используется механизмом обработки изменения реквизитов ТЧ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Если  Объект.Ссылка.Пустая() Тогда
		Объект.Статус 		 = Перечисления.СтатусыПередачиВПереработку.ПередачаВПримерку;
		Объект.Менеджер 	 = ПараметрыСеанса.ТекущийПользователь;
		Объект.СкладОсновной = Справочники.Склады.НайтиПоНаименованию("СКЛАДЫ");
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыНоменклатураПриИзменении(Элемент)
	ТабЧасть = Элементы.Товары.ТекущиеДанные;
	ТабЧасть.КодНоменклатуры =ТабЧасть.Номенклатура.Код;
	Выборка  = ПолучитьЦену(ТабЧасть.Номенклатура);
	Для Каждого Строка Из  Выборка Цикл
		ТабЧасть.Цена = Строка.Цена;
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура ТоварыКоличествоПриИзменении(Элемент)
	ТабЧасть = Элементы.Товары.ТекущиеДанные;
	ТабЧасть.Сумма =ТабЧасть.Количество * ТабЧасть.Цена; 
КонецПроцедуры

&НаКлиенте
Процедура ТоварыСуммаПриИзменении(Элемент)
	ТабЧасть = Элементы.Товары.ТекущиеДанные;
	ТабЧасть.Цена =ТабЧасть.Сумма/ТабЧасть.Количество; 
КонецПроцедуры

&НаСервере

Функция ПолучитьЦену(Номенклатура)Экспорт
	ОбъектОбработка=РеквизитФормыВЗначение("Объект");
	ТабЧасть = ОбъектОбработка.Товары[0];
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ЦеныНоменклатурыСрезПоследних.Номенклатура,
	|	ЦеныНоменклатурыСрезПоследних.ВидЦены,
	|	ЦеныНоменклатурыСрезПоследних.Цена
	|ИЗ
	|	РегистрСведений.ЦеныНоменклатуры.СрезПоследних КАК ЦеныНоменклатурыСрезПоследних
	|ГДЕ
	|	ЦеныНоменклатурыСрезПоследних.Номенклатура = &Номенклатура
	|	И ЦеныНоменклатурыСрезПоследних.ВидЦены = &ВидЦены";
	Запрос.УстановитьПараметр("Номенклатура",Номенклатура);
	Запрос.УстановитьПараметр("ВидЦены",Объект.ВидЦены);
	
	Результат = Запрос.Выполнить().Выгрузить();
	Возврат Результат;	
КонецФункции 

&НаКлиенте
Процедура РаспределениеПоСкладам()
	Если Объект.Проведен Тогда
		Предупреждение("Для правильного распределения по складам нужно отменить проведение документа.",, "Распределение по складам");
		Возврат;	                                                                                 	
	КонецЕсли;
	
	Для каждого СтрТовар из Объект.Товары цикл
		Если ЗначениеЗаполнено(СтрТовар.Склад) тогда
			ЗадатьВопрос = истина;
			Прервать;
		Иначе
			ЗадатьВопрос = ложь;
		КонецЕсли;
	КонецЦикла;
	Если ЗадатьВопрос тогда
		Если Вопрос("Очистить уже распределенные склады?",РежимДиалогаВопрос.ДаНет)=КодВозвратаДиалога.Да тогда
			Для каждого СтрТовар из Объект.Товары цикл
				СтрТовар.Склад = ПолучитьПустуюСсылку();		
			КонецЦикла;
		Иначе
			Возврат;
		КонецЕсли;
	КонецЕсли;

	//Распределяем номенклатуру по складам с учетом остатков
	Состояние("Выполняется распределение товаров по складам.");
    	
	Если НЕ ЗначениеЗаполнено(Объект.СкладОсновной) или ПроверитьГруппу(Объект.СкладОсновной) = Ложь тогда
		Предупреждение("Необходимо заполнить группу Склад, группой!");
		Возврат;
	КонецЕсли;
	Если Объект.Товары.Количество() = 0 тогда
		Предупреждение("Таблица товаров пустая!");
		Возврат;
	КонецЕсли;
	ОбработатьТЗНаСервере();
	//	Для каждого СтрТовар из Объект.Товары цикл
	//	СтруктураДействий = Новый Структура;
	//	ДобавитьВСтруктуруДействияПриИзмененииКоличестваУпаковок(СтруктураДействий,Объект);	
	//	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(СтрТовар, СтруктураДействий, КэшированныеЗначения);
	//КонецЦикла;
	//РассчитатьИтоговыеПоказателиЗаказа(ЭтаФорма);
	ОбновитьТаблицуОстатки();
КонецПроцедуры


&НаКлиентеНаСервереБезКонтекста
Процедура РассчитатьИтоговыеПоказателиЗаказа(Форма)
	
	// Заполнение итогов по таблице "Товары"
	
	КоллекцияТовары = Форма.Объект.Товары;
	Форма.СуммаЗаказано     = КоллекцияТовары.Итог("СуммаСНДС") - КоллекцияТовары.Итог("СуммаСНДСОтменено");
	Форма.СуммаНДСЗаказано  = КоллекцияТовары.Итог("СуммаНДС") - КоллекцияТовары.Итог("СуммаНДСОтменено");
	Форма.СуммаАвтоСкидки   = КоллекцияТовары.Итог("СуммаАвтоматическойСкидки") - КоллекцияТовары.Итог("СуммаАвтоматическойСкидкиОтменено");
	Форма.СуммаРучнойСкидки = КоллекцияТовары.Итог("СуммаРучнойСкидки") - КоллекцияТовары.Итог("СуммаРучнойСкидкиОтменено");
	Форма.СуммаСкидки       = Форма.СуммаАвтоСкидки + Форма.СуммаРучнойСкидки;
	Форма.СуммаОтменено     = КоллекцияТовары.Итог("СуммаСНДСОтменено");
	
	Если КоллекцияТовары.Итог("СуммаСНДСОтменено") = КоллекцияТовары.Итог("СуммаСНДС") Тогда
		Форма.ВсеСтрокиОтменены = Истина;
	Иначе
		Форма.ВсеСтрокиОтменены = Ложь;
	КонецЕсли;
	
	СуммаЗаказано     = КоллекцияТовары.Итог("Сумма") - КоллекцияТовары.Итог("СуммаОтменено");
	
	Если СуммаЗаказано > 0 Тогда
		//bs_Анастасия
		Если Число(СуммаЗаказано + Форма.СуммаСкидки) <> 0 Тогда
		Форма.ПроцентАвтоСкидки   = ?(СуммаЗаказано + Форма.СуммаСкидки,0,Форма.СуммаАвтоСкидки * 100 / (СуммаЗаказано + Форма.СуммаСкидки));
		Форма.ПроцентРучнойСкидки = Форма.СуммаРучнойСкидки * 100 / (СуммаЗаказано + Форма.СуммаСкидки);
		КонецЕсли;
		Форма.ПроцентСкидки       = Форма.ПроцентАвтоСкидки + Форма.ПроцентРучнойСкидки;
	ИначеЕсли Форма.СуммаСкидки > 0 Тогда
		Форма.ПроцентАвтоСкидки   = Форма.СуммаАвтоСкидки * 100 / Форма.СуммаСкидки;
		Форма.ПроцентРучнойСкидки = Форма.СуммаРучнойСкидки * 100 / Форма.СуммаСкидки;
		Форма.ПроцентСкидки       = Форма.ПроцентАвтоСкидки + Форма.ПроцентРучнойСкидки;
	Иначе
		Форма.ПроцентАвтоСкидки   = 0;
		Форма.ПроцентРучнойСкидки = 0;
		Форма.ПроцентСкидки       = 0;
	КонецЕсли;
	
	// Заполнение итогов по этапам оплаты
	
	ПредыдущееЗначениеДаты = Дата(1,1,1);
	Форма.НомерСтрокиПолнойОплаты = 0;
	ПроцентПлатежейОбщий = 0;
	
	Форма.СуммаАвансаДоОбеспечения = 0;
	Форма.СуммаПредоплатыДоОтгрузки = 0;
	Форма.СуммаКредитаПослеОтгрузки = 0;
	Форма.ПроцентАвансаДоОбеспечения = 0;
	Форма.ПроцентПредоплатыДоОтгрузки = 0;
	Форма.ПроцентКредитаПослеОтгрузки = 0;
	
	СоответствиеВариантовОплаты = Новый Соответствие;
	СоответствиеВариантовОплаты.Вставить(ПредопределенноеЗначение("Перечисление.ВариантыОплатыКлиентом.АвансДоОбеспечения"),
		Новый Структура("Сумма, Проценты", "СуммаАвансаДоОбеспечения", "ПроцентАвансаДоОбеспечения")
	);
	СоответствиеВариантовОплаты.Вставить(ПредопределенноеЗначение("Перечисление.ВариантыОплатыКлиентом.КредитПослеОтгрузки"),
		Новый Структура("Сумма, Проценты", "СуммаПредоплатыДоОтгрузки", "ПроцентПредоплатыДоОтгрузки")
	);
	СоответствиеВариантовОплаты.Вставить(ПредопределенноеЗначение("Перечисление.ВариантыОплатыКлиентом.КредитПослеОтгрузки"),
		Новый Структура("Сумма, Проценты", "СуммаКредитаПослеОтгрузки", "ПроцентКредитаПослеОтгрузки")
	);
	
	Для Каждого ТекСтрока Из Форма.Объект.ЭтапыГрафикаОплаты Цикл
		ПроцентПлатежейОбщий = ПроцентПлатежейОбщий + ТекСтрока.ПроцентПлатежа;
		ТекСтрока.ПроцентЗаполненНеВерно = (ПроцентПлатежейОбщий > 100);
		ТекСтрока.ДатаЗаполненаНеВерно = (ПредыдущееЗначениеДаты > ТекСтрока.ДатаПлатежа);
		ПредыдущееЗначениеДаты = ТекСтрока.ДатаПлатежа;
		Если ПроцентПлатежейОбщий = 100 Тогда
			Форма.НомерСтрокиПолнойОплаты = ТекСтрока.НомерСтроки;
		КонецЕсли;
		ИменаЭлементов = СоответствиеВариантовОплаты[ТекСтрока.ВариантОплаты];
		Если ИменаЭлементов <> Неопределено Тогда
			Форма[ИменаЭлементов.Сумма] = Форма[ИменаЭлементов.Сумма] + ТекСтрока.СуммаПлатежа;
			Форма[ИменаЭлементов.Проценты] = Форма[ИменаЭлементов.Проценты] + ТекСтрока.ПроцентПлатежа;
		КонецЕсли;
	КонецЦикла;
	
	// Выбор странцицы отображения НДС
	
	Если Форма.Объект.НалогообложениеНДС = ПредопределенноеЗначение("Перечисление.ТипыНалогообложенияНДС.ПродажаНеОблагаетсяНДС")
		ИЛИ Форма.Объект.НалогообложениеНДС = ПредопределенноеЗначение("Перечисление.ТипыНалогообложенияНДС.ПродажаОблагаетсяЕНВД") Тогда
		
		Форма.Элементы.ГруппаСтраницыНДС.ТекущаяСтраница   = Форма.Элементы.СтраницаБезНДС;
		Форма.Элементы.ГруппаСтраницыВсего.ТекущаяСтраница = Форма.Элементы.СтраницаВсегоБезНДС;
		
	Иначе
		
		Форма.Элементы.ГруппаСтраницыНДС.ТекущаяСтраница   = Форма.Элементы.СтраницаСНДС;
		Форма.Элементы.ГруппаСтраницыВсего.ТекущаяСтраница = Форма.Элементы.СтраницаВсегоСНДС;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьТаблицуОстатки();
	Для Каждого Стр из Объект.Товары цикл
		Если ЗначениеЗаполнено(Стр.Номенклатура) и ЗначениеЗаполнено(Стр.Склад) тогда
			Стр.ОстатокНаСкладе = ПолучитьОстатокНаСкладеТЗ(Стр.Номенклатура, Стр.Склад);
				Иначе Возврат; // Рифат
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

Функция ПроверитьГруппу(пСсылка);
	Возврат пСсылка.ЭтоГруппа
КонецФункции

&НаСервере
Функция ПолучитьОстатокНаСкладеТЗ(пНоменклатура, пСклад);
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	Сегменты.Номенклатура,
		|	Сегменты.Характеристика,
		|	ИСТИНА КАК ИспользуетсяОтборПоСегментуНоменклатуры
		|ПОМЕСТИТЬ ОтборПоСегментуНоменклатуры
		|ИЗ
		|	РегистрСведений.НоменклатураСегмента КАК Сегменты
		|{ГДЕ
		|	Сегменты.Сегмент.* КАК СегментНоменклатуры,
		|	Сегменты.Номенклатура.* КАК Номенклатура,
		|	Сегменты.Характеристика.* КАК Характеристика}
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	Сегменты.Номенклатура,
		|	Сегменты.Характеристика,
		|	ИспользуетсяОтборПоСегментуНоменклатуры
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	График.Номенклатура КАК Номенклатура,
		|	График.Характеристика КАК Характеристика,
		|	График.Склад КАК Склад,
		|	-МИНИМУМ(График.КоличествоКонечныйОстаток) КАК Количество
		|ПОМЕСТИТЬ ВтРезервыПоГрафику
		|ИЗ
		|	РегистрНакопления.ГрафикДвиженияТоваров.ОстаткиИОбороты({(КОНЕЦПЕРИОДА(&ТекущаяДата, ДЕНЬ)) КАК Поле2}, , День, ДвиженияИГраницыПериода, {((Номенклатура, Характеристика) В
		|			    (ВЫБРАТЬ
		|			        ОтборПоСегментуНоменклатуры.Номенклатура,
		|			        ОтборПоСегментуНоменклатуры.Характеристика
		|			    ИЗ
		|			        ОтборПоСегментуНоменклатуры
		|			    ГДЕ
		|			        ОтборПоСегментуНоменклатуры.ИспользуетсяОтборПоСегментуНоменклатуры = &ИспользуетсяОтборПоСегментуНоменклатуры)) КАК Поле2}) КАК График
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаХарактеристика
		|		ПО График.Склад = НастройкаХарактеристика.Склад
		|			И График.Номенклатура = НастройкаХарактеристика.Номенклатура
		|			И График.Характеристика = НастройкаХарактеристика.Характеристика
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаНоменклатура
		|		ПО График.Склад = НастройкаНоменклатура.Склад
		|			И График.Номенклатура = НастройкаНоменклатура.Номенклатура
		|			И (НастройкаНоменклатура.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
		|			И (НастройкаХарактеристика.Склад ЕСТЬ NULL )
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаСклад
		|		ПО График.Склад = НастройкаСклад.Склад
		|			И (НастройкаСклад.Номенклатура = ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка))
		|			И (НастройкаСклад.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
		|			И (НастройкаХарактеристика.Склад ЕСТЬ NULL )
		|			И (НастройкаНоменклатура.Склад ЕСТЬ NULL )
		|ГДЕ
		|	ВЫБОР
		|			КОГДА ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомГрафика)
		|				ТОГДА ВЫБОР
		|						КОГДА НЕ НастройкаХарактеристика.ВариантКонтроля ЕСТЬ NULL 
		|							ТОГДА ВЫБОР
		|									КОГДА НастройкаХарактеристика.ГраницаГрафикаДоступности >= &ТекущаяДата
		|										ТОГДА График.Период <= КОНЕЦПЕРИОДА(НастройкаХарактеристика.ГраницаГрафикаДоступности, ДЕНЬ)
		|									КОГДА НастройкаХарактеристика.СрокПоставки > 0
		|										ТОГДА График.Период <= ДОБАВИТЬКДАТЕ(&ТекущаяДата, ДЕНЬ, НастройкаХарактеристика.СрокПоставки - 1)
		|									ИНАЧЕ ЛОЖЬ
		|								КОНЕЦ
		|						КОГДА НЕ НастройкаНоменклатура.ВариантКонтроля ЕСТЬ NULL 
		|							ТОГДА ВЫБОР
		|									КОГДА НастройкаНоменклатура.ГраницаГрафикаДоступности >= &ТекущаяДата
		|										ТОГДА График.Период <= КОНЕЦПЕРИОДА(НастройкаНоменклатура.ГраницаГрафикаДоступности, ДЕНЬ)
		|									КОГДА НастройкаНоменклатура.СрокПоставки > 0
		|										ТОГДА График.Период <= ДОБАВИТЬКДАТЕ(&ТекущаяДата, ДЕНЬ, НастройкаНоменклатура.СрокПоставки - 1)
		|									ИНАЧЕ ЛОЖЬ
		|								КОНЕЦ
		|						ИНАЧЕ ВЫБОР
		|								КОГДА НастройкаСклад.ГраницаГрафикаДоступности >= &ТекущаяДата
		|									ТОГДА График.Период <= КОНЕЦПЕРИОДА(НастройкаСклад.ГраницаГрафикаДоступности, ДЕНЬ)
		|								КОГДА НастройкаСклад.СрокПоставки > 0
		|									ТОГДА График.Период <= ДОБАВИТЬКДАТЕ(&ТекущаяДата, ДЕНЬ, НастройкаСклад.СрокПоставки - 1)
		|								ИНАЧЕ ЛОЖЬ
		|							КОНЕЦ
		|					КОНЕЦ
		|			ИНАЧЕ ЛОЖЬ
		|		КОНЕЦ
		|	И График.КоличествоКонечныйОстаток < 0
		|
		|СГРУППИРОВАТЬ ПО
		|	График.Номенклатура,
		|	График.Характеристика,
		|	График.Склад
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	Номенклатура,
		|	Характеристика,
		|	Склад
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	СвободныеОстатки.Номенклатура КАК Номенклатура,
		|	СвободныеОстатки.Характеристика КАК Характеристика,
		|	СвободныеОстатки.Склад КАК Склад,
		|	СвободныеОстатки.ВНаличииОстаток КАК ВНаличии,
		|	ВЫБОР
		|		КОГДА ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомРезерва)
		|			ТОГДА СвободныеОстатки.ВРезервеОстаток
		|		КОГДА ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомГрафика)
		|			ТОГДА ЕСТЬNULL(РезервыПоГрафику.Количество, 0)
		|		ИНАЧЕ 0
		|	КОНЕЦ КАК Резерв,
		|	СвободныеОстатки.ВНаличииОстаток - ВЫБОР
		|		КОГДА ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомРезерва)
		|			ТОГДА СвободныеОстатки.ВРезервеОстаток
		|		КОГДА ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомГрафика)
		|			ТОГДА ЕСТЬNULL(РезервыПоГрафику.Количество, 0)
		|		ИНАЧЕ 0
		|	КОНЕЦ КАК Свободно
		|ИЗ
		|	РегистрНакопления.СвободныеОстатки.Остатки(, {((Номенклатура, Характеристика) В
		|			    (ВЫБРАТЬ
		|			        ОтборПоСегментуНоменклатуры.Номенклатура,
		|			        ОтборПоСегментуНоменклатуры.Характеристика
		|			    ИЗ
		|			        ОтборПоСегментуНоменклатуры
		|			    ГДЕ
		|			        ОтборПоСегментуНоменклатуры.ИспользуетсяОтборПоСегментуНоменклатуры = &ИспользуетсяОтборПоСегментуНоменклатуры)) КАК Поле2}) КАК СвободныеОстатки
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаХарактеристика
		|		ПО СвободныеОстатки.Склад = НастройкаХарактеристика.Склад
		|			И СвободныеОстатки.Номенклатура = НастройкаХарактеристика.Номенклатура
		|			И СвободныеОстатки.Характеристика = НастройкаХарактеристика.Характеристика
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаНоменклатура
		|		ПО СвободныеОстатки.Склад = НастройкаНоменклатура.Склад
		|			И СвободныеОстатки.Номенклатура = НастройкаНоменклатура.Номенклатура
		|			И (НастройкаНоменклатура.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
		|			И (НастройкаХарактеристика.Склад ЕСТЬ NULL )
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаСклад
		|		ПО СвободныеОстатки.Склад = НастройкаСклад.Склад
		|			И (НастройкаСклад.Номенклатура = ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка))
		|			И (НастройкаСклад.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
		|			И (НастройкаХарактеристика.Склад ЕСТЬ NULL )
		|			И (НастройкаНоменклатура.Склад ЕСТЬ NULL )
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВтРезервыПоГрафику КАК РезервыПоГрафику
		|		ПО СвободныеОстатки.Номенклатура = РезервыПоГрафику.Номенклатура
		|			И СвободныеОстатки.Характеристика = РезервыПоГрафику.Характеристика
		|			И СвободныеОстатки.Склад = РезервыПоГрафику.Склад
		|			И (ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомГрафика))
		|ГДЕ
		|	(СвободныеОстатки.ВНаличииОстаток <> 0
		|			ИЛИ ВЫБОР
		|				КОГДА ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомРезерва)
		|					ТОГДА СвободныеОстатки.ВРезервеОстаток
		|				КОГДА ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомГрафика)
		|					ТОГДА ЕСТЬNULL(РезервыПоГрафику.Количество, 0)
		|				ИНАЧЕ 0
		|			КОНЕЦ <> 0)
		|	И СвободныеОстатки.Номенклатура = &Номенклатура
		|	И СвободныеОстатки.Склад = &Склад";

	Запрос.УстановитьПараметр("Номенклатура", пНоменклатура);
	Запрос.УстановитьПараметр("Склад", пСклад);
	Запрос.УстановитьПараметр("ТекущаяДата", ТекущаяДата());

	Результат = Запрос.Выполнить();

	ВыборкаДетальныеЗаписи = Результат.Выбрать();
    ОстатокТОвара = 0;
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		ОстатокТОвара = ОстатокТОвара + ВыборкаДетальныеЗаписи.Свободно; 
	КонецЦикла;
	
	Возврат ОстатокТОвара; 
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Процедура ДобавитьВСтруктуруДействияПриИзмененииКоличестваУпаковок(СтруктураДействий, Объект)
	
	СтруктураПересчетаСуммы = ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(Объект);
	СтруктураДействий.Вставить("ПересчитатьКоличествоЕдиниц");
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", СтруктураПересчетаСуммы);
	СтруктураДействий.Вставить("ПересчитатьСуммуСНДС", СтруктураПересчетаСуммы);
	СтруктураДействий.Вставить("ПересчитатьСумму");
	СтруктураДействий.Вставить("ПересчитатьСуммуСУчетомРучнойСкидки", Новый Структура("Очищать", Ложь));
	СтруктураДействий.Вставить("ПересчитатьСуммуСУчетомАвтоматическойСкидки", Новый Структура("Очищать", Истина));
	СтруктураДействий.Вставить("ЗаполнитьДубликатыЗависимыхРеквизитов", ПолучитьСтруктуруЗависимыхРеквизитов());
	
КонецПроцедуры

&НаСервере
Функция ПолучитьПустуюСсылку();
	Возврат справочники.Склады.ПустаяСсылка();
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ПолучитьСтруктуруЗависимыхРеквизитов()
	
	Возврат Новый Структура("Отменено", "Сумма, СуммаНДС, СуммаСНДС, СуммаАвтоматическойСкидки, СуммаРучнойСкидки");
	
КонецФункции

&НаСервере
Процедура ОбработатьТЗНаСервере(); 
	ТЗСкладов = ПолучитьСклады(Объект.СкладОсновной);
	ТЗОстатки = ПолучитьОстатокНаСкладе(ТЗСкладов);
	Дорога = Справочники.Склады.НайтиПоНаименованию("ДоРоГа");
	Для Каждого СтрТовар из Объект.Товары цикл
		Нужно = СтрТовар.Количество;
		Для каждого СтрОстаток Из ТЗОстатки Цикл
			Если СтрТовар.Номенклатура = СтрОстаток.Номенклатура И Нужно > 0 И СтрОстаток.Свободно > 0 И НЕ ЗначениеЗаполнено(СтрТовар.Склад) Тогда				
				
				Если Нужно >= СтрОстаток.Свободно Тогда
					Нужно = Нужно - СтрОстаток.Свободно;
					
					СтрТовар.Склад = СтрОстаток.Склад;
					СтрТовар.Количество = СтрОстаток.Свободно;
					СтрОстаток.Свободно = 0;
					СтрТовар.Сумма = СтрТовар.Количество * СтрТовар.Цена;
					//Если не СтрТовар.ПодЗаказ Тогда 
					//СтрТовар.СПолу = Истина;
					//КонецЕсли;

					Если Нужно > 0 Тогда
						НоваяСтрока = Объект.Товары.Добавить();
						ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрТовар);
						НоваяСтрока.Количество = Нужно;
						НоваяСтрока.Склад = Справочники.Склады.ПустаяСсылка();
						НоваяСтрока.Сумма = НоваяСтрока.Количество * НоваяСтрока.Цена;
					КонецЕсли;   					
				Иначе
					СтрОстаток.Свободно = СтрОстаток.Свободно - Нужно;					 
					Нужно = 0;
					СтрТовар.Склад = СтрОстаток.Склад;
					СтрТовар.Сумма = СтрТовар.Количество * СтрТовар.Цена;
				КонецЕсли;
				
			КонецЕсли;			
		КонецЦикла;  
		
		Если Нужно > 0 И НЕ ЗначениеЗаполнено(СтрТовар.Склад) Тогда
			//НоваяСтрока = Объект.Товары.Добавить();
			//ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрТовар);
			СтрТовар.Склад = Дорога;
			СтрТовар.Количество = Нужно;
			Нужно = 0;		
		КонецЕсли; 
		
	 КонецЦикла;
	
КонецПроцедуры

&НаСервере
Функция ПолучитьСклады(пГрСкладов)
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Родитель", пГрСкладов);
	Запрос.УстановитьПараметр("Подразделение", Объект.Подразделение); // Кудрявцев
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ПриоритетПоСкладам.Склад,
		|	ПриоритетПоСкладам.Приоритет КАК Приоритет
		|ИЗ
		|	РегистрСведений.ПриоритетПоСкладам КАК ПриоритетПоСкладам
		|ГДЕ
		|	ПриоритетПоСкладам.Склад.Родитель В ИЕРАРХИИ(&Родитель)
		|	И (ПриоритетПоСкладам.Склад.Подразделение = &Подразделение
		|			ИЛИ ПриоритетПоСкладам.Склад.Подразделение = ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка))
		|
		|УПОРЯДОЧИТЬ ПО
		|	Приоритет
		|АВТОУПОРЯДОЧИВАНИЕ";

	Результат = Запрос.Выполнить().Выгрузить();
	Возврат  Результат;
	//ЗначениеВРеквизитФормы(Результат,"ТЗСкладов1")
КонецФункции

&НаСервере
Функция ПолучитьОстатокНаСкладе(Склады);
	ЗначНоменкл = новый СписокЗначений;
	Для каждого стртовар из Объект.Товары цикл
		ЗначНоменкл.Добавить(стртовар.Номенклатура);	
	КонецЦикла;
	Запрос = Новый Запрос;
	//Запрос.УстановитьПараметр("Склад",Объект.Склад);
	Запрос.УстановитьПараметр("Склад", Склады);
	Запрос.УстановитьПараметр("ТекущаяДата",ТекущаяДата());
	Запрос.УстановитьПараметр("Номенклатура",ЗначНоменкл);
	Запрос.Текст = 
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	Сегменты.Номенклатура,
		|	Сегменты.Характеристика,
		|	ИСТИНА КАК ИспользуетсяОтборПоСегментуНоменклатуры
		|ПОМЕСТИТЬ ОтборПоСегментуНоменклатуры
		|ИЗ
		|	РегистрСведений.НоменклатураСегмента КАК Сегменты
		|{ГДЕ
		|	Сегменты.Сегмент.* КАК СегментНоменклатуры,
		|	Сегменты.Номенклатура.* КАК Номенклатура,
		|	Сегменты.Характеристика.* КАК Характеристика}
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	Сегменты.Номенклатура,
		|	Сегменты.Характеристика,
		|	ИспользуетсяОтборПоСегментуНоменклатуры
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	График.Номенклатура КАК Номенклатура,
		|	График.Характеристика КАК Характеристика,
		|	График.Склад КАК Склад,
		|	-МИНИМУМ(График.КоличествоКонечныйОстаток) КАК Количество
		|ПОМЕСТИТЬ ВтРезервыПоГрафику
		|ИЗ
		|	РегистрНакопления.ГрафикДвиженияТоваров.ОстаткиИОбороты({(КОНЕЦПЕРИОДА(&ТекущаяДата, ДЕНЬ)) КАК Поле2}, , День, ДвиженияИГраницыПериода, {((Номенклатура, Характеристика) В
		|			    (ВЫБРАТЬ
		|			        ОтборПоСегментуНоменклатуры.Номенклатура,
		|			        ОтборПоСегментуНоменклатуры.Характеристика
		|			    ИЗ
		|			        ОтборПоСегментуНоменклатуры
		|			    ГДЕ
		|			        ОтборПоСегментуНоменклатуры.ИспользуетсяОтборПоСегментуНоменклатуры = &ИспользуетсяОтборПоСегментуНоменклатуры)) КАК Поле2}) КАК График
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаХарактеристика
		|		ПО График.Склад = НастройкаХарактеристика.Склад
		|			И График.Номенклатура = НастройкаХарактеристика.Номенклатура
		|			И График.Характеристика = НастройкаХарактеристика.Характеристика
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаНоменклатура
		|		ПО График.Склад = НастройкаНоменклатура.Склад
		|			И График.Номенклатура = НастройкаНоменклатура.Номенклатура
		|			И (НастройкаНоменклатура.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
		|			И (НастройкаХарактеристика.Склад ЕСТЬ NULL )
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаСклад
		|		ПО График.Склад = НастройкаСклад.Склад
		|			И (НастройкаСклад.Номенклатура = ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка))
		|			И (НастройкаСклад.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
		|			И (НастройкаХарактеристика.Склад ЕСТЬ NULL )
		|			И (НастройкаНоменклатура.Склад ЕСТЬ NULL )
		|ГДЕ
		|	ВЫБОР
		|			КОГДА ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомГрафика)
		|				ТОГДА ВЫБОР
		|						КОГДА НЕ НастройкаХарактеристика.ВариантКонтроля ЕСТЬ NULL 
		|							ТОГДА ВЫБОР
		|									КОГДА НастройкаХарактеристика.ГраницаГрафикаДоступности >= &ТекущаяДата
		|										ТОГДА График.Период <= КОНЕЦПЕРИОДА(НастройкаХарактеристика.ГраницаГрафикаДоступности, ДЕНЬ)
		|									КОГДА НастройкаХарактеристика.СрокПоставки > 0
		|										ТОГДА График.Период <= ДОБАВИТЬКДАТЕ(&ТекущаяДата, ДЕНЬ, НастройкаХарактеристика.СрокПоставки - 1)
		|									ИНАЧЕ ЛОЖЬ
		|								КОНЕЦ
		|						КОГДА НЕ НастройкаНоменклатура.ВариантКонтроля ЕСТЬ NULL 
		|							ТОГДА ВЫБОР
		|									КОГДА НастройкаНоменклатура.ГраницаГрафикаДоступности >= &ТекущаяДата
		|										ТОГДА График.Период <= КОНЕЦПЕРИОДА(НастройкаНоменклатура.ГраницаГрафикаДоступности, ДЕНЬ)
		|									КОГДА НастройкаНоменклатура.СрокПоставки > 0
		|										ТОГДА График.Период <= ДОБАВИТЬКДАТЕ(&ТекущаяДата, ДЕНЬ, НастройкаНоменклатура.СрокПоставки - 1)
		|									ИНАЧЕ ЛОЖЬ
		|								КОНЕЦ
		|						ИНАЧЕ ВЫБОР
		|								КОГДА НастройкаСклад.ГраницаГрафикаДоступности >= &ТекущаяДата
		|									ТОГДА График.Период <= КОНЕЦПЕРИОДА(НастройкаСклад.ГраницаГрафикаДоступности, ДЕНЬ)
		|								КОГДА НастройкаСклад.СрокПоставки > 0
		|									ТОГДА График.Период <= ДОБАВИТЬКДАТЕ(&ТекущаяДата, ДЕНЬ, НастройкаСклад.СрокПоставки - 1)
		|								ИНАЧЕ ЛОЖЬ
		|							КОНЕЦ
		|					КОНЕЦ
		|			ИНАЧЕ ЛОЖЬ
		|		КОНЕЦ
		|	И График.КоличествоКонечныйОстаток < 0
		|
		|СГРУППИРОВАТЬ ПО
		|	График.Номенклатура,
		|	График.Характеристика,
		|	График.Склад
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	Номенклатура,
		|	Характеристика,
		|	Склад
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	СвободныеОстатки.Номенклатура КАК Номенклатура,
		|	СвободныеОстатки.Характеристика КАК Характеристика,
		|	СвободныеОстатки.Склад КАК Склад,
		|	ПриоритетПоСкладам.Приоритет КАК Приоритет,
		|	СвободныеОстатки.ВНаличииОстаток КАК ВНаличии,
		|	ВЫБОР
		|		КОГДА ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомРезерва)
		|			ТОГДА СвободныеОстатки.ВРезервеОстаток
		|		КОГДА ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомГрафика)
		|			ТОГДА ЕСТЬNULL(РезервыПоГрафику.Количество, 0)
		|		ИНАЧЕ 0
		|	КОНЕЦ КАК Резерв,
		|	СвободныеОстатки.ВНаличииОстаток - ВЫБОР
		|		КОГДА ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомРезерва)
		|			ТОГДА СвободныеОстатки.ВРезервеОстаток
		|		КОГДА ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомГрафика)
		|			ТОГДА ЕСТЬNULL(РезервыПоГрафику.Количество, 0)
		|		ИНАЧЕ 0
		|	КОНЕЦ КАК Свободно
		|ИЗ
		|	РегистрНакопления.СвободныеОстатки.Остатки(
		|			,
		|			Номенклатура В (&Номенклатура)
		|				И Склад В (&Склад) {((Номенклатура, Характеристика) В
		|			    (ВЫБРАТЬ
		|			        ОтборПоСегментуНоменклатуры.Номенклатура,
		|			        ОтборПоСегментуНоменклатуры.Характеристика
		|			    ИЗ
		|			        ОтборПоСегментуНоменклатуры
		|			    ГДЕ
		|			        ОтборПоСегментуНоменклатуры.ИспользуетсяОтборПоСегментуНоменклатуры = &ИспользуетсяОтборПоСегментуНоменклатуры)) КАК Поле2}) КАК СвободныеОстатки
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаХарактеристика
		|		ПО СвободныеОстатки.Склад = НастройкаХарактеристика.Склад
		|			И СвободныеОстатки.Номенклатура = НастройкаХарактеристика.Номенклатура
		|			И СвободныеОстатки.Характеристика = НастройкаХарактеристика.Характеристика
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаНоменклатура
		|		ПО СвободныеОстатки.Склад = НастройкаНоменклатура.Склад
		|			И СвободныеОстатки.Номенклатура = НастройкаНоменклатура.Номенклатура
		|			И (НастройкаНоменклатура.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
		|			И (НастройкаХарактеристика.Склад ЕСТЬ NULL )
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкаКонтроляОстатков КАК НастройкаСклад
		|		ПО СвободныеОстатки.Склад = НастройкаСклад.Склад
		|			И (НастройкаСклад.Номенклатура = ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка))
		|			И (НастройкаСклад.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
		|			И (НастройкаХарактеристика.Склад ЕСТЬ NULL )
		|			И (НастройкаНоменклатура.Склад ЕСТЬ NULL )
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВтРезервыПоГрафику КАК РезервыПоГрафику
		|		ПО СвободныеОстатки.Номенклатура = РезервыПоГрафику.Номенклатура
		|			И СвободныеОстатки.Характеристика = РезервыПоГрафику.Характеристика
		|			И СвободныеОстатки.Склад = РезервыПоГрафику.Склад
		|			И (ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомГрафика))
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПриоритетПоСкладам КАК ПриоритетПоСкладам
		|		ПО СвободныеОстатки.Склад = ПриоритетПоСкладам.Склад
		|ГДЕ
		|	(СвободныеОстатки.ВНаличииОстаток <> 0
		|			ИЛИ ВЫБОР
		|				КОГДА ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомРезерва)
		|					ТОГДА СвободныеОстатки.ВРезервеОстаток
		|				КОГДА ЕСТЬNULL(НастройкаХарактеристика.ВариантКонтроля, ЕСТЬNULL(НастройкаНоменклатура.ВариантКонтроля, НастройкаСклад.ВариантКонтроля)) = ЗНАЧЕНИЕ(Перечисление.ВариантыКонтроля.ОстаткиСУчетомГрафика)
		|					ТОГДА ЕСТЬNULL(РезервыПоГрафику.Количество, 0)
		|				ИНАЧЕ 0
		|			КОНЕЦ <> 0)
		|
		|УПОРЯДОЧИТЬ ПО
		|	Приоритет";

	Результат = Запрос.Выполнить().Выгрузить();
    Возврат Результат;
	//ЗначениеВРеквизитФормы(Результат,"ТЗОстатки1")
	//ВыборкаДетальныеЗаписи = Результат.Выбрать();
	//Если ВыборкаДетальныеЗаписи.Следующий() тогда
	//	Возврат ВыборкаДетальныеЗаписи.ВНаличииОстаток-ВыборкаДетальныеЗаписи.ВРезервеОстаток;
	//Иначе
	//	Возврат 0;
	//КонецЕсли;
КонецФункции

&НаСервере
Функция ПоискНоменклатурыНаСервере(КодНоменклатуры)
	Возврат Справочники.Номенклатура.НайтиПоКоду(КодНоменклатуры);
КонецФункции

&НаКлиенте
Процедура ТоварыКодНоменклатурыПриИзменении(Элемент)
	ТабЧасть = Элементы.Товары.ТекущиеДанные;
	ТабЧасть.Номенклатура = ПоискНоменклатурыНаСервере(ТабЧасть.КодНоменклатуры);
	Выборка  = ПолучитьЦену(ТабЧасть.Номенклатура);
	Для Каждого Строка Из  Выборка Цикл
		ТабЧасть.Цена = Строка.Цена;
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура ВидЦеныПриИзменении(Элемент)
	ИзменитьНоменклатуру();
КонецПроцедуры

&НаСервере
Процедура ИзменитьНоменклатуру()
	Для Каждого Строка из Объект.Товары Цикл
		Выборка = ПолучитьЦену(Строка.Номенклатура);
		Для Каждого Элемент из Выборка Цикл
			Строка.Цена = Элемент.Цена;
			Строка.Сумма = Строка.Цена * Строка.Количество; 
		КонецЦикла;
	КонецЦикла;
КонецПроцедуры
