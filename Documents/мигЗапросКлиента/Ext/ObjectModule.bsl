﻿
Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	Если ТипЗнч(ДанныеЗаполнения) = Тип("СправочникСсылка.Партнеры") Тогда
		Партнер = ДанныеЗаполнения;
		ПартнерыИКонтрагенты.ЗаполнитьКонтрагентаПартнераПоУмолчанию(Партнер, Контрагент);
		Организация = ЗначениеНастроекПовтИсп.ПолучитьОрганизациюПоУмолчанию(Организация);
		ДанныеЗаполнения = Неопределено;
	КонецЕсли; 
	ЗаполнитьПоЗначениямАвтозаполнения(ДанныеЗаполнения);
	//++dm 2016.10.24
	Если Не ЗначениеЗаполнено(Подразделение) Тогда 
		Подразделение = Справочники.СтруктураПредприятия.НайтиПоНаименованию("Парк Авеню",Истина);
	КонецЕсли;
	//--dm
КонецПроцедуры

Процедура ЗаполнитьПоЗначениямАвтозаполнения(ДанныеЗаполнения = Неопределено)
	
	ЕстьДанныеЗаполнения = (НЕ ДанныеЗаполнения = Неопределено);
	ДанныеЗаполненияСтруктура = (ТипЗнч(ДанныеЗаполнения) = Тип("Структура"));
	
	Если Не ЕстьДанныеЗаполнения Или ДанныеЗаполненияСтруктура Тогда
		// Заполним основные свойства
		СвойстваАвтозаполнения = Новый Структура("Организация");
		
		Если ДанныеЗаполненияСтруктура Тогда
			ОбщегоНазначенияУТКлиентСервер.ДополнитьСтруктуру(СвойстваАвтозаполнения, ДанныеЗаполнения, Истина);
			
		КонецЕсли;
		
		ОбщегоНазначенияУТ.ЗаполнитьЗначенияСвойствАвтозаполнения(Ссылка, СвойстваАвтозаполнения);
		
		ЗаполнитьЗначенияСвойств(ЭтотОбъект, СвойстваАвтозаполнения);
	КонецЕсли;

КонецПроцедуры

