﻿
////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС

Функция ОпределитьМетодОценкиСтоимости(МассивОрганизаций, Дата) Экспорт

	Если МассивОрганизаций.Количество() = 1 Тогда

		Результат = ЗапасыСервер.ПолучитьМетодОценкиСтоимостиТоваров(МассивОрганизаций[0], Дата);

	Иначе

		Результат = Перечисления.МетодыОценкиСтоимостиТоваров.СредняяЗаМесяц;

	КонецЕсли;

	Возврат Результат;

КонецФункции

Процедура ВыполнитьРегламентноеЗаданиеРасчетСебестоимости() Экспорт

	Дата = ТекущаяДата();

	УстановитьПривилегированныйРежим(Истина);

	Запрос = Новый Запрос(
	"
	|ВЫБРАТЬ
	|	Настройка.Организация КАК Организация
	|ИЗ
	|	РегистрСведений.НастройкаМетодовОценкиСтоимостиТоваров.СрезПоследних(&Период, ) КАК Настройка
	|ГДЕ
	|	Настройка.ОбновлятьСтоимостьРегламентнымЗаданием
	|");
	Запрос.УстановитьПараметр("Период", ТекущаяДата());
	МассивОрганизаций = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Организация");
	Если МассивОрганизаций.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;

	ВыполнитьРасчетСебестоимости(Дата, МассивОрганизаций);

КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

Функция ОрганизацииДляРасчетаСебестоимости(Дата, МассивОрганизаций)

	МассивГрупп = МассивОрганизацийДляРасчетаСебестоимости(Дата, Неопределено, Ложь, Истина);

	Результат = Новый Массив;
	Для Каждого ЭлементГруппа Из МассивГрупп Цикл

		ОрганизацияЕстьВМассиве = Ложь;
		Если ТипЗнч(ЭлементГруппа) = Тип("Массив") Тогда

			Для Каждого Организация Из ЭлементГруппа Цикл
				Если МассивОрганизаций.Найти(Организация) <> Неопределено Тогда

					ОрганизацияЕстьВМассиве = Истина;
					Прервать;

				КонецЕсли;
			КонецЦикла;

		Иначе

			ОрганизацияЕстьВМассиве = МассивОрганизаций.Найти(ЭлементГруппа) <> Неопределено;

		КонецЕсли;

		Если ОрганизацияЕстьВМассиве Тогда

			Если ТипЗнч(ЭлементГруппа) = Тип("Массив") Тогда

				НоваяОрганизация = Новый Массив;
				Для Каждого Организация Из ЭлементГруппа Цикл
					НоваяОрганизация.Добавить(Организация);
				КонецЦикла;

			Иначе

				НоваяОрганизация = ЭлементГруппа;

			КонецЕсли;

			Результат.Добавить(НоваяОрганизация);

		КонецЕсли;

	КонецЦикла;

	Возврат Результат;

КонецФункции

Процедура ВыполнитьРасчетСебестоимости(Дата, МассивОрганизаций)

	МассивДляРасчета = ОрганизацииДляРасчетаСебестоимости(Дата, МассивОрганизаций);
	Если МассивДляРасчета.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;

	Запрос = Новый Запрос(
	"
	|ВЫБРАТЬ
	|	РасчетСебестоимости.Ссылка КАК РасчетСебестоимости,
	|	РасчетСебестоимости.Ссылка.ПредварительныйРасчет КАК ПредварительныйРасчет,
	|	СУММА(1) КАК Количество
	|
	|ИЗ
	|	Документ.РасчетСебестоимостиТоваров.Организации КАК РасчетСебестоимости
	|ГДЕ
	|	РасчетСебестоимости.Организация В(&Организация)
	|	И РасчетСебестоимости.Ссылка.Проведен
	|	И РасчетСебестоимости.Ссылка.Дата Между &НачалоПериода И &КонецПериода
	|
	|СГРУППИРОВАТЬ ПО
	|	РасчетСебестоимости.Ссылка,
	|	ВЫБОР
	|		КОГДА РасчетСебестоимости.Ссылка.ПредварительныйРасчет
	|			ТОГДА ИСТИНА
	|		ИНАЧЕ ЛОЖЬ
	|	КОНЕЦ
	|
	|УПОРЯДОЧИТЬ ПО
	|	Количество УБЫВ
	|");
	Запрос.УстановитьПараметр("НачалоПериода", НачалоМесяца(Дата));
	Запрос.УстановитьПараметр("КонецПериода",  КонецМесяца(Дата));

	ИмяСобытияЖурналаРегистрации = НСтр("ru = 'Обновление стоимости товаров'");

	Для Каждого ЭлементГруппа Из МассивДляРасчета Цикл

		Запрос.УстановитьПараметр("Организация", ЭлементГруппа);
		Результат = Запрос.Выполнить();

		Выборка = Результат.Выбрать();
		Если Выборка.НайтиСледующий(Новый Структура("ПредварительныйРасчет", Ложь)) Тогда

			ЗаписьЖурналаРегистрации(ИмяСобытияЖурналаРегистрации,
					УровеньЖурналаРегистрации.Информация,,
					Выборка.РасчетСебестоимости,
					НСтр("ru = 'Себестоимость не обновлена. Документ проведен в статусе фактический.'"));

			Продолжить;

		КонецЕсли;

		МассивДокументов = Новый Массив;
		Если Не Результат.Пустой() Тогда

			Выборка.Сбросить();
			Пока Выборка.Следующий() Цикл

				МассивДокументов.Добавить(Выборка.РасчетСебестоимости.ПолучитьОбъект());

			КонецЦикла;

		Иначе

			МассивДокументов.Добавить(Документы.РасчетСебестоимостиТоваров.СоздатьДокумент());
			МассивДокументов[МассивДокументов.Количество()-1].ПредварительныйРасчет = Истина;

		КонецЕсли;

		МассивДокументов[0].Организации.Очистить();
		МассивДокументов[0].Дата = Дата;
		МассивДокументов[0].ДополнительныеСвойства.Вставить("РегламентныйРасчет");
		ЗаполнитьОрганизации(МассивДокументов[0], ЭлементГруппа);

		// Отмена проведения других документов.
		Если МассивДокументов.Количество() > 1 Тогда

			Для Счетчик = 1 По МассивДокументов.Количество() - 1 Цикл

				МассивДокументов[Счетчик].УстановитьПометкуУдаления(Истина);

				ЗаписьЖурналаРегистрации(ИмяСобытияЖурналаРегистрации,
						УровеньЖурналаРегистрации.Информация,,
						МассивДокументов[Счетчик].Ссылка,
						НСтр("ru = 'Пометка удаления документа расчета себестоимости'"));

			КонецЦикла;

		КонецЕсли;

		МассивДокументов[0].ВыполнитьРасчет();

		ЗаписьЖурналаРегистрации(ИмяСобытияЖурналаРегистрации,
				УровеньЖурналаРегистрации.Информация,,
				МассивДокументов[0].Ссылка,
				НСтр("ru = 'Обновлена стоимость товаров по организациям: '") + МассивДокументов[0].ПредставлениеОрганизаций);

	КонецЦикла;

КонецПроцедуры

Процедура ЗаполнитьОрганизации(Документ, ОрганизацияИлиМассивОрганизаций)

	Если ТипЗнч(ОрганизацияИлиМассивОрганизаций) = Тип("Массив") Тогда

		Для Каждого ТекОрганизация Из ОрганизацияИлиМассивОрганизаций Цикл
			Документ.Организации.Добавить().Организация = ТекОрганизация;
		КонецЦикла;

	Иначе

		Документ.Организации.Добавить().Организация = ОрганизацияИлиМассивОрганизаций;

	КонецЕсли;

КонецПроцедуры

Функция МассивОрганизацийДляРасчетаСебестоимости(Дата, РасчетСебестоимостиТоваров,
	ТолькоНеРассчитанные = Истина, ДополнятьИзСправочника = Истина, УчитыватьНастройкиПередачи = Истина) Экспорт

	Результат = Новый Массив;

	Запрос = Новый Запрос(
	"
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	Таблица.Организация КАК Организация,
	|	Таблица.КорОрганизация КАК КорОрганизация
	|ИЗ
	|	(ВЫБРАТЬ
	|		Таблица.Организация КАК Организация,
	|		Таблица.КорОрганизация КАК КорОрганизация
	|	ИЗ
	|		(ВЫБРАТЬ РАЗЛИЧНЫЕ
	|			СебестоимостьТоваров.Организация КАК Организация,
	|			СебестоимостьТоваров.КорОрганизация КАК КорОрганизация
	|		ИЗ
	|			РегистрНакопления.СебестоимостьТоваров КАК СебестоимостьТоваров
	|		ГДЕ
	|			СебестоимостьТоваров.Период МЕЖДУ &НачалоПериода И &КонецПериода
	|			И СебестоимостьТоваров.Активность) КАК Таблица
	|	ГДЕ
	|		Таблица.Организация <> Таблица.КорОрганизация
	|		И Таблица.КорОрганизация <> ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		НастройкаПередачи.ОрганизацияВладелец,
	|		НастройкаПередачи.ОрганизацияПродавец
	|	ИЗ
	|		РегистрСведений.НастройкаПередачиТоваровМеждуОрганизациями КАК НастройкаПередачи
	|	ГДЕ
	|		&ИспользоватьПередачиТоваров И &УчитыватьНастройкиПередачи) КАК Таблица
	|
	|УПОРЯДОЧИТЬ ПО
	|	Организация
	|");
	Запрос.УстановитьПараметр("НачалоПериода", НачалоМесяца(Дата));
	Запрос.УстановитьПараметр("КонецПериода",  КонецМесяца(Дата));
	Запрос.УстановитьПараметр("ИспользоватьПередачиТоваров", ПолучитьФункциональнуюОпцию("ИспользоватьПередачиТоваровМеждуОрганизациями"));
	Запрос.УстановитьПараметр("УчитыватьНастройкиПередачи", УчитыватьНастройкиПередачи);

	УстановитьПривилегированныйРежим(Истина);
	РезльтатЗапроса = Запрос.Выполнить();
	УстановитьПривилегированныйРежим(Ложь);

	// Необходимо собрать в группы организации, которые участвовали в перепродажах или межфирменных перемещениях.
	Если Не РезльтатЗапроса.Пустой() Тогда

		МассивГрупп = Новый Массив;
		Выборка = РезльтатЗапроса.Выбрать();
		Пока Выборка.Следующий() Цикл

			ГруппаОрганизации    = НайтиВМассивеГрупп(Выборка.Организация, МассивГрупп);
			ГруппаКорОрганизации = НайтиВМассивеГрупп(Выборка.КорОрганизация, МассивГрупп);
			Если ГруппаОрганизации = Неопределено И ГруппаКорОрганизации = Неопределено Тогда

				ДобавитьВМассивГрупп(Выборка.Организация, МассивГрупп, 0);
				ДобавитьВМассивГрупп(Выборка.КорОрганизация, МассивГрупп, 0);

			ИначеЕсли (ГруппаОрганизации = Неопределено И ГруппаКорОрганизации <> Неопределено)
				  Или (ГруппаОрганизации <> Неопределено И ГруппаКорОрганизации = Неопределено) Тогда

				ДобавитьВМассивГрупп(Выборка.Организация, МассивГрупп, ?(ГруппаОрганизации = Неопределено, ГруппаКорОрганизации, ГруппаОрганизации));
				ДобавитьВМассивГрупп(Выборка.КорОрганизация, МассивГрупп, ?(ГруппаОрганизации = Неопределено, ГруппаКорОрганизации, ГруппаОрганизации));

			ИначеЕсли ГруппаОрганизации <> Неопределено И ГруппаКорОрганизации <> Неопределено Тогда

				Если ГруппаОрганизации <>ГруппаКорОрганизации Тогда

					ПеренестиГруппуВГруппу(МассивГрупп, ГруппаОрганизации, ГруппаКорОрганизации);
					ДобавитьВМассивГрупп(Выборка.Организация, МассивГрупп, ГруппаОрганизации);
					ДобавитьВМассивГрупп(Выборка.Организация, МассивГрупп, ГруппаОрганизации);

				Иначе

					ДобавитьВМассивГрупп(Выборка.Организация, МассивГрупп, ГруппаОрганизации);
					ДобавитьВМассивГрупп(Выборка.Организация, МассивГрупп, ГруппаОрганизации);

				КонецЕсли;

			КонецЕсли;

		КонецЦикла;

		Результат = МассивГрупп;

	КонецЕсли;

	МасиивРассчитанныхОрганизаций = Новый Массив;

	Если ТолькоНеРассчитанные Тогда

		// Найдем все организации по которым себестоимость уже рассчитана
		Запрос = Новый Запрос(
		"
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	РасчетСебестоимости.Организация КАК Организация
		|ИЗ
		|	Документ.РасчетСебестоимостиТоваров.Организации КАК РасчетСебестоимости
		|ГДЕ
		|	РасчетСебестоимости.Ссылка.Дата МЕЖДУ &НачалоПериода И &КонецПериода
		|	И РасчетСебестоимости.Ссылка.Проведен
		|	И РасчетСебестоимости.Ссылка <> &РасчетСебестоимостиТоваров
		|");
		Запрос.УстановитьПараметр("НачалоПериода", НачалоМесяца(Дата));
		Запрос.УстановитьПараметр("КонецПериода",  КонецМесяца(Дата));
		Запрос.УстановитьПараметр("РасчетСебестоимостиТоваров", РасчетСебестоимостиТоваров);
		
		УстановитьПривилегированныйРежим(Истина);
		МасиивРассчитанныхОрганизаций = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Организация");
		УстановитьПривилегированныйРежим(Ложь);

		ВсегоЭлементов = Результат.ВГраница();
		Для Счетчик = 0 По ВсегоЭлементов Цикл

			Индекс = ВсегоЭлементов - Счетчик;
			Группа = Результат[Индекс];
			ВсяГруппаРасссчитана = Истина;
			Для Каждого Организация Из Группа Цикл

				ВсяГруппаРасссчитана = ВсяГруппаРасссчитана И МасиивРассчитанныхОрганизаций.Найти(Организация) <> Неопределено;

			КонецЦикла;

			Если ВсяГруппаРасссчитана Тогда

				Результат.Удалить(Индекс);

			КонецЕсли;

		КонецЦикла;
	КонецЕсли;
	
	Если ДополнятьИзСправочника Тогда
		// Дополним организациями из справочника.
		Запрос = Новый Запрос(
		"
		|ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	Организации.Ссылка КАК Организация,
		|	ВЫБОР
		|		КОГДА Организации.Ссылка = ЗНАЧЕНИЕ(Справочник.Организации.Управленческаяорганизация) ТОГДА
		|			0
		|		ИНАЧЕ
		|			100
		|	КОНЕЦ КАК Приоритет
		|ИЗ
		|	Справочник.Организации КАК Организации
		|ГДЕ
		|	НЕ Организации.ПометкаУдаления
		|	И ВЫБОР
		|			КОГДА НЕ &ИспользоватьУпрОрганизацию
		|					И Организации.Ссылка = ЗНАЧЕНИЕ(Справочник.Организации.Управленческаяорганизация) ТОГДА
		|				ЛОЖЬ
		|			ИНАЧЕ
		|				ИСТИНА
		|		КОНЕЦ
		|	И НЕ Организации.Ссылка В (&МасиивРассчитанныхОрганизаций)
		|
		|УПОРЯДОЧИТЬ ПО
		|	Приоритет Убыв,
		|	Организация
		|");
		Запрос.УстановитьПараметр("МасиивРассчитанныхОрганизаций", МасиивРассчитанныхОрганизаций);
		Запрос.УстановитьПараметр("ИспользоватьУпрОрганизацию", ПолучитьФункциональнуюОпцию("ИспользоватьУправленческуюОрганизацию"));
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() Цикл

			Если НайтиВМассивеГрупп(Выборка.Организация, Результат) = Неопределено Тогда
				Результат.Добавить(Выборка.Организация);
			КонецЕсли;

		КонецЦикла;
	КонецЕсли;

	Возврат Результат;

КонецФункции

Процедура ДобавитьВМассивГрупп(Значение, МассивГрупп, ПозицияГруппы)

	Если МассивГрупп.Количество() < (ПозицияГруппы+1) Тогда

		МассивГрупп.Добавить(Новый Массив);

	КонецЕсли;

	Если МассивГрупп[ПозицияГруппы].Найти(Значение) = Неопределено Тогда 
		МассивГрупп[ПозицияГруппы].Добавить(Значение);
	КонецЕсли;

КонецПроцедуры

Процедура ПеренестиГруппуВГруппу(МассивГрупп, ПозцияГруппыКуда, ПозицияГруппыОткуда)

	Для Каждого Значение Из МассивГрупп[ПозицияГруппыОткуда] Цикл

		Если МассивГрупп[ПозцияГруппыКуда].Найти(Значение) = Неопределено Тогда 
			МассивГрупп[ПозцияГруппыКуда].Добавить(Значение);
		КонецЕсли;

	КонецЦикла;

	МассивГрупп.Удалить(ПозицияГруппыОткуда);

КонецПроцедуры

Функция НайтиВМассивеГрупп(Значение, МассивГрупп)

	Для СчетчикГрупп = 0 По МассивГрупп.Количество() - 1 Цикл
		
		Если ТипЗнч(МассивГрупп[СчетчикГрупп]) = Тип("Массив") Тогда

			Для Каждого ТекущееЗначение Из МассивГрупп[СчетчикГрупп] Цикл
				Если Значение = ТекущееЗначение Тогда

					Возврат СчетчикГрупп;

				КонецЕсли;
			КонецЦикла;

		Иначе

			Если Значение = МассивГрупп[СчетчикГрупп] Тогда

				Возврат СчетчикГрупп;

			КонецЕсли;

		КонецЕсли;

	КонецЦикла;

	Возврат Неопределено;

КонецФункции

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

//Обновление
Процедура ЗаполнитьОрганизацииИМетодОценки() Экспорт

	УстановитьПривилегированныйРежим(Истина);

	Запрос = Новый Запрос(
	"
	|ВЫБРАТЬ
	|	РасчетСебестоимости.Ссылка КАК Ссылка
	|ИЗ
	|	Документ.РасчетСебестоимостиТоваров КАК РасчетСебестоимости
	|ГДЕ
	|	РасчетСебестоимости.МетодОценки = Значение(Перечисление.МетодыОценкиСтоимостиТоваров.ПустаяСсылка)
	|	И РасчетСебестоимости.УдалитьОрганизация <> Значение(Справочник.Организации.ПустаяСсылка)
	|");
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл

		ТекОбъект = Выборка.Ссылка.ПолучитьОбъект();
		ТекОбъект.МетодОценки = ЗначениеНастроекПовтИсп.ПолучитьМетодОценкиСтоимостиТоваров(ТекОбъект.УдалитьОрганизация, ТекОбъект.Дата);
		ТекОбъект.ОбменДанными.Загрузка = Истина;
		ТекОбъект.Записать(РежимЗаписиДокумента.Запись);

	КонецЦикла;

	Запрос = Новый Запрос(
	"
	|ВЫБРАТЬ
	|	РасчетСебестоимости.Ссылка КАК Ссылка
	|ИЗ
	|	Документ.РасчетСебестоимостиТоваров КАК РасчетСебестоимости
	|ГДЕ
	|	НЕ 1 В
	|		(ВЫБРАТЬ ПЕРВЫЕ 1
	|			1
	|		ИЗ
	|			Документ.РасчетСебестоимостиТоваров.Организации КАК Т
	|		ГДЕ
	|			Т.Ссылка = РасчетСебестоимости.Ссылка)
	|");
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл

		ТекОбъект = Выборка.Ссылка.ПолучитьОбъект();

		Если ТекОбъект.Организации.Найти(ТекОбъект.УдалитьОрганизация, "Организация") = Неопределено Тогда
			ТекОбъект.Организации.Добавить().Организация =  ТекОбъект.УдалитьОрганизация;
		КонецЕсли;

		ТекОбъект.ОбменДанными.Загрузка = Истина;
		ТекОбъект.Записать(РежимЗаписиДокумента.Запись);

	КонецЦикла;

КонецПроцедуры