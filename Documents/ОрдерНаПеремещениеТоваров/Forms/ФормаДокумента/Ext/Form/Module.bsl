﻿&НаКлиенте
Перем КэшированныеЗначения; //используется механизмом обработки изменения реквизитов ТЧ

&НаКлиенте
Перем ТекущийСтатус; //клиентский кеш статуса документа (нужен для возможности отката изменения пользователем статуса)

&НаКлиенте
Перем ТекущиеДанныеИдентификатор; //используется для передачи текущей строки в обработчик ожидания

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Обработчик подсистемы "Внешние обработки"
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	
	ОбщегоНазначенияУТ.НастроитьПодключаемоеОборудование(ЭтаФорма);
	
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриЧтенииСозданииНаСервере();
		ОбработкаТабличнойЧастиСервер.ЗаполнитьСтатусыУказанияСерий(Объект,ПараметрыУказанияСерий);
	КонецЕсли;
	
КонецПроцедуры // ПриСозданииНаСервере()

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	// МеханизмВнешнегоОборудования
	Если ИспользоватьПодключаемоеОборудование // Проверка на включенную ФО "Использовать ВО"
	   И МенеджерОборудованияКлиент.ОбновитьРабочееМестоКлиента()Тогда // Проверка на определенность рабочего места ВО
		ОписаниеОшибки = "";

		ПоддерживаемыеТипыВО = Новый Массив();
		ПоддерживаемыеТипыВО.Добавить("СканерШтрихкода");

		Если Не МенеджерОборудованияКлиент.ПодключитьОборудованиеПоТипу(УникальныйИдентификатор, ПоддерживаемыеТипыВО, ОписаниеОшибки) Тогда
			ТекстСообщения = НСтр("ru = 'При подключении оборудования произошла ошибка:
			                      |""%ОписаниеОшибки%"".'");
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ОписаниеОшибки%", ОписаниеОшибки);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
		КонецЕсли;
	КонецЕсли;
	// Конец МеханизмВнешнегоОборудования
	
	ТекущийСтатус = Объект.Статус;
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	// Обработчик механизма "ДатыЗапретаИзменения"
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
	ПриЧтенииСозданииНаСервере();
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	Элементы.ГруппаКомментарий.Картинка = ОбщегоНазначенияУТ.ПолучитьКартинкуКомментария(Объект.Комментарий);
	ОбработкаТабличнойЧастиСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(
		Объект.Товары,
		Новый Структура("ЗаполнитьПризнакХарактеристикиИспользуются",
			Новый Структура("Номенклатура", "ХарактеристикиИспользуются")
		)
	);
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	Оповестить("Запись_ОрдерНаПеремещениеТоваров", ПараметрыЗаписи, Объект.Ссылка);
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	
	// МеханизмВнешнегоОборудования
	ПоддерживаемыеТипыВО = Новый Массив();
	ПоддерживаемыеТипыВО.Добавить("СканерШтрихкода");

	МенеджерОборудованияКлиент.ОтключитьОборудованиеПоТипу(УникальныйИдентификатор, ПоддерживаемыеТипыВО);
	// Конец МеханизмВнешнегоОборудования

КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// ПодключаемоеОборудование
	Если Источник = "ПодключаемоеОборудование"
	   И ВводДоступен() Тогда
		Если ИмяСобытия = "ScanData" Тогда
			//Преобразуем предварительно к ожидаемому формату
			Данные = Новый Массив();
			Если Параметр[1] = Неопределено Тогда
				Данные.Добавить(Новый Структура("Штрихкод, Количество", Параметр[0], 1)); // Достаем штрихкод из основных данных
			Иначе
				Данные.Добавить(Новый Структура("Штрихкод, Количество", Параметр[1][1], 1)); // Достаем штрихкод из дополнительных данных
			КонецЕсли;

			ОбработатьШтрихкоды(Данные);
		КонецЕсли;
	КонецЕсли;
	// Конец ПодключаемоеОборудование

	// Неизвестные штрихкоды
	Если Источник = "ПодключаемоеОборудование"
		И ИмяСобытия = "НеизвестныеШтрихкоды"
		И Параметр.ФормаВладелец = УникальныйИдентификатор Тогда
		
		КэшированныеЗначения.Штрихкоды.Очистить();
		ДанныеШтрихкодов = Новый Массив;
		Для Каждого ПолученныйШтрихкод Из Параметр.ПолученыНовыеШтрихкоды Цикл
			ДанныеШтрихкодов.Добавить(ПолученныйШтрихкод);
		КонецЦикла;
		Для Каждого ПолученныйШтрихкод Из Параметр.ЗарегистрированныеШтрихкоды Цикл
			ДанныеШтрихкодов.Добавить(ПолученныйШтрихкод);
		КонецЦикла;
		
		ОбработатьШтрихкоды(ДанныеШтрихкодов);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	Если ИсточникВыбора.ИмяФормы = "Обработка.ПроверкаКоличестваТоваровВДокументе.Форма.Форма" Тогда
		
		ОбработкаВыбораПроверкаКоличестваТоваровНаСервере(ВыбранноеЗначение);
		
	КонецЕсли;
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ШАПКИ ФОРМЫ

&НаКлиенте
Процедура ПомещениеОтправительПриИзменении(Элемент)
	ПомещениеОтправительПриИзмененииСервер();	
КонецПроцедуры

&НаКлиенте
Процедура ПомещениеПолучательПриИзменении(Элемент)
	ПомещениеПолучательПриИзмененииСервер();	
КонецПроцедуры

&НаКлиенте
Процедура СтатусПриИзменении(Элемент)
	
	Если АдресноеХранениеОтправитель Тогда 
		Элементы.Товары.КоманднаяПанель.ПодчиненныеЭлементы.ТоварыПроверитьКоличество.Доступность = (Объект.Статус = ПредопределенноеЗначение("Перечисление.СтатусыОрдеровНаПеремещение.КПроверке"));
		
		Если ТекущийСтатус = ПредопределенноеЗначение("Перечисление.СтатусыОрдеровНаПеремещение.КОтбору") Тогда
			
			ТекстВопроса = НСтр("ru='При изменении статуса будет произведено перезаполнение ордера по отобранным из ячеек товарам.'");
			
			Ответ = Вопрос(ТекстВопроса,РежимДиалогаВопрос.ОКОтмена,,КодВозвратаДиалога.ОК);
			
			Если Ответ <> КодВозвратаДиалога.ОК Тогда
				Объект.Статус = ТекущийСтатус;
			Иначе
				СтатусПриИзмененииСервер(Истина);
			КонецЕсли;
		Иначе
			СтатусПриИзмененииСервер(Ложь);
		КонецЕсли;
	Иначе
		СтатусПриИзмененииСервер(Ложь);
	КонецЕсли;
	
	ТекущийСтатус = Объект.Статус;

КонецПроцедуры

&НаКлиенте
Процедура СкладПриИзменении(Элемент)
	
	СкладПриИзмененииНаСервере()
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ТАБЛИЦЫ ФОРМЫ ТОВАРЫ

&НаКлиенте
Процедура УпаковкаПриИзменении(Элемент)

	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	
	СтруктураДействий = Новый Структура();
	СтруктураДействий.Вставить("ПересчитатьКоличествоУпаковок");
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока,	СтруктураДействий, КэшированныеЗначения);

КонецПроцедуры

&НаКлиенте
Процедура ТоварыНоменклатураПриИзменении(Элемент)
	Перем ТекущаяСтрока;
	Перем СтруктураДействий;

	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;

	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПроверитьХарактеристикуПоВладельцу", ТекущаяСтрока.Характеристика);
	СтруктураДействий.Вставить("ПроверитьЗаполнитьУпаковкуПоВладельцу"      , ТекущаяСтрока.Упаковка);
	СтруктураДействий.Вставить("ПересчитатьКоличествоЕдиниц");

	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);

КонецПроцедуры

&НаКлиенте
Процедура КоличествоУпаковокПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	
	СтруктураДействий = Новый Структура();
	ДобавитьВСтруктуруДействияПриИзмененииКоличестваУпаковок(СтруктураДействий);
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока,	СтруктураДействий, КэшированныеЗначения);

КонецПроцедуры

&НаКлиенте
Процедура ТоварыПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	
	ОбработкаТабличнойЧастиКлиент.ОбновитьКешированныеЗначенияДляУчетаСерий(
				Элемент,КэшированныеЗначения,ПараметрыУказанияСерий,Копирование);

КонецПроцедуры

&НаКлиенте
Процедура ТоварыПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	ТекущиеДанные = Элементы.Товары.ТекущиеДанные;
	
	Если ОбработкаТабличнойЧастиКлиент.НеобходимоОбновитьСтатусыСерий(
		Элемент,КэшированныеЗначения,ПараметрыУказанияСерий) Тогда
		
		ТекущаяСтрокаИдентификатор = ТекущиеДанные.ПолучитьИдентификатор();
		
		ЗаполнитьСтатусыУказанияСерийПриОкончанииРедактированияСтрокиТЧ(ТекущаяСтрокаИдентификатор, КэшированныеЗначения);
		ОбработкаТабличнойЧастиКлиент.ОбновитьКешированныеЗначенияДляУчетаСерий(
					Элемент,КэшированныеЗначения,ПараметрыУказанияСерий);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыПередУдалением(Элемент, Отказ)
	ОбработкаТабличнойЧастиКлиент.ОбновитьКешированныеЗначенияДляУчетаСерий(
				Элемент,КэшированныеЗначения,ПараметрыУказанияСерий);
КонецПроцедуры

&НаКлиенте
Процедура ТоварыПослеУдаления(Элемент)
	
	Если ОбработкаТабличнойЧастиКлиент.НеобходимоОбновитьСтатусыСерий(
		Элемент,КэшированныеЗначения,ПараметрыУказанияСерий,Истина) Тогда
		
		ЗаполнитьСтатусыУказанияСерийПриОкончанииРедактированияСтрокиТЧ(Неопределено, КэшированныеЗначения);
		ОбработкаТабличнойЧастиКлиент.ОбновитьКешированныеЗначенияДляУчетаСерий(
					Элемент,КэшированныеЗначения,ПараметрыУказанияСерий);
		
	КонецЕсли;
				
КонецПроцедуры

&НаКлиенте
Процедура ТоварыВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	Если Поле = Элементы.ТоварыСтатусУказанияСерий Тогда
		Если ОбработкаТабличнойЧастиКлиент.ПроверитьВозможностьУказанияСерий(ЭтаФорма,ПараметрыУказанияСерий) Тогда
			ТекущиеДанныеИдентификатор = Элементы.Товары.ТекущиеДанные.ПолучитьИдентификатор();
			ПараметрыФормыУказанияСерий = ПараметрыФормыУказанияСерий(ТекущиеДанныеИдентификатор);
			
			ЗначениеВозврата = ОткрытьФормуМодально(ПараметрыФормыУказанияСерий.ИмяФормы,ПараметрыФормыУказанияСерий,ЭтаФорма);
			
			Если ЗначениеВозврата <> Неопределено Тогда
				ОбработатьУказаниеСерийСервер(ПараметрыФормыУказанияСерий);
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

 &НаКлиенте
Процедура ПроверитьКоличество(Команда)
	ОчиститьСообщения();
	
	Отказ = Ложь;
	
	Если Модифицированность
		Или Не Объект.Проведен Тогда
		Ответ = Вопрос(НСтр("ru = 'Перед началом проверки документ будет проведен. Продолжить?'"), РежимДиалогаВопрос.ОКОтмена, ,КодВозвратаДиалога.Отмена);
		Если Ответ = КодВозвратаДиалога.Отмена Тогда
			Отказ = Истина;
		Иначе
			Отказ = Не Записать(Новый Структура("РежимЗаписи", РежимЗаписиДокумента.Проведение));
		КонецЕсли;
	КонецЕсли;
	
	Если Не Отказ Тогда
	
	Если ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПараметрЗаголовок = НСтр("ru = 'Проверка количества в документе %Документ%'");
		ПараметрЗаголовок = СтрЗаменить(ПараметрЗаголовок, "%Документ%", Объект.Ссылка);
	Иначе
		ПараметрЗаголовок = НСтр("ru = 'Проверка количества в ордере на перемещение товаров'");
	КонецЕсли;
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("Ордер", Объект.Ссылка);
	ПараметрыФормы.Вставить("Заголовок", ПараметрЗаголовок);
	ПараметрыФормы.Вставить("Склад", Объект.Склад);
	ПараметрыФормы.Вставить("Помещение", Объект.ПомещениеОтправитель);
	
	ОткрытьФорму("Обработка.ПроверкаКоличестваТоваровВДокументе.Форма.Форма", ПараметрыФормы, ЭтаФорма);
	
	Закрыть();
	
	КонецЕсли;	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьПоОтобраннымТоварам(Команда)

	Если Объект.Товары.Количество() > 0 Тогда

		ТекстВопроса = НСтр("ru = 'Перед заполнением табличная часть будет очищена. Заполнить?'");
		
		Ответ = Вопрос(ТекстВопроса, РежимДиалогаВопрос.ДаНет);
		Если Ответ <> КодВозвратаДиалога.Да Тогда
			Возврат;
		КонецЕсли;

	КонецЕсли;

	ЗаполнитьПоОтобраннымТоварамСервер();
	
КонецПроцедуры

&НаКлиенте
Процедура ПоискПоШтрихкодуВыполнить(Команда)
	
	ОчиститьСообщения();
	ТекШтрихкод = "";
	Если ОбработкаТабличнойЧастиКлиент.ВвестиШтрихкод(ТекШтрихкод) Тогда
		ОбработатьШтрихкоды(ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруДанныхШтрихкода(ТекШтрихкод, 1));
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьДанныеИзТСД(Команда)
	
	ОчиститьСообщения();
	
	ОписаниеОшибки = "";
	
	Если МенеджерОборудованияКлиент.ОбновитьРабочееМестоКлиента() Тогда
		// Подключение устройства
		ИдентификаторУстройства = МенеджерОборудованияКлиент.ВыбратьУстройство("ТерминалСбораДанных",
		    НСтр("ru='Выберите терминал сбора данных'"), НСтр("ru='Терминал сбора данных не подключен'"));
		
		Если ИдентификаторУстройства <> Неопределено Тогда
			
			Результат = МенеджерОборудованияКлиент.ПодключитьОборудованиеПоИдентификатору(УникальныйИдентификатор, ИдентификаторУстройства, ОписаниеОшибки);
			
			Если Результат Тогда
				ВходныеПараметры  = Неопределено;
				ВыходныеПараметры = Неопределено;
				
				Результат = МенеджерОборудованияКлиент.ВыполнитьКоманду(ИдентификаторУстройства,
				                                                        "DownloadDocument",
				                                                        ВходныеПараметры,
				                                                        ВыходныеПараметры);
				
				Если Результат Тогда
					
					ТаблицаЗагрузкиИзТСД = Новый Массив();
					Для Индекс = 0 По ВыходныеПараметры[0].Количество()/2 - 1 Цикл
						
						Штрихкод   = ВыходныеПараметры[0][Индекс * 2 + 0];
						Количество = ВыходныеПараметры[0][Индекс * 2 + 1];
						
						ТаблицаЗагрузкиИзТСД.Добавить(Новый Структура("Штрихкод, Количество", Штрихкод, ?(Количество <> Неопределено, Количество, 0)));
						
					КонецЦикла;
					
					ОбработатьШтрихкоды(ТаблицаЗагрузкиИзТСД);
					
				Иначе
					
					ТекстСообщения = НСтр("ru='При загрузке данных из терминала сбора данных произошла ошибка.
					                          |%ОписаниеОшибки%
					                          |Данные из терминала сбора данных не загружены.'");
					ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ОписаниеОшибки%", ВыходныеПараметры[1]);
					
					ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
				КонецЕсли;
				
				МенеджерОборудованияКлиент.ОтключитьОборудованиеПоИдентификатору(УникальныйИдентификатор, ИдентификаторУстройства);
				
			Иначе
				ТекстСообщения = НСтр("ru='При подключении устройства произошла ошибка.
				                          |%ОписаниеОшибки%
				                          |Данные из терминала сбора данных не загружены.'");
				ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ОписаниеОшибки%", ОписаниеОшибки);
				
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
			КонецЕсли;
		КонецЕсли;
	Иначе
		ТекстСообщения = НСтр("ru = 'Предварительно необходимо выбрать рабочее место подключаемого оборудования текущего сеанса.'");
		
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура УказатьСерии(Команда)
	
	Если ОбработкаТабличнойЧастиКлиент.ПроверитьВозможностьУказанияСерий(ЭтаФорма,ПараметрыУказанияСерий) Тогда
		ТекущиеДанныеИдентификатор = Элементы.Товары.ТекущиеДанные.ПолучитьИдентификатор();
		ПараметрыФормыУказанияСерий = ПараметрыФормыУказанияСерий(ТекущиеДанныеИдентификатор);
		
		ЗначениеВозврата = ОткрытьФормуМодально(ПараметрыФормыУказанияСерий.ИмяФормы,ПараметрыФормыУказанияСерий,ЭтаФорма);
		
		Если ЗначениеВозврата <> Неопределено Тогда
			ОбработатьУказаниеСерийСервер(ПараметрыФормыУказанияСерий);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьСерииПоFEFO(Команда)
	Если ЕстьЗаполненныеСерииПоFEFO() Тогда
		Если Не ОбработкаТабличнойЧастиКлиент.ЗадатьВопросОПерезаполненииСерийПоFEFO() Тогда
			Возврат;
		КонецЕсли;
	КонецЕсли;
	ЗаполнитьСерииПоFEFOСервер();	
КонецПроцедуры

&НаКлиенте
Процедура РазбитьСтроку(Команда)
	
	ТаблицаФормы  = Элементы.Товары;
	ДанныеТаблицы = Объект.Товары;
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	
	НоваяСтрока = ОбработкаТабличнойЧастиКлиент.РазбитьСтрокуТЧ(ДанныеТаблицы, ТаблицаФормы);
	
	Если НоваяСтрока <> Неопределено Тогда
		
		СтруктураДействий = Новый Структура;
		ДобавитьВСтруктуруДействияПриИзмененииКоличестваУпаковок(СтруктураДействий);		
		
		ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
		ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(НоваяСтрока, СтруктураДействий, КэшированныеЗначения);
		
	КонецЕсли;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// При изменении реквизитов

&НаСервере
Процедура СтатусПриИзмененииСервер(ЗаполнятьПоОтобраннымТоварам)
	Если ЗаполнятьПоОтобраннымТоварам Тогда
		ЗаполнитьПоОтобраннымТоварамСервер();
	Иначе
		ПараметрыУказанияСерий = Новый ФиксированнаяСтруктура(Документы.ОрдерНаПеремещениеТоваров.ПараметрыУказанияСерий(Объект));
		ОбработкаТабличнойЧастиСервер.ЗаполнитьСтатусыУказанияСерий(Объект,ПараметрыУказанияСерий);
	КонецЕсли;
	
	Если Объект.Статус = Перечисления.СтатусыОрдеровНаПеремещение.Принят Тогда
		Объект.ДатаОтгрузки = ТекущаяДата();
	КонецЕсли;
	
	УстановитьДоступность(ЭтаФорма);	
КонецПроцедуры

&НаСервере
Процедура ПомещениеОтправительПриИзмененииСервер()
	СтруктураПараметров = Новый Структура("Помещение",Объект.ПомещениеОтправитель);
	АдресноеХранениеОтправитель = ПолучитьФункциональнуюОпцию("ИспользоватьАдресноеХранениеПомещение",СтруктураПараметров);
	
	Элементы.ЗонаОтгрузки.Видимость = АдресноеХранениеОтправитель;
	
	Если АдресноеХранениеОтправитель Тогда
		Объект.ЗонаОтгрузки = Справочники.СкладскиеЯчейки.ЗонаОтгрузкиПоУмолчанию(Объект.Склад, Объект.ПомещениеОтправитель);
		Элементы.Товары.КоманднаяПанель.ПодчиненныеЭлементы.ТоварыПроверитьКоличество.Доступность = (Объект.Статус = Перечисления.СтатусыОрдеровНаПеремещение.КПроверке);
	Иначе 
		Элементы.Товары.КоманднаяПанель.ПодчиненныеЭлементы.ТоварыПроверитьКоличество.Доступность = Истина;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПомещениеПолучательПриИзмененииСервер()
	СтруктураПараметров = Новый Структура("Помещение",Объект.ПомещениеПолучатель);
	АдресноеХранениеПолучатель =  ПолучитьФункциональнуюОпцию("ИспользоватьАдресноеХранениеПомещение",СтруктураПараметров);
	
	Элементы.ЗонаПриемки.Видимость = АдресноеХранениеПолучатель;
	
	Если АдресноеХранениеПолучатель Тогда
		Объект.ЗонаПриемки = Справочники.СкладскиеЯчейки.ЗонаПриемкиПоУмолчанию(Объект.Склад, Объект.ПомещениеПолучатель);
	КонецЕсли;
	
	ПараметрыУказанияСерий = Новый ФиксированнаяСтруктура(Документы.ОрдерНаПеремещениеТоваров.ПараметрыУказанияСерий(Объект));
	ОбработкаТабличнойЧастиСервер.ЗаполнитьСтатусыУказанияСерий(Объект,Документы.ОрдерНаПеремещениеТоваров.ПараметрыУказанияСерий(Объект));
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьПоОтобраннымТоварамСервер()

	ТекОбъект = РеквизитФормыВЗначение("Объект");

	ТекОбъект.Товары.Очистить();
	ТекОбъект.ЗаполнитьОтобраннымиТоварами();

	ЗначениеВРеквизитФормы(ТекОбъект, "Объект");
	
	ОбработкаТабличнойЧастиСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(
		Объект.Товары,
		Новый Структура("ЗаполнитьПризнакХарактеристикиИспользуются",
			Новый Структура("Номенклатура", "ХарактеристикиИспользуются")
		)
	);
	ПараметрыУказанияСерий = Новый ФиксированнаяСтруктура(Документы.ОрдерНаПеремещениеТоваров.ПараметрыУказанияСерий(Объект));
	ОбработкаТабличнойЧастиСервер.ЗаполнитьСтатусыУказанияСерий(Объект,ПараметрыУказанияСерий);	
	
КонецПроцедуры

&НаСервере
Процедура СкладПриИзмененииНаСервере()
	
	ПараметрыУказанияСерий = Новый ФиксированнаяСтруктура(Документы.ОрдерНаПеремещениеТоваров.ПараметрыУказанияСерий(Объект));
	
	ОбработкаТабличнойЧастиСервер.ЗаполнитьСтатусыУказанияСерий(Объект,Документы.ОрдерНаПеремещениеТоваров.ПараметрыУказанияСерий(Объект));
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Подборы и обработка проверки количества

&НаСервере
Процедура ОбработкаВыбораПроверкаКоличестваТоваровНаСервере(ВыбранноеЗначение)
	
	ТаблицаТоваров = ПолучитьИзВременногоХранилища(ВыбранноеЗначение.АдресТоваровВХранилище);
	Объект.Товары.Загрузить(ТаблицаТоваров);
	
	Если ПараметрыУказанияСерий.ИспользоватьСерииНоменклатуры Тогда
		ТаблицаСерий   = ПолучитьИзВременногоХранилища(ВыбранноеЗначение.АдресСерийВХранилище);
		Объект.Серии.Загрузить(ТаблицаСерий);
	КонецЕсли;
	
	ОбработкаТабличнойЧастиСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(
		Объект.Товары,
		Новый Структура("ЗаполнитьПризнакХарактеристикиИспользуются",
			Новый Структура("Номенклатура", "ХарактеристикиИспользуются")
		)
	);
	ОбработкаТабличнойЧастиСервер.ЗаполнитьСтатусыУказанияСерий(Объект,ПараметрыУказанияСерий);
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Штрихкоды и торговое оборудование

&НаКлиенте
Процедура ОбработатьШтрихкоды(ДанныеШтрикодов)
	СтруктураДействийСДобавленнымиСтроками = Новый Структура;
	ДобавитьВСтруктуруДействияПриИзмененииКоличестваУпаковок(СтруктураДействийСДобавленнымиСтроками);

	СтруктураДействийСИзмененнымиСтроками = Новый Структура;
	ДобавитьВСтруктуруДействияПриИзмененииКоличестваУпаковок(СтруктураДействийСИзмененнымиСтроками);
	
	СтруктураДействий = ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруОбработкиШтрихкодов(
	       ДанныеШтрикодов,
	       СтруктураДействийСДобавленнымиСтроками,
	       СтруктураДействийСИзмененнымиСтроками,
		   ПараметрыУказанияСерий);
	
	ОбработатьШтрихкодыСервер(СтруктураДействий,КэшированныеЗначения);
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьНеизвестныеШтрихкоды(СтруктураДействий,КэшированныеЗначения,ЭтаФорма);
	
	Если СтруктураДействий.МассивСтрокССериями.Количество() = 1
		И ОбработкаТабличнойЧастиКлиентСервер.НеобходимоРегистрироватьСерии(ПараметрыУказанияСерий) Тогда
		
		ТекущиеДанныеИдентификатор = СтруктураДействий.МассивСтрокССериями[0];
		
		ПодключитьОбработчикОжидания("ОткрытьФормуУказанияСерий",0.1,Истина);
		
	КонецЕсли;
	
	Если СтруктураДействий.ТекущаяСтрока <> Неопределено Тогда
		Элементы.Товары.ТекущаяСтрока = СтруктураДействий.ТекущаяСтрока;		
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура ОбработатьШтрихкодыСервер(СтруктураПараметровДействия,КэшированныеЗначения)
	ОбработкаТабличнойЧастиСервер.ОбработатьШтрихкоды(ЭтаФорма,Объект,СтруктураПараметровДействия,КэшированныеЗначения);
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Серии

&НаКлиенте
Процедура ОткрытьФормуУказанияСерий()
	Если ТекущиеДанныеИдентификатор = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ПараметрыФормыУказанияСерий = ПараметрыФормыУказанияСерий(ТекущиеДанныеИдентификатор);
	
	ЗначениеВозврата = ОткрытьФормуМодально(ПараметрыФормыУказанияСерий.ИмяФормы ,ПараметрыФормыУказанияСерий,ЭтаФорма);
	
	Если ЗначениеВозврата <> Неопределено Тогда
		ОбработатьУказаниеСерийСервер(ПараметрыФормыУказанияСерий);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСтатусыУказанияСерийПриОкончанииРедактированияСтрокиТЧ(ТекущаяСтрокаИдентификатор, КэшированныеЗначения)
	
	ОбработкаТабличнойЧастиСервер.ЗаполнитьСтатусыУказанияСерийПриОкончанииРедактированияСтрокиТЧ(Объект, 
				ПараметрыУказанияСерий, ТекущаяСтрокаИдентификатор, КэшированныеЗначения);
			
КонецПроцедуры

&НаСервере
Процедура ОбработатьУказаниеСерийСервер(ПараметрыФормыУказанияСерий)
	
	ОбработкаТабличнойЧастиСервер.ОбработатьУказаниеСерий(Объект,ПараметрыУказанияСерий,ПараметрыФормыУказанияСерий);

КонецПроцедуры

&НаСервере
Функция ПараметрыФормыУказанияСерий(ТекущиеДанныеИдентификатор)
	
	Возврат ОбработкаТабличнойЧастиСервер.ПоместитьСерииВХранилище(Объект, ПараметрыУказанияСерий, ТекущиеДанныеИдентификатор, ЭтаФорма);
	
КонецФункции

////////////////////////////////////////////////////////////////////////////////
// Прочее

&НаСервере
Процедура ЗаполнитьСерииПоFEFOСервер()
	ОбработкаТабличнойЧастиСервер.ЗаполнитьСерииПоFEFO(Объект,ПараметрыУказанияСерий);	
КонецПроцедуры

&НаСервере
Функция ЕстьЗаполненныеСерииПоFEFO()
	Возврат ОбработкаТабличнойЧастиСервер.ЕстьСтрокиСЗаполненнымиПоFEFOСериями(Объект.Товары);
КонецФункции

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	СтруктураПараметров = Новый Структура("Помещение",Объект.ПомещениеОтправитель);
	АдресноеХранениеОтправитель = ПолучитьФункциональнуюОпцию("ИспользоватьАдресноеХранениеПомещение",СтруктураПараметров);
	Элементы.ЗонаОтгрузки.Видимость = АдресноеХранениеОтправитель;
	
	ПараметрыУказанияСерий = Новый ФиксированнаяСтруктура(Документы.ОрдерНаПеремещениеТоваров.ПараметрыУказанияСерий(Объект));
	
	СтруктураПараметров = Новый Структура("Помещение",Объект.ПомещениеПолучатель);
	АдресноеХранениеПолучатель =  ПолучитьФункциональнуюОпцию("ИспользоватьАдресноеХранениеПомещение",СтруктураПараметров);
	
	Элементы.ЗонаПриемки.Видимость = АдресноеХранениеПолучатель;
	
	Если АдресноеХранениеОтправитель Тогда
		Элементы.Товары.КоманднаяПанель.ПодчиненныеЭлементы.ТоварыПроверитьКоличество.Доступность = (Объект.Статус = Перечисления.СтатусыОрдеровНаПеремещение.КПроверке);
	КонецЕсли;
	
	ОбработкаТабличнойЧастиСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(
		Объект.Товары,
		Новый Структура("ЗаполнитьПризнакХарактеристикиИспользуются",
			Новый Структура("Номенклатура", "ХарактеристикиИспользуются")
		)
	);
	
	Элементы.ГруппаКомментарий.Картинка = ОбщегоНазначенияУТ.ПолучитьКартинкуКомментария(Объект.Комментарий);
	
	УстановитьДоступность(ЭтаФорма);
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьДоступность(Форма)
	Форма.Элементы.ТоварыПроверитьКоличество.Доступность          = (Форма.Объект.Статус = ПредопределенноеЗначение("Перечисление.СтатусыОрдеровНаПеремещение.КПроверке"));
	
	Форма.Элементы.ТоварыЗаполнитьПоОтобраннымТоварам.Доступность = (Форма.Объект.Статус = ПредопределенноеЗначение("Перечисление.СтатусыОрдеровНаПеремещение.КПроверке")
																	Или Форма.Объект.Статус = ПредопределенноеЗначение("Перечисление.СтатусыОрдеровНаПеремещение.КОтгрузке")
																	Или Форма.Объект.Статус = ПредопределенноеЗначение("Перечисление.СтатусыОрдеровНаПеремещение.Принят"));
															
	Форма.Элементы.ТоварыЗаполнитьСерииПоFEFO.Доступность = Форма.ПараметрыУказанияСерий.ПланированиеОтгрузки
															Или Форма.ПараметрыУказанияСерий.ПланированиеОтбора; 
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ДобавитьВСтруктуруДействияПриИзмененииКоличестваУпаковок(СтруктураДействий)
	
	СтруктураДействий.Вставить("ПересчитатьКоличествоЕдиниц");
	
КонецПроцедуры
