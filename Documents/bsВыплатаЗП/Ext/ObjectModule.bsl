﻿
Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	Если ЗначениеЗаполнено(Менеджер) Тогда
		Сообщить("Реквизит Менеджер обязателен для заполнения!");
	КонецЕсли;
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	ДвиженияБонусы = Движения.bsЗП;
	ДвиженияБонусы.Записывать = Истина;
	
	стрДвижение = ДвиженияБонусы.Добавить();
	стрДвижение.Период = Дата;
	стрДвижение.Менеджер = Менеджер;
	стрДвижение.ВидДвижения = ВидДвиженияНакопления.Расход;
	стрДвижение.СуммаБонуса = Сумма; 
	

КонецПроцедуры
