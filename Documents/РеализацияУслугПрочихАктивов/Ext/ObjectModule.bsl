﻿
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	Перем ВсеРеквизиты;
	Перем РеквизитыОперации;
	НепроверяемыеРеквизиты = Новый Массив;
	
	Документы.РеализацияУслугПрочихАктивов.ЗаполнитьИменаРеквизитовПоХозяйственнойОперации(ХозяйственнаяОперация, ВсеРеквизиты, РеквизитыОперации);
	ОбщегоНазначенияУТКлиентСервер.ЗаполнитьМассивНепроверяемыхРеквизитов(ВсеРеквизиты, РеквизитыОперации, НепроверяемыеРеквизиты);
	
	Если Не ЗначениеЗаполнено(Соглашение) Или Не ОбщегоНазначения.ПолучитьЗначениеРеквизита(Соглашение, "ИспользуютсяДоговорыКонтрагентов") Тогда
		НепроверяемыеРеквизиты.Добавить("Договор");
	КонецЕсли;
	Если НалогообложениеНДС = Перечисления.ТипыНалогообложенияНДС.ПродажаНеОблагаетсяНДС Тогда
		НепроверяемыеРеквизиты.Добавить("Доходы.СтавкаНДС");
	КонецЕсли;
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, НепроверяемыеРеквизиты);
	
	ВзаиморасчетыСервер.ПроверитьДатуПлатежа(ЭтотОбъект, Отказ);
	
	Если Не Отказ Тогда
		Отказ = ОбщегоНазначенияУТ.ПроверитьЗаполнениеРеквизитовОбъекта(ЭтотОбъект, ПроверяемыеРеквизиты);
	КонецЕсли;
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	ПроведениеСервер.УстановитьРежимПроведения(ЭтотОбъект, РежимЗаписи, РежимПроведения);
	ДополнительныеСвойства.Вставить("ЭтоНовый", ЭтоНовый());
	ДополнительныеСвойства.Вставить("РежимЗаписи", РежимЗаписи);

	Документы.СчетФактураВыданный.ПроверитьРеквизитыСчетФактуры(Ссылка, ПометкаУдаления, Организация, Валюта);
	
	Если РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		ДозаполнитьДанныеДоходов();
		ОчиститьНеиспользуемыеРеквизиты();
	КонецЕсли;
	
	СуммаДокумента = ЦенообразованиеКлиентСервер.ПолучитьСуммуДокумента(Доходы, ЦенаВключаетНДС);
   	ВзаиморасчетыСервер.ЗаполнитьСуммуВзаиморасчетов(ЭтотОбъект);
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	ПроведениеСервер.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства, РежимПроведения);
	Документы.РеализацияУслугПрочихАктивов.ИнициализироватьДанныеДокумента(Ссылка, ДополнительныеСвойства);
	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);

	ДоходыИРасходыСервер.ОтразитьПрочиеДоходы(ДополнительныеСвойства, Движения, Отказ);
	ВзаиморасчетыСервер.ОтразитьРасчетыСКлиентами(ДополнительныеСвойства, Движения, Отказ);
	ВзаиморасчетыСервер.ОтразитьРасчетыСКлиентамиПоследовательность(ДополнительныеСвойства, ПринадлежностьПоследовательностям, Отказ);
	
	СформироватьСписокРегистровДляКонтроля();

	ПроведениеСервер.ЗаписатьНаборыЗаписей(ЭтотОбъект);
	ПроведениеСервер.ВыполнитьКонтрольРезультатовПроведения(ЭтотОбъект, Отказ);
	ПроведениеСервер.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	ПроведениеСервер.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства);
	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);

	СформироватьСписокРегистровДляКонтроля();

	ПроведениеСервер.ЗаписатьНаборыЗаписей(ЭтотОбъект);
	ПроведениеСервер.ВыполнитьКонтрольРезультатовПроведения(ЭтотОбъект, Отказ);
	ПроведениеСервер.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);
КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	ТипДанныхЗаполнения = ТипЗнч(ДанныеЗаполнения);
	
	Если ТипДанныхЗаполнения = Тип("СправочникСсылка.СоглашенияСКлиентами") Тогда
		УсловияПродаж = ПродажиСервер.ПолучитьУсловияПродаж(ДанныеЗаполнения);
		
		Если УсловияПродаж.ХозяйственнаяОперация <> Перечисления.ХозяйственныеОперации.РеализацияКлиенту Тогда
			ТекстОшибки = НСтр("ru='Ввод на основании соглашения с операцией `%Операция%.` не допускается.'");
			ТекстОшибки = СтрЗаменить(ТекстОшибки, "%Операция%", УсловияПродаж.ХозяйственнаяОперация); 
			ВызватьИсключение ТекстОшибки;
		КонецЕсли;
		ДопустимыеСтатусы = Новый Массив();
		ДопустимыеСтатусы.Добавить(Перечисления.СтатусыСоглашенийСКлиентами.Действует);
		ОшибкиСтатуса = (УсловияПродаж.СтатусСоглашения <> Перечисления.СтатусыСоглашенийСКлиентами.Действует);
		ОбщегоНазначенияУТ.ПроверитьВозможностьВводаНаОснованииСоглашения(УсловияПродаж.Типовое);
		ОбщегоНазначенияУТ.ПроверитьВозможностьВводаНаОсновании(ДанныеЗаполнения, УсловияПродаж.СтатусСоглашения, , ОшибкиСтатуса, ДопустимыеСтатусы);
		
		Если (Не УсловияПродаж.Типовое) И ЗначениеЗаполнено(УсловияПродаж.Партнер) Тогда
			Партнер = УсловияПродаж.Партнер;
		КонецЕсли;
		Соглашение = ДанныеЗаполнения;
		Документы.РеализацияУслугПрочихАктивов.ЗаполнитьПоУсловиямПродаж(ЭтотОбъект, УсловияПродаж);
	КонецЕсли;
	
	Документы.РеализацияУслугПрочихАктивов.ЗаполнитьПоУмолчанию(ЭтотОбъект);
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// Прочее

Процедура СформироватьСписокРегистровДляКонтроля()

	Массив = Новый Массив;

	Если ДополнительныеСвойства.РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		
		Массив.Добавить(Движения.РасчетыСКлиентами);
		
	КонецЕсли;

	ДополнительныеСвойства.ДляПроведения.Вставить("РегистрыДляКонтроля", Массив);

КонецПроцедуры

Процедура ДозаполнитьДанныеДоходов()
	Для Каждого Строка из Доходы Цикл
		Если 0 = Строка.Количество Тогда
			Строка.Количество = 1;
		КонецЕсли;
		Если Неопределено = Строка.СтавкаНДС Тогда
			Строка.СтавкаНДС = Перечисления.СтавкиНДС.БезНДС;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

Процедура ОчиститьНеиспользуемыеРеквизиты()
	Перем ВсеРеквизиты;
	Перем РеквизитыОперации;
	ОчищаемыеРеквизиты = Новый Массив;
	
	Документы.РеализацияУслугПрочихАктивов.ЗаполнитьИменаРеквизитовПоХозяйственнойОперации(ХозяйственнаяОперация, ВсеРеквизиты, РеквизитыОперации);
	ДенежныеСредстваСервер.ОчиститьНеиспользуемыеРеквизиты(ЭтотОбъект, ВсеРеквизиты, РеквизитыОперации);
КонецПроцедуры
