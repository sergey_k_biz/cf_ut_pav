﻿
Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	Если ЗначениеЗаполнено(Дизайнер) Тогда
		Сообщить("Реквизит дезайнер обязателен для заполнения!");
	КонецЕсли;
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	ДвиженияБонусы = Движения.msБонусы;
	ДвиженияБонусы.Записывать = Истина;
	
	стрДвижение = ДвиженияБонусы.Добавить();
	стрДвижение.Период = Дата;
	стрДвижение.Дизайнер = Дизайнер;
	стрДвижение.ВидДвижения = ВидДвиженияНакопления.Расход;
	стрДвижение.СуммаБонуса = Сумма; 
	

КонецПроцедуры
