﻿////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ


&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	АвтоЗаголовок = Ложь;
	Заголовок = НСтр("ru = 'прайс с картинками'");
	
	ОтборНоменклатура = Параметры.Ключ;
//Кем +	
	СформироватьОтчет("ПрайсСКартинкамиИзЗаказаПоставщику");
	
КонецПроцедуры



////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура СформироватьОтчетТоварыВПути(Команда)
	
	ОбновитьОтчет("ПрайсСКартинкамиИзЗаказаПоставщику");
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

&НаСервере
Процедура ОбновитьОтчет(ИмяОтчета)
	
	ТаблицаРезультатаОтчета = ТаблицаРезультатаОтчета(ИмяОтчета);
	ТаблицаРезультатаОтчета.Очистить();
	
	ОтчетОбъект = РеквизитФормыВЗначение(ИмяОтчета);
	ОтчетОбъект.СкомпоноватьРезультат(ТаблицаРезультатаОтчета);
	
КонецПроцедуры

&НаСервере
Процедура СформироватьОтчет(ИмяОтчета, ИмяВариантаНастроек = "ПрайсЛистСКартинками")
//Кем
	отборНоменклатура = Новый Массив();
Для Каждого ЭлементКлюча из  Параметры.Ключ Цикл
	отборНоменклатура.Добавить(ЭлементКлюча)
КонецЦикла;
//Кем

	Если Не ПравоДоступа("Использование", Метаданные.Отчеты[ИмяОтчета]) Тогда
		Возврат;
	КонецЕсли;
	
	// Загрузить настройки отчета из схемы компоновки.
	ОтчетОбъект = РеквизитФормыВЗначение(ИмяОтчета);
	АдресСхемы  = ПоместитьВоВременноеХранилище(ОтчетОбъект.СхемаКомпоновкиДанных, УникальныйИдентификатор);
	
	ОтчетОбъект.КомпоновщикНастроек.Инициализировать(Новый ИсточникДоступныхНастроекКомпоновкиДанных(АдресСхемы));
	ОтчетОбъект.КомпоновщикНастроек.ЗагрузитьНастройки(ОтчетОбъект.СхемаКомпоновкиДанных.ВариантыНастроек[ИмяВариантаНастроек].Настройки);
	
	// Добавить отбор по номенклатуре.
	ЭлементОтбора = ОтчетОбъект.КомпоновщикНастроек.Настройки.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	
	ЭлементОтбора.ЛевоеЗначение  = Новый ПолеКомпоновкиДанных("Номенклатура");
	ЭлементОтбора.ВидСравнения   = ВидСравненияКомпоновкиДанных.ВСписке;
	ЭлементОтбора.ПравоеЗначение = отборНоменклатура;
	ЭлементОтбора.Использование  = Истина;
	
	// Вывести отчет.
	ОтчетОбъект.СкомпоноватьРезультат(ТаблицаРезультатаОтчета(ИмяОтчета),ДанныеРасшифровки);
	
КонецПроцедуры

&НаСервере
Функция ТаблицаРезультатаОтчета(ИмяОтчета)
	
	Возврат ЭтаФорма["ТаблицаОтчета" + ИмяОтчета];
	
КонецФункции

&НаКлиенте
Процедура ТаблицаОтчетаТоварыВПутиОбработкаРасшифровки(Элемент, Расшифровка, СтандартнаяОбработка)
	СтандартнаяОбработка = ложь;
	Документ = ОбработатьНасервере(Расшифровка);
	Попытка
		Документ.ПолучитьФорму("ФормаДокумента").ОткрытьМодально();		
	Исключение
	КонецПопытки
КонецПроцедуры

&НаСервере
Функция ОбработатьНасервере(Расшифровка)
    Поля = ДанныеРасшифровки.Элементы[Расшифровка].ПолучитьПоля();
    Возврат ?(Поля.КОличество()=0,Неопределено,Поля[0].Значение);
КонецФункции
