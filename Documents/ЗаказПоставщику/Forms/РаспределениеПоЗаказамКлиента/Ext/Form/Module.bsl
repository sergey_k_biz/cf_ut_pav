﻿
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Номенклатура = ЭтаФорма.Параметры.Номенклатура; 
	Количество = ЭтаФорма.Параметры.Количество;
	Тз_поЗаказам.Загрузить(ДанныеФормыВЗначение(ЭтаФорма.Параметры.Объект,Тип("ДокументОбъект.ЗаказПоставщику")).НоменклатураПоЗаказамКлиента.Выгрузить());
	Элементы.Тз_поЗаказам.ОтборСтрок = Новый ФиксированнаяСтруктура("Номенклатура",Номенклатура);
	Элементы.Тз_поЗаказам.Обновить();
КонецПроцедуры

&НаКлиенте
Процедура Тз_поЗаказамПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	//Отказ = Истина;
	//НовСтр = Тз_поЗаказам.Добавить();
	//НовСтр.Номенклатура = Номенклатура;
	//Элементы.Тз_поЗаказам.Обновить();
КонецПроцедуры


&НаКлиенте
Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	Если ТекущийЭлемент.Имя = "ОК" тогда
		Для каждого СТр из Тз_поЗаказам цикл
			Если НЕ ЗначениеЗаполнено(СТр.ЗаказКлиента) или НЕ ЗначениеЗаполнено(СТр.Количество) тогда
				Предупреждение("Не заполнены обязательные поля!");
				Отказ = Истина;
				Прервать;
			КонецЕсли;
		КонецЦикла;
		КоличествоПоФОрме= 0;
		Для каждого СТрм из Тз_поЗаказам.НайтиСтроки(Новый Структура("Номенклатура",Номенклатура)) цикл
			КоличествоПоФОрме = КоличествоПоФОрме + СТрм.Количество;	
		КонецЦикла;
		Если Количество < КоличествоПоФОрме тогда
			Предупреждение("Количество привышает количество по заказу поставщика!");
			Отказ = Истина;
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры


&НаКлиенте
Процедура Тз_поЗаказамПередОкончаниемРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования, Отказ)
	Элемент.ТекущиеДанные.Номенклатура = Номенклатура;
КонецПроцедуры

