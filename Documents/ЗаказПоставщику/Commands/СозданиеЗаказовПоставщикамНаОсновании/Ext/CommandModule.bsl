﻿&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ПроверитьВозможностьВводаНаОсновании(ПараметрКоманды);
	ПараметрыФормы = Новый Структура("ДокументОснование", ПараметрКоманды);
	ОткрытьФорму(
		"Документ.ЗаказПоставщику.Форма.СозданиеЗаказовПоставщикамНаОсновании", ПараметрыФормы,
		ПараметрыВыполненияКоманды.Источник, ПараметрыВыполненияКоманды.Уникальность, ПараметрыВыполненияКоманды.Окно
	);
КонецПроцедуры

&НаСервере
Процедура ПроверитьВозможностьВводаНаОсновании(ПараметрКоманды)
	
	СтруктураРеквизитов = ОбщегоНазначения.ПолучитьЗначенияРеквизитов(ПараметрКоманды,"Статус,Проведен");
	
	МассивДопустимыхСтатусов = Новый Массив();
	МассивДопустимыхСтатусов.Добавить(Перечисления.СтатусыЗаказовКлиентов.Согласован);
	МассивДопустимыхСтатусов.Добавить(Перечисления.СтатусыЗаказовКлиентов.КОбеспечению);
	МассивДопустимыхСтатусов.Добавить(Перечисления.СтатусыЗаказовКлиентов.КОтгрузке);

	Если МассивДопустимыхСтатусов.Найти(СтруктураРеквизитов.Статус)=Неопределено Тогда
		ЕстьОшибкиСтатус=Истина
	Иначе
		ЕстьОшибкиСтатус=Ложь
	КонецЕсли;
	
	ОбщегоНазначенияУТ.ПроверитьВозможностьВводаНаОсновании(
		ПараметрКоманды,
		СтруктураРеквизитов.Статус,
		НЕ СтруктураРеквизитов.Проведен,
		ЕстьОшибкиСтатус,
		МассивДопустимыхСтатусов
	);
	
КонецПроцедуры