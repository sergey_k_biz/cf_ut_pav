﻿////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ 

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("Структура") Тогда
		Если ДанныеЗаполнения.Свойство("ДокументОснование") Тогда
            ДанныеЗаполнения = ДанныеЗаполнения.ДокументОснование;
		Иначе
			ЗаполнитьЗначенияСвойств(ЭтотОбъект,ДанныеЗаполнения);
		КонецЕсли;
	КонецЕсли;
	
	ЗаполнитьРеквизитыПоУмолчанию();
	
	ДокументОснование = ЗапасыСервер.ПолучитьОснованиеОформленияИзлишковНедостачТоваров(ДанныеЗаполнения);
	
	Если ЗначениеЗаполнено(Склад)
		И ПолучитьФункциональнуюОпцию("ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач", Новый Структура("Склад",Склад))
		И Не ЗначениеЗаполнено(ДокументОснование) Тогда
		
		ТекстСообщения = НСтр("ru='На складе ""%Склад%"" используется ордерная схема для отражения излишков и недостач, поэтому ""Списание товаров"" нужно вводить по основанию.'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения,"%Склад%",Склад);
		
		ВызватьИсключение ТекстСообщения;
		
	КонецЕсли;	
		
	ЗаполнитьПоОснованию();
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)

	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;

	ПроведениеСервер.УстановитьРежимПроведения(ЭтотОбъект, РежимЗаписи, РежимПроведения);

	ДополнительныеСвойства.Вставить("ЭтоНовый",    ЭтоНовый());
	ДополнительныеСвойства.Вставить("РежимЗаписи", РежимЗаписи);

	Если РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		ЗаполнитьВидыЗапасов(Отказ);
	КонецЕсли;

КонецПроцедуры // ПередЗаписью()

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	ОбработкаТабличнойЧастиСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект,МассивНепроверяемыхРеквизитов,Отказ);
	
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач", Новый Структура("Склад", Склад)) Тогда
		МассивНепроверяемыхРеквизитов.Добавить("ДокументОснование");
	КонецЕсли;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)

	ПроведениеСервер.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства, РежимПроведения);

	Документы.СписаниеНедостачТоваров.ИнициализироватьДанныеДокумента(Ссылка, ДополнительныеСвойства);

	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);

	ЗапасыСервер.ОтразитьТоварыНаСкладах(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьСвободныеОстатки(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьТоварыКОформлениюИзлишковНедостач(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьТоварыОрганизаций(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьТоварыКОформлениюОтчетовКомитента(ДополнительныеСвойства, Движения, Отказ);
	
	ДоходыИРасходыСервер.ОтразитьСебестоимостьТоваров(ДополнительныеСвойства, Движения, Отказ);
	
	СформироватьСписокРегистровДляКонтроля();

	ПроведениеСервер.ЗаписатьНаборыЗаписей(ЭтотОбъект);

	ПроведениеСервер.ВыполнитьКонтрольРезультатовПроведения(ЭтотОбъект, Отказ);

	ПроведениеСервер.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);
		Если Константы.mgИспользоватьУправленческийУчет.Получить() Тогда
		
		Если не Отказ Тогда
			Если Константы.mgНеПроводитьОтгрузкуСОшибкойПартий.Получить() Тогда
				// нужно останавливать при ошибке 
				Отказ=msОбщий.ВыполнитьДвиженияПоРегиструПартии(Движения.mgУправленческиеПартии);
				//Отказ = msОбщий.ВыполнитьДвиженияПоРегиструПартииДоп(Движения.mgУправленческиеПартии);
			иначе
				// не нужно
				msОбщий.ВыполнитьДвиженияПоРегиструПартии(Движения.mgУправленческиеПартии);
				//msОбщий.ВыполнитьДвиженияПоРегиструПартииДоп(Движения.mgУправленческиеПартии);
			КонецЕсли; 
			
			
		КонецЕсли;	
	КонецЕсли;


КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)

	ПроведениеСервер.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства);

	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);

	СформироватьСписокРегистровДляКонтроля();

	ПроведениеСервер.ЗаписатьНаборыЗаписей(ЭтотОбъект);

	ПроведениеСервер.ВыполнитьКонтрольРезультатовПроведения(ЭтотОбъект, Отказ);

	ПроведениеСервер.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);

КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)

	ЗаполнитьРеквизитыПоУмолчанию();
	
	Если ВидыЗапасов.Количество() > 0 Тогда
		ВидыЗапасов.Очистить();
	КонецЕсли;

	ДокументОснование = Неопределено;
	Склад = Неопределено;

КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// Инициализация и заполнение

Процедура ЗаполнитьТабличнуюЧастьТовары() Экспорт

	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ТоварыКОформлению.Номенклатура КАК Номенклатура,
	|	ТоварыКОформлению.Характеристика КАК Характеристика,
	|	-СУММА(ТоварыКОформлению.КОформлениюАктовОстаток) КАК Количество
	|ИЗ
	|	(ВЫБРАТЬ
	|		ТоварыКОформлению.Номенклатура КАК Номенклатура,
	|		ТоварыКОформлению.Характеристика КАК Характеристика,
	|		ТоварыКОформлению.КОформлениюАктовОстаток КАК КОформлениюАктовОстаток
	|	ИЗ
	|		РегистрНакопления.ТоварыКОформлениюИзлишковНедостач.Остатки(
	|				,
	|				ДокументОснование = &ДокументОснование
	|					И Склад = &Склад) КАК ТоварыКОформлению
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		ТоварыКОформлению.Номенклатура,
	|		ТоварыКОформлению.Характеристика,
	|		ВЫБОР
	|			КОГДА ТоварыКОформлению.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	|				ТОГДА -ТоварыКОформлению.КОформлениюАктов
	|			ИНАЧЕ ТоварыКОформлению.КОформлениюАктов
	|		КОНЕЦ
	|	ИЗ
	|		РегистрНакопления.ТоварыКОформлениюИзлишковНедостач КАК ТоварыКОформлению
	|	ГДЕ
	|		ТоварыКОформлению.Регистратор = &СписаниеНедостачТоваров
	|		И ТоварыКОформлению.ДокументОснование = &ДокументОснование
	|		И ТоварыКОформлению.Активность = ИСТИНА) КАК ТоварыКОформлению
	|
	|СГРУППИРОВАТЬ ПО
	|	ТоварыКОформлению.Номенклатура,
	|	ТоварыКОформлению.Характеристика
	|
	|ИМЕЮЩИЕ
	|	СУММА(ТоварыКОформлению.КОформлениюАктовОстаток) < 0";
	Запрос.УстановитьПараметр("СписаниеНедостачТоваров", Ссылка);
	Запрос.УстановитьПараметр("ДокументОснование", ДокументОснование);
	Запрос.УстановитьПараметр("Склад", Склад);
	
	Результат = Запрос.Выполнить();
	Если Не Результат.Пустой() Тогда

		Товары.Загрузить(Результат.Выгрузить());

	Иначе

		ТекстСообщения = НСтр("ru = 'Нет данных для заполнения по основанию ""%ДокументОснование%"" .'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ДокументОснование%", ДокументОснование);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, "ДокументОснование");

	КонецЕсли;

КонецПроцедуры

Процедура ЗаполнитьПоОснованию() Экспорт
	
	Если Не ЗначениеЗаполнено(ДокументОснование) Тогда
		
		Если ЗначениеЗаполнено(Склад) Тогда
			ВидЦены = Справочники.Склады.УчетныйВидЦены(Склад);
		КонецЕсли;
		
		Возврат;
	КонецЕсли;
	
	Если ТипЗнч(ДокументОснование) = Тип("ДокументСсылка.ПриказНаПроведениеИнвентаризацииТоваров") Тогда
		ЗначенияРеквизитов = ОбщегоНазначения.ПолучитьЗначенияРеквизитов(ДокументОснование,
		                     Новый Структура("Склад, ВидЦены"));
	Иначе
		ЗначенияРеквизитов = ОбщегоНазначения.ПолучитьЗначенияРеквизитов(ДокументОснование,
		                     Новый Структура("Склад,ВидЦены","Склад","Склад.УчетныйВидЦены"));
	КонецЕсли;		
	
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, ЗначенияРеквизитов);
	
	ЗаполнитьТабличнуюЧастьТовары();

КонецПроцедуры

Процедура ЗаполнитьСтатьюРасходовПоУмолчанию()
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ ПЕРВЫЕ 1
	|	ДанныеДокумента.СтатьяРасходов КАК СтатьяРасходов,
	|	ДанныеДокумента.АналитикаРасходов КАК АналитикаРасходов
	|ИЗ
	|	Документ.СписаниеНедостачТоваров КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ответственный = &Ответственный
	|	И ДанныеДокумента.Проведен
	|
	|УПОРЯДОЧИТЬ ПО
	|	ДанныеДокумента.Дата УБЫВ
	|");
	Запрос.УстановитьПараметр("Ответственный", Ответственный);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		СтатьяРасходов = Выборка.СтатьяРасходов;
		АналитикаРасходов = Выборка.АналитикаРасходов;
	КонецЕсли;

КонецПроцедуры

Процедура ЗаполнитьРеквизитыПоУмолчанию()

	Ответственный = Пользователи.ТекущийПользователь();

	Организация   = ЗначениеНастроекПовтИсп.ПолучитьОрганизациюПоУмолчанию(Организация);
	Склад         = ЗначениеНастроекПовтИсп.ПолучитьСкладПоУмолчанию(Склад);
	
	Если ПолучитьФункциональнуюОпцию("ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач", Новый Структура("Склад",Склад)) Тогда
		Склад = Неопределено;
	КонецЕсли;
	
	Подразделение = ЗначениеНастроекПовтИсп.ПодразделениеПользователя(Ответственный, Подразделение);
	ЗаполнитьСтатьюРасходовПоУмолчанию();
	
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Виды запасов

Функция ВременныеТаблицыДанныхДокумента() Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	&Организация КАК Организация,
	|	&Склад КАК Склад,
	|	Неопределено КАК Партнер,
	|	ЗНАЧЕНИЕ(Справочник.СоглашенияСПоставщиками.ПустаяСсылка) КАК Соглашение,
	|	ЗНАЧЕНИЕ(Справочник.Валюты.ПустаяСсылка) КАК Валюта,
	|	ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПустаяСсылка) КАК НалогообложениеНДС,
	|
	|	ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка) КАК Подразделение,
	|	ЗНАЧЕНИЕ(Справочник.Пользователи.ПустаяСсылка) КАК Менеджер,
	|	ЗНАЧЕНИЕ(Справочник.СделкиСКлиентами.ПустаяСсылка) КАК Сделка,
	|	ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.СписаниеТоваров) КАК ХозяйственнаяОперация,
	|	Ложь КАК ЕстьСделкиВТабличнойЧасти,
	|	&ДеятельностьОблагаетсяЕНВД КАК ДеятельностьОблагаетсяЕНВД
	|	
	|ПОМЕСТИТЬ ТаблицаДанныхДокумента
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаТоваров.НомерСтроки КАК НомерСтроки,
	|	ТаблицаТоваров.Номенклатура КАК Номенклатура,
	|	ТаблицаТоваров.Характеристика КАК Характеристика,
	|	ТаблицаТоваров.ДокументРеализации КАК ДокументРеализации,
	|	ТаблицаТоваров.Количество КАК Количество,
	|	&Склад КАК Склад,
	|	ЗНАЧЕНИЕ(Справочник.СделкиСКлиентами.ПустаяСсылка) КАК Сделка,
	|	ЗНАЧЕНИЕ(Перечисление.СтавкиНДС.ПустаяСсылка) КАК СтавкаНДС,
	|	0 КАК СуммаСНДС,
	|	0 КАК СуммаНДС,
	|	0 КАК СуммаВознаграждения,
	|	0 КАК СуммаНДСВознаграждения
	|	
	|ПОМЕСТИТЬ ТаблицаТоваров
	|ИЗ
	|	&ТаблицаТоваров КАК ТаблицаТоваров
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаВидыЗапасов.НомерСтроки КАК НомерСтроки,
	|	ТаблицаВидыЗапасов.Номенклатура КАК Номенклатура,
	|	ТаблицаВидыЗапасов.Характеристика КАК Характеристика,
	|	ТаблицаВидыЗапасов.ДокументРеализации КАК ДокументРеализации,
	|	ТаблицаВидыЗапасов.ВидЗапасов КАК ВидЗапасов,
	|	ТаблицаВидыЗапасов.НомерГТД КАК НомерГТД,
	|	ТаблицаВидыЗапасов.Количество КАК Количество,
	|	&Склад КАК Склад,
	|	ЗНАЧЕНИЕ(Справочник.СделкиСКлиентами.ПустаяСсылка) КАК Сделка
	|	
	|ПОМЕСТИТЬ ТаблицаВидыЗапасов
	|ИЗ
	|	&ТаблицаВидыЗапасов КАК ТаблицаВидыЗапасов
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Номенклатура,
	|	Характеристика
	|");
	
	МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.УстановитьПараметр("Склад", Склад);
	Запрос.УстановитьПараметр("ДеятельностьОблагаетсяЕНВД", Справочники.Организации.РозничнаяТорговляОблагаетсяЕНВД(Организация, Склад, Дата));
	Запрос.УстановитьПараметр("ТаблицаТоваров", ЗапасыСервер.ТаблицаДополненнаяОбязательнымиКолонками(Товары.Выгрузить()));
	Запрос.УстановитьПараметр("ТаблицаВидыЗапасов", ЗапасыСервер.ТаблицаДополненнаяОбязательнымиКолонками(ВидыЗапасов.Выгрузить()));
	
	Запрос.Выполнить();
	
	Возврат МенеджерВременныхТаблиц;
	
КонецФункции

Процедура СформироватьВременнуюТаблицуТоваровИАналитики(МенеджерВременныхТаблиц) Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	ТаблицаТоваров.Номенклатура КАК Номенклатура,
	|	ТаблицаТоваров.Характеристика КАК Характеристика,
	|	ТаблицаТоваров.Склад КАК Склад,
	|
	|	ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка) КАК Подразделение,
	|	ЗНАЧЕНИЕ(Справочник.Пользователи.ПустаяСсылка) КАК Менеджер,
	|	ЗНАЧЕНИЕ(Справочник.СделкиСКлиентами.ПустаяСсылка) КАК Сделка,
	|	ЗНАЧЕНИЕ(Справочник.Партнеры.ПустаяСсылка) КАК Партнер,
	|	ЗНАЧЕНИЕ(Справочник.СоглашенияСПоставщиками.ПустаяСсылка) КАК Соглашение,
	|	ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПустаяСсылка) КАК НалогообложениеНДС,
	|
	|	ТаблицаТоваров.Количество КАК Количество
	|	
	|ПОМЕСТИТЬ ТаблицаТоваровИАналитики
	|ИЗ
	|	ТаблицаТоваров КАК ТаблицаТоваров
	|
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		ТаблицаДанныхДокумента КАК ТаблицаДанныхДокумента
	|	ПО
	|		Истина
	|ГДЕ
	|	ТаблицаТоваров.Номенклатура.ТипНоменклатуры <> ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.Услуга)
	|;
	|");
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	Запрос.Выполнить();
	
КонецПроцедуры

Функция ИзмененыРеквизитыДокумента(МенеджерВременныхТаблиц)
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	ВЫБОР КОГДА ДанныеДокумента.Организация <> СохраненныеДанные.Организация ТОГДА
	|		Истина
	|	КОГДА ДанныеДокумента.Склад <> СохраненныеДанные.Склад ТОГДА
	|		Истина
	|	ИНАЧЕ
	|		Ложь
	|	КОНЕЦ КАК РеквизитыИзменены
	|ИЗ
	|	ТаблицаДанныхДокумента КАК ДанныеДокумента
	|
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		Документ.СписаниеНедостачТоваров КАК СохраненныеДанные
	|	ПО
	|		СохраненныеДанные.Ссылка = &Ссылка
	|");
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		РеквизитыИзменены = Выборка.РеквизитыИзменены;
	Иначе
		РеквизитыИзменены = Ложь;
	КонецЕсли;
	
	Возврат РеквизитыИзменены;
	
КонецФункции

Процедура СформироватьДоступныеВидыЗапасов(МенеджерВременныхТаблиц) Экспорт
	
	ЗапасыСервер.ВидыЗапасовЛюбыеБезИнтеркомпани(
		Организация,
		МенеджерВременныхТаблиц
	);
	
КонецПроцедуры

Процедура СообщитьОбОшибкахЗаполненияВидовЗапасов(ТаблицаОшибок)
	
	Для Каждого СтрокаТаблицы Из ТаблицаОшибок Цикл
		
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru = 'Номенклатура: %1 
			|Списание превышает остаток товара организации %2 на складе %3 на %4 %5'"),
			ОбщегоНазначенияУТ.ПолучитьПредставлениеНоменклатуры(СтрокаТаблицы.Номенклатура, СтрокаТаблицы.Характеристика),
			Организация,
			Склад,
			СтрокаТаблицы.Количество,
			СтрокаТаблицы.ЕдиницаИзмерения
		);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			ТекстСообщения,
			Ссылка
		);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ЗаполнитьВидыЗапасов(Отказ)
	
	УстановитьПривилегированныйРежим(Истина);
	
	МенеджерВременныхТаблиц = ВременныеТаблицыДанныхДокумента();
	ПерезаполнитьВидыЗапасов = ДополнительныеСвойства.Свойство("ПерезаполнитьВидыЗапасов");
	Если Не Проведен
	 ИЛИ ПерезаполнитьВидыЗапасов
	 ИЛИ ИзмененыРеквизитыДокумента(МенеджерВременныхТаблиц)
	 ИЛИ ЗапасыСервер.ПроверитьИзменениеТоваровПоКоличеству(МенеджерВременныхТаблиц) Тогда
	 
		СформироватьДоступныеВидыЗапасов(МенеджерВременныхТаблиц);
		ЗапасыСервер.УстановитьБлокировкуОстатковТоваровОрганизаций(МенеджерВременныхТаблиц);
		ЗапасыСервер.ТаблицаОстатковТоваровОрганизаций(Ссылка, Организация, Дата, ДополнительныеСвойства, МенеджерВременныхТаблиц);
		ТаблицаОшибок = ЗапасыСервер.ТаблицаОшибокЗаполненияВидовЗапасов();
		
		ЗапасыСервер.ЗаполнитьВидыЗапасовДокумента(
			МенеджерВременныхТаблиц,
			ДополнительныеСвойства,
			ВидыЗапасов,
			ТаблицаОшибок,
			Отказ
		);
		ВидыЗапасов.Свернуть("Номенклатура, Характеристика, ВидЗапасов, НомерГТД", "Количество");
		СообщитьОбОшибкахЗаполненияВидовЗапасов(ТаблицаОшибок);
		
	КонецЕсли;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Прочее

Процедура СформироватьСписокРегистровДляКонтроля()

	Массив = Новый Массив;
	// Контроль при перепроведении и отмене проведения

	//Массив.Добавить(Движения.ТоварыКОформлениюИзлишковНедостач);

	ДополнительныеСвойства.ДляПроведения.Вставить("РегистрыДляКонтроля", Массив);

КонецПроцедуры

