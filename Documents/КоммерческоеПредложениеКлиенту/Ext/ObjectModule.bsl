﻿////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС

// Процедура ЗаполнитьНаборыЗначенийДоступа заполняет наборы значений доступа
// по объекту в таблице с полями:
//  - НомерНабора     Число                                     (необязательно, если набор один),
//  - ВидДоступа      ПланВидовХарактеристикСсылка.ВидыДоступа, (обязательно),
//  - ЗначениеДоступа Неопределено, СправочникСсылка или др.    (обязательно),
//  - Чтение          Булево (необязательно, если набор для всех прав; устанавливается для одной строки набора),
//  - Добавление      Булево (необязательно, если набор для всех прав; устанавливается для одной строки набора),
//  - Изменение       Булево (необязательно, если набор для всех прав; устанавливается для одной строки набора),
//  - Удаление        Булево (необязательно, если набор для всех прав; устанавливается для одной строки набора).
//
//  Вызывается из процедуры УправлениеДоступом.ЗаписатьНаборыЗначенийДоступа(),
// если объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьНаборыЗначенийДоступа" и
// из таких же процедур объектов, у которых наборы значений доступа зависят от наборов этого
// объекта (в этом случае объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьЗависимыеНаборыЗначенийДоступа").
//
// Параметры:
//  Таблица      - ТабличнаяЧасть,
//                 РегистрСведенийНаборЗаписей.НаборыЗначенийДоступа,
//                 ТаблицаЗначений, возвращаемая УправлениеДоступом.ТаблицаНаборыЗначенийДоступа().
//
Процедура ЗаполнитьНаборыЗначенийДоступа(Таблица) Экспорт

	СтрокаТаб = Таблица.Добавить();
	СтрокаТаб.НомерНабора     = 1;
	СтрокаТаб.ВидДоступа      = ПланыВидовХарактеристик.ВидыДоступа.Организации;
	СтрокаТаб.ЗначениеДоступа = Организация;

	СтрокаТаб = Таблица.Добавить();
	СтрокаТаб.НомерНабора     = 2;
	СтрокаТаб.ВидДоступа      = ПланыВидовХарактеристик.ВидыДоступа.ГруппыПартнеров;
	СтрокаТаб.ЗначениеДоступа = Партнер;

КонецПроцедуры

// Заполняет условия продаж в коммерческом предложении
//
// Параметры:
//	УсловияПродаж - Структура - Структура для заполнения
//
Процедура ЗаполнитьУсловияПродаж(Знач УсловияПродаж) Экспорт
	
	Если УсловияПродаж = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Валюта = УсловияПродаж.Валюта;
	ХозяйственнаяОперация = УсловияПродаж.ХозяйственнаяОперация;
	
	Если ЗначениеЗаполнено(УсловияПродаж.Организация) Тогда
		Организация = УсловияПродаж.Организация;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(УсловияПродаж.ГрафикОплаты) Тогда
		ГрафикОплаты = УсловияПродаж.ГрафикОплаты;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(УсловияПродаж.ФормаОплаты) Тогда
		ФормаОплаты = УсловияПродаж.ФормаОплаты;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(УсловияПродаж.Склад) Тогда
		Склад = УсловияПродаж.Склад;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(УсловияПродаж.СрокПоставки) Тогда
		СрокПоставки = УсловияПродаж.СрокПоставки;
	КонецЕсли;
	
	ЦенаВключаетНДС      = УсловияПродаж.ЦенаВключаетНДС;
	НалогообложениеНДС   = УсловияПродаж.НалогообложениеНДС;
	
	Если Не ЗначениеЗаполнено(СрокДействия) Тогда
		СрокДействия = УсловияПродаж.ДатаОкончанияДействия;
	КонецЕсли;
	
КонецПроцедуры

// Заполняет условия продаж по умолчанию в коммерческом предложении
//
Процедура ЗаполнитьУсловияПродажПоУмолчанию() Экспорт
	
	Если ЗначениеЗаполнено (Партнер) Тогда
		
		УсловияПродажПоУмолчанию = ПродажиСервер.ПолучитьУсловияПродажПоУмолчанию(
			Партнер,
			Дата,
			Новый Структура("УчитыватьГруппыСкладов,ВыбранноеСоглашение", Истина, Соглашение)
		);
		
		Если УсловияПродажПоУмолчанию = Неопределено Тогда
			Возврат;
		КонецЕсли;
		
		Соглашение = УсловияПродажПоУмолчанию.Соглашение;
		ЗаполнитьУсловияПродаж(УсловияПродажПоУмолчанию);
		
		СтруктураПересчетаСуммы = ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(ЭтотОбъект);
		ПродажиСервер.ЗаполнитьЦены(
			Товары, // Табличная часть
			, // Массив строк или структура отбора
			Новый Структура( // Параметры заполнения
				"Дата, Валюта, Соглашение, ПоляЗаполнения",
				Дата,
				Валюта,
				Соглашение,
				"Цена, СтавкаНДС, ВидЦены"
			),
			Новый Структура( // Структура действий с измененными строками
				"ПересчитатьСумму, ПересчитатьСуммуСНДС, ПересчитатьСуммуНДС, ПересчитатьСуммуРучнойСкидки, ОчиститьАвтоматическуюСкидку, ПересчитатьСуммуСУчетомРучнойСкидки",
				"КоличествоУпаковок", СтруктураПересчетаСуммы, СтруктураПересчетаСуммы, "КоличествоУпаковок", Неопределено, Новый Структура("Очищать", Ложь)
			)
		);
		
	КонецЕсли;
	
КонецПроцедуры

// Заполняет условия продаж по соглашению в коммерческом предложении клиенту
//
Процедура ЗаполнитьУсловияПродажПоCоглашению() Экспорт
	
	УсловияПродаж = ПродажиСервер.ПолучитьУсловияПродаж(Соглашение, Истина);
	ЗаполнитьУсловияПродаж(УсловияПродаж);
	
	СтруктураПересчетаСуммы = ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(ЭтотОбъект);
	ПродажиСервер.ЗаполнитьЦены(
		Товары, // Табличная часть
		, // Массив строк или структура отбора
		Новый Структура( // Параметры заполнения
			"Дата, Валюта, Соглашение, ПоляЗаполнения",
			Дата,
			Валюта,
			Соглашение,
			"Цена, СтавкаНДС, ВидЦены"
		),
		Новый Структура( // Структура действий с измененными строками
			"ПересчитатьСумму, ПересчитатьСуммуСНДС, ПересчитатьСуммуНДС, ПересчитатьСуммуРучнойСкидки, ОчиститьАвтоматическуюСкидку, ПересчитатьСуммуСУчетомРучнойСкидки",
			"КоличествоУпаковок", СтруктураПересчетаСуммы, СтруктураПересчетаСуммы, "КоличествоУпаковок", Неопределено, Новый Структура("Очищать", Ложь)
		)
	);
	
КонецПроцедуры

//Заполняет контактное лицо по умолчанию в коммерческом предложении
//
Процедура ЗаполнитьКонтактноеЛицоПоУмолчанию() Экспорт
	
	КонтактноеЛицо = ЗначениеНастроекПовтИсп.ПолучитьКонтактноеЛицоПартнераПоУмолчанию(Партнер);
	
КонецПроцедуры

// Устанавливает статус для объекта документа
//
// Параметры:
//	НовыйСтатус - Строка - Имя статуса, который будет установлен у заказов
//	ДополнительныеПараметры - Структура - Структура дополнительных параметров установки статуса
//
// Возвращаемое значение:
//	Булево - Истина, в случае успешной установки нового статуса
//
Функция УстановитьСтатус(НовыйСтатус, ДополнительныеПараметры) Экспорт
	
	Статус = Перечисления.СтатусыКоммерческихПредложенийКлиентам[НовыйСтатус];
	
	Возврат ПроверитьЗаполнение();
	
КонецФункции

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	ТипДанныхЗаполнения = ТипЗнч(ДанныеЗаполнения);
	
	Если ТипДанныхЗаполнения = Тип("Структура") Тогда
		ЗаполнитьДокументПоОтбору(ДанныеЗаполнения);
	ИначеЕсли ТипДанныхЗаполнения = Тип("СправочникСсылка.СделкиСКлиентами") Тогда
		ЗаполнитьДокументНаОснованииСделкиПоПродаже(ДанныеЗаполнения);
	ИначеЕсли ТипДанныхЗаполнения = Тип("СправочникСсылка.СоглашенияСКлиентами") Тогда
		ЗаполнитьДокументНаОснованииИндивидуальногоСоглашенияСКлиентом(ДанныеЗаполнения);
	ИначеЕсли ТипДанныхЗаполнения = Тип("ДокументСсылка.КоммерческоеПредложениеКлиенту") Тогда
		ЗаполнитьДокументНаОснованииКоммерческогоПредложенияКлиенту(ДанныеЗаполнения);
	КонецЕсли;
	
	ИнициализироватьДокумент();
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	ОбработкаТабличнойЧастиСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект,МассивНепроверяемыхРеквизитов,Отказ);

	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты,МассивНепроверяемыхРеквизитов);
	
	ОбработкаТабличнойЧастиСервер.ПроверитьЗаполнениеКоличества(ЭтотОбъект, ПроверяемыеРеквизиты, Отказ);

	// Срок действия коммерческого предложения должен быть не меньше даты документа
	Если ЗначениеЗаполнено(СрокДействия) И СрокДействия < НачалоДня(Дата) Тогда
		
		ТекстОшибки = НСтр("ru='Срок действия должен быть не меньше даты коммерческого предложения'");
		
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
		ТекстОшибки,
		ЭтотОбъект,
		"СрокДействия",
		,
		Отказ);
		
	КонецЕсли;
	
	Если Не Отказ Тогда
		Отказ = ОбщегоНазначенияУТ.ПроверитьЗаполнениеРеквизитовОбъекта(ЭтотОбъект, ПроверяемыеРеквизиты);
	КонецЕсли;
	
	ПродажиСервер.ПроверитьКорректностьЗаполненияДокументаПродажи(ЭтотОбъект, Отказ);
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;

	ПроведениеСервер.УстановитьРежимПроведения(ЭтотОбъект, РежимЗаписи, РежимПроведения);

	СуммаАктивногоВарианта = РассчитатьСуммуАктивногоВарианта();
	
	Если СуммаАктивногоВарианта <> СуммаДокумента Тогда
		СуммаДокумента = СуммаАктивногоВарианта;
	КонецЕсли;
	
	ОбщегоНазначенияУТ.ИзменитьПризнакСогласованностиДокумента(ЭтотОбъект,
		РежимЗаписи,
		Перечисления.СтатусыКоммерческихПредложенийКлиентам.НеСогласовано
	);
	
	// Очистим реквизиты документа не используемые для хозяйственной операции.
	МассивВсехРеквизитов = Новый Массив;
	МассивРеквизитовОперации = Новый Массив;
	
	Документы.КоммерческоеПредложениеКлиенту.ЗаполнитьИменаРеквизитовПоХозяйственнойОперации(
		ХозяйственнаяОперация,
		МассивВсехРеквизитов,
		МассивРеквизитовОперации
	);
	ДенежныеСредстваСервер.ОчиститьНеиспользуемыеРеквизиты(
		ЭтотОбъект,
		МассивВсехРеквизитов,
		МассивРеквизитовОперации
	);
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	Согласован         = Ложь;
	Статус             = Перечисления.СтатусыКоммерческихПредложенийКлиентам.НеСогласовано;
	СрокДействия       = '00010101';
	ДокументОснование  = Документы.КоммерческоеПредложениеКлиенту.ПустаяСсылка();
	
	СкидкиРассчитаны = Ложь;
	СкидкиНаценкиСервер.ОтменитьСкидки(ЭтотОбъект, "Товары", Истина);
	
	ИнициализироватьДокумент();
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// Инициализация и заполнение

Процедура ЗаполнитьДокументНаОснованииСделкиПоПродаже(Основание)
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	СделкиСКлиентами.Ссылка              КАК Сделка,
	|	СделкиСКлиентами.Партнер             КАК Партнер,
	|	СделкиСКлиентами.СоглашениеСКлиентом КАК Соглашение,
	|	СделкиСКлиентами.ПервичныйСпрос.(
	|		Истина            КАК Активность,
	|		ТекстовоеОписание КАК ТекстовоеОписание
	|	) КАК ПервичныйСпрос
	|ИЗ
	|	Справочник.СделкиСКлиентами КАК СделкиСКлиентами
	|ГДЕ
	|	СделкиСКлиентами.Ссылка = &Основание
	|");
	
	Запрос.УстановитьПараметр("Основание",Основание);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Выборка.Следующий();
	ОбщегоНазначенияУТ.ПроверитьВозможностьВводаНаОснованииСделкиПоПродаже(Выборка.Партнер);
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, Выборка);
	
	Товары.Загрузить(Выборка.ПервичныйСпрос.Выгрузить());
	
	Если ЗначениеЗаполнено(Выборка.Соглашение) Тогда
		ЗаполнитьУсловияПродажПоCоглашению();
	Иначе
		ЗаполнитьУсловияПродажПоУмолчанию();
	КонецЕсли;
	
КонецПроцедуры

Процедура ЗаполнитьДокументНаОснованииИндивидуальногоСоглашенияСКлиентом(Знач ДокументОснование)
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	СоглашениеСКлиентом.Ссылка      КАК Соглашение,
	|	СоглашениеСКлиентом.Партнер     КАК Партнер,
	|
	|	СоглашениеСКлиентом.Статус      КАК СтатусДокумента,
	|	ВЫБОР
	|		КОГДА
	|			СоглашениеСКлиентом.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыСоглашенийСКлиентами.Действует)
	|		ТОГДА
	|			ЛОЖЬ
	|		ИНАЧЕ
	|			ИСТИНА
	|	КОНЕЦ КАК ЕстьОшибкиСтатус,
	|	СоглашениеСКлиентом.Типовое КАК ЕстьОшибкиТиповое
	|
	|ИЗ
	|	Справочник.СоглашенияСКлиентами  КАК СоглашениеСКлиентом
	|ГДЕ
	|	СоглашениеСКлиентом.Ссылка = &ДокументОснование
	|");
	
	Запрос.УстановитьПараметр("ДокументОснование", ДокументОснование);
	
	РезультатЗапроса = Запрос.ВыполнитьПакет();
	
	Выборка = РезультатЗапроса[0].Выбрать();
	Выборка.Следующий();
	
	МассивДопустимыхСтатусов = Новый Массив();
	МассивДопустимыхСтатусов.Добавить(Перечисления.СтатусыСоглашенийСКлиентами.Действует);
	
	ОбщегоНазначенияУТ.ПроверитьВозможностьВводаНаОснованииСоглашения(Выборка.ЕстьОшибкиТиповое);
		
	ОбщегоНазначенияУТ.ПроверитьВозможностьВводаНаОсновании(Выборка.Соглашение,
		Выборка.СтатусДокумента,
		,
		Выборка.ЕстьОшибкиСтатус,
		МассивДопустимыхСтатусов
	);
	
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, Выборка);
	ЗаполнитьУсловияПродажПоCоглашению();
	
КонецПроцедуры

Процедура ЗаполнитьДокументНаОснованииКоммерческогоПредложенияКлиенту(Знач ДокументОснование)
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	КоммерческоеПредложениеКлиенту.Ссылка                           КАК ДокументОснование,
	|	КоммерческоеПредложениеКлиенту.Партнер                          КАК Партнер,
	|	КоммерческоеПредложениеКлиенту.Сделка                           КАК Сделка,
	|	КоммерческоеПредложениеКлиенту.Организация                      КАК Организация,
	|	КоммерческоеПредложениеКлиенту.КонтактноеЛицо                   КАК КонтактноеЛицо,
	|	КоммерческоеПредложениеКлиенту.Валюта                           КАК Валюта,
	|	КоммерческоеПредложениеКлиенту.СуммаДокумента                   КАК СуммаДокумента,
	|	КоммерческоеПредложениеКлиенту.ГрафикОплаты                     КАК ГрафикОплаты,
	|	КоммерческоеПредложениеКлиенту.ДополнительнаяИнформация         КАК ДополнительнаяИнформация,
	|	КоммерческоеПредложениеКлиенту.Соглашение                       КАК Соглашение,
	|	КоммерческоеПредложениеКлиенту.СрокПоставки                     КАК СрокПоставки,
	|	КоммерческоеПредложениеКлиенту.Соглашение.ДатаОкончанияДействия КАК СрокДействия,
	|	КоммерческоеПредложениеКлиенту.ЦенаВключаетНДС                  КАК ЦенаВключаетНДС,
	|	КоммерческоеПредложениеКлиенту.НалогообложениеНДС               КАК НалогообложениеНДС,
	|	КоммерческоеПредложениеКлиенту.ФормаОплаты                      КАК ФормаОплаты,
	|	КоммерческоеПредложениеКлиенту.Склад                            КАК Склад,
	|	КоммерческоеПредложениеКлиенту.КартаЛояльности                  КАК КартаЛояльности,
	|
	|	КоммерческоеПредложениеКлиенту.Статус                           КАК СтатусДокумента,
	|	НЕ КоммерческоеПредложениеКлиенту.Проведен                      КАК ЕстьОшибкиПроведен,
	|
	|	КоммерческоеПредложениеКлиенту.Товары.(
	|		Номенклатура,
	|		Характеристика,
	|		Упаковка,
	|		КоличествоУпаковок,
	|		Количество,
	|		ВидЦены,
	|		Цена,
	|		ПроцентРучнойСкидки,
	|		СуммаРучнойСкидки,
	|		СтавкаНДС,
	|		СуммаНДС,
	|		СуммаСНДС,
	|		Сумма,
	|		Активность,
	|		ТекстовоеОписание
	|	) КАК Товары,
	|
	|	КоммерческоеПредложениеКлиенту.СкидкиНаценки.(
	|		КлючСвязи КАК КлючСвязи,
	|		СкидкаНаценка КАК СкидкаНаценка,
	|		Сумма КАК Сумма
	|	) КАК СкидкиНаценки
	|
	|ИЗ
	|	Документ.КоммерческоеПредложениеКлиенту КАК КоммерческоеПредложениеКлиенту
	|ГДЕ
	|	КоммерческоеПредложениеКлиенту.Ссылка = &ДокументОснование");
	
	Запрос.УстановитьПараметр("ДокументОснование", ДокументОснование);
	
	РезультатЗапроса = Запрос.Выполнить();
	Выборка = РезультатЗапроса.Выбрать();
	Выборка.Следующий();
	
	ОбщегоНазначенияУТ.ПроверитьВозможностьВводаНаОсновании(
		Выборка.ДокументОснование,
		,
		Выборка.ЕстьОшибкиПроведен
	);
	
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, Выборка);
	Товары.Загрузить(Выборка.Товары.Выгрузить());
	
	СтруктураПараметры = Новый Структура;
	СтруктураПараметры.Вставить("ПрименятьКОбъекту",                Истина);
	СтруктураПараметры.Вставить("ТолькоПредварительныйРасчет",      Ложь);
	СтруктураПараметры.Вставить("ВосстанавливатьУправляемыеСкидки", Истина);
	СтруктураПараметры.Вставить("УправляемыеСкидки", Новый СписокЗначений);
	
	СкидкиНаценки.Загрузить(Выборка.СкидкиНаценки.Выгрузить());
	СкидкиНаценкиСервер.РассчитатьПоКоммерческомуПредложениюКлиенту(ЭтотОбъект, СтруктураПараметры);
	СкидкиРассчитаны = Истина;
	
КонецПроцедуры

Процедура ЗаполнитьДокументПоОтбору(Знач ДанныеЗаполнения)
	
	Если ДанныеЗаполнения.Свойство("Партнер") Тогда
		
		Партнер = ДанныеЗаполнения.Партнер;
		ЗаполнитьУсловияПродажПоУмолчанию();
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ИнициализироватьДокумент()
	
	Менеджер       = Пользователи.ТекущийПользователь();
	Валюта         = ДоходыИРасходыСервер.ПолучитьВалютуУправленческогоУчета(Валюта);
	Организация    = ЗначениеНастроекПовтИсп.ПолучитьОрганизациюПоУмолчанию(Организация);
	Склад          = ЗначениеНастроекПовтИсп.ПолучитьСкладПоУмолчанию(Склад, ПолучитьФункциональнуюОпцию("ИспользоватьСкладыВТабличнойЧастиДокументовПродажи"));
	КонтактноеЛицо = ЗначениеНастроекПовтИсп.ПолучитьКонтактноеЛицоПартнераПоУмолчанию(Партнер);
	
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьСогласованиеКоммерческихПредложений")
	 Или Пользователи.РолиДоступны("ОтклонениеОтУсловийПродаж") Тогда
		Статус = Перечисления.СтатусыКоммерческихПредложенийКлиентам.Действует;
	Иначе
		Статус = Перечисления.СтатусыКоммерческихПредложенийКлиентам.НеСогласовано;
	КонецЕсли;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Прочее

Функция РассчитатьСуммуАктивногоВарианта()
	
	ПараметрыОтбора = Новый Структура;
	ПараметрыОтбора.Вставить("Активность", Истина);
	
	НайденныеСтроки = Товары.НайтиСтроки(ПараметрыОтбора);
	
	СуммаАктивногоВарианта = 0;
	
	Если НайденныеСтроки.Количество() > 0 Тогда
		
		Если ЦенаВключаетНДС Тогда
			
			ТаблицаАктивныхПозиций = Товары.Выгрузить(НайденныеСтроки, "Сумма");
			ТаблицаАктивныхПозиций.Свернуть(, "Сумма");
			СуммаАктивногоВарианта = ТаблицаАктивныхПозиций[0].Сумма;
			
		Иначе
			
			ТаблицаАктивныхПозиций = Товары.Выгрузить(НайденныеСтроки, "Сумма,СуммаНДС");
			ТаблицаАктивныхПозиций.Свернуть(, "Сумма,СуммаНДС");
			СуммаАктивногоВарианта = ТаблицаАктивныхПозиций[0].Сумма + ТаблицаАктивныхПозиций[0].СуммаНДС;
			
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат СуммаАктивногоВарианта;
	
КонецФункции