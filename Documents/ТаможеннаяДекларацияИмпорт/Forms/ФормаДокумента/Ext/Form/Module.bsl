﻿&НаКлиенте
Перем КэшированныеЗначения; // используется механизмом обработки изменения реквизитов ТЧ

&НаКлиенте
Перем КешРазделов; // используется для обработок связи между разделами и товарами

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	// Обработчик подсистемы "Свойства"
	УправлениеСвойствами.ПриСозданииНаСервере(ЭтаФорма, Объект, "ГруппаДополнительныеРеквизиты");
	// Обработчик механизма "ВерсионированиеОбъектов"
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	// Обработчик подсистемы "Внешние обработки"
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	// Обработчик механизма "ДатыЗапретаИзменения"
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
	ПриЧтенииСозданииНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	ПараметрыЗаписи.Свойство("РежимЗаписи", РежимЗаписи);
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	Если Объект.Статус = Перечисления.СтатусыТаможенныхДеклараций.ВыпущеноСТаможни И РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		Отказ = Объект.Товары.НайтиСтроки(Новый Структура("НомерРаздела", 0)).Количество() > 0;
		Если Отказ Тогда
			Элементы.ТоварыПоказатьВсеТовары.Пометка = Истина;
			НастроитьЗаполнениеТоваров();
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	// Обработчик механизма "Свойства"
	УправлениеСвойствами.ПередЗаписьюНаСервере(ЭтаФорма, ТекущийОбъект);
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	ЗаполнитьДанныеФормы();
	ЗаполнитьДополнительныеПризнакиТоваров();
	Элементы.СтраницаКомментарий.Картинка = ОбщегоНазначенияУТ.ПолучитьКартинкуКомментария(Объект.Комментарий);
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	Оповестить("Запись_ТаможеннаяДекларацияИмпорт", ПараметрыЗаписи, Объект.Ссылка);
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	// Подсистема "Свойства"
	Если УправлениеСвойствамиКлиент.ОбрабатыватьОповещения(ЭтаФорма, ИмяСобытия, Параметр) Тогда
		ОбновитьЭлементыДополнительныхРеквизитов();
	КонецЕсли;
	
	Если ИмяСобытия = "ЗачтенаОплата" И Параметр = Объект.Ссылка Тогда
		//ПеречитатьСостояниеРасчетов();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	Если ИсточникВыбора.ИмяФормы = "Обработка.ПодборТоваровВДокументЗакупки.Форма.Форма" Тогда
		ОбработкаВыбораДобавитьТовары(ВыбранноеЗначение, "АдресТоваровВХранилище");
	ИначеЕсли ИсточникВыбора.ИмяФормы = "ОбщаяФорма.ПодборТоваровПоПоступлению" Тогда
		ОбработкаВыбораДобавитьТовары(ВыбранноеЗначение, "АдресТоваровВХранилище", "Товары");
	ИначеЕсли ИсточникВыбора.ИмяФормы = "РегистрНакопления.ТоварыОрганизацийКОформлению.Форма.ПодборПоОстаткам" Тогда
		ОбработкаВыбораПодобратьИзОформления(ВыбранноеЗначение);
	КонецЕсли;
	Если Окно <> Неопределено Тогда
		Окно.Активизировать();
	КонецЕсли;
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ШАПКИ ФОРМЫ

&НаКлиенте
Процедура НомерГТДОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	ОбработкаТабличнойЧастиКлиент.НомерГТДОбработкаВыбора(ВыбранноеЗначение, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура СтатусПриИзменении(Элемент)
	Если ЗначениеЗаполнено(Объект.Статус) И СтатусДокумента <> Объект.Статус
		И Объект.Статус = ПредопределенноеЗначение("Перечисление.СтатусыТаможенныхДеклараций.ВыпущеноСТаможни")
	Тогда
		ДатаВыпуска = Объект.Дата;
		Если ВвестиДату(ДатаВыпуска, НСтр("ru='Дата выпуска с таможни'"), ЧастиДаты.Дата) Тогда
			Объект.Дата = КонецДня(ДатаВыпуска);
		КонецЕсли;
	КонецЕсли;
	СтатусДокумента = Объект.Статус;
КонецПроцедуры

&НаКлиенте
Процедура ВалютаПриИзменении(Элемент)
	ПересчитатьСуммыПоВалюте();
КонецПроцедуры

&НаКлиенте
Процедура ПоставщикПриИзменении(Элемент)
	ПоставщикПриИзмененииСервер();
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	ОрганизацияПриИзмененииСервер();
КонецПроцедуры

&НаКлиенте
Процедура ПоступлениеТоваровПриИзменении(Элемент)
	ПоступлениеТоваровПриИзмененииСервер();
КонецПроцедуры

&НаКлиенте
Процедура ХозяйственнаяОперацияПриИзменении(Элемент)
	ХозяйственнаяОперацияПриИзмененииСервер();
КонецПроцедуры

&НаКлиенте
Процедура ПартнерПриИзменении(Элемент)
	ПартнерПриИзмененииСервер();
	ПересчитатьСуммыПоВалюте();
КонецПроцедуры

&НаКлиенте
Процедура КонтрагентПриИзменении(Элемент)
	КонтрагентПриИзмененииСервер();
КонецПроцедуры

&НаКлиенте
Процедура СоглашениеПриИзменении(Элемент)
	СоглашениеПриИзмененииСервер();
	ПересчитатьСуммыПоВалюте();
КонецПроцедуры

&НаКлиенте
Процедура ДоговорПриИзменении(Элемент)
	ЗакупкиСервер.ЗаполнитьБанковскиеСчетаПоДоговору(Объект.Договор, Объект.БанковскийСчетОрганизации, Объект.БанковскийСчетКонтрагента);
	
	ЗаполнитьДанныеФормы();
	НастроитьЭлементыФормы();
КонецПроцедуры

&НаКлиенте
Процедура ВалютаВзаиморасчетовПриИзменении(Элемент)
	Если ЗначениеЗаполнено(Объект.ВалютаВзаиморасчетов) Тогда
		ВалютаВзаиморасчетовПриИзмененииСервер();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура СтраницыПриСменеСтраницы(Элемент, ТекущаяСтраница)
	Если ТекущаяСтраница = Элементы.СтраницаРазделыТовары Тогда
		АктивироватьРаздел();
	КонецЕсли;
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ТАБЛИЦЫ ФОРМЫ РАЗДЕЛЫ

&НаКлиенте
Процедура РазделыПриАктивизацииСтроки(Элемент)
	НомерРаздела = ?(Элементы.Разделы.ТекущиеДанные<>Неопределено, Элементы.Разделы.ТекущиеДанные.НомерРаздела, 0);
	ПодключитьОбработчикОжидания("АктивироватьРаздел", 0.2, Истина);
КонецПроцедуры

&НаКлиенте
Процедура РазделыПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	Если НоваяСтрока Тогда
		Элемент.ТекущиеДанные.НомерРаздела = МаксимальныйНомерРаздела(Объект.Разделы)+1;
		НомерРаздела = Элемент.ТекущиеДанные.НомерРаздела;
		АктивироватьРаздел();
	КонецЕсли;
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура РазделыПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	Если НоваяСтрока и Не ОтменаРедактирования Тогда
		Элементы.ТоварыНомерРаздела.СписокВыбора.Добавить(НомерРаздела, Формат(НомерРаздела, Элементы.ТоварыНомерРаздела.Формат));
	КонецЕсли;
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура РазделыПередУдалением(Элемент, Отказ)
	Разделы = ОбщегоНазначенияУТКлиентСервер.ВыделенныеЭлементыКоллекции(Объект.Разделы, Элементы.Разделы.ВыделенныеСтроки);
	КешРазделов = Новый Соответствие;
	Для Каждого Раздел Из Разделы Цикл
		КешРазделов.Вставить(Раздел.НомерРаздела, Раздел.НомерРаздела);
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура РазделыПослеУдаления(Элемент)
	ОчиститьНомераРазделовТоваров(КешРазделов);
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура РазделыТаможеннаяСтоимостьПриИзменении(Элемент)
	Раздел = Элементы.Разделы.ТекущиеДанные;
	ПересчитатьПоля(Раздел, "СуммаПошлины, СуммаНДС", Раздел);
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура РазделыСтавкаПошлиныПриИзменении(Элемент)
	Раздел = Элементы.Разделы.ТекущиеДанные;
	ПересчитатьПоля(Раздел, "СуммаПошлины, СуммаНДС", Раздел);
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура РазделыСуммаПошлиныПриИзменении(Элемент)
	Раздел = Элементы.Разделы.ТекущиеДанные;
	ПересчитатьПоля(Раздел, "СтавкаПошлины, СуммаНДС", Раздел);
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура РазделыСтавкаНДСПриИзменении(Элемент)
	Раздел = Элементы.Разделы.ТекущиеДанные;
	ПересчитатьПоля(Раздел, "СуммаНДС", Раздел);
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура РазделыСуммаНДСПриИзменении(Элемент)
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ТАБЛИЦЫ ФОРМЫ ТОВАРЫ

&НаКлиенте
Процедура ТоварыПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	Если НоваяСтрока И Не Копирование Тогда
		Элементы.Товары.ТекущиеДанные.НомерРаздела = НомерРаздела;
	КонецЕсли;
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура ТоварыПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура ТоварыПослеУдаления(Элемент)
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура ТоварыНоменклатураПриИзменении(Элемент)
    ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	Действия = Новый Структура;
    Действия.Вставить("ПроверитьХарактеристикуПоВладельцу",ТекущаяСтрока.Характеристика);
    Действия.Вставить("ПроверитьЗаполнитьУпаковкуПоВладельцу",ТекущаяСтрока.Упаковка);
    Действия.Вставить("ПересчитатьКоличествоЕдиниц");
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, Действия, КэшированныеЗначения);
КонецПроцедуры

&НаКлиенте
Процедура ТоварыХарактеристикаПриИзменении(Элемент)
    ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	Действия = Новый Структура;
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, Действия, КэшированныеЗначения);
КонецПроцедуры

&НаКлиенте
Процедура ТоварыКоличествоУпаковокПриИзменении(Элемент)
    ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	Действия = Новый Структура;
	Действия.Вставить("ПересчитатьКоличествоЕдиниц");
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, Действия, КэшированныеЗначения);
КонецПроцедуры

&НаКлиенте
Процедура ТоварыУпаковкаПриИзменении(Элемент)
    ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	Действия = Новый Структура;
    Действия.Вставить("ПересчитатьКоличествоЕдиниц");
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, Действия, КэшированныеЗначения);
КонецПроцедуры

&НаКлиенте
Процедура ТоварыТаможеннаяСтоимостьПриИзменении(Элемент)
	Товар = Элементы.Товары.ТекущиеДанные;
	ПересчитатьПоля(Товар, "СуммаПошлины, СуммаНДС", СтавкиРаздела(Объект.Разделы, Товар.НомерРаздела));
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура ТоварыСуммаПошлиныПриИзменении(Элемент)
	Товар = Элементы.Товары.ТекущиеДанные;
	ПересчитатьПоля(Товар, "СуммаНДС", СтавкиРаздела(Объект.Разделы, Товар.НомерРаздела));
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура ТоварыСуммаНДСПриИзменении(Элемент)
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура РаспределитьНаТовары(Команда)
	Если Объект.Товары.Количество() > 0 И Объект.Разделы.Количество() > 0 Тогда
		РаспределитьНаТоварыСервер();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура РассчитатьПоТоварам(Команда)
	Если Объект.Товары.Количество() > 0 И Объект.Разделы.Количество() > 0 Тогда
		РассчитатьПоТоварамСервер();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПоказатьВсеТовары(Команда)
	Элементы.ТоварыПоказатьВсеТовары.Пометка = Не Элементы.ТоварыПоказатьВсеТовары.Пометка;
	НастроитьЗаполнениеТоваров();
	АктивироватьРаздел();
КонецПроцедуры

&НаКлиенте
Процедура УстановитьНомерРаздела(Команда)
	УстановитьНомерРазделаСервер(НомерРаздела);
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьНомерРаздела(Команда)
	УстановитьНомерРазделаСервер(НомерРаздела, Новый Структура("НомерРаздела", 0));
КонецПроцедуры

&НаКлиенте
Процедура ПодобратьТовары(Команда)
	Склад = ?(Элементы.Разделы.ТекущиеДанные<>Неопределено, Элементы.Разделы.ТекущиеДанные.Склад, Неопределено);
	ПараметрыПодбора =
		Новый Структура(
			"Документ, Дата, Склад, Соглашение,
			|ОтборПоТипуНоменклатуры, РежимПодбораИспользоватьСкладыВТабличнойЧасти,
			|РежимПодбораИсключитьГруппыДоступныеВЗаказах, РежимПодбораБезСуммовыхПараметров",
			Объект.Ссылка, ДатаДокумента(Объект.Дата), Склад, ПоступлениеСоглашение,
			ПредопределенноеЗначение("Перечисление.ТипыНоменклатуры.Товар"), Истина,
			Истина, Истина);
	ОткрытьФорму("Обработка.ПодборТоваровВДокументЗакупки.Форма", ПараметрыПодбора, ЭтаФорма, УникальныйИдентификатор);
КонецПроцедуры

&НаКлиенте
Процедура ПодобратьИзПоступления(Команда)
	ПараметрыПодбора =
		Новый Структура(
			"ДокументПоступления, Валюта, Дата, ЦенаВключаетНДС, ПолучатьСерииНоменклатуры",
			Объект.ПоступлениеТоваров, Объект.Валюта, ДатаДокумента(Объект.Дата), Ложь, Ложь);
	ОткрытьФорму("ОбщаяФорма.ПодборТоваровПоПоступлению", ПараметрыПодбора, ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура ПодобратьИзОформления(Команда)
	Если ФинансыКлиент.ПроверитьВозможностьЗаполненияТабличнойЧасти(ЭтаФорма, Неопределено, Новый Структура("Организация, Поставщик")) Тогда
		ОтборТоваров = Новый Структура("НомерРаздела", НомерРаздела);
		АдресВХранилище = ПоместитьВХранилище("Товары", "Номенклатура, Характеристика, Склад, Количество", ОтборТоваров);
		Склад = ?(Элементы.Разделы.ТекущиеДанные<>Неопределено, Элементы.Разделы.ТекущиеДанные.Склад, Неопределено);
		ПараметрыПодбора =
			Новый Структура(
				"АдресВХранилище, Организация, Склад, Дата, Поставщик, ДеятельностьОблагаетсяЕНВД",
				АдресВХранилище, Объект.Организация, Склад, Объект.Дата, Объект.Поставщик, Ложь);
		ОткрытьФорму("РегистрНакопления.ТоварыОрганизацийКОформлению.Форма.ПодборПоОстаткам", ПараметрыПодбора, ЭтаФорма);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтоимостьПоСоглашению(Команда)
	СоглашениеСПоставщиком = ВыбратьСоглашениеСПоставщиком();
	Если ЗначениеЗаполнено(СоглашениеСПоставщиком) Тогда
		УстановитьСтоимостьСервер(СоглашениеСПоставщиком);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьСтоимостьПоСоглашению(Команда)
	СоглашениеСПоставщиком = ВыбратьСоглашениеСПоставщиком();
	Если ЗначениеЗаполнено(СоглашениеСПоставщиком) Тогда
		УстановитьСтоимостьСервер(СоглашениеСПоставщиком, Неопределено, Новый Структура("ТаможеннаяСтоимость", 0.));
	КонецЕсли;
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

///////////////////////////////////////////////////////////////////////////////
// При изменении реквизитов

&НаСервере
Процедура ВалютаПриИзмененииСервер(ВалютаСтарая, ВалютаНовая)
	ДатаДокумента = ДатаДокумента(Объект.Дата);
	КурсыСтарые = РаботаСКурсамиВалют.ПолучитьКурсВалюты(ВалютаСтарая, ДатаДокумента);
	КурсыНовые  = РаботаСКурсамиВалют.ПолучитьКурсВалюты(ВалютаНовая, ДатаДокумента);
	
	Для Каждого Раздел Из Объект.Разделы Цикл
		Раздел.ТаможеннаяСтоимость =
			РаботаСКурсамиВалютКлиентСервер.ПересчитатьИзВалютыВВалюту(
				Раздел.ТаможеннаяСтоимость, ВалютаСтарая, ВалютаНовая,
				КурсыСтарые.Курс, КурсыНовые.Курс, КурсыСтарые.Кратность, КурсыНовые.Кратность);
		ПересчитатьПоля(Раздел, "СуммаПошлины, СуммаНДС", Раздел);
	КонецЦикла;
	
	ТекущийРаздел = 0;
	СтавкаПошлины = 0.;
	Для Каждого Товар Из Объект.Товары Цикл
		Товар.ТаможеннаяСтоимость =
			РаботаСКурсамиВалютКлиентСервер.ПересчитатьИзВалютыВВалюту(
				Товар.ТаможеннаяСтоимость, ВалютаСтарая, ВалютаНовая,
				КурсыСтарые.Курс, КурсыНовые.Курс, КурсыСтарые.Кратность, КурсыНовые.Кратность);
		Если ТекущийРаздел <> Товар.НомерРаздела Тогда
			ТекущийРаздел = Товар.НомерРаздела;
			Ставки = СтавкиРаздела(Объект.Разделы, ТекущийРаздел);
		КонецЕсли;
		ПересчитатьПоля(Товар, "СуммаПошлины, СуммаНДС", Ставки);
	КонецЦикла;
	
	Объект.ТаможенныйСбор = 
		РаботаСКурсамиВалютКлиентСервер.ПересчитатьИзВалютыВВалюту(
			Объект.ТаможенныйСбор, ВалютаСтарая, ВалютаНовая,
			КурсыСтарые.Курс, КурсыНовые.Курс, КурсыСтарые.Кратность, КурсыНовые.Кратность);
	Объект.ТаможенныйШтраф =
		РаботаСКурсамиВалютКлиентСервер.ПересчитатьИзВалютыВВалюту(
			Объект.ТаможенныйШтраф, ВалютаСтарая, ВалютаНовая,
			КурсыСтарые.Курс, КурсыНовые.Курс, КурсыСтарые.Кратность, КурсыНовые.Кратность);
			
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура ПересчитатьСуммыПоВалюте()
	Если ЗначениеЗаполнено(Объект.Валюта) И ВалютаДокумента <> Объект.Валюта Тогда
		ВалютаПриИзмененииСервер(ВалютаДокумента, Объект.Валюта);
		ЦенообразованиеКлиент.ОповеститьОбОкончанииПересчетаСуммВВалюту(ВалютаДокумента, Объект.Валюта);
	КонецЕсли;
	ВалютаДокумента = Объект.Валюта;
КонецПроцедуры

&НаСервере
Процедура ПоставщикПриИзмененииСервер()
	ЗаполнитьДанныеФормы();
	НастроитьЭлементыФормы();
КонецПроцедуры

&НаСервере
Процедура ОрганизацияПриИзмененииСервер()
	Если ЗначениеЗаполнено(Объект.Организация) Тогда
		ЗаполнитьДоговорПоУмолчанию();
		Объект.БанковскийСчетОрганизации = ЗначениеНастроекПовтИсп.ПолучитьБанковскийСчетОрганизацииПоУмолчанию(Объект.Организация, , Объект.БанковскийСчетОрганизации);
	КонецЕсли;
	ЗаполнитьДанныеФормы();
	НастроитьЭлементыФормы();
КонецПроцедуры

&НаСервере
Процедура ПоступлениеТоваровПриИзмененииСервер()
	Если ЗначениеЗаполнено(Объект.ПоступлениеТоваров) Тогда
		ПоступлениеРеквизиты =
			ОбщегоНазначения.ПолучитьЗначенияРеквизитов(Объект.ПоступлениеТоваров,
				"Партнер, Организация, Соглашение");
		Если Объект.Поставщик <> ПоступлениеРеквизиты.Партнер Тогда
			Объект.Поставщик = ПоступлениеРеквизиты.Партнер;
		КонецЕсли;
		Если Объект.Организация <> ПоступлениеРеквизиты.Организация Тогда
			Объект.Организация = ПоступлениеРеквизиты.Организация;
		КонецЕсли;
		ПоступлениеСоглашение = ПоступлениеРеквизиты.Соглашение;
	Иначе
		ПоступлениеСоглашение = Неопределено;
	КонецЕсли;
	
	НастроитьЗаполнениеТоваров();
КонецПроцедуры

&НаСервере
Процедура ХозяйственнаяОперацияПриИзмененииСервер()
	ЗаполнитьДанныеФормы();
	Если РасчетыСТаможней Тогда
		Объект.Валюта = Константы.ВалютаРегламентированногоУчета.Получить();
		Объект.ВалютаВзаиморасчетов = Объект.Валюта;
		Объект.ФормаОплаты = Перечисления.ФормыОплаты.Безналичная;
	КонецЕсли;
	НастроитьЭлементыФормы();
КонецПроцедуры

&НаСервере
Процедура ПартнерПриИзмененииСервер()
	Если ЗначениеЗаполнено(Объект.Партнер) Тогда
    	Документы.ТаможеннаяДекларацияИмпорт.ЗаполнитьПоПартнеру(Объект, Объект.Партнер);
	КонецЕсли;
	
	ЗаполнитьДанныеФормы();
	НастроитьЭлементыФормы();
КонецПроцедуры

&НаСервере
Процедура КонтрагентПриИзмененииСервер()
	Если ЗначениеЗаполнено(Объект.Контрагент) Тогда
		ЗаполнитьДоговорПоУмолчанию();
		Объект.БанковскийСчетКонтрагента = ЗначениеНастроекПовтИсп.ПолучитьБанковскийСчетКонтрагентаПоУмолчанию(Объект.Контрагент, , Объект.БанковскийСчетКонтрагента);
	КонецЕсли;
	НастроитьЭлементыФормы();
КонецПроцедуры

&НаСервере
Процедура СоглашениеПриИзмененииСервер()
	Если ЗначениеЗаполнено(Объект.Соглашение) Тогда
		УсловияЗакупок = ЗакупкиСервер.ПолучитьУсловияЗакупок(Объект.Соглашение);
		Документы.ТаможеннаяДекларацияИмпорт.ЗаполнитьПоУсловиямЗакупок(Объект, УсловияЗакупок);
	КонецЕсли;
	
	ЗаполнитьДанныеФормы();
	НастроитьЭлементыФормы();
КонецПроцедуры

&НаСервере
Процедура ВалютаВзаиморасчетовПриИзмененииСервер()
	ЗаполнитьДоговорПоУмолчанию();
	НастроитьЭлементыФормы();
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Обработка команд формы на сервере

&НаСервере
Процедура РаспределитьНаТоварыСервер()
	Отбор = Новый Структура("НомерРаздела");
	Разделы = ОбщегоНазначенияУТКлиентСервер.ВыделенныеЭлементыКоллекции(Объект.Разделы, Элементы.Разделы.ВыделенныеСтроки);
	Для Каждого Раздел Из Разделы Цикл
		Отбор.НомерРаздела = Раздел.НомерРаздела;
		ТоварыРаздела = Объект.Товары.Выгрузить(Отбор);
		
		Если ТоварыРаздела.Количество() > 0 Тогда
			Коэффициенты = ТоварыРаздела.ВыгрузитьКолонку("ТаможеннаяСтоимость");
			НовыеСуммы = ОбщегоНазначения.РаспределитьСуммуПропорциональноКоэффициентам(Раздел.ТаможеннаяСтоимость, Коэффициенты);
			Если НовыеСуммы <> Неопределено Тогда
				ТоварыРаздела.ЗагрузитьКолонку(НовыеСуммы, "ТаможеннаяСтоимость");
			КонецЕсли;
			НовыеСуммы = ОбщегоНазначения.РаспределитьСуммуПропорциональноКоэффициентам(Раздел.СуммаПошлины, Коэффициенты);
			Если НовыеСуммы <> Неопределено Тогда
				ТоварыРаздела.ЗагрузитьКолонку(НовыеСуммы, "СуммаПошлины");
			КонецЕсли;
			НовыеСуммы = ОбщегоНазначения.РаспределитьСуммуПропорциональноКоэффициентам(Раздел.СуммаНДС, Коэффициенты);
			Если НовыеСуммы <> Неопределено Тогда
				ТоварыРаздела.ЗагрузитьКолонку(НовыеСуммы, "СуммаНДС");
			КонецЕсли;
		КонецЕсли;
		
		Для каждого Товар Из ТоварыРаздела Цикл
			ОбъектТовар = Объект.Товары[Товар.НомерСтроки-1];
			Объект.Товары[Товар.НомерСтроки-1].ТаможеннаяСтоимость = ?(Раздел.ТаможеннаяСтоимость > 0., Товар.ТаможеннаяСтоимость, 0.);
			Объект.Товары[Товар.НомерСтроки-1].СуммаПошлины = ?(Раздел.СуммаПошлины > 0., Товар.СуммаПошлины, 0.);
			Объект.Товары[Товар.НомерСтроки-1].СуммаНДС = ?(Раздел.СуммаНДС > 0., Товар.СуммаНДС, 0.);
		КонецЦикла;
	КонецЦикла;
	
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаСервере
Процедура РассчитатьПоТоварамСервер()
	Отбор = Новый Структура("НомерРаздела");
	Разделы = ОбщегоНазначенияУТКлиентСервер.ВыделенныеЭлементыКоллекции(Объект.Разделы, Элементы.Разделы.ВыделенныеСтроки);
	Для Каждого Раздел Из Разделы Цикл
		Отбор.НомерРаздела = Раздел.НомерРаздела;
		ТоварыРаздела = Объект.Товары.НайтиСтроки(Отбор);
		Если ТоварыРаздела.Количество() > 0 Тогда
			Раздел.ТаможеннаяСтоимость = 0.;
			ПересчитатьПоля(Раздел, "СуммаПошлины, СуммаНДС", Раздел);
			Для Каждого Товар Из ТоварыРаздела Цикл
				Раздел.ТаможеннаяСтоимость = Раздел.ТаможеннаяСтоимость + Товар.ТаможеннаяСтоимость;
				Раздел.СуммаПошлины = Раздел.СуммаПошлины + Товар.СуммаПошлины;
				Раздел.СуммаНДС = Раздел.СуммаНДС + Товар.СуммаНДС;
			КонецЦикла;
			ПересчитатьПоля(Раздел, "СтавкаПошлины", Раздел);
		КонецЕсли;
	КонецЦикла;
	
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаСервере
Процедура УстановитьНомерРазделаСервер(ТекущийНомер, ОтборТоваров = Неопределено)
	Если ОтборТоваров = Неопределено Тогда
		ТоварыЗаполнения = ОбщегоНазначенияУТКлиентСервер.ВыделенныеЭлементыКоллекции(Объект.Товары, Элементы.Товары.ВыделенныеСтроки);
	Иначе
		ТоварыЗаполнения = Объект.Товары.НайтиСтроки(ОтборТоваров);
	КонецЕсли;
	Для Каждого Товар Из ТоварыЗаполнения Цикл
		Товар.НомерРаздела = ТекущийНомер;
	КонецЦикла;
КонецПроцедуры

&НаСервере
Процедура ОбработкаВыбораДобавитьТовары(ВыбранноеЗначение, ИмяАдреса, ИмяТабчасти=Неопределено)
	Действия = Новый Структура;
	УстановитьДействияДополнительныхПризнаковТоваров(Действия);
	Ставки = СтавкиРаздела(Объект.Разделы, НомерРаздела);
	
	БуферТоваров = ПолучитьИзВременногоХранилища(ВыбранноеЗначение[ИмяАдреса]);
	Если ИмяТабчасти<>Неопределено Тогда
		БуферТоваров = БуферТоваров[ИмяТабчасти];
	КонецЕсли;
	Для каждого ЭлементБуфера Из БуферТоваров Цикл
		Товар = Объект.Товары.Добавить();
		ЗаполнитьЗначенияСвойств(Товар, ЭлементБуфера, "Номенклатура, Характеристика, Упаковка, КоличествоУпаковок, Количество, Склад");
		Товар.ТаможеннаяСтоимость = ЭлементБуфера.Сумма;
		Товар.НомерРаздела = НомерРаздела;
		ОбработкаТабличнойЧастиСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВСтруктуре(Товар, Действия);
		ПересчитатьПоля(Товар, "СуммаПошлины, СуммаНДС", Ставки);
	КонецЦикла;
	
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаСервере
Процедура ОбработкаВыбораПодобратьИзОформления(ВыбранноеЗначение)
	Действия = Новый Структура;
	УстановитьДействияДополнительныхПризнаковТоваров(Действия);
	Ставки = СтавкиРаздела(Объект.Разделы, НомерРаздела);
	
	СтрокиУдаления = Объект.Товары.НайтиСтроки(Новый Структура("НомерРаздела", НомерРаздела));
	Для Каждого СтрокаУдаления Из СтрокиУдаления Цикл
		Объект.Товары.Удалить(СтрокаУдаления);
	КонецЦикла;
	
	БуферТоваров = ПолучитьИзВременногоХранилища(ВыбранноеЗначение);
	Для каждого ЭлементБуфера Из БуферТоваров Цикл
		Товар = Объект.Товары.Добавить();
		ЗаполнитьЗначенияСвойств(Товар, ЭлементБуфера, "Номенклатура, Характеристика, КоличествоУпаковок, Количество, Склад");
		Товар.НомерРаздела = НомерРаздела;
		ОбработкаТабличнойЧастиСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВСтруктуре(Товар, Действия);
		ПересчитатьПоля(Товар, "СуммаПошлины, СуммаНДС", Ставки);
	КонецЦикла;
	
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаСервере
Процедура УстановитьСтоимостьСервер(Соглашение, Знач УсловиеЦен = Неопределено, ОтборТоваров = Неопределено)
	Если ОтборТоваров = Неопределено Тогда
		ТоварыЗаполнения = ОбщегоНазначенияУТКлиентСервер.ВыделенныеЭлементыКоллекции(Объект.Товары, Элементы.Товары.ВыделенныеСтроки);
	Иначе
		ТоварыЗаполнения = Объект.Товары.НайтиСтроки(ОтборТоваров);
	КонецЕсли;
	УсловиеЦен = ?(ЗначениеЗаполнено(УсловиеЦен), УсловиеЦен, Справочники.УсловияЦенПоставщиков.ПустаяСсылка());
	
	ПараметрыЗаполнения =
		Новый Структура("ПоляЗаполнения, Дата, Валюта, Соглашение, УсловиеЦеныПоставщика",
						"Цена", ДатаДокумента(Объект.Дата), Объект.Валюта, Соглашение, УсловиеЦен);
	ЦеныРассчитаны = Закупки.ЗаполнитьЦены(Объект.Товары, ТоварыЗаполнения, ПараметрыЗаполнения, Неопределено);
	
	Действия = Новый Структура;
	УстановитьДействияДополнительныхПризнаковТоваров(Действия);
	
	ТекущийРаздел = 0;
	СтавкаПошлины = 0.;
	Для Каждого Товар Из ТоварыЗаполнения Цикл
		Товар.ТаможеннаяСтоимость = Товар.КоличествоУпаковок * Товар.Цена;
		ОбработкаТабличнойЧастиСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВСтруктуре(Товар, Действия);
		Если ТекущийРаздел <> Товар.НомерРаздела Тогда
			ТекущийРаздел = Товар.НомерРаздела;
			Ставки = СтавкиРаздела(Объект.Разделы, ТекущийРаздел);
		КонецЕсли;
		ПересчитатьПоля(Товар, "СуммаПошлины, СуммаНДС", Ставки);
	КонецЦикла;
	
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

///////////////////////////////////////////////////////////////////////////////
// Поддержка таблиц формы

&НаКлиенте
Процедура АктивироватьРаздел()
	ПоРазделам = Не Элементы.ТоварыПоказатьВсеТовары.Пометка;
	Товары = Элементы.Товары;
	Если ПоРазделам И (Товары.ОтборСтрок = Неопределено Или Товары.ОтборСтрок.НомерРаздела <> НомерРаздела) Тогда // включить отбор
		Товары.ОтборСтрок = Новый ФиксированнаяСтруктура("НомерРаздела", НомерРаздела);
	ИначеЕсли Не ПоРазделам И Товары.ОтборСтрок<>Неопределено Тогда // выключить отбор
		Товары.ОтборСтрок = Неопределено;
	КонецЕсли; // ничего не менять
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция МаксимальныйНомерРаздела(Разделы)
	МаксимальныйНомер = 0;
	Для Каждого Раздел Из Разделы Цикл
		Если Раздел.НомерРаздела > МаксимальныйНомер Тогда
			МаксимальныйНомер = Раздел.НомерРаздела;
		КонецЕсли;
	КонецЦикла;
	Возврат МаксимальныйНомер;
КонецФункции

&НаСервере
Процедура ОчиститьНомераРазделовТоваров(СтарыеНомера)
	Для Каждого Товар Из Объект.Товары Цикл
		Если СтарыеНомера[Товар.НомерРаздела]<>Неопределено Тогда
			Товар.НомерРаздела = 0;
		КонецЕсли;
	КонецЦикла;
	Выбор = Элементы.ТоварыНомерРаздела.СписокВыбора;
	Для Каждого СтарыйНомер Из СтарыеНомера Цикл
		НайденныйЭлемент = Выбор.НайтиПоЗначению(СтарыйНомер.Значение);
		Выбор.Удалить(НайденныйЭлемент);
	КонецЦикла;
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция СтавкиРаздела(Разделы, ТекущийНомер)
	Ставки = Новый Структура("СтавкаПошлины, СтавкаНДС", 0., Неопределено);
	Найдено = Разделы.НайтиСтроки(Новый Структура("НомерРаздела", ТекущийНомер));
	Если Найдено.Количество() > 0 Тогда
		ЗаполнитьЗначенияСвойств(Ставки, Найдено[0]);
	КонецЕсли;
	Возврат Ставки;
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Процедура ПересчитатьПоля(ТекущиеДанные, Знач ПоляПересчета, Знач Ставки) Экспорт
	ПоляПересчета = НРег(ПоляПересчета);
	Если Найти(ПоляПересчета, "суммапошлины")>0 Тогда
		ТекущиеДанные.СуммаПошлины = ТекущиеДанные.ТаможеннаяСтоимость * Ставки.СтавкаПошлины / 100.;
	ИначеЕсли Найти(ПоляПересчета, "ставкапошлины")>0 Тогда
		ТекущиеДанные.СтавкаПошлины =
			100. * ?(ТекущиеДанные.ТаможеннаяСтоимость=0., 0., ТекущиеДанные.СуммаПошлины / ТекущиеДанные.ТаможеннаяСтоимость);
	КонецЕсли;
	Если Найти(ПоляПересчета, "суммандс")>0 Тогда
		ТекущиеДанные.СуммаНДС =
			ЦенообразованиеКлиентСервер.РассчитатьСуммуНДС(
				ТекущиеДанные.ТаможеннаяСтоимость + ТекущиеДанные.СуммаПошлины,
				ЦенообразованиеКлиентСервер.ПолучитьСтавкуНДСЧислом(Ставки.СтавкаНДС), Ложь
			);
	КонецЕсли;
КонецПроцедуры

///////////////////////////////////////////////////////////////////////////////
// Прочее

&НаСервере
Функция ПоместитьВХранилище(ИмяТабчасти, ИменаПолей, ПараметрыОтбора)
	Возврат ПоместитьВоВременноеХранилище(Объект[ИмяТабчасти].Выгрузить(ПараметрыОтбора, ИменаПолей), УникальныйИдентификатор);
КонецФункции

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	ЗаполнитьДанныеФормы();
	ЗаполнитьДополнительныеПризнакиТоваров();
	НастроитьЭлементыФормы();
	НастроитьВыборРазделов();
	Элементы.СтраницаКомментарий.Картинка = ОбщегоНазначенияУТ.ПолучитьКартинкуКомментария(Объект.Комментарий);
	
	Если Объект.Разделы.Количество() > 0 И НомерРаздела < 1 Тогда
		НомерРаздела = Объект.Разделы[0].НомерРаздела;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДанныеФормы()
	Если Не ЗначениеЗаполнено(ВалютаДокумента) Тогда
		ВалютаДокумента = Объект.Валюта;
	КонецЕсли;
	РасчетыСТаможней = (Объект.ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ОформлениеГТДСамостоятельно);
	СтатусДокумента = Объект.Статус;
	
	ПоступлениеСоглашение = ОбщегоНазначения.ПолучитьЗначениеРеквизита(Объект.ПоступлениеТоваров, "Соглашение");
	
	ЗаполнитьДанныеФормыИтоги(ЭтаФорма);
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ЗаполнитьДанныеФормыИтоги(Данные)
	Разделы = Данные.Объект.Разделы;
	Товары = Данные.Объект.Товары;
	Данные.РазностьСтоимостей = Разделы.Итог("ТаможеннаяСтоимость") - Товары.Итог("ТаможеннаяСтоимость");
	Данные.РазностьПошлин = Разделы.Итог("СуммаПошлины") - Товары.Итог("СуммаПошлины");
	Данные.РазностьНДС = Разделы.Итог("СуммаНДС") - Товары.Итог("СуммаНДС");
КонецПроцедуры

&НаСервере
Процедура УстановитьДействияДополнительныхПризнаковТоваров(Действия)
	Действия.Вставить("ЗаполнитьПризнакХарактеристикиИспользуются", Новый Структура("Номенклатура", "ХарактеристикиИспользуются"));
КонецПроцедуры

&НаСервере
Процедура УстановитьПараметрыВыбораВидаЗапасов()
	МассивПараметров = Новый Массив;
	МассивПараметров.Добавить(Новый СвязьПараметраВыбора("Отбор.Организация", "Объект.Организация"));
	Если ПолучитьФункциональнуюОпцию("ФормироватьВидыЗапасовПоГруппамФинансовогоУчета") Тогда
		МассивПараметров.Добавить(Новый СвязьПараметраВыбора("Отбор.Номенклатура", "Элементы.Товары.ТекущиеДанные.Номенклатура"));
	КонецЕсли;
	Если ПолучитьФункциональнуюОпцию("ФормироватьВидыЗапасовПоПодразделениямМенеджерам") Тогда
		МассивПараметров.Добавить(Новый СвязьПараметраВыбора("Отбор.Подразделение", "Объект.Подразделение"));
		МассивПараметров.Добавить(Новый СвязьПараметраВыбора("Отбор.Менеджер", "Объект.Менеджер"));
	КонецЕсли;
	Если ПолучитьФункциональнуюОпцию("ФормироватьВидыЗапасовПоСделкам") Тогда
		МассивПараметров.Добавить(Новый СвязьПараметраВыбора("Отбор.Сделка", "Элементы.Товары.ТекущиеДанные.Сделка"));
	КонецЕсли;
	Если ПолучитьФункциональнуюОпцию("ФормироватьВидыЗапасовПоПоставщикам") Тогда
		МассивПараметров.Добавить(Новый СвязьПараметраВыбора("Отбор.Поставщик", "Объект.Поставщик"));
	КонецЕсли;
	Элементы.ТоварыВидЗапасов.СвязиПараметровВыбора = Новый ФиксированныйМассив(МассивПараметров);
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДополнительныеПризнакиТоваров()
	Действия = Новый Структура;
	УстановитьДействияДополнительныхПризнаковТоваров(Действия);
	ОбработкаТабличнойЧастиСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(Объект.Товары, Действия);
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьФормаОплатыСписокВыбора()
	Список = Элементы.ФормаОплаты.СписокВыбора;
	Список.Очистить();
	Список.Добавить(Перечисления.ФормыОплаты.Безналичная);
	Если Не РасчетыСТаможней Тогда
		Список.Добавить(Перечисления.ФормыОплаты.Наличная);
		Список.Добавить(Перечисления.ФормыОплаты.ПустаяСсылка(), НСтр("ru='Любая'"));
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура НастроитьЭлементыФормы()
	Перем ВсеРеквизиты;
	Перем РеквизитыОперации;
	
	Элементы.Партнер.Заголовок = ?(РасчетыСТаможней, НСтр("ru='Таможня'"), НСтр("ru='Брокер'"));
	ПараметрыВыбора = Новый Массив;
	Если РасчетыСТаможней Тогда
		ПараметрыВыбора.Добавить(Новый ПараметрВыбора("Отбор.ПрочиеОтношения", РасчетыСТаможней));
	Иначе
		ПараметрыВыбора.Добавить(Новый ПараметрВыбора("Отбор.Поставщик", Истина));
	КонецЕсли;
	Элементы.Партнер.ПараметрыВыбора = Новый ФиксированныйМассив(ПараметрыВыбора);
	
	Элементы.ПоступлениеТоваров.Доступность = ЗначениеЗаполнено(Объект.Поставщик);
	
	Элементы.Валюта.ТолькоПросмотр = РасчетыСТаможней;
	Элементы.ВалютаВзаиморасчетов.ТолькоПросмотр = РасчетыСТаможней;

	Элементы.Соглашение.Доступность = Не РасчетыСТаможней;
	ЗакупкиСервер.УстановитьДоступностьДоговора(Объект, Элементы.Договор.Доступность, Объект.Договор);
	Элементы.Договор.Доступность = Элементы.Договор.Доступность И Не РасчетыСТаможней;
	Элементы.Договор.ОтметкаНезаполненного =
		Элементы.Договор.Доступность И ЗначениеЗаполнено(Объект.Соглашение) И Не ЗначениеЗаполнено(Объект.Договор);
		
	ЗаполнитьФормаОплатыСписокВыбора();
	
	НастроитьЗаполнениеТоваров();
	УстановитьПараметрыВыбораВидаЗапасов();
КонецПроцедуры

&НаСервере
Процедура НастроитьВыборРазделов()
	Выбор = Элементы.ТоварыНомерРаздела.СписокВыбора;
	Выбор.Очистить();
	Для Каждого Раздел Из Объект.Разделы Цикл
		Выбор.Добавить(Раздел.НомерРаздела, Формат(Раздел.НомерРаздела, Элементы.ТоварыНомерРаздела.Формат));
	КонецЦикла;
	Выбор.СортироватьПоЗначению();
	Выбор.Вставить(0, 0, Формат(0, Элементы.ТоварыНомерРаздела.Формат));
КонецПроцедуры

&НаСервере
Процедура НастроитьЗаполнениеТоваров()
	ПоРазделам = Не Элементы.ТоварыПоказатьВсеТовары.Пометка;
	ПоПоступлению = ЗначениеЗаполнено(Объект.ПоступлениеТоваров);
	Элементы.ТоварыПодобратьИзПоступления.Видимость = ПоРазделам И ПоПоступлению;
	Элементы.ТоварыПодобратьТовары.Видимость = ПоРазделам И Не ПоПоступлению;
	Элементы.ТоварыПодобратьИзОформления.Видимость = Элементы.ТоварыПодобратьТовары.Видимость;
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДоговорПоУмолчанию()
	Договор = ЗакупкиСервер.ПолучитьДоговорПоУмолчанию(
		Объект.Договор, Объект.Партнер, Объект.Контрагент, Объект.Организация,
		Документы.ТаможеннаяДекларацияИмпорт.ОперацииОтбораСоглашенийДоговоров(), Объект.ВалютаВзаиморасчетов, Объект.Соглашение
	);
	Если (Договор <> Объект.Договор) Тогда
		Объект.Договор = Договор;
		ЗакупкиСервер.ЗаполнитьБанковскиеСчетаПоДоговору(Объект.Договор, Объект.БанковскийСчетОрганизации, Объект.БанковскийСчетКонтрагента);
	КонецЕсли;
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ДатаДокумента(ОбъектДата)
	Возврат ?(ЗначениеЗаполнено(ОбъектДата), ОбъектДата, ТекущаяДата());
КонецФункции

&НаКлиенте
Функция ВыбратьСоглашениеСПоставщиком()
	Если ЗначениеЗаполнено(ПоступлениеСоглашение) Тогда
		ВыбранноеСоглашение = ПоступлениеСоглашение;
	Иначе
		ПараметрыПодбора = Новый Структура("ДатаДокумента, Отбор", ДатаДокумента(Объект.Дата), Новый Структура);
		ПараметрыПодбора.Отбор.Вставить("Партнер", Объект.Поставщик);
		ПараметрыПодбора.Отбор.Вставить("ХозяйственнаяОперация", ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ЗакупкаПоИмпорту"));
		ВыбранноеСоглашение = ОткрытьФормуМодально("Справочник.СоглашенияСПоставщиками.ФормаВыбора", ПараметрыПодбора);
	КонецЕсли;
	Возврат ВыбранноеСоглашение;
КонецФункции

///////////////////////////////////////////////////////////////////////////////
// Стандартные механизмы БСП

&НаКлиенте
Процедура Подключаемый_РедактироватьСоставСвойств(Команда)
	УправлениеСвойствамиКлиент.РедактироватьСоставСвойств(ЭтаФорма, Объект.Ссылка);
КонецПроцедуры

&НаСервере
Процедура ОбновитьЭлементыДополнительныхРеквизитов()
	УправлениеСвойствами.ОбновитьЭлементыДополнительныхРеквизитов(ЭтаФорма, РеквизитФормыВЗначение("Объект"));
КонецПроцедуры
