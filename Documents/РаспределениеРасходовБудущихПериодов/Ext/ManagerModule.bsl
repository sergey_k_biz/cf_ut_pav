﻿
///////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

///////////////////////////////////////////////////////////////////////////////
// Проведение

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, СтруктураДополнительныеСвойства) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = ТекстЗапросаТаблицаПрочиеРасходы();
	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	
	МассивРезультатов = Запрос.ВыполнитьПакет();
    
	СтруктураДополнительныеСвойства.ТаблицыДляДвижений.Вставить("ТаблицаПрочиеРасходы", МассивРезультатов[0].Выгрузить());
    
КонецПроцедуры

Функция ТекстЗапросаТаблицаПрочиеРасходы()

	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаРасходы.НомерСтроки КАК НомерСтроки,
	|	ТаблицаРасходы.Дата КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) КАК ВидДвижения,
	|	ТаблицаРасходы.Ссылка.Организация КАК Организация,
	|	ТаблицаРасходы.Ссылка.Подразделение КАК Подразделение,
	|	ТаблицаРасходы.Ссылка.СтатьяРасходов КАК СтатьяРасходов,
	|	ТаблицаРасходы.Ссылка.АналитикаРасходов КАК АналитикаРасходов,
	|	ТаблицаРасходы.Сумма КАК Сумма,
	|	0 КАК СуммаБезНДС
	|ИЗ
	|	Документ.РаспределениеРасходовБудущихПериодов.РаспределениеРасходов КАК ТаблицаРасходы
	|ГДЕ
	|	ТаблицаРасходы.Ссылка = &Ссылка
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ТаблицаРасходы.НомерСтроки КАК НомерСтроки,
	|	ТаблицаРасходы.Дата КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
	|	ТаблицаРасходы.Ссылка.Организация КАК Организация,
	|	ТаблицаРасходы.Ссылка.Подразделение КАК Подразделение,
	|	ТаблицаРасходы.СтатьяРасходов КАК СтатьяРасходов,
	|	ТаблицаРасходы.АналитикаРасходов КАК АналитикаРасходов,
	|	ТаблицаРасходы.Сумма КАК Сумма,
	|	ВЫБОР КОГДА ТаблицаРасходы.СтатьяРасходов.ВариантРаспределенияРасходов = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаСебестоимостьТоваров) ТОГДА
	|		ТаблицаРасходы.Сумма
	|	ИНАЧЕ
	|		0
	|	КОНЕЦ КАК СуммаБезНДС
	|ИЗ
	|	Документ.РаспределениеРасходовБудущихПериодов.РаспределениеРасходов КАК ТаблицаРасходы
	|ГДЕ
	|	ТаблицаРасходы.Ссылка = &Ссылка
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|";

	Возврат ТекстЗапроса;

КонецФункции
